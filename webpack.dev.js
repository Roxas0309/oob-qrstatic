const { merge } = require('webpack-merge');
const common = require('./webpack.common.js');
const BundleAnalyzerPlugin = require('webpack-bundle-analyzer').BundleAnalyzerPlugin;

module.exports = merge(common, {
    mode: 'development',
    devtool: 'inline-source-map',
    devServer: {
        historyApiFallback: true,
        contentBase: './',
        hot: true
    },
    externals: {
        config: JSON.stringify({
             apiReg: '/api'
            // apiReg:'http://149.129.239.139:28080'
            // apiReg: 'https://devoob.yokke.co.id/'
        })
    },
    plugins: [
        new BundleAnalyzerPlugin({
            analyzerPort: 3031
        })
    ]
});