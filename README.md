# OOB QR Static

Web-based MAAS System App

## Installing depedencies

Run `npm install` to execute download all required depedencies for the project. The depedencies are listed in `package.json` file.

## Development server

Run `npm start` for a dev server. Navigate to `http://localhost:3030/`. The app will automatically reload if you change any of the source files.

## Build

Run `npm run build` to build the project. The bundle built will be store in `dist` directory while artifacts will be stored in the `public` directory.

## Build Service Worker to runing install app in home screen
1. in NPM script click generate-wb or terminal: run npm generate-wb
2. in NPM script click build or terminal: npm run build
3. in terminal run: .\post-build.sh
4. get file dist in folder project
