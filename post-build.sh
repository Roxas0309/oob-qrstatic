# PLEASE RUN THIS AFTER RUNNING 'npm run build' TO FIX BUNDLE ASSETS

cp sw.js dist
cp sw.js.map dist
cp firebase-messaging-sw dist
cp public/manifest.json dist
cp workbox*.js dist
cp workbox*.js.map dist
rm dist/workbox-config.js
rm -rf dist/public/img
cp -R public dist/public/public

# cp src/serviceWorker.js dist
# rm -rf dist/public/img
# cp -R public dist/public/public
# rm dist/public/public/index.html