import { submitConstants } from '../constant';

export const submitActions = {
    startPrepare, endPrepare, startSubmit, endSubmit
};

function startPrepare() {
    return { type: submitConstants.PREPARE_START };
}

function endPrepare() {
    return { type: submitConstants.PREPARE_END };
}

function startSubmit() {
    return { type: submitConstants.SUBMIT_START };
}

function endSubmit() {
    return { type: submitConstants.SUBMIT_END };
}