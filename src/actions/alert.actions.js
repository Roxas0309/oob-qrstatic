import { alertConstants } from '../constant';

export const alertActions = {
    success,
    error,
    toggleModal,
    toggleModalShow,
    toggleModalHide,
    clear,
    reset
};

function success(message) {
    return { type: alertConstants.SUCCESS, message };
}

function error(message) {
    return { type: alertConstants.ERROR, message };
}

function toggleModal() {
    return { type: alertConstants.MODAL };
}

function toggleModalShow() {
    return { type: alertConstants.MODAL_SHOW };
}

function toggleModalHide() {
    return { type: alertConstants.MODAL_HIDE };
}


function clear() {
    return { type: alertConstants.CLEAR };
}

function reset() {
    return dispatch => {
        dispatch({ type: alertConstants.CLEAR });
    }
}