export const testConstants = {
    TEST_PROCESS_STATE: 'TEST_PROCESS_STATE'
};

export const testActions = {
    test,
};

function test() {
    return dispatch => {
        dispatch({ type: testConstants.TEST_PROCESS_STATE, payload: 'test' });
    }
}