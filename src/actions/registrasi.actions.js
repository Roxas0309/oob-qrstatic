import { registrasiService } from "../services";
import { registrasiConstants } from "../constant";
import { alertActions, submitActions } from '.';


export const registrasiActions = {
    validateInfoRekening,
    sendOtp,
    validateOtp,
    getJenisUsaha,
    getOmsetUsaha,
    getLokasiUsaha,
    getLokasiUsahaNon
};

function getLokasiUsaha() {
    return dispatch => {
        dispatch({ type: registrasiConstants.GET_LOKASIUSAHA_LOADING, isLoading: true });
        dispatch({ type: registrasiConstants.GET_LOKASIUSAHA_ALERT, message: null });
        registrasiService.getLokasiUSaha()
            .then(response => {
                dispatch({ type: registrasiConstants.GET_LOKASIUSAHA_LOADING, isLoading: false });
                dispatch({ type: registrasiConstants.GET_LOKASIUSAHA_SUCCESS, result: response.data });
            }, error => {
                dispatch({ type: registrasiConstants.GET_LOKASIUSAHA_LOADING, isLoading: false });
                dispatch({ type: registrasiConstants.GET_LOKASIUSAHA_ALERT, message: error.message });
                dispatch({ type: registrasiConstants.GET_LOKASIUSAHA_FAILED });
            })

    }
}
function getLokasiUsahaNon() {
    return dispatch => {
        dispatch({ type: registrasiConstants.GET_LOKASIUSAHANON_LOADING, isLoading: true });
        dispatch({ type: registrasiConstants.GET_LOKASIUSAHANON_ALERT, message: null });
        registrasiService.getLokasiUsahaNon()
            .then(response => {
                dispatch({ type: registrasiConstants.GET_LOKASIUSAHANON_LOADING, isLoading: false });
                dispatch({ type: registrasiConstants.GET_LOKASIUSAHANON_SUCCESS, result: response.data });
            }, error => {
                dispatch({ type: registrasiConstants.GET_LOKASIUSAHANON_LOADING, isLoading: false });
                dispatch({ type: registrasiConstants.GET_LOKASIUSAHANON_ALERT, message: error.message });
                dispatch({ type: registrasiConstants.GET_LOKASIUSAHANON_FAILED });
            })

    }
}

function getJenisUsaha() {
    return dispatch => {
        dispatch({ type: registrasiConstants.GET_JENISUSAHA_LOADING, isLoading: true });
        dispatch({ type: registrasiConstants.GET_JENISUSAHA_ALERT, message: null });
        registrasiService.getJenisUsaha()
            .then(response => {
                dispatch({ type: registrasiConstants.GET_JENISUSAHA_LOADING, isLoading: false });
                dispatch({ type: registrasiConstants.GET_JENISUSAHA_SUCCESS, result: response });
            }, error => {
                dispatch({ type: registrasiConstants.GET_JENISUSAHA_LOADING, isLoading: false });
                dispatch({ type: registrasiConstants.GET_JENISUSAHA_ALERT, message: error.message });
                dispatch({ type: registrasiConstants.GET_JENISUSAHA_FAILED });
            })

    }
}

function getOmsetUsaha() {
    return dispatch => {
        dispatch({ type: registrasiConstants.GET_OMSETUSAHA_LOADING, isLoading: true });
        dispatch({ type: registrasiConstants.GET_OMSETUSAHA_ALERT, message: null });
        registrasiService.getOmsetUsaha()
            .then(result => {
                dispatch({ type: registrasiConstants.GET_OMSETUSAHA_LOADING, isLoading: false });
                dispatch({ type: registrasiConstants.GET_OMSETUSAHA_SUCCESS, result: result.data });
            }, error => {
                dispatch({ type: registrasiConstants.GET_OMSETUSAHA_LOADING, isLoading: false });
                dispatch({ type: registrasiConstants.GET_OMSETUSAHA_ALERT, message: error.message });
                dispatch({ type: registrasiConstants.GET_OMSETUSAHA_FAILED });
            })

    }
}

function validateInfoRekening(payload) {
    const { dob, namaIbuKandung, namaPemilikUsaha, nomorKtp, nomorRekening } = payload;
    return dispatch => {
        return new Promise(resolve => { 
            dispatch(submitActions.startSubmit());
            dispatch({type: registrasiConstants.VALIDATE_LOADING_INFOREKENING, isLoading: true});
            registrasiService.validateInfoRek(dob, namaIbuKandung, namaPemilikUsaha, nomorKtp, nomorRekening)
            .then(result => {
                dispatch(submitActions.endSubmit());
                dispatch({type: registrasiConstants.VALIDATE_LOADING_INFOREKENING, isLoading: false});
                dispatch({ type: registrasiConstants.VALIDATE_SUCCESS_INFOREKENING });
                resolve(result)
            },
            error => {
                dispatch(submitActions.endSubmit());
                dispatch({type: registrasiConstants.VALIDATE_LOADING_INFOREKENING, isLoading: false});
                dispatch({ type: registrasiConstants.VALIDATE_FAILED_INFOREKENING });
                resolve(error)
            })

        }).catch(err => {
            console.log('errror fetch: ', err)
        })
    }
}

function sendOtp(payload) {
    const { cifInEncrypt } = payload;
    return dispatch => {
        return new Promise(resolve => { 
            dispatch(submitActions.startSubmit());
            dispatch({type: registrasiConstants.OTP_SEND_LOADING, isLoading: true});
            registrasiService.sendOtp(cifInEncrypt)
            .then(result => {
                dispatch(submitActions.endSubmit());
                dispatch({type: registrasiConstants.OTP_SEND_LOADING, isLoading: false});
                dispatch({ type: registrasiConstants.OTP_SEND_SUCCESS });
                resolve(result)
            },
            error => {
                dispatch(submitActions.endSubmit());
                dispatch({type: registrasiConstants.OTP_SEND_LOADING, isLoading: false});
                dispatch({ type: registrasiConstants.OTP_SEND_FAILED });
                resolve(error)
            })

        })
    }
}

function validateOtp(payload) {
    const { encrpyptValidationOtp, otpValue } = payload;
    return dispatch => {
        return new Promise(resolve => { 
            dispatch(submitActions.startSubmit());
            dispatch({type: registrasiConstants.OTP_VALIDATE_LOADING, isLoading: true});
            registrasiService.validateOtp(encrpyptValidationOtp, otpValue)
            .then(result => {
                dispatch(submitActions.endSubmit());
                dispatch({type: registrasiConstants.OTP_VALIDATE_LOADING, isLoading: false});
                dispatch({ type: registrasiConstants.OTP_VALIDATE_SUCCESS });
                resolve(result)
            },
            error => {
                dispatch(submitActions.endSubmit());
                dispatch({type: registrasiConstants.OTP_VALIDATE_LOADING, isLoading: false});
                dispatch({ type: registrasiConstants.OTP_VALIDATE_FAILED });
                resolve(error)
            }).catch(error => {
                dispatch(submitActions.endSubmit());
                dispatch({type: registrasiConstants.OTP_VALIDATE_LOADING, isLoading: false});
                dispatch({ type: registrasiConstants.OTP_VALIDATE_FAILED });
            })

        })
    }
}