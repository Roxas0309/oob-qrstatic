import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from "react-redux";
import { syncHistoryWithStore } from 'react-router-redux';
import { createBrowserHistory } from 'history';
import { Offline, Online } from "react-detect-offline";
import './assets/css/index.scss';
import App from './App';
//import * as serviceWorker from './serviceWorker';
import store from "./store/index";
import { createMuiTheme, ThemeProvider } from '@material-ui/core';
import LoadingKoneksi from './helpers/loadingMaker/LoadingKoneksi';

const theme = createMuiTheme({
  palette: {
    primary: {
      main: '#0057E7'
              },
    secondary: {
      main: '#6B7984'
              }
          },
    MuiInputLabel: {
      shrink: true, 
    },
});


syncHistoryWithStore(
  createBrowserHistory(),
  store
);

ReactDOM.render(
  <Provider store={store}>
    
    {/* <Offline>
      <LoadingKoneksi onLoading={true}/>
    </Offline>
    <Online>
      <ThemeProvider theme={theme}></ThemeProvider>
    </Online> */}
    <App />
  </Provider>
 ,
  document.getElementById('root')
);

