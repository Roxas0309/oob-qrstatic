import { handleResponse, request, requestNoBody, connection } from '../helpers'; 

export const registrasiService = {
    validateInfoRek, 
    sendOtp,
    validateOtp,
    getJenisUsaha,
    getOmsetUsaha,
    getLokasiUSaha,
    getLokasiUsahaNon

};

function getLokasiUSaha() {
    return fetch(connection.registrasi.getLokasiUsaha.url(), requestNoBody(connection.registrasi.getLokasiUsaha.method))
        .then(handleResponse);
}
function getLokasiUsahaNon() {
    const body = {
    };
    return fetch(connection.registrasi.getLokasiUsahaNon.url(), requestNoBody(connection.registrasi.getLokasiUsahaNon.method))
        .then(handleResponse);
}
function getJenisUsaha() {
    const body = {
    };
    return fetch(connection.registrasi.getJenisUsaha.url(), requestNoBody(connection.registrasi.getJenisUsaha.method))
        .then(handleResponse);
}
function getOmsetUsaha() {
    return fetch(connection.registrasi.getOmsetUsaha.url(), requestNoBody(connection.registrasi.getOmsetUsaha.method))
        .then(handleResponse).then(res => console.log('res service: ', res));
}

function validateInfoRek(dob, namaIbuKandung, namaPemilikUsaha, nomorKtp, nomorRekening) {
    const body = {
        dob: dob, 
        namaIbuKandung: namaIbuKandung, 
        namaPemilikUsaha: namaPemilikUsaha, 
        nomorKtp: nomorKtp, 
        nomorRekening: nomorRekening
    };
    return fetch(connection.registrasi.validateInfoRekening.url(), request(connection.registrasi.validateInfoRekening.method, body))
        .then(handleResponse);
}

function sendOtp(cifInEncrypt) {
    const body = {
        cifInEncrypt: cifInEncrypt
    };
    return fetch(connection.registrasi.sendOtp.url(), request(connection.registrasi.sendOtp.method, body))
        .then(handleResponse);
}

function validateOtp(encrpyptValidationOtp, otpValue) {
    const body = {
        encrpyptValidationOtp: encrpyptValidationOtp,
        otpValue: otpValue
    };
    return fetch(connection.registrasi.validateOtp.url(), request(connection.registrasi.validateOtp.method, body))
        .then(handleResponse);
}