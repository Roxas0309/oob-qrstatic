import { testConstants } from '../actions/test.actions';

const initialState = {
  message: ''
};

export function test(state = initialState, action) {
  switch (action.type) {
    case testConstants.TEST_PROCESS_STATE:
      return Object.assign({}, state, {
        message: action.payload
      });
    default:
      return state
  }
}