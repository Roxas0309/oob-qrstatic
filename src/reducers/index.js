import { combineReducers } from 'redux';
import { routerReducer } from 'react-router-redux';

import { test } from './test.reducer'
import { registrasi } from './registrasi.reducer'
import { submit } from './submit.reducer'
import { alert } from './alert.reducer'


const rootReducer = combineReducers({
    test,
    registrasi,
    submit,
    alert,
    routing: routerReducer
});

export default rootReducer;