import { registrasiConstants } from '../constant'

const initialState = {
    dataForm: {},
    dataInfoRek: [],
    alertInfoRek: null,
    isLoadingInfoRek: false,
    isLoadingSendOtp: false,
    isLoadingValOtp: false,

    jenisUsahaData: [],
    jenisUsahaLoading: false,
    jenisUsahaAlert: null,

    loksaiUsahaData: [],
    lokasiUsahaLoading: false,
    lokasiUsahaAlert: null,

    lokasiUSahaNonData: [],
    lokasiUSahaNonLoading: false,
    lokasiUSahaNonAlert: null,

    omsetUsahaData: [],
    omsetUsahaLoading: false,
    omsetUsahaAlert: null,


};

export function registrasi(state = initialState, action){
    switch(action.type) {

        /////////////// lokasi usaha permanen//////////////
        case registrasiConstants.GET_LOKASIUSAHA_LOADING:
            return Object.assign({}, state, {
                lokasiUsahaLoading: action.isLoading
            });
        case registrasiConstants.GET_LOKASIUSAHA_SUCCESS:
            return Object.assign({}, state, {
                loksaiUsahaData: action.result
            });
        case registrasiConstants.GET_LOKASIUSAHA_FAILED:
            return Object.assign({}, state, {
                loksaiUsahaData: []
            });
        case registrasiConstants.GET_LOKASIUSAHA_ALERT:
            return Object.assign({}, state, {
                lokasiUsahaAlert: action.message
            });
        /////////////// lokasi usaha Non permanen//////////////
        case registrasiConstants.GET_LOKASIUSAHANON_LOADING:
            return Object.assign({}, state, {
                lokasiUSahaNonLoading: action.isLoading
            });
        case registrasiConstants.GET_LOKASIUSAHANON_SUCCESS:
            return Object.assign({}, state, {
                lokasiUSahaNonData: action.result
            });
        case registrasiConstants.GET_LOKASIUSAHANON_FAILED:
            return Object.assign({}, state, {
                lokasiUSahaNonData: []
            });
        case registrasiConstants.GET_LOKASIUSAHANON_ALERT:
            return Object.assign({}, state, {
                lokasiUSahaNonAlert: action.message
            });

        ///////////////// jenis usaha//////////////
        case registrasiConstants.GET_JENISUSAHA_LOADING:
            return Object.assign({}, state, {
                jenisUsahaLoading: action.isLoading
            });
        case registrasiConstants.GET_JENISUSAHA_SUCCESS:
            return Object.assign({}, state, {
                jenisUsahaData: action.result
            });
        case registrasiConstants.GET_JENISUSAHA_FAILED:
            return Object.assign({}, state, {
                jenisUsahaData: []
            });
        case registrasiConstants.GET_JENISUSAHA_ALERT:
            return Object.assign({}, state, {
                jenisUsahaAlert: action.message
            });
        ///////////////// omset//////////////
        case registrasiConstants.GET_OMSETUSAHA_LOADING:
            return Object.assign({}, state, {
                omsetUsahaLoading: action.isLoading
            });
        case registrasiConstants.GET_OMSETUSAHA_SUCCESS:
            return Object.assign({}, state, {
                omsetUsahaData: action.result
            });
        case registrasiConstants.GET_OMSETUSAHA_FAILED:
            return Object.assign({}, state, {
                omsetUsahaData: []
            });
        case registrasiConstants.GET_OMSETUSAHA_ALERT:
            return Object.assign({}, state, {
                omsetUsahaAlert: action.message
            });
        ////////validate info rek//////////////
        case registrasiConstants.VALIDATE_ALERT_INFOREKENING:
            return Object.assign({}, state, {
                alertInfoRek: action.message
            });
        case registrasiConstants.VALIDATE_LOADING_INFOREKENING:
            return Object.assign({}, state, {
                isLoadingInfoRek: action.isLoading
            });
        case registrasiConstants.VALIDATE_SUCCESS_INFOREKENING:
            return Object.assign({}, state, {
                dataInfoRek: action.result,
            });
        case registrasiConstants.VALIDATE_FAILED_INFOREKENING:
            
        ///////otp send///////////////////
        case registrasiConstants.OTP_SEND_LOADING:
            return Object.assign({}, state, {
                isLoadingSendOtp: action.isLoading
            });
        ///////otp validate///////////////////
        case registrasiConstants.OTP_VALIDATE_LOADING:
            return Object.assign({}, state, {
                isLoadingValOtp: action.isLoading
            });      
        default:
            return state
    }
}

