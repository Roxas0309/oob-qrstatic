import React from "react";
import styled from "styled-components";

const ButtonContainer = styled.div`
  /* width: 100%; */
  /* width:343px;
  height: 56px; */
`;

const ButtonDetails = styled.button`
  /* margin-top:10px; */
  width:100%;
  height:56px;
  box-shadow: 0px 4px 4px -4px #8CC3F8 0px 4px 8px 2px #C5E1FB;
  text-transform: none;
  background-color: #2661da;
  font-family: Nunito;
  font-style: normal;
  font-weight: bold;
  font-size: 16px;
  line-height: 22px;
  text-align: center;
  color: #ffffff;
  border-radius: 8px;
  border:none;
`;

export default function ButtonComponent(props) {
  return (
    <div>
      <ButtonContainer>
        <ButtonDetails {...props}>{props.children}</ButtonDetails>{" "}
      </ButtonContainer>
    </div>
  );
}
