import React from 'react';
import { Router, Route, Switch } from 'react-router-dom';
import './assets/css/App.scss'
import { createBrowserHistory } from 'history';

import Login from './view/Login/Login';
import registrasi from './view/Registrasi/registrasi';
import startReg from './view/Registrasi/startReg';
import regPt from './view/Registrasi/regPT';
import regPtend from './view/Registrasi/regPT-end';
import termsCondition from './view/Registrasi/termsCondition';
import infoRekening from './view/Registrasi/infoRekening';
import kodeOTP from './view/Verifikasi/kodeOTP';
import inputKodeOTP from './view/Verifikasi/inputKodeOTP';
import verifSukses from './view/Verifikasi/verifSukses';
import inputDataOwner from './view/InformasiOwner/inputDataOwner';
import startKartuOwner from './view/InformasiOwner/startKartuOwner';
import uploadKartuOwner from './view/InformasiOwner/uploadKartuOwner';
import uploadKtp from './view/InformasiOwner/uploadKtp';
import uploadWajahKtp from './view/InformasiOwner/uploadWajahKtp';
import uploadNpwp from './view/InformasiOwner/uploadNpwp';
import informasiUsaha from './view/InformasiUsaha/informasiUsaha';
import uploadInformasiUsaha from './view/InformasiUsaha/uploadInformasiUsaha';
import uploadTempatUsaha from './view/InformasiUsaha/uploadTempatUsaha';
import uploadBarangJasa from './view/InformasiUsaha/uploadBarangJasa';
import uploadFotoOwner from './view/InformasiUsaha/uploadFotoOwner';
import informasiLokasiUsaha from './view/informasiLokasiUsaha/informasiLokasiUsaha';
// import provinsi from './view/informasiLokasiUsaha/AlamatUsaha/Provinsi_';
// import Provinsi from './view/informasiLokasiUsaha/AlamatUsaha/Provinsi';
// import Kota from './view/informasiLokasiUsaha/AlamatUsaha/Kota';
// import Kecamatan from './view/informasiLokasiUsaha/AlamatUsaha/Kecamatan';
// import Kelurahan from './view/informasiLokasiUsaha/AlamatUsaha/Kelurahan';
import ReviewData from './view/review&submit/reviewData';
import ResponReviewData from './view/review&submit/responReview';
import infoRekeningGagal from './view/Registrasi/infoRekeningGagal';
import infoDataSudahAda from './view/Registrasi/infoDataSudahAda';
import testingpicture from './view/testing/testingpicture';
import tensorflow from './view/testing/tensorflow';
import imageMaker from './helpers/imageMaker/imageMaker';
import homeScreen from './view/home/homeScreen';
import PrivateRouteComp from './master/PrivateRouteComp';
import CekStatusPendaftaran from './view/cekstatus/cekStatusPendaftaran';
import StatusPendaftaran from './view/cekstatus/statusPendaftaran';
import MakePassword from './view/Login/MakePassword';
import SuccessChangePassword from './view/Login/SuccessChangePassword';
import cekStatusPendaftaran from './view/cekstatus/cekStatusPendaftaran';
import profileSaya from './view/home/detail/profileSaya';
import informasiSaya from './view/home/detail/informasiSaya';
import helpSaya from './view/home/detail/helpSaya';
import pendaftaranRekeningBaru from './view/pendaftaranRekeningBaru/pendaftaranRekeningBaru';
import notifikasiSaya from './view/home/detail/notifikasiSaya';
import historyTrx from './view/historyTransaksi/historyTransaksi';
import pencairanSaya from './view/home/detail/pencairanSaya';
import riwayatTransaksi from './view/home/detail/riwayatTransaksi';
import { BACK_END_POINT, GET, HEADER_AUTH, POST, SESSION_ITEM, wsWithBody, wsWithoutBody } from './master/masterComponent';
import ChangePassword from './view/Login/ChangePassword';
import snkMitraQris from './view/home/detail/TermAndCondition/snkMitraQris';
import kebijakanPrivasiNHukum from './view/home/detail/TermAndCondition/kebijakanPrivasiNHukum';
import tentangAplikasiUsahaMandiri from './view/home/detail/TermAndCondition/tentangAplikasiUsahaMandiri';
import tanyaJawabQris from './view/home/detail/TermAndCondition/tanyaJawabQris';
import kontenMeningkatPenjualan from './view/home/detail/TermAndCondition/kontenMeningkatPenjualan';
import informasiSayaUpgrade from './view/home/detail/informasiSayaUpgrade';
import ForgetPassword from './view/Login/ForgetPassword';
// import firebase from './master/push-notification';

const history = createBrowserHistory();
const loading = () => <div className="animated fadeIn pt-3 text-center">Loading...</div>;


function App() {
  React.useEffect(()=>{
    var sessionItems = localStorage.getItem(SESSION_ITEM);
      wsWithoutBody(BACK_END_POINT+'/loginCtl/generateMySession',GET,{ ...HEADER_AUTH, 
        'session-item' : sessionItems }).then(
        response=>{
          localStorage.setItem(SESSION_ITEM, response.data);
        }).catch(
          error=>{
          this.props.history.push('/');
          location.reload();
         });
  });

 

  return (
    <Router history={history}>

      <React.Suspense fallback={loading}>
        <Switch>
          <Route path="/login" exact component={Login} />
          <Route path="/" exact component={registrasi} />
          <Route path="/startReg" exact component={startReg} />
          <Route path="/regPt" exact component={regPt} />
          <Route path="/regPt-end" exact component={regPtend} />
          <Route path="/termsCondition" exact component={termsCondition} />
          <Route path="/infoRekening" exact component={infoRekening} />
          <Route path="/kodeOTP" exact component={kodeOTP} />
          <Route path="/inputKodeOTP" exact component={inputKodeOTP} />
          <Route path="/verifSukses" exact component={verifSukses} />
          <Route path="/inputDataOwner" exact component={inputDataOwner} />
          <Route path="/startKartuOwner" exact component={startKartuOwner} />
          <Route path="/uploadKartuOwner" exact component={uploadKartuOwner} />
          <Route path="/uploadKtp" exact component={uploadKtp} />
          <Route path="/uploadWajahKtp" exact component={uploadWajahKtp} />
          <Route path="/uploadNpwp" component={uploadNpwp} />
          <Route path="/informasiUsaha" component={informasiUsaha} />
          <Route path="/uploadInformasiUsaha" component={uploadInformasiUsaha} />
          <Route path="/uploadTempatUsaha" component={uploadTempatUsaha} />
          <Route path="/uploadBarangJasa" component={uploadBarangJasa} />
          <Route path="/uploadFotoOwner" component={uploadFotoOwner} />
          <Route path="/informasiLokasiUsaha" component={informasiLokasiUsaha} />
          <Route path="/cekStatusPendaftaran" component={CekStatusPendaftaran} />
          <Route path="/statusPendaftaran" component={StatusPendaftaran} />
          <Route path="/pendaftaranRekeningBaru" component={pendaftaranRekeningBaru} />
          {/* <Route path="/provinsi" exact component={provinsi} /> */}
          {/* <Route exact path="/provinsi" render={(props) => <Provinsi {...props} /> } />
          <Route path="/kota/:provId/:provName" render={(props) => <Kota {...props} /> } />
          <Route path="/kecamatan/:kotId/:kotName/:provId/:provName" render={(props) => <Kecamatan {...props} /> } />
          <Route path="/kelurahan/:kecId/:kecName/:kotId/:kotName/:provId/:provName" render={(props) => <Kelurahan {...props} /> } /> */}
          <Route path="/reviewData" exact component={ReviewData} />
          <Route path="/responReviewData" exact component={ResponReviewData} />
          <Route path="/infoRekeningGagal" exact component={infoRekeningGagal} />
          <Route path="/infoDataSudahAda" exact component={infoDataSudahAda} />
          <Route path="/testingpicture" exact component={testingpicture}/>
          <Route path="/tensorflow" exact component={tensorflow}/>
          <Route path="/imageMaker" exact  component={imageMaker}/>
          <Route path="/makePassword" exact  component={MakePassword}/>
          <Route path="/successChangePassword" exact  component={SuccessChangePassword}/>
          <Route path="/cekStatusPendaftaran" exact  component={cekStatusPendaftaran}/>
          <Route path="/statusPendaftaran" exact  component={StatusPendaftaran}/>
          <Route path="/changePassword" exact  component={ChangePassword}/>
          <Route path="/forgetPassword" exact  component={ForgetPassword}/>
          <PrivateRouteComp path="/homeScreen" exact component={homeScreen}/>
          <PrivateRouteComp path="/profileSaya" exact component={profileSaya}/>
          <PrivateRouteComp path="/informasiSaya" exact component={informasiSaya}/>
          <PrivateRouteComp path="/informasiSayaUpgrade" exact component={informasiSayaUpgrade}/>
          <PrivateRouteComp path="/helpSaya" exact component={helpSaya}/>
          <PrivateRouteComp path="/notifikasiSaya" exact component={notifikasiSaya}/>
          <PrivateRouteComp path="/historyTrx" exact component={historyTrx}/>
          <PrivateRouteComp path="/pencairanSaya" exact component={pencairanSaya}/>
          <PrivateRouteComp path="/riwayatTransaksi" exact component={riwayatTransaksi}/>
          <PrivateRouteComp path="/snkMitraQris" exact component={snkMitraQris}/>
          <PrivateRouteComp path="/kebijakanPrivasiNHukum" exact component={kebijakanPrivasiNHukum}/>
          <PrivateRouteComp path="/tentangAplikasiUsahaMandiri" exact component={tentangAplikasiUsahaMandiri}/>
          <PrivateRouteComp path="/tanyaJawabQris" exact component={tanyaJawabQris}/>
          <PrivateRouteComp path="/kontenMeningkatPenjualan" exact component={kontenMeningkatPenjualan}/>
        </Switch>
      </React.Suspense>
    </Router>
  );
}

export default App;
