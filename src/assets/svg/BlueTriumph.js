import React from 'react';

export default class BlueTriumph extends React.Component{

    render(){
        return (<svg width="90" height="60" viewBox="0 0 90 60" fill="none" xmlns="http://www.w3.org/2000/svg">
        <g clipPath="url(#clip0)">
        <rect width="90" height="60" fill="white"/>
        <circle opacity="0.2" cx="43.3685" cy="31.6312" r="21.3685" fill="#2972EB"/>
        <mask id="mask0" mask-type="alpha" maskUnits="userSpaceOnUse" x="37" y="-3" width="54" height="52">
        <rect x="37.3218" y="-3" width="53.2197" height="51.607" rx="25.8035" fill="#C4C4C4"/>
        </mask>
        <g mask="url(#mask0)">
        <ellipse cx="61.3249" cy="22.799" rx="4.01452" ry="8.02903" transform="rotate(-26.285 61.3249 22.799)" fill="#FFC329"/>
        <rect x="36.1211" y="35.2432" width="10.7054" height="24.6553" rx="5.35269" transform="rotate(-26.285 36.1211 35.2432)" fill="#0041AE"/>
        <path d="M40.9796 20.9033L54.3205 7.11787C54.561 6.8694 54.975 6.93415 55.1281 7.24416L69.3352 36.0089C69.4823 36.3068 69.3034 36.6626 68.9766 36.7222L50.461 40.1003L40.9796 20.9033Z" fill="#4081ED"/>
        <path d="M31.3267 37.6131C28.7084 32.312 30.8833 25.8922 36.1844 23.2739L40.9836 20.9036L50.4651 40.1005L45.6659 42.4709C40.3648 45.0891 33.9449 42.9142 31.3267 37.6131Z" fill="#0057E7"/>
        <g style="mix-blend-mode:multiply" opacity="0.4">
        <path d="M31.3252 37.6133L45.7229 30.5022L50.4636 40.1007L43.2387 43.6691C39.2773 45.6256 34.4798 44.0004 32.5232 40.0389L31.3252 37.6133Z" fill="#C4C4C4"/>
        </g>
        <g style="mix-blend-mode:multiply" opacity="0.4">
        <path d="M45.729 30.5029L62.5263 22.2067L69.3439 36.01C69.491 36.3079 69.3121 36.6637 68.9853 36.7233L50.4697 40.1014L45.729 30.5029Z" fill="#0041AE"/>
        </g>
        <path d="M51.6479 42.5L53.151 41.7577C53.6461 41.5131 54.2458 41.7162 54.4904 42.2114L55.3374 43.9263C55.582 44.4215 55.3788 45.0212 54.8836 45.2658L53.3806 46.0081L51.6479 42.5Z" fill="#FFC329"/>
        <g opacity="0.5">
        <rect x="66.311" y="18.3848" width="9.50709" height="2.85213" rx="1.42606" transform="rotate(-26.285 66.311 18.3848)" fill="#FFC329"/>
        <rect x="64.2759" y="14.2666" width="6.65497" height="2.85213" rx="1.42606" transform="rotate(-52.6099 64.2759 14.2666)" fill="#FFC329"/>
        <rect width="6.65497" height="2.85213" rx="1.42606" transform="matrix(-1 -0.000696475 -0.000696475 1 75.6968 22.6719)" fill="#FFC329"/>
        </g>
        </g>
        <mask id="mask1" mask-type="alpha" maskUnits="userSpaceOnUse" x="22" y="9" width="44" height="44">
        <circle cx="43.6434" cy="31.2699" r="21.3685" fill="#E8F3FC"/>
        </mask>
        <g mask="url(#mask1)">
        <g filter="url(#filter0_d)">
        <ellipse cx="61.5871" cy="22.6535" rx="4.01452" ry="8.02903" transform="rotate(-26.285 61.5871 22.6535)" fill="#FFC329"/>
        <rect x="36.3833" y="35.0977" width="10.7054" height="24.6553" rx="5.35269" transform="rotate(-26.285 36.3833 35.0977)" fill="#0041AE"/>
        <path d="M41.2418 20.7578L54.5827 6.97236C54.8232 6.72389 55.2372 6.78864 55.3903 7.09865L69.5974 35.8634C69.7445 36.1612 69.5656 36.5171 69.2388 36.5767L50.7232 39.9548L41.2418 20.7578Z" fill="#4081ED"/>
        <path d="M31.5889 37.4676C28.9706 32.1665 31.1455 25.7466 36.4466 23.1284L41.2458 20.758L50.7273 39.955L45.9281 42.3254C40.627 44.9436 34.2071 42.7687 31.5889 37.4676Z" fill="#0057E7"/>
        <g style="mix-blend-mode:multiply" opacity="0.4">
        <path d="M31.5874 37.4678L45.9851 30.3567L50.7258 39.9551L45.9266 42.3255C40.6255 44.9437 34.2056 42.7689 31.5874 37.4678Z" fill="#C4C4C4"/>
        </g>
        <g style="mix-blend-mode:multiply" opacity="0.4">
        <path d="M45.9912 30.3574L62.7885 22.0611L69.6061 35.8645C69.7532 36.1624 69.5743 36.5182 69.2475 36.5778L50.7319 39.9559L45.9912 30.3574Z" fill="#0041AE"/>
        </g>
        <path d="M51.9102 42.3545L53.4132 41.6121C53.9084 41.3676 54.508 41.5707 54.7526 42.0659L55.5996 43.7808C55.8442 44.276 55.641 44.8757 55.1459 45.1203L53.6428 45.8626L51.9102 42.3545Z" fill="#FFC329"/>
        <rect x="63.8882" y="10.6279" width="9.6602" height="4.14008" rx="2.07004" transform="rotate(-52.6099 63.8882 10.6279)" fill="#0057E7"/>
        </g>
        </g>
        </g>
        <defs>
        <filter id="filter0_d" x="25.8481" y="3.78418" width="54.5997" height="55.419" filterUnits="userSpaceOnUse" colorInterpolationFilters="sRGB">
        <feFlood floodOpacity="0" result="BackgroundImageFix"/>
        <feColorMatrix in="SourceAlpha" type="matrix" values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 127 0"/>
        <feOffset dx="-1" dy="2"/>
        <feColorMatrix type="matrix" values="0 0 0 0 0 0 0 0 0 0.341177 0 0 0 0 0.905882 0 0 0 0.2 0"/>
        <feBlend mode="normal" in2="BackgroundImageFix" result="effect1_dropShadow"/>
        <feBlend mode="normal" in="SourceGraphic" in2="effect1_dropShadow" result="shape"/>
        </filter>
        <clipPath id="clip0">
        <rect width="90" height="60" fill="white"/>
        </clipPath>
        </defs>
        </svg>
        
        );
    }

}
