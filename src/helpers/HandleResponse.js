export function handleResponse(response) {
    return response.text().then(text => {
        const result = text && JSON.parse(text);
        if (result.code === 200){
            return result;
        }else {
            const error = result;
            return Promise.reject(error);
        }
    });
}