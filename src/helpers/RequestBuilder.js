import { getLanguage } from 'react-switch-lang';

export const HEADER_AUTH = {
    //'Accept-Language': getLanguage(),
    'Accept': '*/*',
    'Content-Type': 'application/json',
    'secret-id' : 'YRuYy-dSiaK-LLdWA',
    'secret-key' : '4Vbxc-uoGWA-LVYg5'
};

export function request(method, body) {
    return {
        method: method,
        headers: HEADER_AUTH,
        body: JSON.stringify(body)
    };
}

export function requestNoBody(method) {
    return {
        method: method,
        headers: HEADER_AUTH
    };
}