import React from 'react';
import compose from 'recompose/compose';
import { withStyles } from '@material-ui/core/styles';
import { Backdrop, CircularProgress } from '@material-ui/core';
const useStyles = theme => ({
    paper: {
        marginTop: theme.spacing(8),
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center'
      },
      backdrop: {
        zIndex: theme.zIndex.drawer + 1,
        color: '#fff',
      }
});

class LoadingMaker extends React.Component{
    constructor(props) {
        super(props);
    }

    render() {
        const { classes } = this.props;
        return (
            <Backdrop className={classes.backdrop} open={this.props.onLoading} >
            <div className={classes.paper}>
                        <span><h4 style={{color: 'white', fontWeight : 'bold',
                        fontSize: '4.6vw' ,fontFamily : 'Nunito, sans-serif',}}>
                            Mohon tunggu sebentar</h4></span>
                        <CircularProgress style={{color : 'white', width:'9.05vw', height:'9.05vw'}}/>
            </div>
            </Backdrop>
        )
    }
}

export default compose(
    withStyles(useStyles),
  )(LoadingMaker);