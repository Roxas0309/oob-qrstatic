import React from 'react';
import compose from 'recompose/compose';
import { withStyles } from '@material-ui/core/styles';
import { Backdrop, Box, Button, CircularProgress, Dialog } from '@material-ui/core';
const useStyles = theme => ({
    paper: {
        marginTop: theme.spacing(8),
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center'
      },
      backdrop: {
        zIndex: theme.zIndex.drawer + 1,
        color: '#fff',
      }
});

class LoadingKoneksi extends React.Component{
    constructor(props) {
        super(props);
    }

    render() {
        const { classes } = this.props;
        const closeDialog = false

        return (
            <Dialog fullScreen style={{height: '238px', width: '100%', position: 'absolute',
                                    top: 'unset' ,bottom: '0px'}} 
                    open={this.props.onLoading} onClose={closeDialog}>
                <Box style={{height: '50px', justifyContent: 'center', alignItems: 'center', 
                    display: 'block', textAlign: 'center', marginBottom: '20px'}}>
                    <p style={{fontFamily: 'Montserrat', fontSize: '20px', lineHeight: '30px', fontWeight: '700', 
                        marginTop: '24px', marginBottom: '16px', marginInlineStart: '16px', marginInlineEnd: '16px'}}>Ups, tidak ada koneksi internet</p>
                    <p style={{fontSize: '16px', lineHeight: '24px', fontWeight: '400', marginInlineStart: '16px', marginInlineEnd: '16px',
                        marginTop: '16px', marginBottom: '16px', color: 'rgba(48, 59, 74, 0.72)'}}>Cek ketersediaan paket data dan koneksi ke Wi-Fi Anda, ya. </p>
                
                
                </Box>
                <Box >
                <Button disableElevation type="submit" size="large" variant="contained" color="primary"
                        style={{backgroundColor: '#0057E7', marginInlineStart: '17px'}}
                        onClick={() => location.reload()}>
                    Oke
                </Button>
                </Box>
              
            </Dialog>
            // <Backdrop className={classes.backdrop} open={this.props.onLoading} >
            //     <div className={classes.paper}>
            //                 <span><h4 style={{color: 'white', fontWeight : 'bold',
            //                 fontSize: '4.6vw' ,fontFamily : 'Nunito, sans-serif',}}>
            //                     Sedang mencari koneksi. . .</h4></span>
            //                 <CircularProgress style={{color : 'white', width:'9.05vw', height:'9.05vw'}}/>
            //     </div>
            // </Backdrop>
        )
    }
}

export default compose(
    withStyles(useStyles),
  )(LoadingKoneksi);