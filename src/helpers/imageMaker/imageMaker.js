import { Box, Button } from "@material-ui/core";
import React from "react";
import ButtonComponent from "../../containers/button/ButtonComponent";
import { BACK_END_POINT, GET, wsWithoutBody } from "../../master/masterComponent";
import LoadingCamera from "../loadingMaker/LoadingCamera";
import LoadingMaker from "../loadingMaker/LoadingMaker";
import "./tensor-style.css";
import Compressor from 'compressorjs';

const nameTitle = {
    ktp: "Posisikan KTP pada kotak foto yang tersedia dan pastikan data dapat terbaca.",
    ktpWajah: "Posisikan wajah dan KTP pada bingkai yang tersedia.",
    npwp: "Posisikan NPWP pada bingkai yang tersedia dan pastikan data dapat terbaca.",
    tmptUsaha: "Posisikan tempat usaha di bingkai yang tersedia.",
    brgJasa: "Posisikan Barang atau Jasa di bingkai yang tersedia.",
    owner: "Posisikan Pemilik usaha di bingkai yang tersedia."
}

export default class imageMaker extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            showButton: false,
            baseUrl: null,
            baseUrlFormal: null,
            query: new URLSearchParams(props.location.search).get("_query"),
            fullScreenshoot: new URLSearchParams(props.location.search).get("_fullScreenshoot"),
            canvasNumber: new URLSearchParams(props.location.search).get("_canvasNumber"),
            originForm: new URLSearchParams(props.location.search).get("_originForm"),
            faceMode : new URLSearchParams(props.location.search).get("_faceMode"),
            getFile: false,
            yourFaceMode:'environment',
            userCanvasx:0,
            userCanvasy:0,
            onRenderCamera:false
        }
    }

    videoRef = React.createRef();
    canvasValidatorRef = React.createRef();
    roundValidatorRef = React.createRef();
    cardValidatorRef = React.createRef();

    componentDidMount() {

        
      

        window.onpopstate = function(event) {
            window.location.reload();
        };
    if(this.state.query === "imgNpwp"){

        if(localStorage.getItem('pathImg') === null || localStorage.getItem('pathImg') === undefined){
            this.setState({getFile: false})
            this.renderMagicCamera();
            //this.setState({baseUrl: localStorage.getItem('pathImg'), showButton : true})
        }else{
            this.setState({baseUrl: localStorage.getItem('pathImg'), showButton : true, getFile: true})
        }
            
       
    }else{
        this.setState({getFile: false})
        this.renderMagicCamera();
    }
    
    }

    componentWillUnmount(){
        location.reload()
    }

    renderMagicCamera = () =>{
        if (navigator.mediaDevices && navigator.mediaDevices.getUserMedia) {
            if (this.state.canvasNumber === "1") {
                var canvasValidator = document.querySelector('#canvas-validator');
                var refvid = this.videoRef.current;
            }

            else if (this.state.canvasNumber === "2") {
                // var roundValidator = document.querySelector('#round-validator');
                // var refvid = this.videoRef.current;
                // roundValidator.style.top = 30 + "px";
                // roundValidator.style.left = ((window.innerWidth - roundValidator.width) / 2) + "px";


                // var cardValidator = document.querySelector('#card-validator');
                // var refvid = this.videoRef.current;
                // cardValidator.style.bottom = 115 + "px";
                // cardValidator.style.left = ((window.innerWidth - cardValidator.width) / 2) + "px";
            }
            else if (this.state.canvasNumber === "3") {
                var canvasValidator = document.querySelector('#canvas-validator');
                var refvid = this.videoRef.current;
            }

            var faceMode = localStorage.getItem('faceMode');
            // if(faceMode == null){
            //     faceMode = 'user';
            // }
            if(this.state.query==='imgKtpWajah'){
                faceMode = 'user';
                this.setState({yourFaceMode:'user'})
            }
            else{
                faceMode = 'environment';
            }

            this.setState({onRenderCamera:true});
            wsWithoutBody(BACK_END_POINT + "/configuration-component-maker/get-camera-component",GET).then(response => {
           
                var resp = {}; 
                resp = response.data;
                console.log("resp : " + JSON.stringify(resp));

               

                var video = {
                    "facingMode": faceMode,
                    ...resp
                }

                var value = {
                    audio: false,
                    video : {...video}
                }

           
                console.log("value camera now : " + JSON.stringify(value) );

                navigator.mediaDevices.getUserMedia(value)
               .then(stream => {
                        window.stream = stream;
                        this.videoRef.current.srcObject = stream;
                        this.setState({onRenderCamera:false});
                        // this.setState({onRenderCamera:true});
                        // return new Promise((resolve, reject) => {
                        //     this.videoRef.current.onloadedmetadata = () => {
                        //         resolve();
                        //         this.setState({onRenderCamera:false});
                        //     };
                        // });
                }).catch(error=>{
                    alert(" application error said : " + error);
                    this.setState({onRenderCamera:false});
                });


            })
        }
        else{
            alert("Enabled to open the camera. Make sure your browser able to access the camera.")
        }
    }


    //full screenshoot
    screenShoot = () => {
        var img = document.querySelector('img') || document.createElement('img');
        var canvasValidator = document.querySelector('#canvas-validator');
      
        var video = document.querySelector('video');
        var canvas = canvas || document.createElement('canvas');
        canvas.width = video.clientWidth;
        canvas.height =  video.clientHeight;
    
        var context = canvas.getContext('2d');
    
        context.drawImage(video,0,0,video.clientWidth,video.clientHeight);
        var baseUrl = canvas.toDataURL('image/png');
        // console.log('canvasValidator.offsetLeft : ', canvasValidator.offsetLeft)
        // console.log('canvasValidator.offsetTop : ', canvasValidator.offsetTop)
        // console.log('canvasValidator.clientWidth : ', canvasValidator.clientWidth)
        // console.log('canvasValidator.clientWidth : ', canvasValidator.clientHeight)
        // console.log('getImage canvas: ', baseUrl)
        this.setState({ baseUrl: baseUrl, showButton : true, userCanvasx:video.clientWidth, userCanvasy: video.clientHeight });
    }

    // scales the canvas by (float) scale < 1
// returns a new canvas containing the scaled image.
downScaleCanvas = (cv, scale) => {
    alert("downScaleCanvas run");
    if (!(scale < 1) || !(scale > 0)) throw ('scale must be a positive number <1 ');
    var sqScale = scale * scale; // square scale = area of source pixel within target
    var sw = cv.width; // source image width
    var sh = cv.height; // source image height
    var tw = Math.floor(sw * scale); // target image width
    var th = Math.floor(sh * scale); // target image height
    var sx = 0, sy = 0, sIndex = 0; // source x,y, index within source array
    var tx = 0, ty = 0, yIndex = 0, tIndex = 0; // target x,y, x,y index within target array
    var tX = 0, tY = 0; // rounded tx, ty
    var w = 0, nw = 0, wx = 0, nwx = 0, wy = 0, nwy = 0; // weight / next weight x / y
    // weight is weight of current source point within target.
    // next weight is weight of current source point within next target's point.
    var crossX = false; // does scaled px cross its current px right border ?
    var crossY = false; // does scaled px cross its current px bottom border ?
    var sBuffer = cv.getContext('2d').
    getImageData(0, 0, sw, sh).data; // source buffer 8 bit rgba
    var tBuffer = new Float32Array(3 * tw * th); // target buffer Float32 rgb
    var sR = 0, sG = 0,  sB = 0; // source's current point r,g,b
    /* untested !
    var sA = 0;  //source alpha  */    

    for (sy = 0; sy < sh; sy++) {
        ty = sy * scale; // y src position within target
        tY = 0 | ty;     // rounded : target pixel's y
        yIndex = 3 * tY * tw;  // line index within target array
        crossY = (tY != (0 | ty + scale)); 
        if (crossY) { // if pixel is crossing botton target pixel
            wy = (tY + 1 - ty); // weight of point within target pixel
            nwy = (ty + scale - tY - 1); // ... within y+1 target pixel
        }
        for (sx = 0; sx < sw; sx++, sIndex += 4) {
            tx = sx * scale; // x src position within target
            tX = 0 |  tx;    // rounded : target pixel's x
            tIndex = yIndex + tX * 3; // target pixel index within target array
            crossX = (tX != (0 | tx + scale));
            if (crossX) { // if pixel is crossing target pixel's right
                wx = (tX + 1 - tx); // weight of point within target pixel
                nwx = (tx + scale - tX - 1); // ... within x+1 target pixel
            }
            sR = sBuffer[sIndex    ];   // retrieving r,g,b for curr src px.
            sG = sBuffer[sIndex + 1];
            sB = sBuffer[sIndex + 2];

            /* !! untested : handling alpha !!
               sA = sBuffer[sIndex + 3];
               if (!sA) continue;
               if (sA != 0xFF) {
                   sR = (sR * sA) >> 8;  // or use /256 instead ??
                   sG = (sG * sA) >> 8;
                   sB = (sB * sA) >> 8;
               }
            */
            if (!crossX && !crossY) { // pixel does not cross
                // just add components weighted by squared scale.
                tBuffer[tIndex    ] += sR * sqScale;
                tBuffer[tIndex + 1] += sG * sqScale;
                tBuffer[tIndex + 2] += sB * sqScale;
            } else if (crossX && !crossY) { // cross on X only
                w = wx * scale;
                // add weighted component for current px
                tBuffer[tIndex    ] += sR * w;
                tBuffer[tIndex + 1] += sG * w;
                tBuffer[tIndex + 2] += sB * w;
                // add weighted component for next (tX+1) px                
                nw = nwx * scale
                tBuffer[tIndex + 3] += sR * nw;
                tBuffer[tIndex + 4] += sG * nw;
                tBuffer[tIndex + 5] += sB * nw;
            } else if (crossY && !crossX) { // cross on Y only
                w = wy * scale;
                // add weighted component for current px
                tBuffer[tIndex    ] += sR * w;
                tBuffer[tIndex + 1] += sG * w;
                tBuffer[tIndex + 2] += sB * w;
                // add weighted component for next (tY+1) px                
                nw = nwy * scale
                tBuffer[tIndex + 3 * tw    ] += sR * nw;
                tBuffer[tIndex + 3 * tw + 1] += sG * nw;
                tBuffer[tIndex + 3 * tw + 2] += sB * nw;
            } else { // crosses both x and y : four target points involved
                // add weighted component for current px
                w = wx * wy;
                tBuffer[tIndex    ] += sR * w;
                tBuffer[tIndex + 1] += sG * w;
                tBuffer[tIndex + 2] += sB * w;
                // for tX + 1; tY px
                nw = nwx * wy;
                tBuffer[tIndex + 3] += sR * nw;
                tBuffer[tIndex + 4] += sG * nw;
                tBuffer[tIndex + 5] += sB * nw;
                // for tX ; tY + 1 px
                nw = wx * nwy;
                tBuffer[tIndex + 3 * tw    ] += sR * nw;
                tBuffer[tIndex + 3 * tw + 1] += sG * nw;
                tBuffer[tIndex + 3 * tw + 2] += sB * nw;
                // for tX + 1 ; tY +1 px
                nw = nwx * nwy;
                tBuffer[tIndex + 3 * tw + 3] += sR * nw;
                tBuffer[tIndex + 3 * tw + 4] += sG * nw;
                tBuffer[tIndex + 3 * tw + 5] += sB * nw;
            }
        } // end for sx 
    } // end for sy

    // create result canvas
    var resCV = document.createElement('canvas');
    resCV.width = tw;
    resCV.height = th;
    var resCtx = resCV.getContext('2d');
    var imgRes = resCtx.getImageData(0, 0, tw, th);
    var tByteBuffer = imgRes.data;
    // convert float32 array into a UInt8Clamped Array
    var pxIndex = 0; //  
    for (sIndex = 0, tIndex = 0; pxIndex < tw * th; sIndex += 3, tIndex += 4, pxIndex++) {
        tByteBuffer[tIndex] = Math.ceil(tBuffer[sIndex]);
        tByteBuffer[tIndex + 1] = Math.ceil(tBuffer[sIndex + 1]);
        tByteBuffer[tIndex + 2] = Math.ceil(tBuffer[sIndex + 2]);
        tByteBuffer[tIndex + 3] = 255;
    }
    // writing result to canvas.
    resCtx.putImageData(imgRes, 0, 0);
    resCV;
    debugger;
    return resCV;
}

    screenShootCanvas = () => {
        var img = document.querySelector('img') || document.createElement('img');
        var canvasValidator = document.querySelector('#canvas-validator');
      
        var video = document.querySelector('video');
        var canvas = canvas || document.createElement('canvas');
     
        canvas.width = video.offsetWidth;
        canvas.height = video.offsetHeight;
    
        var context = canvas.getContext('2d');
        context.mozImageSmoothingEnabled = true;
        context.imageSmoothingQuality = "High";
        context.webkitImageSmoothingEnabled = true;
        context.msImageSmoothingEnabled = true;
        context.imageSmoothingEnabled = true;
        context.drawImage(video,0,0,video.offsetWidth,video.offsetHeight);
        // context.drawImage(video, canvasValidator.offsetLeft,
        //     canvasValidator.offsetTop, canvasValidator.clientWidth, canvasValidator.clientHeight,
        //     0, 0
        //     , canvas.width, canvas.height);

        var url = canvas.toDataURL('image/png');
        var canvas2 =  document.createElement('canvas');
       
        canvas2.width =  canvasValidator.offsetWidth;
        canvas2.height =  canvasValidator.offsetHeight;
        var context2 = canvas2.getContext('2d');

        
        context2.drawImage(canvas, canvasValidator.offsetLeft ,
            canvasValidator.offsetTop,  canvasValidator.offsetWidth,
            canvasValidator.offsetHeight, 0, 0,  canvasValidator.offsetWidth, 
            canvasValidator.offsetHeight);
         
         

        // var baseUrl = canvas2.toDataURL('image/png');
        var baseUrl = canvas2.toDataURL('image/png');
        // const contentType = 'image/png';
        // const b64Data = baseUrl.split(";")[1].split(",")[1];
        // const byteCharacters = atob(b64Data);
        // const byteArrays = [];
        // var sliceSize = 512
        // for (let offset = 0; offset < byteCharacters.length; offset += sliceSize) {
        //   const slice = byteCharacters.slice(offset, offset + sliceSize);

        //   const byteNumbers = new Array(slice.length);
        //   for (let i = 0; i < slice.length; i++) {
        //     byteNumbers[i] = slice.charCodeAt(i);
        //   }

        //   const byteArray = new Uint8Array(byteNumbers);
        //   byteArrays.push(byteArray);
        // }

        // const blob = new Blob(byteArrays, {type: contentType});


        // var file = new File([blob], "test.png", {type: contentType});

        // var baseUrlBaru = "";
        // new Compressor(file, {
        //     quality: 0.3, // 0.6 can also be used, but its not recommended to go below.
        //     success: (compressedResult) => {
        //         var urlCreator = window.URL || window.webkitURL;
        //         var imageUrl = urlCreator.createObjectURL(compressedResult);
        //    //      this.setState({ baseUrl: imageUrl, baseUrlFormal:url,showButton : true });
        //     },
        //   });

         
        // var baseUrl = reader.result;     
        // console.log('canvasValidator.offsetLeft : ', canvasValidator.offsetLeft)
        // console.log('canvasValidator.offsetTop : ', canvasValidator.offsetTop)
        // console.log('canvasValidator.clientWidth : ', canvasValidator.clientWidth)
        // console.log('canvasValidator.clientWidth : ', canvasValidator.clientHeight)
        // console.log('getImage canvas: ', baseUrl)
        this.setState({ baseUrl: baseUrl, baseUrlFormal:url,showButton : true });
    }


    
    fotoUlang = () => {
        this.setState({ baseUrl: '' });
        location.reload();
    }

    ambilUlang = () => {
        this.setState({baseUrl : ''});
        location.reload();
    }


    ambilFile = (event) => {
      if (event.target.files && event.target.files[0]) {
      //   this.setState({    overflow: hidden;
      //     imgFile: URL.createObjectURL(event.target.files[0]), openFile: true,
      //     imgBlob: event.target.files[0]
      //   });

        let rd = new FileReader();
          var imgBlob = event.target.files[0]
          var val

          if(imgBlob.size>2000000){
            alert("Tidak Dapat Mengupload File Ukuran Lebih dari 2Mb.")
            console.log('imgblob ' + imgBlob.type);
          }
          else{
          rd.readAsDataURL(imgBlob);
          rd.onload = () => {
              val = rd.result
              localStorage.setItem('pathImg', val)
              this.setState({baseUrl : ''});
              this.setState({ baseUrl: val, showButton : true});      
          }
        }
      }
     }

    saveThisPhotoGaleri = () => {
        localStorage.removeItem('cekNpwp')
        localStorage.setItem(this.state.query,localStorage.getItem('pathImg'))
        this.props.history.push('/'+this.state.originForm, {from: '/imageMaker'})
        location.reload();
    }
    saveThisPhoto = () => {
        localStorage.removeItem('cekNpwp')
        localStorage.setItem(this.state.query, this.state.baseUrl)
        this.props.history.push("/" + this.state.originForm, {from: '/imageMaker'});
        localStorage.removeItem('faceMode');
        location.reload();
    }

    userMode = () =>{
        localStorage.setItem('faceMode','user');
        location.reload();
    }

    environmentMode = () =>{
        localStorage.setItem('faceMode','environment');
        location.reload();
    }


    videoPhotoShoot = () => {
        if (this.state.baseUrl === null || this.state.baseUrl === '') {
            const [size, setSize] = React.useState({ width: window.innerWidth, height: window.innerHeight });
            
            React.useEffect(() => {
                const checkSize = () => {
                    setSize({
                      width: window.innerWidth,
                      height: window.innerHeight,
                    });
                };
              
                window.addEventListener('resize', checkSize);
                return () => window.removeEventListener('resize', checkSize);
              
              }, []);

              var stageWidth = size.width % 2 !== 0 ? size.width - 1 : size.width;
              var stageHeight = size.height % 2 !== 0 ? size.height - 1 : size.height;

            return <span>
            
              
             <video
                className={this.state.yourFaceMode === 'environment' ?
                           "size video-custom-maker":
                           "size video-custom-maker-user rotate-me"
                          }
                autoPlay={true}
                muted="muted"
                ref={this.videoRef}
                playsInline
                //style={{ transform: 'scaleX(-1,1)'}}
            />
                {
                    this.state.canvasNumber === "1" ?
                        <div style={{justifyContent: 'center', alignItems: 'center', display: 'flex'}}>
                            <canvas id="canvas-validator"
                                className="size-validator"
                                ref={this.canvasValidatorRef}
                            />
                        </div> :
                        this.state.canvasNumber === "2" ?
                        
                            <div style={{justifyContent: 'center', alignItems: 'center', display: 'flex'}}>
                                 <canvas id="canvas-validator-full-cover" style={{"maxWidth" : (window.innerWidth-10)}}
                                className="size-validator"
                                ref={this.canvasValidatorRef}
                            />
                                <canvas id="round-validator-versi2"
                                    className="round-validator-versi2"
                                    ref={this.roundValidatorRef}
                                    style={{borderRadius:'67%', top:'10vh', width:'42vw', height:'34vh', border:'3px #ffffff solid', position:'absolute'}}
                                    // width={stageWidth - 150}
                                    // height={stageHeight / 2.5}
                                />
                                <canvas id="card-validator-versi2"
                                    ref={this.cardValidatorRef}
                                    className="card-validator-versi2"
                                    style={{position:'absolute', border:'3px #ffffff solid', 
                                    left:'19vw!important', top:'49vh', height:'18vh', width:'59vw'}}
                                />
                            </div>
                            : <div></div>
                }
                <div className="div-value-white-canvas-up">
                <svg style={{marginTop:'6px', marginRight:'11px'}} onClick={()=>{
                    this.props.history.push("/"+this.state.originForm);
                     location.reload();
                }}
                width="16" height="16" viewBox="0 0 16 16" fill="none" xmlns="http://www.w3.org/2000/svg">
<path d="M9.48726 7.99996L15.8148 1.67242L14.3277 0.185303L8.00014 6.51284L1.67277 0.185462L0.185649 1.67258L6.51303 7.99996L0.185547 14.3274L1.67267 15.8146L8.00014 9.48708L14.3278 15.8147L15.8149 14.3276L9.48726 7.99996Z" fill="#0057E7"/>
</svg>

                </div>  
                <div className="div-value-white-canvas-down">

                <div className="container-button">
                  {/* <button onClick={this.userMode} className="faceUser userBtns">
                                <i className="fa fa-user"></i>
                    </button> */}
                       
                        <div style={{backgroundColor: 'rgba(240,237,237,1)', border:'none',marginBottom:'40%', width:'70px',height:'70px',
                    display:'flex',justifyContent:'center',borderRadius:'50%',position:'relative'}} 
                        onClick={
                            this.state.fullScreenshoot === "yes" ?
                                this.screenShoot :
                                this.screenShootCanvas} 
                       >
                                    <div className ="sub-icon-div"></div>
                        </div>

                    {/* <button onClick={this.environmentMode}className="faceEnvironment environmentBtns">
                     <i className="fa fa-street-view"></i>
                    </button> */}
                </div>

                </div>
             
            </span>
        } else {
            return <div style={{justifyContent: 'center', alignItems: 'center', display: 'flex'}}>

{/* <img style={{position:'absolute', top:'0.2vh'}}  src={this.state.baseUrl}></img> 

<img style={{position:'absolute','top': '28vh'}}   src={this.state.baseUrlFormal}></img>  */}
                {
                    this.state.canvasNumber === "1" ?
                        <img id="canvas-validator"
                        className="size-validator"
                            src={this.state.baseUrl}></img> :
                        this.state.canvasNumber === "2" ||
                         this.state.canvasNumber === "3" ?
                         <div id="canvas-validator-fullflavour"
                              className="size-validator rotate-me">
                            <img  src={this.state.baseUrl}  width={this.state.userCanvasx} height={this.state.userCanvasy} />
                                </div>
                            : <div></div>
                }
            </div>
        }
    }

    render() {
        return (
            <div id="tested-id">

<LoadingCamera onLoading={this.state.onRenderCamera}
            loadingWord={'Proses login sedang berlangsung'}
          ></LoadingCamera>


                {!this.state.showButton && <p style={{
                    textAlign: 'center',fontSize: '3.564vw', color: '#FFFFFF',
                    position: 'absolute', zIndex: '1',top:'5vh'
                    ,height:'52px', fontSize:'16px', width:'100%'
                    
                }}>
                    {this.state.query === "imgKtp" && nameTitle.ktp}
                    {this.state.query === "imgNpwp" && nameTitle.npwp}
                    {/* {this.state.query === "imgKtpWajah" && nameTitle.ktpWajah} */}
                    {this.state.query === "tmptUsaha" && nameTitle.tmptUsaha}
                    {this.state.query === "fotoOwner" && nameTitle.owner}
                    {this.state.query === "brngJasa" && nameTitle.brgJasa}
                </p>}
                {!this.state.showButton && <p style={{
                    textAlign: 'center',fontSize: '3.564vw', color: '#FFFFFF',
                    position: 'absolute', zIndex: '1',top:'2.6vh',lineHeight:'2.3vh',  width:'100%',
                    height:'49px', fontSize:'16px'
                }}>
                    {this.state.query === "imgKtpWajah" && nameTitle.ktpWajah}
                </p>}
                {this.state.getFile ? 
                    <Box style={{justifyContent:'center', alignItems: 'center', 
                                display: 'flex', marginTop: '20px'}}>
                        <img src={localStorage.getItem('pathImg')} height={400} width={'80%'}/>
                    </Box>
                :<this.videoPhotoShoot></this.videoPhotoShoot>}
                {this.state.getFile ? 
              
              
              <Box hidden = {!this.state.showButton} id="box-component-button" style={{ position: 'fixed', bottom: '20px', marginInlineStart: '20px', minWidth: '90%' }}>
                   {/* <Button id="box-component-button-child1" size="large" onClick={this.saveThisPhotoGaleri} style={{
                            width: "100%", marginTop: '10px', height: '56px',
                            textTransform: 'none'
                        }} variant="contained" color="primary">
                            Gunakan Gambar
                    </Button> */}

<div className="div-value-white-canvas-up">
                <svg style={{marginTop:'6px',marginRight:'35px'}} onClick={()=>{
                    this.props.history.push("/"+this.state.originForm);
                     location.reload();
                }}
                width="16" height="16" viewBox="0 0 16 16" fill="none" xmlns="http://www.w3.org/2000/svg">
<path d="M9.48726 7.99996L15.8148 1.67242L14.3277 0.185303L8.00014 6.51284L1.67277 0.185462L0.185649 1.67258L6.51303 7.99996L0.185547 14.3274L1.67267 15.8146L8.00014 9.48708L14.3278 15.8147L15.8149 14.3276L9.48726 7.99996Z" fill="#0057E7"/>
</svg>

                </div>     
                    <ButtonComponent style={{zIndex:'2'}} id="box-component-button-child1" size="large" onClick={this.saveThisPhotoGaleri}>
                    Gunakan Gambar
                    </ButtonComponent>
                    
                    <Button size="large"  style={{
                        width: "100%", marginTop: '10px', height: '56px',zIndex:'2',
                        textTransform: 'none', marginBottom: '10px', backgroundColor: '#E8F3FC', color: '#0057E7'
                    }} variant="contained" component="label">
                        Ambil Ulang Gambar
                        <input type="file" accept="image/*" onChange={this.ambilFile} style={{display: 'none'}} />
                    </Button>

                  
                </Box> :
                <div className="blockade-choose">
                <Box hidden = {!this.state.showButton} id="box-component-button" style={{ position: 'fixed', bottom: '0vh',  minWidth: '100%', paddingLeft:'6.3vw', backgroundColor:'white'}}>
                 
                <div className="div-value-white-canvas-up">
                <svg style={{marginTop:'6px', marginRight:'35px'}} onClick={()=>{
                    this.props.history.push("/"+this.state.originForm);
                     location.reload();
                }}
                width="16" height="16" viewBox="0 0 16 16" fill="none" xmlns="http://www.w3.org/2000/svg">
<path d="M9.48726 7.99996L15.8148 1.67242L14.3277 0.185303L8.00014 6.51284L1.67277 0.185462L0.185649 1.67258L6.51303 7.99996L0.185547 14.3274L1.67267 15.8146L8.00014 9.48708L14.3278 15.8147L15.8149 14.3276L9.48726 7.99996Z" fill="#0057E7"/>
</svg>

                </div>  
                 
                    <Button id="box-component-button-child1" size="large" onClick={this.saveThisPhoto} style={{
                             marginTop: '10px',
                            textTransform: 'none', height: '56px', width:'87.8vw',
                        }} variant="contained" color="primary">
                            Gunakan Foto
                    </Button>

                    <Button size="large" onClick={this.fotoUlang} style={{
                        width:'87vw',marginTop: '10px', height: '56px',
                        textTransform: 'none', marginBottom: '10px', backgroundColor: '#E8F3FC', color: '#0057E7'
                    }} variant="contained" >
                        Foto Ulang
                    </Button>
                </Box>
                </div>
                }
            </div>

        );
    }

}