import { Box, Button } from "@material-ui/core";
import React from "react";
import ButtonComponent from "../../containers/button/ButtonComponent";
import "./tensor-stylev2.css";

const nameTitle = {
    ktp: "Posisikan KTP pada kotak foto yang tersedia dan pastikan data dapat terbaca.",
    ktpWajah: "Posisikan wajah dan KTP pada bingkai yang tersedia.",
    npwp: "Posisikan NPWP pada bingkai yang tersedia dan pastikan data dapat terbaca.",
    tmptUsaha: "Posisikan tempat usaha di bingkai yang tersedia.",
    brgJasa: "Posisikan Barang atau Jasa di bingkai yang tersedia.",
    owner: "Posisikan Pemilik usaha di bingkai yang tersedia."
}

export default class imageMaker extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            showButton: false,
            baseUrl: null,
            query: new URLSearchParams(props.location.search).get("_query"),
            fullScreenshoot: new URLSearchParams(props.location.search).get("_fullScreenshoot"),
            canvasNumber: new URLSearchParams(props.location.search).get("_canvasNumber"),
            originForm: new URLSearchParams(props.location.search).get("_originForm"),
            faceMode : new URLSearchParams(props.location.search).get("_faceMode"),
            getFile: false,
            yourFaceMode:'environment'
        }
    }

    videoRef = React.createRef();
    canvasValidatorRef = React.createRef();
    roundValidatorRef = React.createRef();
    cardValidatorRef = React.createRef();
   
    componentDidMount() {
        window.onpopstate = function(event) {
            window.location.reload();
        };
    if(this.state.query === "imgNpwp"){

        if(localStorage.getItem('pathImg') === null || localStorage.getItem('pathImg') === undefined){
            this.setState({getFile: false})
            this.renderMagicCamera();
            //this.setState({baseUrl: localStorage.getItem('pathImg'), showButton : true})
        }else{
            this.setState({baseUrl: localStorage.getItem('pathImg'), showButton : true, getFile: true})
        }
            
       
    }else{
        this.setState({getFile: false})
        this.renderMagicCamera();
    }
    
    }

    renderMagicCamera = () =>{
        if (navigator.mediaDevices && navigator.mediaDevices.getUserMedia) {

            if (this.state.canvasNumber === "1") {
                var canvasValidator = document.querySelector('#canvas-validator');
                var refvid = this.videoRef.current;
            }

            else if (this.state.canvasNumber === "2") {
                // var roundValidator = document.querySelector('#round-validator');
                // var refvid = this.videoRef.current;
                // roundValidator.style.top = 30 + "px";
                // roundValidator.style.left = ((window.innerWidth - roundValidator.width) / 2) + "px";


                // var cardValidator = document.querySelector('#card-validator');
                // var refvid = this.videoRef.current;
                // cardValidator.style.bottom = 115 + "px";
                // cardValidator.style.left = ((window.innerWidth - cardValidator.width) / 2) + "px";
            }
            else if (this.state.canvasNumber === "3") {
                var canvasValidator = document.querySelector('#canvas-validator');
                var refvid = this.videoRef.current;
            }

            var faceMode = localStorage.getItem('faceMode');
            // if(faceMode == null){
            //     faceMode = 'user';
            // }
            if(this.state.query==='imgKtpWajah'){
                faceMode = 'user';
                this.setState({yourFaceMode:'user'})
            }
            else{
                faceMode = 'environment';
            }
            navigator.mediaDevices.getUserMedia({
                audio: false,
                video: {
                    facingMode: 'user',
                    height: window.innerHeight
                    // height: { ideal: 480 },
                    // width: { ideal: 640 }

                    //width: { min: 640, ideal: 640 },
                    //  height: { min: 480, ideal: 480 }
                    //aspectRatio: { ideal: 1.7777777778 }
                },
            })
                .then(stream => {
                    window.stream = stream;

                    this.videoRef.current.srcObject = stream;
                    return new Promise((resolve, reject) => {
                        this.videoRef.current.onloadedmetadata = () => {
                            resolve();
                        };
                    });
            })
        
        }
    }


render() {
    return (
          <div>

<div style={{position:'fixed',top:'2vh',right:'3vw'}}>
<svg width="4.26vw" height="2.39vh" viewBox="0 0 16 16" fill="none" xmlns="http://www.w3.org/2000/svg">
<path d="M9.48726 7.99996L15.8148 1.67242L14.3277 0.185303L8.00014 6.51284L1.67277 0.185462L0.185649 1.67258L6.51303 7.99996L0.185547 14.3274L1.67267 15.8146L8.00014 9.48708L14.3278 15.8147L15.8149 14.3276L9.48726 7.99996Z" fill="#0057E7"/>
</svg>
</div>

           <video
                className={this.state.yourFaceMode === 'environment' ?
                           "size video-custom-maker":
                           "size video-custom-maker"
                          }
                autoPlay={true}
                muted="muted"
                ref={this.videoRef}
                playsInline
                //style={{ transform: 'scaleX(-1,1)'}}
            />

<div className="div-value-white-canvas-down">

<div className="container-button">
  {/* <button onClick={this.userMode} className="faceUser userBtns">
                <i className="fa fa-user"></i>
    </button> */}
       
        <div style={{backgroundColor: 'rgba(240,237,237,1)', border:'none',marginBottom:'40%', width:'70px',height:'70px',
    display:'flex',justifyContent:'center',borderRadius:'50%',position:'relative'}} 
        onClick={
            this.state.fullScreenshoot === "yes" ?
                this.screenShoot :
                this.screenShootCanvas} 
       >
                    <div className ="sub-icon-div"></div>
        </div>

    {/* <button onClick={this.environmentMode}className="faceEnvironment environmentBtns">
     <i className="fa fa-street-view"></i>
    </button> */}
</div>

</div>



          </div>




          
        )
    }

}