export function validate(value, key) {
    //multi validator
    if (Array.isArray(key)) {
        let allPassed = true;
        key.map(k => {
            //recursive as single validator
            if (!validate(value, k)) {
                allPassed = false;
            }
        });
        return allPassed;
    }
    //single validatior
    else {
        if (key === 'required') {
            return required(value);
        }
        else if (['numeric', 'email', 'alphanumeric'].includes(key)) {
            return patternMatch(value, key);
        }
        else if (key.indexOf("minlength-") === 0) {
            return minLength(value, key.split("-")[1]);
        }
        else if (key.indexOf("maxlength-") === 0) {
            return maxLength(value, key.split("-")[1]);
        }
        else {
            return true;
        }
    }
}

function required(value) {
    let patternRegex = /\S+/
    if (Array.isArray(value)) {
        if (value.length < 1) {
            return false
        }
    }
    else {
        if (!patternRegex.test(value)) {
            return false
        }
    }
    return true;
}

function patternMatch(value, type) {
    let patternRegex = {
        numeric: /^\d+$/,
        email: /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/,
        alphanumeric: /^[a-z0-9]+$/
    }
    if (Array.isArray(value)) {
        if (value.length > 0) {
            value.length.map(val => {
                if (val && !patternRegex[type].test(val)) {
                    return false
                }
            })
        }
    }
    else {
        if (value && !patternRegex[type].test(value)) {
            return false
        }
    }
    return true;
}

function minLength(value, amount) {
    if (value && value.length > 0 && value.length < amount) {
        return false;
    }
    return true;
}

function maxLength(value, amount) {
    if (value && value.length > 0 && value.length > amount) {
        return false;
    }
    return true;
}