const activeLang = localStorage.getItem('lang') || navigator.language || navigator.userLanguage;
const lang = () => {
    try {
        return require('../assets/lang/' + activeLang.toLowerCase() + '.json');
    } catch (err) {
        try {
            return {};
        } catch (err) {
            return {};
        }
    }
};

function writeObj(string, obj) {
    const params = string.match(/\[(.*?)\]/g);
    try {
        params.forEach(param => {
            let value = obj[param.substring(1, param.length - 1)];
            if (value === undefined) {
                value = "";
            }
            string = string.replace(param, value);
        });
    } catch (err) {
        console.log("LANG ERROR", err);
    }
    return string;
}

export function t(key, obj) {
    return lang() ? lang()[key] ? obj ? writeObj(lang()[key], obj) : lang()[key] : obj ? writeObj(key, obj) : key : key;
}