import config from 'config';

export const connection = {
    registrasi: {
        getLokasiUsaha: {
            method: 'GET',
            url: function () {
                return `${config.apiReg}/usahaProperties/lokasiPermanenUsaha`;
            }
        },
        getLokasiUsahaNon: {
            method: 'GET',
            url: function () {
                return `${config.apiReg}/usahaProperties/lokasiNonPermanenUsaha`;
            }
        },
        getJenisUsaha: {
            method: 'GET',
            url: function () {
                return `${config.apiReg}/usahaProperties/jenisUsaha`;
            }
        },
        getOmsetUsaha: {
            method: 'GET',
            url: function () {
                return `${config.apiReg}/usahaProperties/omsetUsaha`;
            }
        },
        validateInfoRekening: {
            method: 'PUT',
            url: function () {
                return `${config.apiReg}/UserOob/validasiInformasiRekening`;
            }
        },
        sendOtp: {
            method: 'POST',
            url: function () {
                return `${config.apiReg}/otpCtl/sendMyOTp/forRegistration`;
            }
        },
        validateOtp: {
            method: 'PUT',
            url: function () {
                return `${config.apiReg}/otpCtl/validateMyOtp/forRegistration`;
            }
        }
    }
}