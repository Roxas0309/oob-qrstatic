import React from 'react';

class SampleComponent extends React.PureComponent {
  constructor(props) {
    super(props);
  }

  render() {
    return (
        <div>
            Sample Component
        </div>
    );
  }
}

export default SampleComponent;