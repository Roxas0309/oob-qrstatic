import { Link } from '@material-ui/core';
import React from 'react';
import ArrowBackIcon from '@material-ui/icons/ArrowBack';
import '../master/HeaderInfo.scss';
export default class HeaderInfoPencairan extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div>
                <div style={{
                    display: 'flex', minWidth: '100%',
                    marginLeft: '0px',
                     marginTop: '10px',
                      marginBottom: '4.072vw'
                }}>
                    <div className="header-bg-blue-riwayat">
                        <div className="incubator-word">
                            <Link href={this.props.urlBack} >
                                <ArrowBackIcon style={{ left: '4vw', bottom : '7vw',
                                position : 'absolute',
                                fontSize: '5.62vw', color: '#FFFFFF' }} />
                            </Link>
                            <p>
                            {this.props.headerName}
                            </p>
                        </div>
                    </div>
                </div>

            </div>
        );
    }
}