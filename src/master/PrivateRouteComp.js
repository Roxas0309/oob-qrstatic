import React from 'react';
import { createBrowserHistory } from 'history';
import { Redirect, Route } from 'react-router-dom';
// import LoadingComponent from '../Master/LoadingLib/LoadingComponent';
import Async from 'react-async';
import { BACK_END_POINT, HEADER_AUTH, POST, TOKEN_AUTH, wsCallingNoBody, SESSION_ITEM } from './masterComponent';
import LoadingMaker from '../helpers/loadingMaker/LoadingMaker';
const loadConfig = () =>
fetch(BACK_END_POINT+"/loginCtl/refresh", {
    headers: { ...HEADER_AUTH , 'secret-token' : localStorage.getItem('token-auth'),
    'session-item' : localStorage.getItem(SESSION_ITEM)},
    method: POST
})
    .then(response => response.json())

    // wsCallingNoBody(BACK_END_POINT+"/loginCtl/refresh", POST,
    //     { ...HEADER_AUTH , 'secret-token' : localStorage.getItem('token-auth')})

var scope = {
    splitterStyle: {
        height: 100
    }
};

const history = createBrowserHistory();
export default function PrivateRouteComp(props){
        return (
                <Async promiseFn={loadConfig}>
                    {({ data, isLoading }) => {
                        if (isLoading) {
                            return <LoadingMaker onLoading={true}
                                    ></LoadingMaker>
                            //return <div>Sedang Loading</div>
                        }
                        else {
                            if (data == undefined) {
                                history.push('/')
                                location.reload();
                                return <Redirect to="/" />
                            }
                            else if (data.status!==200) {
                                history.push('/login')
                                location.reload();
                                return <Redirect to='/login' />
                            }
                            else {
                                localStorage.setItem(TOKEN_AUTH,data.result)
                                    return <Route exact path={props.path}
                                        component={props.component} />
                            }
                        }
                    }
                    }

                </Async>
        )
} 