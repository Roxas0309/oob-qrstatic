import React, { Component } from "react";
import axios from 'axios';
export default class masterComponent extends Component{}

export const removeAllValueButAuth = () =>{
        var keys = Object.keys(localStorage);
        keys.map((value)=>{
            if(value !== TOKEN_AUTH && value !=='token-info' && value !== SESSION_ITEM)
            localStorage.removeItem(value);
        })
   
}


export var wsCallingNoBody = (url, method, headers) =>
    fetch(url, {
        headers: headers || {},
        method: method
    })
        .then(response => response.json())


export var wsCallingWithBody = (url, body, method, headers) =>
    fetch(url, {
        headers: headers,
        body: body,
        method: method
    })
        //.then(res => (res.ok ? res : Promise.reject(res)))
        .then(response => response.json())

export var wsWithoutBody = (url, method, headers) => {
    var head = {
        headers: headers
    }
    switch (method) {
        case GET:
            return axios.get(url, head);
        case DELETE:
            return axios.delete(url, head);
        case PUT:
            return axios.put(url, head);
        case POST:
            return axios.post(url, head);
    }

}

export var wsWithBody = (url, method, body, headers) => {
    var head = {
        headers: headers
    }

    switch (method) {
        case POST:
            return axios.post(url, body, head);
        case PUT:
            return axios.put(url, body, head);
    }
}


export const HEADER_AUTH = {
    "secret-id" : "YRuYy-dSiaK-LLdWA",
    "secret-key" : "4Vbxc-uoGWA-LVYg5"
};
export const BACK_END_POINT = process.env.NODE_ENV === 
'development'?"/api":"/api";
// 'development'?"http://149.129.239.139:28080":"/api";

export const SESSION_ITEM = 'session-item';
export const TOKEN_AUTH = "token-auth";
export const TOKEN_COOKIES = 'token-cookies';
export const GET = 'GET';
export const DELETE = 'DELETE';
export const PUT = 'PUT';
export const POST = 'POST';
export const TIMES_OUT = '2000';
export const URL_SOCKET="/api/ws/";
export const SOCKET_TOPIC_TRX="/topic/transaction";
