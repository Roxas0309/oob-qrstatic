import React, { Component } from "react";
import { AppBar, Toolbar, Link, Typography, Grid, Paper, Button, ListItem, ListItemText, List, CardContent, ListItemSecondaryAction, IconButton, Box } from '@material-ui/core'
import ArrowBackIcon from '@material-ui/icons/ArrowBack';
import { createBrowserHistory } from "history";
import EventIcon from '@material-ui/icons/Event';
import moment from 'moment';
import ArrowForwardIosIcon from '@material-ui/icons/ArrowForwardIos';
import PilihPeriode from "./PilihPeriode";
import DetailTransaksi from "./DetailTransaksi";

const dataTran = [
    {
        viaPay: "OVO",
        viaKet: "Pembayaran via OVO (**82)",
        tgl: "2 September 2020",
        jam: "19:00",
        qty: 70000,
        biaya: 350
    },
    {
        viaPay: "Shopee",
        viaKet: "Pembayaran via Shopee (**72)",
        tgl: "3 September 2020",
        jam: "19:10",
        qty: 55000,
        biaya: 150
    },
    {
        viaPay: "Gopay",
        viaKet: "Pembayaran via Gopay (**88)",
        tgl: "4 September 2020",
        jam: "20:20",
        qty: 150000,
        biaya: 250
    },
    {
        viaPay: "Dana",
        viaKet: "Pembayaran via Dana (**60)",
        tgl: "5 September 2020",
        jam: "21:10",
        qty: 210000,
        biaya: 180
    },
    {
        viaPay: "LinkAja",
        viaKet: "Pembayaran via LinkAja (**01)",
        tgl: "6 September 2020",
        jam: "19:20",
        qty: 500000,
        biaya: 200
    }
  ]

const history = createBrowserHistory();
class historyTransaksi extends Component {


    constructor(props){
        super(props);

        this.state = {
            selectedIndex: 0,
            openPeriod: false,
            periode: false,
            starDate: null,
            endDate: null,
            openDetail: false
        }

        this.getDataPeriode = this.getDataPeriode.bind(this)
    }

    componentDidMount(){
        let dNow = moment(new Date()).format("DD");
        let dd = new Date;
        let de = dd.getDate() - dd.getDay()
        

        if(dNow){
            this.setState({selectedIndex: Number(dNow)-Number(de)})
        }
    }

    getDataPeriode(start, end) {
        const { handle } = this.props
        this.setState({
            periode: true,
            starDate: start,
            endDate: end
        })
    }

    openDetail = () => {
        this.setState({openDetail: true})
    }
    closeDetail= () => {
        this.setState({openDetail: false})
    }

    addListTran(){
        let nf = new Intl.NumberFormat();
        return dataTran.map((value, id) => {
            return <List component="nav" key={id} style={{width: '100%', borderTop: 'solid 1px #E5E8EA', 
                paddingTop: '0px', flexDirection: 'row', display: 'flex'
                }}>
                <ListItem button onClick={this.openDetail} style={{whiteSpace: 'nowrap'}}>
                    <ListItemText  
                        primary={<Typography 
                            style={{fontFamily: 'Nunito', fontSize: '14px', 
                            fontWeight: '400', lineHeight: '21px', 
                            color: '#000000'}}>{value.viaKet}</Typography>}
                        secondary={<Typography 
                            style={{fontFamily: 'Nunito', fontSize: '12px', 
                            fontWeight: '400', lineHeight: '14px', color: '#ADA6AE'}}>{value.jam}</Typography>}/>                                            
                </ListItem>
                <ListItem button onClick={this.openDetail} style={{textAlign: 'center'}}>
                        <ListItemText 
                        primary={<Typography 
                            style={{fontFamily: 'Nunito', fontSize: '16px', padding: '10px',
                            fontWeight: '700', lineHeight: '24px', color: '#011A2C'}}>
                                Rp {nf.format(value.qty)}</Typography>}
                        secondary={<Typography 
                            style={{fontFamily: 'Nunito', fontSize: '12px', 
                            fontWeight: '400', lineHeight: '14px', color: '#ADA6AE'}}>
                                Biaya (Rp {nf.format(value.biaya)})</Typography>}/>                                            
                </ListItem>
                <DetailTransaksi isShow={this.state.openDetail} isClose={this.closeDetail} dataBiaya={value.biaya} dataTgl={value.tgl} dataVia={value.viaPay} dataJam={value.jam}/>
            </List>

        })
    }

    render() {
        const { classes } = this.props
        let totalTran = 0
        
        const openDetail = () => {
            this.setState({openDetail: true})
        }

        if(Array.isArray(dataTran)){
           totalTran = dataTran.reduce((val, i) => val = val + i.qty, 0)
            
        }
        moment.locale('id');
        var curr = new Date;
        var d1 = curr.getDate() - curr.getDay();
        var d2 = d1 + 1;
        var d3 = d1 + 2;
        var d4 = d1 + 3;
        var d5 = d1 + 4;
        var d6 = d1 + 5;
        var d7 = d1 + 6;

        

        let minggu = moment(new Date(curr.setDate(d1)).toUTCString()).format("DD");
        let senen = moment(new Date(curr.setDate(d2)).toUTCString()).format("DD");
        let selasa = moment(new Date(curr.setDate(d3)).toUTCString()).format("DD");
        let rabu = moment(new Date(curr.setDate(d4)).toUTCString()).format("DD");
        let kamis = moment(new Date(curr.setDate(d5)).toUTCString()).format("DD");
        let jumat = moment(new Date(curr.setDate(d6)).toUTCString()).format("DD");
        let sabtu = moment(new Date(curr.setDate(d7)).toUTCString()).format("DD");

        const klik = (event, index) => {
            this.setState({selectedIndex: index})
        }

        const lihatPencarian = () => {

        }
        

        const openPeriod = () => {
            this.setState({openPeriod: true})
        }
        const closePeriode = () => {
            this.setState({openPeriod: false})
        }

        return (
            <div style={{overflowY: 'scroll'}}>
                <AppBar style={{backgroundColor: '#0057E7', position: 'fixed', boxShadow: 'unset'}}>
                    <Toolbar style={{minHeight: '50px'}}>
                        <Link href="/homeScreen">
                            <ArrowBackIcon style={{color:'white'}}/>
                        </Link>
                        <div style={{width: '100%', textAlign: 'center'}}>
                            <Typography style={{fontFamily: 'Nunito', fontSize: '16px',
                        fontWeight: '700', lineHeight: '24px'}}>Riwayat Transaksi</Typography>
                        </div>
                    </Toolbar>
                </AppBar>
                <div >
                    <Paper style={{height: '135px', position: 'fixed', width: '100vw', 
                        boxShadow: '0px 4px 10px rgba(0, 0, 0, 0.15)', top: '50px', zIndex: '2' }}>
                        <Grid container spacing={2} style={{marginTop: '5px'}}>
                            <Grid item xs style={{justifyContent: 'center', alignItems: 'center', 
                                    display: 'flex'}}>
                                <Typography style={{fontSize: '24px', fontWeight: '400', lineHeight: '33px'}}>
                                    Desember 2020</Typography>
                            </Grid>
                            <Grid item xs style={{justifyContent: 'center', alignItems: 'center', 
                                    display: 'flex', maxWidth: '40%'}}>
                                <Button onClick={openPeriod} style={{width: 'auto', height: '36px', fontSize: '14px', 
                                                fontFamily: 'TTInterfaces', textTransform: 'none',
                                                border: 'solid 1px #007ACF', color: '#007ACF'}}
                                    variant="outlined"
                                    startIcon={<EventIcon style={{color: '#007ACF'}}/>}
                                >
                                    Periode
                                </Button>
                            </Grid>
                        </Grid>
                        {/* -------view pilih periodee---------------- */}
                        <PilihPeriode isShow={this.state.openPeriod} isClose={closePeriode} handler={this.getDataPeriode}/>
                        {/* ------------------------------------------ */}
                        {this.state.periode ? 
                        <Box style={{display: 'flex', alignItems: 'center', justifyContent: 'center'}}>
                            <p style={{fontFamily: 'Nunito',fontSize: '16px', fontWeight: '700', 
                                                        lineHeight: '21.82px',color: '#007ACF'}}>
                                {moment(this.state.starDate).format("DD MMM")} - 
                                {moment(this.state.endDate).format("DD MMM")}
                            </p>
                        </Box>
                        :
                        <List className="listHist" style={{display: 'flex', flexDirection: 'row', justifyContent: 'center', alignItems: 'baseline', marginLeft: '10px', marginRight: '10px'}}>
                            <ListItem button selected={this.state.selectedIndex === 0} onClick={(event) => klik(event, 0)} 
                                    >
                                <ListItemText primary={<Typography 
                                                    style={{ fontFamily: 'Nunito',fontSize: '16px', fontWeight: '700', 
                                                    lineHeight: '21.8px', color: '#007ACF' }}>{minggu}</Typography>}
                                                secondary={<Typography 
                                                        style={{ fontFamily: 'Nunito',fontSize: '14px', fontWeight: '400', 
                                                        lineHeight: '21px',color: '#007ACF' }}>Min</Typography>}/>
                            </ListItem>
                            <ListItem button selected={this.state.selectedIndex === 1} onClick={(event) => klik(event, 1)}
                                    >
                                <ListItemText primary={<Typography 
                                                    style={{ fontFamily: 'Nunito',fontSize: '16px', fontWeight: '700', 
                                                    lineHeight: '21.8px', color: '#007ACF' }}>{senen}</Typography>}
                                                secondary={<Typography 
                                                        style={{ fontFamily: 'Nunito',fontSize: '14px', fontWeight: '400', 
                                                        lineHeight: '21px',color: '#007ACF' }}>Sen</Typography>}/>
                            </ListItem >
                            <ListItem button selected={this.state.selectedIndex === 2} onClick={(event) => klik(event, 2)}
                                >
                                <ListItemText primary={<Typography 
                                                    style={{ fontFamily: 'Nunito',fontSize: '16px', fontWeight: '700', 
                                                    lineHeight: '21.8px', color: '#007ACF' }}>{selasa}</Typography>}
                                                secondary={<Typography 
                                                        style={{ fontFamily: 'Nunito',fontSize: '14px', fontWeight: '400', 
                                                        lineHeight: '21px',color: '#007ACF' }}>Sel</Typography>}/>
                            </ListItem>
                            <ListItem button selected={this.state.selectedIndex === 3} onClick={(event) => klik(event, 3)} 
                                >
                                <ListItemText primary={<Typography 
                                                    style={{ fontFamily: 'Nunito',fontSize: '16px', fontWeight: '700', 
                                                    lineHeight: '21.8px', color: '#007ACF' }}>{rabu}</Typography>}
                                                secondary={<Typography 
                                                        style={{ fontFamily: 'Nunito',fontSize: '14px', fontWeight: '400', 
                                                        lineHeight: '21px',color: '#007ACF' }}>Rab</Typography>}/>
                            </ListItem>
                            <ListItem button selected={this.state.selectedIndex === 4} onClick={(event) => klik(event, 4)} 
                                >
                                <ListItemText primary={<Typography 
                                                    style={{ fontFamily: 'Nunito',fontSize: '16px', fontWeight: '700', 
                                                    lineHeight: '21.8px', color: '#007ACF' }}>{kamis}</Typography>}
                                                secondary={<Typography 
                                                        style={{ fontFamily: 'Nunito',fontSize: '14px', fontWeight: '400', 
                                                        lineHeight: '21px',color: '#007ACF' }}>Kam</Typography>}/>
                            </ListItem>
                            <ListItem button selected={this.state.selectedIndex === 5} onClick={(event) => klik(event, 5)} 
                                >
                                <ListItemText primary={<Typography 
                                                    style={{ fontFamily: 'Nunito',fontSize: '16px', fontWeight: '700', 
                                                    lineHeight: '21.8px', color: '#007ACF' }}>{jumat}</Typography>}
                                                secondary={<Typography 
                                                        style={{ fontFamily: 'Nunito',fontSize: '14px', fontWeight: '400', 
                                                        lineHeight: '21px',color: '#007ACF' }}>Jum</Typography>}/>
                            </ListItem>
                            <ListItem button selected={this.state.selectedIndex === 6} onClick={(event) => klik(event, 6)}
                                >
                                <ListItemText primary={<Typography 
                                                    style={{ fontFamily: 'Nunito',fontSize: '16px', fontWeight: '700', 
                                                    lineHeight: '21.8px', color: '#007ACF' }}>{sabtu}</Typography>}
                                                secondary={<Typography 
                                                        style={{ fontFamily: 'Nunito',fontSize: '14px', fontWeight: '400', 
                                                        lineHeight: '21px',color: '#007ACF' }}>Sab</Typography>}/>
                            </ListItem>
                        </List>
                        }
                    </Paper>
                    <Paper style={{position: 'absolute',  
                            marginTop: '185px', boxShadow: 'unset', marginBottom: '90px', overflowX: 'hidden', overflowY: 'hidden'}}>
                        <Grid item >
                            <CardContent style={{padding: '16px'}}>
                                <Typography style={{fontFamily: 'Nunito', fontSize: '12px', fontWeight: '700',
                                            lineHeight: '16.37px', color: '#1A1A1A'}}>
                                    TOTAL TRANSAKSI
                                </Typography>
                                <Typography style={{fontFamily: 'Montserrat', fontWeight: '700', 
                                            lineHeight: '29.26px', fontSize: '24px', color: '#011A2C', marginTop: '5px', marginBottom: '5px'}}>
                                    Rp 600.000
                                </Typography>
                                <Typography style={{fontFamily: 'Nunito', fontSize: '16px', lineHeight: '24px', 
                                            fontWeight: '400', color: '#615A5A'}}>
                                    Belum termasuk biaya admin
                                </Typography>
                                </CardContent>
                        </Grid>
                        {dataTran ?
                        <Grid container spacing={2} >
                        {this.addListTran()}
                            <Grid item style={{position: 'fixed', bottom:'0px', left: '0px', height: '72px', 
                                    width: '100vw',backgroundColor: '#FFFFFF', zIndex: '1',
                                    boxShadow: '0px 4px 10px rgba(0, 0, 0, 0.15)'}}>
                                <List style={{display: 'contents'}}>
                                    <ListItem button onClick={openDetail}>
                                        <ListItemText style={{color: '#007ACF', marginTop: '0px'}} 
                                            primary="Lihat Pencairan" secondary="Total yang sudah masuk rekening"/>
                                        <ListItemSecondaryAction>
                                            <IconButton style={{color: '#007ACF'}} edge="end" aria-label="delete">
                                                <ArrowForwardIosIcon />
                                            </IconButton>
                                        </ListItemSecondaryAction>
                                    </ListItem>
                                </List>
                            </Grid>
                                </Grid>
                               
                                :
                                <Grid item >
                                    <CardContent style={{padding: '16px'}}>
                                        <Typography style={{fontFamily: 'Nunito', fontSize: '12px', fontWeight: '700',
                                                    lineHeight: '16.37px', color: '#1A1A1A'}}>
                                            DATA TIDAK DITEMUKAN
                                        </Typography>
                                    </CardContent>
                                </Grid>

                    }

                    </Paper>

                    {/* // <Grid container spacing={2} key={id}>
                                //     <Grid item xs style={{borderTop: 'solid 1px #E5E8EA', paddingTop: '0px'}}>
                                        
                                //     </Grid>
                                //     <List style={{borderTop: 'solid 1px #E5E8EA', paddingTop: '0px'}}>
                                //             <ListItem button onClick={() => openDetail}>
                                //                 <ListItemText 
                                                    primary={<Typography 
                                                        style={{fontFamily: 'Nunito', fontSize: '14px', 
                                                        fontWeight: '400', lineHeight: '21px', 
                                                        color: '#000000'}}>{value.viaKet}</Typography>}
                                                    secondary={<Typography 
                                                        style={{fontFamily: 'Nunito', fontSize: '12px', 
                                                        fontWeight: '400', lineHeight: '14px', color: '#ADA6AE'}}>{value.jam}</Typography>}/>                                            
                                            </ListItem>
                                        </List>
                                
                                <Grid item style={{justifyContent: 'center', alignItems: 'center', 
                                        display: 'flex', maxWidth: '50%', paddingTop: '0px'}}>

                                        <List>
                                            <ListItem button onClick={openDetail}>
                                                <ListItemText 
                                                    primary={<Typography 
                                                        style={{fontFamily: 'Nunito', fontSize: '16px', padding: '10px',
                                                        fontWeight: '700', lineHeight: '24px', color: '#011A2C'}}>
                                                            Rp {nf.format(value.qty)}</Typography>}
                                                    secondary={<Typography 
                                                        style={{fontFamily: 'Nunito', fontSize: '12px', 
                                                        fontWeight: '400', lineHeight: '14px', color: '#ADA6AE'}}>
                                                            Biaya (Rp {nf.format(value.biaya)})</Typography>}/>                                            
                                            </ListItem>
                                        </List>
                                </Grid>
                                <Grid item style={{position: 'fixed', bottom:'0px', left: '0px', height: '72px', 
                                        width: '100vw',backgroundColor: '#FFFFFF', 
                                        boxShadow: '0px 4px 10px rgba(0, 0, 0, 0.15)'}}>
                                    <List style={{display: 'contents'}}>
                                        <ListItem button onClick={openDetail}>
                                            <ListItemText style={{color: '#007ACF', marginTop: '0px'}} 
                                                primary="Lihat Pencairan" secondary="Total yang sudah masuk rekening"/>
                                            <ListItemSecondaryAction>
                                                <IconButton onClick={() => lihatPencarian} style={{color: '#007ACF'}} edge="end" aria-label="delete">
                                                    <ArrowForwardIosIcon />
                                                </IconButton>
                                            </ListItemSecondaryAction>
                                        </ListItem>
                                    </List>
                                </Grid>
                                <DetailTransaksi isShow={this.state.openDetail} isClose={closeDetail} dataBiaya={value.biaya} dataTgl={value.tgl} dataVia={value.viaPay} dataJam={value.jam}/>
                                </Grid> */}
                    
                    
                </div>
            </div>
        )
    }
}
export default historyTransaksi
