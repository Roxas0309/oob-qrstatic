import React, { useEffect, useState } from 'react'
import { AppBar, Dialog, IconButton, Toolbar, Link, Box, Button, Grid, ListItem, ListItemText, List, Typography } from '@material-ui/core'
import CloseIcon from '@material-ui/icons/Close';
import ArrowForwardIosIcon from '@material-ui/icons/ArrowForwardIos';
import moment from 'moment';
import Calendar from 'react-calendar';
import classNames from "classnames";
import 'react-calendar/dist/Calendar.css';
import { date } from 'yup';
//moment.locale('id');

const PilihPeriode = (props) => {
    const{ isShow, isClose, handler } = props
    const [ warna, setWarna ] = useState(1);
    const [selIn, setSelIn] = useState(1);
    const [startDate, setStartDate] = useState(new Date());
    const [endDate, setEndDate] = useState(null);
    
    const classColor = classNames(
        warna === 1 ? "aktifOver" : "nonAktifOver"
      );

    const klik = (event, index) => {
        setSelIn(index)

        if(index === 1) {
            setWarna(1)
        }else{
            setWarna(2)
        }
        
    }

    const getDataPeriod = () => {
        handler(startDate, endDate)
        isClose()
        
        
    }

    return (
        <Dialog fullScreen role="presentation" tabIndex="-1" open={isShow}>
            <AppBar position="static" style={{backgroundColor: '#FFFF', boxShadow: 'unset'}}>
                <Toolbar style={{minHeight: '50px'}}>
                    <IconButton onClick={isClose} edge="end" style={{position: 'absolute', right: '0px', width: '80px'}}>
                        <CloseIcon style={{color:'#ADA6AE'}}/>
                    </IconButton>
                </Toolbar>
            </AppBar>
            <div>
                <Box style={{justifyContent: 'center', alignItems: 'center', 
                        textAlign: 'center', width: '100vw'}}>
                    <h4 style={{fontSize: '18px', color:'#2E2E2E', fontWeight: '700', 
                                    lineHeight: '22.5px', marginTop: '5px', fontFamily: 'TTInterfaces'}}>Pilih Periode</h4>
                    <p style={{marginLeft: '50px', marginRight: '50px', fontSize: '14px', color:'#615A5A', 
                                fontWeight: '500', lineHeight: '21px', marginTop: '10px', fontFamily: 'TTInterfaces'}}>
                        Transaksi yang ditampilkan adalah minimal 7 hari dan maksimal 90 hari</p>
                </Box>
            </div>

            <List className="listPeriod" style={{flexDirection: 'row', display: 'flex', justifyContent: 'center', 
                            alignItems: 'center'}}>
                <ListItem style={{display: 'flex', height: '60px', width: '140px', 
                                    border: '1px solid #C4C4C4', textAlign: 'center', borderRadius: '4px'}}
                            button selected={selIn === 1} onClick={(event) => klik(event, 1)}>
                    <ListItemText primary={<Typography 
                                    style={{fontSize: '12px', color:'#ADA6AE', fontWeight: '400', 
                                    lineHeight: '22px', margin: '0px', fontFamily: 'TTInterfaces'}}>Dari Tanggal</Typography>}
                                  secondary={<Typography className={selIn === 1 ? 'aktifOver' : 'nonAktifOver'}
                                    style={{fontSize: '16px', fontWeight: '500', 
                                    lineHeight: '22px', margin: '0px', fontFamily: 'TTInterfaces'}}>
                                        {moment(startDate).format("DD MMM YYYY")}
                                    </Typography>}/>
                </ListItem>

                <ArrowForwardIosIcon style={{width: '7.41px', height: '12px', 
                            marginLeft:'12px', marginRight: '12px',color:'#615A5A'}}/>

                <ListItem style={{display: 'flex', height: '60px', width: '140px', 
                                    border: '1px solid #C4C4C4',textAlign: 'center', borderRadius: '4px'}}
                            button selected={selIn === 2} onClick={(event) => klik(event, 2)}>
                    <ListItemText primary={<Typography 
                                    style={{fontSize: '12px', 
                                    color: '#ADA6AE', 
                                    fontWeight: '400', 
                                    lineHeight: '22px', margin: '0px', fontFamily: 'TTInterfaces'}}>Sampai Tanggal</Typography>}
                                  secondary={<Typography className={selIn === 2 ? 'aktifOver' : 'nonAktifOver'}
                                    style={{fontSize: '16px', fontWeight: '500', 
                                    lineHeight: '22px', margin: '0px', fontFamily: 'TTInterfaces'}}>
                                        {endDate ? moment(endDate).format("DD MMM YYYY") : 'Pilih Tanggal'}
                                        </Typography>}/>
                </ListItem>
            </List>
            <Box style={{display: 'flex', marginTop: '16px', justifyContent: 'center'}}>
            {selIn === 1 &&
                    
                <Box style={{display: 'flex', flexDirection: 'column'}}>
                    <Box style={{display: 'flex', flexDirection: 'row'}}>
                        <Button style={{position: 'absolute', left: '10px', color: 'blue', bottom: '20px'}}>Batal</Button>
                        <Button onClick={() => {handler(startDate, endDate); isClose()}} style={{position: 'absolute', right: '10px', color: 'blue', bottom: '20px'}}>Oke</Button>
                    </Box>
                    <Calendar
                    onChange={date => setStartDate(date)}
                    value={startDate}
                    /> 
                    
                </Box>
                }
            {selIn === 2 &&
                
                <Box style={{display: 'flex', flexDirection: 'column'}}>
                    <Box style={{display: 'flex', flexDirection: 'row'}}>
                        <Button style={{position: 'absolute', left: '10px', color: 'blue', bottom: '20px'}}>Batal</Button>
                        <Button onClick={() => {handler(startDate, endDate); isClose()}} style={{position: 'absolute', right: '10px', color: 'blue', bottom: '20px'}}>Oke</Button>
                    </Box>
                    <Calendar
                    onChange={(val) => setEndDate(val)}
                    value={endDate}
                    /> 
                     
                </Box>}
            </Box>

        </Dialog>
    )
}

export default PilihPeriode
