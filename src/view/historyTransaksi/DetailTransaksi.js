import React, { useEffect, useState } from 'react'
import { AppBar, Dialog, IconButton, Toolbar, Link, Box, Button, Grid, ListItem, ListItemText, List, Typography, ListItemAvatar, Avatar, Collapse } from '@material-ui/core'
import CloseIcon from '@material-ui/icons/Close';
import moment from 'moment';
import logoTime from '../../../public/img/in-progress.png'
import ExpandLess from '@material-ui/icons/ExpandLess';
import ExpandMore from '@material-ui/icons/ExpandMore';

const DetailTransaksi = (props) => {
    const { dataBiaya, dataTgl, dataVia, dataJam, isClose, isShow} = props
    const [open, setOpen] = React.useState(false);
    const [openDet, setOpenDet] = React.useState(false);
    let nf = new Intl.NumberFormat();
    const handleClick = () => {
        setOpen(!open);
      };
    const handleClickDet = () => {
    setOpenDet(!openDet);
    };
    return (
        <Dialog fullScreen role="presentation" tabIndex="-1" open={isShow}>
            <AppBar style={{backgroundColor: '#FFFF', position: 'fixed', boxShadow: 'unset'}}>
                <Toolbar style={{minHeight: '50px'}}>
                    <div style={{width: '100%', textAlign: 'center'}}>
                        <Typography style={{fontFamily: 'Nunito', fontSize: '16px',
                    fontWeight: '700', lineHeight: '24px', color: 'black'}}>Rincian</Typography>
                    </div>
                    <IconButton onClick={isClose} edge="end" style={{position: 'absolute', right: '0px', width: '80px'}}>
                        <CloseIcon style={{color:'#ADA6AE'}}/>
                    </IconButton>
                </Toolbar>
            </AppBar>
            <Box style={{borderBottom: 'solid 1px #C4C4C4', display: 'flex', 
                    justifyContent: 'center', marginRight: '16px', marginLeft: '16px',}}>
                    <p style={{fontFamily: 'TTInterfaces', fontSize: '16px',
                    fontWeight: '400', lineHeight: '24px', marginTop: '60px'}}>{dataTgl} {dataJam}</p>
            </Box>
            <Box style={{borderBottom: 'solid 1px #C4C4C4', display: 'flex', 
                marginRight: '16px', marginLeft: '16px', justifyContent: 'center', marginTop: '16px', flexDirection: 'column', textAlign: 'center'}}>
                <p style={{fontFamily: 'TTInterfaces', fontSize: '16px',
                fontWeight: '400', lineHeight: '24px'}}>Pembayaran Berhasil Via {dataVia}</p>
                <Box style={{display: 'flex', justifyContent: 'center', textAlign: 'center', 
                    alignItems: 'center', height: '40px'}}>
                    <img src={logoTime} width={16} height={16}/>
                    <p style={{ fontFamily: 'TTInterfaces',fontSize: '14px', fontWeight: '400', 
                                lineHeight: '24px', color: 'rgba(142, 140, 140, 0.83)', marginLeft: '5px',
                                marginBottom: '0px', marginTop:'3px' }}>

                        Proses masuk rekening
                    </p>
                </Box>
                <List style={{marginBottom: '10px'}}>
                    <ListItem button onClick={handleClick} style={{backgroundColor: '#EAF6FF', 
                        width: 'auto'}}>
                        <ListItemText primary={<Typography 
                                            style={{fontFamily: 'TTInterfaces', fontSize: '16px', 
                                            fontWeight: '500', lineHeight: '24px', color: '#1A1A1A'}}>
                                                Total yang diterima</Typography>} />
                        <ListItemText primary={<Typography 
                                            style={{fontFamily: 'TTInterfaces', fontSize: '18px', 
                                            fontWeight: '700', lineHeight: '24px', color: '#1A1A1A', textAlign: 'right', marginRight: '10px'}}>
                                                Rp {nf.format(dataBiaya)}</Typography>} />
                            {open ? <ExpandLess /> : <ExpandMore />}
                    </ListItem>
                    <Collapse in={open} timeout="auto" unmountOnExit>
                        <List component="div" disablePadding>
                            <ListItem button style={{paddingLeft: '20px'}}>
                                <ListItemText primary="Expand Total yang diterima" />
                            </ListItem>
                        </List>
                    </Collapse>
                </List>

            </Box>
            <Box style={{borderBottom: 'solid 1px #C4C4C4', display: 'flex', flexDirection: 'row',
                    marginRight: '16px', marginLeft: '16px', height: '56'}}>
                <List style={{width: '100%', display: 'flex'}}>
                    <ListItem style={{width: '100%', paddingLeft: '0px', paddingRight: '0px'}}>
                        <ListItemText primary={<Typography 
                                                style={{fontFamily: 'TTInterfaces', fontSize: '16px',
                                                fontWeight: '400', lineHeight: '24px', color:'#615A5A'}}>
                                                    Nomor Refrensi</Typography>} />
                        <ListItemText primary={<Typography 
                                                style={{fontFamily: 'TTInterfaces', fontSize: '16px',
                                                fontWeight: '500', lineHeight: '24px', color:'#1A1A1A', textAlign: 'right'}}>
                                                    6517528163nxsjy</Typography>} />
                    </ListItem>
                </List>
            </Box>
            <Box style={{borderBottom: 'solid 1px #C4C4C4', display: 'flex', flexDirection: 'row',
                    justifyContent: 'center', marginRight: '16px', marginLeft: '16px', height: '56'}}>
                <List style={{width: '100%'}}>
                    <ListItem button onClick={handleClickDet} style={{width: '100%', paddingLeft: '0px', paddingRight: '0px'}}>
                        <ListItemText primary={<Typography 
                                            style={{fontFamily: 'TTInterfaces', fontSize: '16px', 
                                            fontWeight: '500', lineHeight: '22px', color: '#1A1A1A'}}>
                                                Transaksi Detail</Typography>} />
                        
                        {open ? <ExpandLess /> : <ExpandMore />}
                    </ListItem>
                <Collapse in={openDet} timeout="auto" unmountOnExit>
                    <List component="div" disablePadding>
                        <ListItem button style={{paddingLeft: '20px'}}>
                            <ListItemText primary="Expand Transaksi Detail" />
                        </ListItem>
                    </List>
                </Collapse>
            </List>
            </Box>
        </Dialog>
    )

}

export default DetailTransaksi
