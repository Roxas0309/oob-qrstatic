import React, { Component } from "react";
import Link from "@material-ui/core/Link";
import ArrowBackIcon from "@material-ui/icons/ArrowBack";
import { AppBar, Box, Button, Grid, Toolbar, Typography } from "@material-ui/core";
import Skeleton from "@material-ui/lab/Skeleton";
import logoInfoUsaha from "../../../public/img/Mask Group.png";

class informasiUsaha extends Component {

  componentWillUnmount(){
    location.reload()
  }

  render() {
    const { loading = false } = this.props;

    const lanjut = () => [
      this.props.history.push('/uploadInformasiUsaha', {from: '/informasiUsaha'})
    ]

    return (
      <div style={{ overflow: "hidden"}}>
        <AppBar style={{backgroundColor: 'transparent', position: 'absolute', 
                boxShadow: 'unset'}}>
            <Toolbar style={{minHeight: '60px'}}>
                <Link style={{position: 'absolute',
                            alignItems: 'center', display: 'flex'}} 
                  onClick={() => this.props.history.push('/uploadKartuOwner', {from: '/informasiUsaha'})}>
                    <ArrowBackIcon/>
                </Link>
                <div style={{width: '100%', textAlign: 'center'}}>
                    <Typography style={{fontFamily: 'Nunito', fontSize: '14px',
                fontWeight: '400', lineHeight: '21px', color: '#303B4A'}}>Langkah 3 dari 4</Typography>
                </div>
            </Toolbar>
        </AppBar>


        <Box
          style={{
            marginTop: "70px",
            marginInline: "16px",
          }}
        >
          <h4
            style={{
              textAlign: "center",
              color: "#121518",
              fontSize: "18px",
              lineHeight: "24px",
              fontWeight: "bold",
              fontFamily: "Montserrat",
              marginBottom: "0px",
            }}
          >
            Informasi Usaha
          </h4>
          <p
            style={{
              color: "#303B4A",
              marginTop: "0px",
              textAlign: "center",
              fontSize: "16px",
              padding: "10px",
              lineHeight: "24px",
              marginBottom: '0px'
            }}
          >
            Lengkapi data dan foto usaha Anda
          </p>
        </Box>
        <Grid
          container
          wrap="nowrap"
          style={{
            justifyContent: "center",
            alignItems: "center",
            display: "flex",
            marginTop: "50px",
          }}
        >
          {loading ? (
            <Skeleton animation="wave" variant="rect" height={268}></Skeleton>
          ) : (
            <Box
              style={{
                textAlign: "center",
                position: "fixed",
                display: "contents",
              }}
            >
              <img src={logoInfoUsaha} style={{ height: 268 }} />
            </Box>
          )}

          <Button onClick={lanjut}
              size="large"
              style={{width: "91%", marginTop: '10px', marginInline: '0px',
                            textTransform: 'none', marginBottom: '10px', fontFamily: 'Nunito', 
                                fontSize: '16px', fontWeight: '700'}}
              disableElevation
              variant="contained"
              color="primary"
            >
              Lanjutkan
            </Button>

        </Grid>

        {/* <Box
          style={{
            // position: "fixed",
            bottom: "20px",
            // marginInlineStart: "16px",
            // minWidth: "90%",
          }}
        >
          
        </Box> */}
      </div>
    );
  }
}

export default informasiUsaha;
