import React, { Component } from 'react'
import Link from '@material-ui/core/Link';
import ArrowBackIcon from '@material-ui/icons/ArrowBack';
import { AppBar, Backdrop, Box, Button, CircularProgress, Fade, Grid, TextField, Toolbar, Typography, withStyles } from '@material-ui/core';
import Skeleton from '@material-ui/lab/Skeleton';   
import CameraAltIcon from '@material-ui/icons/CameraAlt';
import EditIcon from '@material-ui/icons/Edit';
import * as Yup from 'yup';
import { ErrorMessage, Field, Form, Formik } from 'formik';
import ModalJenisUsaha from './ModalJenisUsaha';
import { compose } from 'recompose';
import sukse from '../../../public/img/success.gif';
import gagal from '../../../public/img/unapproved.gif';
import camera from '../../../public/img/Camera 1.png';
import ModalOmset from './ModalOmset';
import { BACK_END_POINT, GET, HEADER_AUTH, wsWithoutBody } from '../../master/masterComponent';
import ArrowDropDownIcon from '@material-ui/icons/ArrowDropDown';

const ITEM_HEIGHT = 48;
const ITEM_PADDING_TOP = 8;
const MenuProps = {
  PaperProps: {
    style: {
      maxHeight: ITEM_HEIGHT * 4.5 + ITEM_PADDING_TOP,
      width: 250,
    },
  },
};

  const useStyles = theme => ({
    errorImg: {
        border: '1px solid red',
    },
    suksesImg: {
        border: 'none',
    },
    placeholder: {
        height: 'auto',
        justifyContent: 'center',
        alignItems: 'center',
        display: 'flex'
    },
    backdrop: {
        zIndex: theme.zIndex.drawer + 1,
        color: '#fff',
      },
    paper: {
    backgroundColor: theme.palette.background.paper,
    border: '1px solid lightgray',
    boxShadow: theme.shadows[5],
    padding: theme.spacing(2, 4, 3),
    textAlign: 'center',
    borderRadius: '5px',
    alignItems: 'center',
    justifyContent: 'center',
    minWidth: '150px'

    },
  });
//const history = createBrowserHistory();
class uploadInformasiUsaha extends Component {

    timer = null

    constructor(props){
        super(props);

        this.state = { 
            tmptUsaha: null,
            brngJasa: null,
            owner: null,
            openJenis: false,
            openOmset: false,
            valJenis: "",
            valOmset: "",
            valName: "",
            valTlp:'',
            dataJenis: {},
            dataOmset: {},
            filter: '',
            loading: false,
            query: 'idle',
            proses: '',
            openAlert: false,
            isErrorTmptUsaha: false,
            isErrorBrgJasa: false,
            isErrorOwner: false,
            errorMsgTmptUsaha: "",
            errorMsgBrgJasa: "",
            errorMsgOwner: "",
            errorClassTmptUsaha: '',
            errorClassBrgJasa: '',
            errorClassOwner: '',
            regerxp: /^[0-9\b]+$/,
            alpha: /^[a-zA-Z_]+([a-zA-Z_]+)*$/,
            isValidFoto: false,
            maxNamaUsaha:100
        }

        this.openOmset = React.createRef();
        this.onchangeOptJenis = React.createRef();
        this.handleChangeJenis = React.createRef();
        this.handleChangeOmset = React.createRef();
        this.handlerModal = this.handlerModal.bind(this);
        this.handlerModalOmset = this.handlerModalOmset.bind(this)

    }

    componentDidMount(){
        this.setState({tmptUsaha: localStorage.getItem('tmptUsaha')})
        this.setState({brngJasa: localStorage.getItem('brngJasa')})
        this.setState({owner: localStorage.getItem('fotoOwner')})
        const newJenisUsaha = {}
        const newOmset = {}

        console.log('jenisUsaha: ',localStorage.getItem('jenisUsaha'))
        console.log('omset: ',localStorage.getItem('omset'))

        if(localStorage.getItem('tmptUsaha') && localStorage.getItem('brngJasa') &&
        localStorage.getItem('fotoOwner') && localStorage.getItem('jenisUsaha') &&
        localStorage.getItem('omset')){
            this.setState({isValidFoto: true})
        }
       
        wsWithoutBody(BACK_END_POINT+"/usahaProperties/jenisUsaha",GET,HEADER_AUTH).then(response =>{
           
            response.data.forEach((el, idx) =>{
                newJenisUsaha[idx+1] = {
                    jenisUsaha:el.nama,
                    idName:el.idName
               }
            })

        }).catch(error=>{
            alert(error.response.data.message);
        })

        wsWithoutBody(BACK_END_POINT+"/usahaProperties/omsetUsaha",GET,HEADER_AUTH).then(response =>{
           
            response.data.forEach((el, idx) => {
                newOmset[idx+1] = {
                     omset:el.nama,
                     idName:el.idName
                }
            });
        }).catch(error=>{
            alert(error.response.data.message);
        })
       

        this.setState({dataJenis: newJenisUsaha})
        this.setState({dataOmset: newOmset})

        if(localStorage.getItem('jenisUsaha') != undefined){
            this.setState({valJenis: localStorage.getItem('jenisUsaha')})
        }

        if(localStorage.getItem('omset') != undefined){
            this.setState({valOmset: localStorage.getItem('omset')})
        }

        

    }

    componentWillUnmount(){
        location.reload()
    }

    openDialog = (name) => {
        if(name === "jenisUsaha")
            this.setState({openJenis: true})
        else    
            this.setState({openOmset: true})
    }

    handleSubmit = (event) => {
        event.preventDefault();
    }

    handlerModal(jenisUsaha,idName) {
        this.setState({
          valJenis: jenisUsaha
        })
        localStorage.setItem('jenisUsaha', jenisUsaha)
        localStorage.setItem('jenisUsahaIdName', idName)

        if(localStorage.getItem('tmptUsaha') && localStorage.getItem('brngJasa') &&
        localStorage.getItem('fotoOwner') && localStorage.getItem('jenisUsaha') &&
        localStorage.getItem('omset')){
            this.setState({isValidFoto: true})
        }

      }
    handlerModalOmset(omset,idName) {
        this.setState({
          valOmset: omset
        })
        localStorage.setItem('omset', omset)
        localStorage.setItem('omsetIdName', idName)

        if(localStorage.getItem('tmptUsaha') && localStorage.getItem('brngJasa') &&
        localStorage.getItem('fotoOwner') && localStorage.getItem('jenisUsaha') &&
        localStorage.getItem('omset')){
            this.setState({isValidFoto: true})
        }
      }

    render() {
        const {loading = false, handler, classes} = this.props;

        this.onchangeOptJenis = () => {
            this.setState({openJenis: true})
        }

        const formTextField = (props) => {
            const {
                form: {setFieldValue, setFieldTouched},
                field: {name},
            } = props;

            const onChange = React.useCallback((event) => {
                const {value} = event.target;
             
                if(name == "namaUsaha"){
                    if(value.length<=(this.state.maxNamaUsaha+1)){ 
                        console.log("valuee: ", value);
                        localStorage.setItem("namaUsaha", value);
                        setFieldTouched(name, value ? true : false);
                        setFieldValue(name, value ? value : "");
                    }
                }else if(name == "noTlp"){
                    if(value.length<16){ 
                    if (value === "" || this.state.regerxp.test(value)) {
                        console.log("valuee: ", value);
                        localStorage.setItem("noTlp", value);
                        setFieldTouched(name, value ? true : false);
                        setFieldValue(name, value ? value : "");
                      }
                    }
                }else{
                   
                }

                
                
            },[setFieldValue, setFieldTouched, name])

            const onBlur = React.useCallback((event) => {
                const {value} = event.target;
                setFieldTouched(name, value ? false : true);
            },[setFieldValue, setFieldTouched, name])

            return <TextField {...props} name={name} onChange={onChange} onBlur={onBlur} />
        }

        const openOmset = () => {
            this.setState({openOmset: true})
        }
        const closOmset = () => {
            this.setState({openOmset: false})
        }
        const openJenis = () => {
            this.setState({openJenis: true})
        }
        const closJenis = () => {
            this.setState({openJenis: false})
        }
        const klikLanjut = () => {
            if( new URLSearchParams(this.props.location.search).get("_onCorrection")==="yes"){  
                this.props.history.push('/reviewData') 
            }
            else{
                this.props.history.push('/informasiUsaha', {from: '/uploadInformasiUsaha'})
            }

            location.reload();
        }
        
        const numb = /^[0-9\b]+$/;

        return (
            <div>
                {/* -----------------alert----------- */}
                <Backdrop className={classes.backdrop} open={this.state.openAlert} >
                    <div className={classes.placeholder}>

                    {
                        this.state.query === 'success' ? 
                                (<Box className={classes.paper}>
                                    <Typography component={'span'} variant={'body2'} >
                                        <span><h4 style={{color: 'black'}}>Berhasil terkirim</h4></span>
                                        <img src={sukse}
                                        style={{ height: 50, width: 50 }}></img>
                                    </Typography>
                                    {/* <Link>
                                        <Button onClick={klikLanjut} variant="contained" 
                                            style={{width: '100%', marginTop: '20px'}} color="primary" 
                                            disableElevation>
                                            Lanjut
                                        </Button>
                                    </Link> */}
                                </Box>
                                
                                ) :

                        this.state.query == 'failed' ? 
                             (<Typography component={'span'} variant={'body2'} className={classes.paper}>
                             <span><h4 style={{color: 'black'}}>Gagal terkirim</h4></span>
                             <img src={gagal} style={{ height: 50, width: 50 }}></img>
                             <span><h4 style={{color: 'black'}}>Jenis Usaha atau Omzet belum dipilih</h4></span>
                         </Typography>) :
                         (<Fade in={this.state.query === 'progress'}
                            style={{transitionDelay: this.state.query === 'progress' ? '500ms' : '0ms'}}
                            unmountOnExit
                            >
                                <div className={classes.paper}>
                                    <span><h4 style={{color: 'black'}}>
                                        Mengirim Data</h4></span>
                                    <CircularProgress />
                                </div>
                            </Fade>)
                        }

                    </div>
                </Backdrop>
                {/* --------------------------------- */}
                <AppBar style={{backgroundColor: 'transparent', position: 'absolute', 
                        boxShadow: 'unset'}}>
                    <Toolbar style={{minHeight: '60px'}}>
                        <Link style={{position: 'absolute',
                            alignItems: 'center', display: 'flex'}}
                            onClick={klikLanjut}>
                            <ArrowBackIcon/>
                        </Link>
                        <div style={{width: '100%', textAlign: 'center'}}>
                            <Typography style={{fontFamily: 'Nunito', fontSize: '14px',
                        fontWeight: '400', lineHeight: '21px', color: '#303B4A'}}>Langkah 3 dari 4</Typography>
                        </div>
                    </Toolbar>
                </AppBar>
                
                <Box style={{marginTop: '70px', marginBottom: '40px'}}>
                <h4 style={{fontFamily: 'Montserrat', fontSize: '18px', textAlign: 'center', marginTop: '0px',
                        fontWeight: '700', lineHeight: '27px', marginInline: '16px'}}>Informasi Usaha</h4>

                <Box className="classInfoUsaha" style={{marginInlineStart: '16px', marginInlineEnd: '16px'}}>
                    <Formik
                        initialValues={{
                            namaUsaha : localStorage.getItem('namaUsaha') ? localStorage.getItem('namaUsaha') : '',
                            
                            noTlp: localStorage.getItem('noTlp') ? localStorage.getItem('noTlp') : '',
                            jenisUsaha: localStorage.getItem('jenisUsaha') ? localStorage.getItem('jenisUsaha') : '',
                            omset: localStorage.getItem('omset') ? localStorage.getItem('omset') : ''
                        }}
                        validationSchema={Yup.object().shape({
                            namaUsaha: 
                            Yup.string().required('Harus diisi').min(3, 'Nama harus terdiri dari 3-'+this.state.maxNamaUsaha+' karakter')
                            .max(this.state.maxNamaUsaha, 'Nama harus terdiri dari 3-'+this.state.maxNamaUsaha+' karakter'),
                            // jenisUsaha:Yup.string().required('Harus diisi'),
                            // omset: Yup.string().required('Harus diisi')
                            noTlp: Yup.string()
                                .max(14, 'Nomor telepon tidak valid')
                                .matches(numb,{
                                    excludeEmptyString:true,
                                })
                        })}
                        onSubmit={({ namaUsaha, noTlp, jenisUsaha, omset }, actions) => {
                            
                            
                            if(this.state.tmptUsaha === null){
                                actions.setSubmitting(false)
                                this.setState({
                                    errorMsgTmptUsaha: "Foto Tempat Usaha Harus diisi",
                                    errorClassTmptUsaha: classes.errorImg, isErrorTmptUsaha: true
                                })
                                return false;
                            }else{
                                this.setState({
                                    errorMsgTmptUsaha: "",
                                    errorClassTmptUsaha: classes.suksesImg, isErrorTmptUsaha: false
                                })
                            }

                            if(this.state.brngJasa === null){
                                actions.setSubmitting(false)
                                this.setState({
                                    errorMsgBrgJasa: "Foto Barang atau Jasa Harus diisi",
                                    errorClassBrgJasa: classes.errorImg, isErrorBrgJasa: true
                                })
                                return false;
                            }else{
                                this.setState({
                                    errorMsgBrgJasa: "",
                                    errorClassBrgJasa: classes.suksesImg, isErrorBrgJasa: false
                                })
                            }

                            if(this.state.owner === null){
                                actions.setSubmitting(false)
                                this.setState({
                                    errorMsgOwner: "Foto Pemilik di Tempat Usaha Harus diisi",
                                    errorClassOwner: classes.errorImg, isErrorOwner: true
                                })
                                return false;
                            }else{
                                this.setState({
                                    errorMsgOwner: "",
                                    errorClassOwner: classes.suksesImg, isErrorOwner: false
                                })
                            }

                            this.setState({openAlert: true})
                            clearTimeout(this.timer);
                            if(this.state.query !== 'idle'){
                                this.setState({query: 'idle'});
                                return;
                            }
                    
                            this.setState({query: 'progress'});
                            this.timer = setTimeout(() => {
                
                                if((this.state.valJenis === "") || (this.state.valOmset === "")){
                                    
                                    this.setState({proses: 'failed'})
                                    this.setState({query: this.state.proses});
                                    this.timer = setTimeout(() => {
                                        this.setState({openAlert: false})
                                        this.setState({query: 'idle'});
                                    }, 5000)
                                    
                                    actions.setSubmitting(false);
                
                                }else{

                                    localStorage.setItem('usahaInfo_namaUsaha', namaUsaha)
                                    localStorage.setItem('usahaInfo_noTlp',
                                     noTlp)
                                    localStorage.setItem('usahaInfo_jenisUsaha', this.state.valJenis)
                                    localStorage.setItem('usahaInfo_omset', this.state.valOmset)
                                    this.setState({proses: 'success'})
                                    this.setState({query: this.state.proses});
                                    this.timer = setTimeout(() => {
                                        this.props.history.push('/informasiLokasiUsaha', {from: '/uploadInformasiUsaha'})
                                        location.reload();
                                    }, 3000)

                                    
                                   
                                }
                            }, 5000);

                        }}
                    >

                    {({ values, isValid, errors, touched, setFieldTouched, setFieldValue, isSubmitting }) => (
                        
                      <Form autoComplete="off">
                          <Box style={{marginBottom: '16px'}}>
                              <Field
                              autoComplete="off"
                              component={formTextField}
                              id="namaUsaha"
                              name={"namaUsaha"}
                              label="Nama Usaha"
                              onPaste={
                                (e)=>{
                                  e.preventDefault()
                                  return false;
                                }
                              }
                              variant="filled"
                              fullWidth
                              value={values.namaUsaha}
                              error ={(errors.namaUsaha && touched.namaUsaha)}
                              className={'form-control' + (errors.namaUsaha && touched.namaUsaha ? ' is-invalid' : '')}
                              />
                              <ErrorMessage name="namaUsaha" component="div" className="invalid-feedback" />
                          </Box>
                          <Box style={{marginBottom: '16px'}}>
                              <Field
                              autoComplete="off"
                              type="text"
                              component={formTextField}
                              id="noTlp"
                              name={"noTlp"}
                              onPaste={
                                (e)=>{
                                  e.preventDefault()
                                  return false;
                                }
                              }
                              label="Nomor Telepon (jika ada)"
                              variant="filled"
                              value={values.noTlp}
                              fullWidth
                              error ={(errors.noTlp && touched.noTlp)}
                              className={'form-control' + (errors.noTlp && touched.noTlp ? ' is-invalid' : '')}
                              />
                                <ErrorMessage name="noTlp" component="div" className="invalid-feedback" />
                          </Box>
                          <Box style={{marginBottom: '16px',display: 'inline-block', 
                                position: 'relative', width: '100%'}}>
                                    <ArrowDropDownIcon style={{position: 'absolute', right: 10, top: 15, width: 20, height: 20, zIndex: 2}}/>
                              <Field
                              component={formTextField}
                              onClick={openJenis}
                              id="jenisUsaha"
                              name={"jenisUsaha"}
                              label="Jenis Usaha"
                              variant="filled"
                              value={this.state.valJenis}
                              fullWidth
                              InputProps={{
                                readOnly: true,
                                }}
                              error ={(errors.jenisUsaha && touched.jenisUsaha)}
                              className={'form-control' + (errors.jenisUsaha && touched.jenisUsaha ? ' is-invalid' : '')}
                              />
                              <ErrorMessage name="jenisUsaha" component="div" className="invalid-feedback" />
                          </Box>
                          <ModalJenisUsaha isShowModal={this.state.openJenis} valueJenis={this.state.dataJenis}
                                toggleClose={closJenis} handler={this.handlerModal}/>
                         
                          
                          <Box style={{marginBottom: '32px',display: 'inline-block', 
                                position: 'relative', width: '100%'}}>
                                    {/* <ArrowDropDownIcon style={{position: 'absolute', right: 10, top: 30, width: 20, height: 20, zIndex: 2}}/> */}
                                    <ArrowDropDownIcon style={{position: 'absolute', right: 10, top: 15, width: 20, height: 20, zIndex: 2}}/>
                          <Field
                                component={formTextField}
                                onClick={openOmset}
                                label="Rata-rata Omzet per Bulan"
                                variant="filled"
                                value={this.state.valOmset}
                                name={"omset"}
                                // rows={2}
                                multiline
                                fullWidth
                                InputProps={{
                                    readOnly: true,
                                }}
                                error ={(errors.omset && touched.omset)}
                                className={'form-control' + (errors.omset && touched.omset ? ' is-invalid' : '')}
                              >
                            </Field>
                            <ModalOmset isShowModal={this.state.openOmset} valueOmset={this.state.dataOmset}
                                toggleCloseOmset={closOmset} handlerOmset={this.handlerModalOmset}/>
                          </Box>


                          <Grid container spacing={2} style={{marginTop: '32px'}}>

                            {this.state.isErrorTmptUsaha ? 
                                <Grid item xs>
                                    <span style={{fontSize: '16px', color: 'red', lineHeight: '24px', fontWeight: 'bold'}}>
                                    Foto Tempat Usaha harus diisi</span>
                                </Grid>
                                :
                                <Grid item xs={8}>
                                        <span style={{fontSize: '16px', color: '#3A4D5B', lineHeight: '24px', fontWeight: 'bold'}}>Foto Tempat Usaha</span>
                                    
                                </Grid>}

                            {/* <Grid item xs={8}>
                                {this.state.isErrorTmptUsaha ? 
                                    <span style={{fontSize: '16px', color: 'red', lineHeight: '24px', fontWeight: 'bold'}}>
                                            Foto Tempat Usaha harus diisi</span> :
                                    <span style={{fontSize: '16px', color: '#3A4D5B', lineHeight: '24px', fontWeight: 'bold'}}>Foto Tempat Usaha</span>
                                }
                            </Grid> */}
                            {this.state.tmptUsaha != null && 
                            <Grid item xs={4}>
                                <Link onClick={() => this.props.history.push('/uploadTempatUsaha', {from: '/uploadInformasiUsaha'})}
                                style={{display: 'flex', flexDirection: 'row', 
                                        justifyContent: 'flex-end', alignItems: 'center'}}>
                                 <svg style={{fontSize: "1rem", paddingRight: '3px'}} width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
<path d="M22.5402 6.06741C22.3163 5.24481 21.8214 4.72001 21.7246 4.62421L20.2975 3.19661C19.6845 2.58241 18.9356 2.26611 18.1459 2.26611C17.7574 2.26611 17.3591 2.34261 16.962 2.49791C16.1672 2.80921 15.6203 3.34131 15.5609 3.40181L4.12116 14.8373C3.95756 14.9998 3.84616 15.207 3.79926 15.433L2.67326 20.8426C2.59206 21.2331 2.71286 21.6382 2.99516 21.9193C3.21906 22.1442 3.52116 22.266 3.83156 22.266C3.91176 22.266 3.99306 22.2577 4.07226 22.241L9.48376 21.1153C9.70986 21.0685 9.91706 20.957 10.0797 20.7935L21.5194 9.35801C22.8308 8.04801 22.7225 6.73381 22.5402 6.06741ZM8.83796 19.207L4.88546 20.0291L5.70756 16.0794L14.6217 7.16841L17.7507 10.2973L8.83796 19.207ZM20.1054 7.94351L19.1652 8.88341L16.0363 5.75441L16.9752 4.81581C17.0597 4.73401 17.3531 4.49261 17.6902 4.36051C17.8505 4.29781 18.0039 4.26611 18.146 4.26611C18.4107 4.26611 18.6514 4.37831 18.8831 4.61051L20.3089 6.03661C20.311 6.03891 20.5207 6.26261 20.6112 6.59511C20.733 7.04081 20.5678 7.48161 20.1054 7.94351Z" fill="#0057E7"/>
</svg>
                                    <p style={{margin: '0px'}}>Ubah</p>
                                </Link>
                            </Grid>
                            }
                        </Grid>     

                        <Grid container wrap="nowrap" style={{justifyContent: 'center', alignItems: 'center', 
                            display: 'flex', marginTop: '0px'}}>
                        {loading ? (<Skeleton animation="wave" variant="rect" height={200}></Skeleton>) : 
                        (
                        this.state.tmptUsaha == null ? 
                        <Box className={this.state.errorClassTmptUsaha} style={{justifyContent: 'center', textAlign: 'center', alignItems: 'center', 
                                    minHeight: '200px', backgroundColor: '#e2f2ff', marginTop: '11px',
                                    width: '100%', borderRadius: '10px', display: 'flex'}}>
                            <Link onClick={() => this.props.history.push('/uploadTempatUsaha', {from: '/uploadInformasiUsaha'})}>
                                <img src={camera} />
                                <span><p style={{marginTop: '0px'}}>Ambil Foto</p></span>
                            </Link>
                        </Box>
                            :
                            <Box style={{justifyContent: 'center', textAlign: 'center', alignItems: 'center', 
                                        marginTop: '11px', minHeight: '200px', backgroundColor: '#e2f2ff', 
                                        width: '100%', borderRadius: '10px', display: 'flex'}}>
                                <img src={this.state.tmptUsaha} style={{height: '200px', width: '100%', minHeight: '200px', 
                                    //objectPosition: '50% 50%', transform: 'rotateY(180deg)',
                                    boxShadow: '0 2px 0 rgba(90,97,105,.11), 0 4px 8px rgba(90,97,105,.12), 0 10px 10px rgba(90,97,105,.06), 0 7px 70px rgba(90,97,105,.1)',
                                    borderRadius: '.625rem'}}/>
                            </Box>
                            
                            )}
                        </Grid>

                        <Grid container spacing={2} style={{marginTop: '32px'}}>
                        {this.state.isErrorBrgJasa ? 
                            <Grid item xs>
                                <span style={{fontSize: '16px', color: 'red', lineHeight: '24px', fontWeight: 'bold'}}>
                                    Foto Barang atau Jasa harus diisi</span>
                            </Grid>
                            :
                            <Grid item xs={8}>
                                    <span style={{fontSize: '16px', color: '#3A4D5B', lineHeight: '24px', fontWeight: 'bold'}}>Foto Barang atau Jasa</span>
                                
                            </Grid>}
                            {/* <Grid item xs={8}>
                                {this.state.isErrorBrgJasa ? 
                                    <span style={{fontSize: '16px', color: 'red', lineHeight: '24px', fontWeight: 'bold'}}>
                                            Foto Barang atau Jasa harus diisi</span> :
                                    <span style={{fontSize: '16px', color: '#3A4D5B', lineHeight: '24px', fontWeight: 'bold'}}>Foto Barang atau Jasa</span>
                                }
                            </Grid> */}
                            {this.state.brngJasa != null && 
                            <Grid item xs={4}>
                                <Link onClick={() => this.props.history.push('/uploadBarangJasa', {from: '/uploadInformasiUsaha'})}
                                style={{display: 'flex', flexDirection: 'row', 
                                        justifyContent: 'flex-end', alignItems: 'center'}}>
                                  <svg style={{fontSize: "1rem", paddingRight: '3px'}} width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
<path d="M22.5402 6.06741C22.3163 5.24481 21.8214 4.72001 21.7246 4.62421L20.2975 3.19661C19.6845 2.58241 18.9356 2.26611 18.1459 2.26611C17.7574 2.26611 17.3591 2.34261 16.962 2.49791C16.1672 2.80921 15.6203 3.34131 15.5609 3.40181L4.12116 14.8373C3.95756 14.9998 3.84616 15.207 3.79926 15.433L2.67326 20.8426C2.59206 21.2331 2.71286 21.6382 2.99516 21.9193C3.21906 22.1442 3.52116 22.266 3.83156 22.266C3.91176 22.266 3.99306 22.2577 4.07226 22.241L9.48376 21.1153C9.70986 21.0685 9.91706 20.957 10.0797 20.7935L21.5194 9.35801C22.8308 8.04801 22.7225 6.73381 22.5402 6.06741ZM8.83796 19.207L4.88546 20.0291L5.70756 16.0794L14.6217 7.16841L17.7507 10.2973L8.83796 19.207ZM20.1054 7.94351L19.1652 8.88341L16.0363 5.75441L16.9752 4.81581C17.0597 4.73401 17.3531 4.49261 17.6902 4.36051C17.8505 4.29781 18.0039 4.26611 18.146 4.26611C18.4107 4.26611 18.6514 4.37831 18.8831 4.61051L20.3089 6.03661C20.311 6.03891 20.5207 6.26261 20.6112 6.59511C20.733 7.04081 20.5678 7.48161 20.1054 7.94351Z" fill="#0057E7"/>
</svg>   <p style={{margin: '0px'}}>Ubah</p>
                                </Link>
                            </Grid>
                            }
                        </Grid>
                        <Grid container wrap="nowrap" style={{justifyContent: 'center', alignItems: 'center', 
                                display: 'flex', marginTop: '0px'}}>
                            {loading ? (<Skeleton animation="wave" variant="rect" height={200}></Skeleton>) : 
                            (
                                this.state.brngJasa == null ? <Box className={this.state.errorClassBrgJasa} style={{justifyContent: 'center', textAlign: 'center', alignItems: 'center', 
                                minHeight: '200px', backgroundColor: '#e2f2ff', marginTop: '11px',
                                width: '100%', borderRadius: '10px', display: 'flex'}}>
                                    <Link onClick={() => this.props.history.push('/uploadBarangJasa', {from: '/uploadInformasiUsaha'})}>
                                        {/* <CameraAltIcon /> */}
                                        <img src={camera} />
                                        <span><p style={{marginTop: '0px'}}>Ambil Foto</p></span>
                                    </Link>
                                </Box>
                                :
                                <Box style={{justifyContent: 'center', textAlign: 'center', alignItems: 'center', 
                                minHeight: '200px', backgroundColor: '#e2f2ff', marginTop: '11px', 
                                width: '100%', borderRadius: '10px', display: 'flex'}}>
                                    <img src={this.state.brngJasa} style={{height: '200px', width: '100%',  minHeight: '200px',
                                    //objectPosition: '50% 50%', transform: 'rotateY(180deg)',
                                    boxShadow: '0 2px 0 rgba(90,97,105,.11), 0 4px 8px rgba(90,97,105,.12), 0 10px 10px rgba(90,97,105,.06), 0 7px 70px rgba(90,97,105,.1)',
                                    borderRadius: '.625rem'}}/>
                                </Box>
                            )}
                        </Grid>

                        <Grid container spacing={2} style={{marginTop: '32px'}}>
                        {this.state.isErrorOwner ? 
                        <Grid item xs>
                            <span style={{fontSize: '16px', color: 'red', lineHeight: '24px', fontWeight: 'bold'}}>
                                            Foto Pemilik di Tempat Usaha Harus diisi</span>
                        </Grid>
                        :
                            <Grid item xs={8}>
                                    <span style={{fontSize: '16px', color: '#3A4D5B', lineHeight: '24px', fontWeight: 'bold'}}>Foto Pemilik di Tempat Usaha</span>
                                
                            </Grid>}
                            {this.state.owner != null && 
                            <Grid item xs={4}>
                                <Link onClick={() => this.props.history.push('/uploadFotoOwner', {from: '/uploadInformasiUsaha'})}
                                    style={{display: 'flex', flexDirection: 'row', 
                                        justifyContent: 'flex-end', alignItems: 'center'}}>
                                  <svg style={{fontSize: "1rem", paddingRight: '3px'}} width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
<path d="M22.5402 6.06741C22.3163 5.24481 21.8214 4.72001 21.7246 4.62421L20.2975 3.19661C19.6845 2.58241 18.9356 2.26611 18.1459 2.26611C17.7574 2.26611 17.3591 2.34261 16.962 2.49791C16.1672 2.80921 15.6203 3.34131 15.5609 3.40181L4.12116 14.8373C3.95756 14.9998 3.84616 15.207 3.79926 15.433L2.67326 20.8426C2.59206 21.2331 2.71286 21.6382 2.99516 21.9193C3.21906 22.1442 3.52116 22.266 3.83156 22.266C3.91176 22.266 3.99306 22.2577 4.07226 22.241L9.48376 21.1153C9.70986 21.0685 9.91706 20.957 10.0797 20.7935L21.5194 9.35801C22.8308 8.04801 22.7225 6.73381 22.5402 6.06741ZM8.83796 19.207L4.88546 20.0291L5.70756 16.0794L14.6217 7.16841L17.7507 10.2973L8.83796 19.207ZM20.1054 7.94351L19.1652 8.88341L16.0363 5.75441L16.9752 4.81581C17.0597 4.73401 17.3531 4.49261 17.6902 4.36051C17.8505 4.29781 18.0039 4.26611 18.146 4.26611C18.4107 4.26611 18.6514 4.37831 18.8831 4.61051L20.3089 6.03661C20.311 6.03891 20.5207 6.26261 20.6112 6.59511C20.733 7.04081 20.5678 7.48161 20.1054 7.94351Z" fill="#0057E7"/>
</svg> <p style={{margin: '0px'}}>Ubah</p>
                                </Link>
                            </Grid>
                            }
                        </Grid>

                        <Grid container wrap="nowrap" style={{justifyContent: 'center', alignItems: 'center', 
                                display: 'flex', marginTop: '0px'}}>
                            {loading ? (<Skeleton animation="wave" variant="rect" height={200}></Skeleton>) : 
                            (
                                this.state.owner == null ? <Box className={this.state.errorClassOwner} style={{justifyContent: 'center', textAlign: 'center', alignItems: 'center', 
                                marginTop: '11px', minHeight: '200px', backgroundColor: '#e2f2ff', 
                                width: '100%', borderRadius: '10px', display: 'flex'}}>
                                    <Link onClick={() => this.props.history.push('/uploadFotoOwner', {from: '/uploadInformasiUsaha'})}>
                                        {/* <CameraAltIcon /> */}
                                        <img src={camera} />
                                        <span><p style={{marginTop: '0px'}}>Ambil Foto</p></span>
                                    </Link>
                                </Box>
                                :
                                <Box style={{justifyContent: 'center', textAlign: 'center', alignItems: 'center', 
                                        marginTop: '11px', minHeight: '200px', backgroundColor: '#e2f2ff', 
                                        width: '100%', borderRadius: '10px', display: 'flex'}}>
                                <img src={this.state.owner} style={{height: '200px', width: '100%', 
                                   // objectPosition: '50% 50%', transform: 'rotateY(180deg)',
                                    boxShadow: '0 2px 0 rgba(90,97,105,.11), 0 4px 8px rgba(90,97,105,.12), 0 10px 10px rgba(90,97,105,.06), 0 7px 70px rgba(90,97,105,.1)',
                                    borderRadius: '.625rem'}}/>
                            </Box>
                            )}
                        </Grid>

                        {/* <Box style={{position: 'relative', bottom: '20px', 
                                    marginInlineStart: '20px', minWidth: '90%', marginInlineEnd: '30px',
                                    marginTop: '50px'}}>
                                <Button variant="contained" disableElevation disabled={isSubmitting} type="submit" size="large" style={{width: "100%", marginTop: '10px', 
                                        textTransform: 'none', marginBottom: '10px'}} color="primary">
                                    Lanjutkan
                                </Button>
                            
                        </Box> */}
                        {console.log('isValid', isValid)}
                        {console.log('isValue', values)}
                        <Button style={{position: 'relative', width: "100%", marginTop: '70px', marginInline: '0px',
                            textTransform: 'none', marginBottom: '32px', fontFamily: 'Nunito', 
                                fontSize: '16px', fontWeight: '700'}}
                            type="submit" variant="contained" disableElevation 
                            disabled={isSubmitting || !isValid || !this.state.isValidFoto}  size="large" 
                            color="primary">
                                Lanjut
                            </Button>

                        {/* <Box style={{marginInlineEnd: '20px', marginTop: '44px', marginBottom: '32px', position: 'absolute', width: '100%'}}>
                            <Button disabled={this.state.disBut} 
                                    style={{position: 'absolute', position: 'absolute', bottom: 'unset'}}
                                    disabled={isSubmitting} type="submit" variant="contained" disableElevation size="large" color="primary">
                                    Kirim
                            </Button>
                        </Box> */}


                          
                      </Form>
                    )}


                    </Formik>

                </Box>
                </Box>
                
                
            </div>
        )
    }
}

export default compose(
    withStyles(useStyles)
)(uploadInformasiUsaha)
