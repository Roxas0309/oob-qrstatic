import React from 'react';
import { AppBar, Box, Dialog, FormControl, IconButton, InputAdornment, InputLabel, List, ListItem, ListItemText, OutlinedInput, Toolbar, Typography } from '@material-ui/core';
import CloseIcon from '@material-ui/icons/Close';
import SearchIcon from '@material-ui/icons/Search';

function ModalJenisUsaha(props) {
    const { isShowModal, toggleClose, handler, valueJenis } = props
    const [filter, setFilter] = React.useState("");

    const handleSearchChange = (e) => {
        setFilter(e.target.value)
      };

      

    const dataJenis = (id) => {
    const { jenisUsaha } = valueJenis[id]
    const { idName } = valueJenis[id]
    return (
        <List className="listModal" key={id}>
                <ListItem button type="button" value={{jenisUsaha:jenisUsaha,idName:idName}} 
                    onClick={() => {handler(jenisUsaha,idName); toggleClose()}} >
                    <ListItemText value={jenisUsaha} primary={jenisUsaha}/>
            </ListItem> 
                
        </List>
        )
    }

    const closeJenis = () => {
        return toggleClose, setFilter("")
    }

    return (
        <Dialog fullScreen onClose={toggleClose} open={isShowModal}>

                <AppBar style={{backgroundColor: 'transparent', position: 'relative', 
                        boxShadow: 'unset'}}>
                    <Toolbar style={{minHeight: '100px'}}>
                        <div style={{width: '100%', textAlign: 'center'}}>
                            <Typography style={{fontSize: '16px', lineHeight: '24px', fontWeight: '700', 
                            marginTop: '10px', color: '#1A1A1A'}}>Jenis Usaha</Typography>
                        </div>
                        <IconButton onClick={toggleClose} edge="start" color="inherit" aria-label="close"
                            style={{position:'absolute', width: '20px', right: '20px', zIndex: 1, top: '30px', color: '#ADA6AE'}}>
                            <CloseIcon />
                        </IconButton>
                        
                    </Toolbar>
                </AppBar>
                {/* <IconButton onClick={toggleClose} edge="start" color="inherit" aria-label="close"
                    style={{position:'absolute', width: '20px', right: '20px', zIndex: 1, top: '10px', color: '#ADA6AE'}}>
                    <CloseIcon />
                </IconButton>
                <Box style={{height: '70px', justifyContent: 'center', alignItems: 'center', 
                    display: 'flex', marginBottom: '20px'}}>
                    <p style={{fontSize: '16px', lineHeight: '24px', fontWeight: '700', 
                        marginTop: '40px', marginBottom: '40px'}}>Jenis Usaha</p>
                </Box> */}
            
            {/* <FormControl variant="outlined" style={{margin: '10px'}}>
                <InputLabel htmlFor="outlined-adornment">Jenis Usaha</InputLabel>
                <OutlinedInput
                    id="outlined-adornment"
                    placeholder="Cari Jenis Usaha"
                    label="Jenis Usaha"
                    autoFocus={true}
                    value={filter}
                    onChange={handleSearchChange}
                    startAdornment={<InputAdornment position="start"><SearchIcon /></InputAdornment>}
                    labelWidth={60}
                />
            </FormControl> */}

            {valueJenis ? 
                (Object.keys(valueJenis).map((id) => valueJenis[id].jenisUsaha.includes(filter) &&
                    dataJenis(id))):
                    <List>
                    <ListItem>
                    <ListItemText primary="Data Kosong"/>
                </ListItem> 
            </List>
            }
        </Dialog>
    )
}

export default ModalJenisUsaha
