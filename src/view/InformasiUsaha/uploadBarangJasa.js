import React, { Component } from 'react'
import ArrowBackIcon from '@material-ui/icons/ArrowBack';
import { AppBar, Box, Button, Grid, Link, Toolbar} from '@material-ui/core';
import { withStyles } from '@material-ui/core/styles';
import Skeleton from '@material-ui/lab/Skeleton';
import { compose } from 'recompose';
import BrngJasa from  '../../../public/img/iklan/foto-barang-jasa-new.png';

const useStyles = theme => ({
    appBar: {
      position: 'relative',
      height: '40px',
      justifyContent: 'center'
    },
    title: {
      flex: 1,
      fontSize: '15px',
      textAlign: 'center',
    },
    formControl: {
      margin: theme.spacing(1),
      minWidth: 50,
      position: 'relative',
      zIndex: '1'
    }

  });


class uploadBarangJasa extends Component {

    constructor(props){
        super(props);

        this.state = {
            dataUri: '',
            openDialog: false,
            cropImg: '',
            open: false,
            cam: '',
            cropper: null,
            dataCropper: "#",
            disableBtn: true
        }

        this.takePhoto = React.createRef();
        this.handleOpen = React.createRef();
        this.handleClose = React.createRef();
        this.handleSlect = React.createRef();
        this.handleCloseSub = React.createRef();
        this.handleOpenSub = React.createRef();
        this.saveLocalStorage = React.createRef();

        this.getDataCrop = React.createRef();
        this.cropUlang = React.createRef();
        this.cropChange = React.createRef();
        
        

    }

    componentWillUnmount(){
      location.reload()
    }

    

    render() {
        const {loading = false, classes} = this.props;
        const isFullscreen = false;

        this.saveLocalStorage = () => {
          localStorage.setItem('brngJasa', this.state.dataUri);
          this.props.history.push('/uploadKartuOwner', {from: '/uploadBarangJasa'})

        }

        this.handleSlect = (event) => {
          this.setState({cam: event.target.value})
        }

        this.getDataCrop = () => {
          if (typeof this.state.cropper.getCroppedCanvas() !== undefined) {
              this.setState({dataCropper: this.state.cropper.getCroppedCanvas().toDataURL(), disableBtn: false})

          }
          
        }

        this.cropChange = (crop) => {
          this.setState({dataCropper: "#", 
          disableBtn: false, cropper: crop})
          
        }

        this.fotoUlang = () => {
          this.setState({dataCropper: "#", 
          disableBtn: true, open: false, openDialog: true })
          localStorage.removeItem('brngJasa')
          
        }

        this.takePhoto = (data) => {
            this.setState({dataUri: data, openDialog: true, open: false});
        }
        
        this.handleClose = () => {
            this.setState({open: false})
        }
    
        this.handleOpen = () => {
            this.setState({open: true})
        }

        this.handleCloseSub = () => {
          this.setState({openDialog: false, dataCropper: "#"})
      }
  
      this.handleOpenSub = () => {
          this.setState({openDialog: true})
      }
        return (
            <div style={{paddingInline: '16px',overflow: 'hidden'}}>
                <AppBar style={{backgroundColor: 'transparent', position: 'absolute', 
                    boxShadow: 'unset'}}>
                    <Toolbar onClick={() => this.props.history.push('/uploadInformasiUsaha', {from: '/uploadBarangJasa'})} 
                      style={{minHeight: '60px'}}>
                        <Link style={{position: 'absolute'}} >
                            <ArrowBackIcon/>
                        </Link>
                    </Toolbar>
                </AppBar>
                <h4 style={{textAlign: 'center', color: '#121518', marginTop: '70px', fontSize: '18px', 
                    lineHeight: '27px', fontWeight: 'bold', fontFamily: 'Montserrat', marginBottom: '40px'}}>Foto Barang atau Jasa</h4>
                <Grid container wrap="nowrap" style={{justifyContent: 'center', alignItems: 'center', 
                        display: 'flex', marginTop: '30px'}}>
                   
                    {loading ? (<Skeleton animation="wave" variant="rect" height={110}></Skeleton>) : 
                    (<Box style={{textAlign: 'center', position: 'fixed', display: 'contents'}}>
                        <img src={BrngJasa} style={{ height: 110 }} />
                    </Box>)}

                </Grid>
                <Box style={{marginTop: '40px'}}>
                    <ul className="fa-ul" style={{
                        fontSize: 'large',
                        letterSpacing: '0.2px',
                        lineHeight: 1.5,
                        marginRight: '5px'

                    }}>
                        <li style={{fontSize: '8px', color: '#6B7984', lineHeight: '24px', marginBottom: '9px'}}><span className="fa-li"><i className="fa fa-circle"></i></span>                        <p style={{fontSize:"16px",marginLeft:'14px'}}>
Pastikan hasil foto jelas.</p></li>
                    </ul>
                    <Button size="large"  style={{
                          marginTop: "10px",
                          textTransform: "none",
                          marginBottom: "10px",
                          marginLeft:"4vw",
                          fontFamily: "Nunito",
                          fontSize: "16px",
                          fontWeight: "700",
                          alignItems:'center',
                          width: '91%',
                          marginInline: '0px'
                        }} disableElevation variant="contained" color="primary"
                            onClick={this.getMyPhotoPlease}>
                        {/* <CameraAltIcon style={{marginRight: '5px'}}/> */}
                          Ambil Foto
                    </Button>
                </Box>
                
            </div>
        )
    }

getMyPhotoPlease = () => {
  this.props.history.push({pathname : '/imageMaker',search : "?_query=brngJasa&_canvasNumber=1&_originForm=uploadInformasiUsaha", from: '/uploadBarangJasa'});
  location.reload();
}
}



export default compose(
    withStyles(useStyles)
)(uploadBarangJasa)
