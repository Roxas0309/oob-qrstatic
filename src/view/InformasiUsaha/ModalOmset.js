import React from 'react';
import { AppBar, Box, Dialog, FormControl, IconButton, InputAdornment, InputLabel, List, ListItem, ListItemText, OutlinedInput, Toolbar, Typography } from '@material-ui/core';
import CloseIcon from '@material-ui/icons/Close';
import SearchIcon from '@material-ui/icons/Search';

function ModalOmset(props) {
    const { isShowModal, toggleCloseOmset, handlerOmset, valueOmset } = props
    const [filter, setFilter] = React.useState("");

    const handleSearchChange = (e) => {
        setFilter(e.target.value)
      };

    const dataOmset = (id) => {
    const { omset } = valueOmset[id]
    const { idName } = valueOmset[id]
    return (
        <List className="listModal" key={id} >
                <ListItem button type="button" value={{omset:omset,idName:idName}} 
                    onClick={() => {handlerOmset(omset,idName); toggleCloseOmset()}} >
                    <ListItemText value={omset} primary={omset}/>
            </ListItem> 
                
        </List>
        )
    }

    const closeJenis = () => {
        return toggleCloseOmset, setFilter("")
    }

    return (
        <Dialog fullScreen style={{height: '250px', width: '100%', position: 'fixed',top: 'unset' ,bottom: '0px'}} open={isShowModal} onClose={toggleCloseOmset}>
            
            <AppBar style={{backgroundColor: 'transparent', position: 'relative', 
                        boxShadow: 'unset'}}>
                    <Toolbar style={{minHeight: '60px', justifyContent: 'center', alignItems: 'center'}}>
                        <div style={{width: '100%', textAlign: 'left'}}>
                            <Typography style={{fontSize: '16px', lineHeight: '24px', fontWeight: '700', 
                            marginTop: '10px', color: '#1A1A1A', margin: '0px'}}>Rata-rata Omzet per Bulan</Typography>
                        </div>
                        <IconButton onClick={toggleCloseOmset} edge="start" color="inherit" aria-label="close"
                            style={{position:'absolute', width: '20px', right: '20px', zIndex: 1, top: '5px', color: '#0057E7'}}>
                            <CloseIcon />
                        </IconButton>
                        
                    </Toolbar>
                </AppBar>
            
          
            {/* <Box style={{height: '50px', justifyContent: 'left', alignItems: 'center', 
                display: 'flex', marginBottom: '20px'}}>
                <p style={{fontSize: '16px', lineHeight: '24px', fontWeight: '700', 
                    marginTop: '20px', marginBottom: '0px', marginLeft: '16px'}}>Rata-rata Omzet per Bulan</p>
            </Box> */}
            {/* <FormControl variant="outlined" style={{margin: '10px'}}>
                <InputLabel htmlFor="outlined-adornment">Omzet per Bulan</InputLabel>
                <OutlinedInput
                    id="outlined-adornment"
                    placeholder="Cari Omzet per Bulan"
                    label="Omzet per Bulan"
                    autoFocus={true}
                    value={filter}
                    onChange={handleSearchChange}
                    startAdornment={<InputAdornment position="start"><SearchIcon /></InputAdornment>}
                    labelWidth={60}
                />
            </FormControl> */}

            {valueOmset ? 
                (Object.keys(valueOmset).map((id) => valueOmset[id].omset.includes(filter) &&
                    dataOmset(id))):
                    <List >
                        <ListItem >
                    <ListItemText primary="Data Kosong"/>
                </ListItem> 
            </List>
            }
        </Dialog>
    )
}

export default ModalOmset
