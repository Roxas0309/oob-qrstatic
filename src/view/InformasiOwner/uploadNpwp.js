import React, { Component } from 'react'
import Link from '@material-ui/core/Link';
import ArrowBackIcon from '@material-ui/icons/ArrowBack';
import { AppBar, Box, Button, Grid, Toolbar, withStyles } from '@material-ui/core';
import { compose } from 'recompose';
import { createBrowserHistory } from 'history';
import NPWP from '../../../public/img/npwp.png'

const useStyles = theme => ({
    appBar: {
      position: 'relative',
      height: '40px',
      justifyContent: 'center'
    },
    title: {
      flex: 1,
      fontSize: '15px',
      textAlign: 'center',
    },
    formControl: {
      margin: theme.spacing(1),
      minWidth: 50,
      position: 'relative',
      zIndex: '1'
    }   

  });

class uploadNpwp extends Component {

    constructor(props){
        super(props);

        this.state = {
            dataUri: '',
            openDialog: false,
            open: false,
            cam: '',
            imgFile: null,
            imgBlob: '',
            openFile: false,
        }

        this.takePhoto = React.createRef();
        this.handleOpen = React.createRef();
        this.handleClose = React.createRef();
        this.handleSlect = React.createRef();
        this.handleCloseSub = React.createRef();
        this.handleOpenSub = React.createRef();
        this.saveLocalStorage = React.createRef();
        this.saveLocalStorageFoto = React.createRef();
        this.onImageChange = this.onImageChange.bind(this)
        this.handleOpenFile = React.createRef();
        this.handleCloseFile = React.createRef();

    }

    componentWillUnmount(){
        location.reload()
    }

    getMyPhotoPlease = () => {
        localStorage.removeItem('pathImg')
        this.props.history.push({pathname : '/imageMaker',search : "?_query=imgNpwp&_canvasNumber=1&_originForm=uploadKartuOwner", from: '/uploadNpwp'});
        location.reload();
      }

      onImageChange = (event) => {
        if (event.target.files && event.target.files[0]) {
            let rd = new FileReader();
            var imgBlob = event.target.files[0];
            var val;
            // if (imgBlob.type !== 'image/png' || imgBlob.type !== 'image/jpg' || imgBlob.type !== 'image/jpeg')
            console.log('imgblob type ' + imgBlob.type);
            if(imgBlob.size>2000000){
                alert("Tidak Dapat Mengupload File Ukuran Lebih dari 2Mb.")
                console.log('imgblob ' + imgBlob.type);
            }
            else if(!imgBlob.type.includes("image")){
                alert("upload file tidak sesuai format")
                console.log('imgblob ' + imgBlob.type);
            }else{
                rd.readAsDataURL(imgBlob);
                console.log('masuk ke data ini.');
              
                rd.onload = () => {
                    val = rd.result
                    localStorage.setItem('pathImg', val);
                    this.props.history.push({pathname : '/imageMaker',search : "?_query=imgNpwp&_canvasNumber=1&_originForm=uploadKartuOwner", from: '/uploadNpwp'});
                    location.reload();
                }
              
            }
        }
       }

    
  
    render() {
      
        
        return (
            <div style={{paddingInline: '16px', overflow: 'hidden'}}>
                <AppBar style={{backgroundColor: 'transparent', position: 'absolute', 
                    boxShadow: 'unset'}}>
                    <Toolbar onClick={() => this.props.history.push('/uploadKartuOwner', {from: '/uploadNpwp'})} 
                      style={{minHeight: '60px'}}>
                        <Link style={{position: 'absolute'}} >
                            <ArrowBackIcon/>
                        </Link>
                    </Toolbar>
                </AppBar>
                <h4 style={{textAlign: 'center', color: '#121518', marginTop: '70px', fontSize: '18px', 
                    lineHeight: '27px', fontWeight: 'bold', fontFamily: 'Montserrat', marginBottom: '59px'}}>
                        Foto Kartu NPWP</h4>
                <Grid container wrap="nowrap" style={{justifyContent: 'center', alignItems: 'center', 
                        display: 'flex', marginTop: '30px'}}>
                   
                    <Box style={{textAlign: 'center', position: 'fixed', display: 'contents'}}>
                        <img src={NPWP} style={{ height: 120 }} />
                    </Box>
                </Grid>
                <Box style={{marginTop: '40px'}}>
                    <ul className="fa-ul" style={{
                        fontSize: 'large',
                        letterSpacing: '0.2px',
                        lineHeight: 1.5,
                        marginRight: '10px'

                    }}>
                        <li style={{fontSize: '8px', color: '#6B7984', lineHeight: '24px', marginBottom: '9px'}}><span className="fa-li"><i className="fa fa-circle"></i></span>                        <p style={{fontSize:"16px",marginLeft:'14px'}}>
Pastikan foto NPWP tidak terpotong dan tulisan bisa terbaca jelas</p></li>
                    </ul>
                </Box>



                    

                <Box style={{position: 'fixed', bottom: '20px', minWidth: '90%'}}>
                    <Button size="large"
                    
                    style={{
                        marginBottom:"57px",

                        marginTop: "10px",
                        textTransform: "none",
                        marginLeft:"4vw",
                        fontFamily: "Nunito",
                        fontSize: "16px",
                        height: '56px',
                        fontWeight: "700",
                        alignItems:'center',
                        width: '100%',
                        marginInline: '0px'
                      }} disableElevation
                    variant="contained" color="primary"
                            onClick={this.getMyPhotoPlease}    >
                            {/* <CameraAltIcon style={{marginRight: '5px'}}/> */}
                              Ambil Foto
                        </Button>
                    <Button  size="large"
                     style={{
                        marginTop: "10px",
                        textTransform: "none",
                        marginBottom: "10px",
                        marginLeft:"4vw",
                        fontFamily: "Nunito",
                        fontSize: "16px",
                        height: '56px',
                        fontWeight: "700",
                        alignItems:'center',
                        width: '100%',
                        color:'rgba(0,87,231,1)',
                        marginInline: '0px',
                        backgroundColor: "rgba(232,243,252,1)",
                      }} disableElevation
                     variant="contained" component="label">
                              Pilih Dari Galeri
                              <input type="file" accept="image/png,image/jpg,image/jpeg" onChange={this.onImageChange} 
                              style={{display: 'none'}} />
                    </Button>
                </Box>
                
            </div>
        )
    }
}

export default compose(
    withStyles(useStyles)
) (uploadNpwp)
