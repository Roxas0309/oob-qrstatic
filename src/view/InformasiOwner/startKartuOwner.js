import React, { Component } from "react";
import Link from "@material-ui/core/Link";
import ArrowBackIcon from "@material-ui/icons/ArrowBack";
import { AppBar, Box, Button, Card, Grid, TextField, Toolbar, Typography } from "@material-ui/core";
import Skeleton from "@material-ui/lab/Skeleton";
import KTP from "../../../public/img/logoKtp.png";

class startKartuOwner extends Component {

  componentWillUnmount(){
   // location.href ="/startKartuOwner"
    location.reload()
  }
  render() {
    const { loading = false } = this.props;
    const lanjut = () => {
      this.props.history.push('/uploadKartuOwner', {from: '/startKartuOwner'})
    }
    return (
      <div style={{ overflow: "hidden"}}>

        <AppBar style={{backgroundColor: 'transparent', position: 'absolute', 
                boxShadow: 'unset'}}>
            <Toolbar style={{minHeight: '60px'}}>
                <Link style={{position: 'absolute',
                            alignItems: 'center', display: 'flex'}} onClick={() => this.props.history.push('/inputDataOwner', {from: '/startKartuOwner'})}>
                    <ArrowBackIcon/>
                </Link>
                <div style={{width: '100%', textAlign: 'center'}}>
                    <Typography style={{fontFamily: 'Nunito', fontSize: '14px',
                fontWeight: '400', lineHeight: '21px', color: '#303B4A'}}>Langkah 2 dari 4</Typography>
                </div>
            </Toolbar>
        </AppBar>

        <Box
          style={{
            marginTop: "70px",
            // marginInlineStart: "16px",
            // marginInlineEnd: "16px",
          }}
        >
          <h4
            style={{
              textAlign: "center",
              color: "#121518",
              marginTop: "0px",
              marginBottom: '0px',
              fontSize: "18px",
              lineHeight: "24px",
              fontWeight: "bold",
              fontFamily: "Montserrat",
            }}
          >
            Foto Kartu KTP dan NPWP
          </h4>
          <p
            style={{
              marginTop: "5px",
              color: "#303B4A",
              marginBottom: "10px",
              textAlign: "center",
              fontSize: "16px",
              padding: "10px",
              lineHeight: "24px",
            }}
          >
            Siapkan kartu dan ikuti petunjuk pengambilan foto.
          </p>
        </Box>
        <Grid
          container
          wrap="nowrap"
          style={{
            justifyContent: "center",
            alignItems: "center",
            display: "flex",
            marginTop: "30px",
          }}
        >
          {loading ? (
            <Skeleton animation="wave" variant="rect" height={268}></Skeleton>
          ) : (
            <Box
              style={{
                textAlign: "center",
                position: "fixed",
                display: "contents",
              }}
            >
              <img src={KTP} style={{ height: 268 }} />
            </Box>
          )}

          <Button onClick={lanjut}
              size="large"
               style={{width: "91%", marginTop: '10px', marginInline: '0px',
                            textTransform: 'none', marginBottom: '10px', fontFamily: 'Nunito', 
                                fontSize: '16px', fontWeight: '700'}}
              disableElevation
              variant="contained"
              color="primary"
            >
              Lanjutkan
            </Button>
        </Grid>
      </div>
    );
  }
}

export default startKartuOwner;
