import React, { Component } from "react";
import Link from "@material-ui/core/Link";
import ArrowBackIcon from "@material-ui/icons/ArrowBack";
import { withStyles } from "@material-ui/core/styles";
import { compose } from "recompose";
import { createBrowserHistory } from "history";
import ktpVerif from "../../../public/img/ktpVerif.png";
import { AppBar, Box, Grid, Toolbar, Button } from "@material-ui/core";
import Skeleton from "@material-ui/lab/Skeleton";
import ButtonComponent from "../../containers/button/ButtonComponent";

const useStyles = (theme) => ({
  appBar: {
    position: "relative",
    height: "40px",
    justifyContent: "center",
  },
  title: {
    flex: 1,
    fontSize: "15px",
    textAlign: "center",
  },
  formControl: {
    margin: theme.spacing(1),
    minWidth: 50,
    position: "relative",
    zIndex: "1",
  },
});


class uploadKtp extends Component {
  constructor(props) {
    super(props);

    this.state = {
      dataUri: "",
      openDialog: false,
      cropImg: "",
      open: false,
      cam: "",
      cropper: null,
      dataCropper: "#",
      disableBtn: true,
    };

    this.takePhoto = React.createRef();
    this.handleOpen = React.createRef();
    this.handleClose = React.createRef();
    this.handleSlect = React.createRef();
    this.handleCloseSub = React.createRef();
    this.handleOpenSub = React.createRef();
    this.saveLocalStorage = React.createRef();

    this.getDataCrop = React.createRef();
    this.cropUlang = React.createRef();
    this.cropChange = React.createRef();
  }

  componentWillUnmount(){
    location.reload()
  }

  getMyPhotoPlease = () => {
    this.props.history.push({
      pathname: "/imageMaker",
      search: "?_query=imgKtp&_canvasNumber=1&_originForm=uploadKartuOwner",
      from: '/uploadKtp'
    });
    location.reload();
  };

  componentDidMount(){
     window.onpopstate = function(event) {
            window.location.reload();
        };
  }

  render() {
    const { loading = false, classes } = this.props;
    const isFullscreen = false;

    this.saveLocalStorage = () => {
      localStorage.setItem("imgKtp", this.state.dataUri);
      this.props.history.push("/uploadKartuOwner", {from: '/uploadKtp'});
    };

    this.handleSlect = (event) => {
      this.setState({ cam: event.target.value });
    };

    this.getDataCrop = () => {
      if (typeof this.state.cropper.getCroppedCanvas() !== undefined) {
        this.setState({
          dataCropper: this.state.cropper.getCroppedCanvas().toDataURL(),
          disableBtn: false,
        });
      }
    };

    this.cropChange = (crop) => {
      this.setState({ dataCropper: "#", disableBtn: false, cropper: crop });
    };

    this.fotoUlang = () => {
      this.setState({
        dataCropper: "#",
        disableBtn: true,
        open: false,
        openDialog: true,
      });
      localStorage.removeItem("imgKtp");
    };

    this.takePhoto = (data) => {
      this.setState({ dataUri: data, openDialog: true, open: false });
    };

    this.handleClose = () => {
      this.setState({ open: false });
      localStorage.removeItem("imgKtp");
    };

    this.handleOpen = () => {
      this.setState({ open: true });
    };

    this.handleCloseSub = () => {
      this.setState({ openDialog: false, dataCropper: "#" });
      localStorage.removeItem("imgKtp");
    };

    this.handleOpenSub = () => {
      this.setState({ openDialog: true });
    };
    return (
      <div style={{paddingInline: '16px', overflow: 'hidden'}}>
        <AppBar style={{backgroundColor: 'transparent', position: 'absolute', 
            boxShadow: 'unset'}}>
            <Toolbar onClick={() => this.props.history.push('/uploadKartuOwner', {from: '/uploadKtp'})} 
              style={{minHeight: '60px'}}>
                <Link style={{position: 'absolute'}} >
                    <ArrowBackIcon/>
                </Link>
            </Toolbar>
        </AppBar>
        <h4
          style={{
            textAlign: "center",
            color: "#121518",
            marginTop: "70px",
            fontSize: "18px",
            lineHeight: "27px",
            fontWeight: "bold",
            fontFamily: "Montserrat",
            marginBottom: "39px",
          }}
        >
          Foto Kartu KTP
        </h4>
        <Grid
          container
          wrap="nowrap"
          style={{
            justifyContent: "center",
            alignItems: "center",
            display: "flex",
            marginTop: "30px",
          }}
        >
          {loading ? (
            <Skeleton animation="wave" variant="rect" height={110}></Skeleton>
          ) : (
            <Box
              style={{
                textAlign: "center",
                position: "fixed",
                display: "contents",
              }}
            >
              <img src={ktpVerif} style={{ height: 110 }} />
            </Box>
          )}
        </Grid>
        <Box
          style={{
            // marginTop: "40px"
          }}
        >
          <ul
            className="fa-ul"
            style={{
              fontSize: "large",
              letterSpacing: "0.2px",
              lineHeight: 1.5,
              marginRight: "5px",
            }}
          >
            <li
              style={{
                fontSize: "8px",
                color: "#6B7984",
                lineHeight: "24px",
                marginBottom: "9px",
              }}
            >
              <span className="fa-li">
                <i className="fa fa-circle"></i>
              </span>
              <p style={{fontSize:"16px",marginLeft:'14px'}}>
              Pakai kartu KTP asli
              </p>
            </li>
            <li
              style={{
                fontSize: "8px",
                color: "#6B7984",
                lineHeight: "24px",
                marginBottom: "9px",
              }}
            >
              <span className="fa-li">
                <i className="fa fa-circle"></i>
              </span>
              <p style={{fontSize:"16px",marginLeft:'14px'}}>
              Pastikan hasil foto tidak terpotong</p>
            </li>
            <li
              style={{
                fontSize: "8px",
                color: "#6B7984",
                lineHeight: "24px",
                marginBottom: "9px",
              }}
            >
              <span className="fa-li">
                <i className="fa fa-circle"></i>
              </span>
              <p style={{fontSize:"16px",marginLeft:'14px'}}>
              Pastikan tulisan di foto bisa terbaca jelas</p>
            </li>
            {/* <li style={{fontSize: '16px', color: '#6B7984', lineHeight: '24px'}}><span className="fa-li"><i className="fa fa-circle"></i></span>Pastikan KTP Anda masih berlaku, jika belum punya e-KTP</li> */}
          </ul>
          {/* <Button
            size="large"
            style={{
              marginTop: "10px",
              textTransform: "none",
              marginBottom: "10px",
              fontFamily: "Nunito",
              fontSize: "16px",
              fontWeight: "700",
              alignItems:'center',
              width: '100%',
              marginInline: '0px'
            }}
            variant="contained"
            disableElevation
            color="primary"
            onClick={this.getMyPhotoPlease}
          >
            Ambil Foto
          </Button> */}

          {/* <ButtonComponent style={{marginBottom:"10px", marginTop:"271px"}} onClick={this.getMyPhotoPlease}>
          Ambil Foto
          </ButtonComponent>
   */}

<Box 
// style={{marginTop: '40px'}}
>
                    {/* <ul className="fa-ul" style={{
                        fontSize: 'large',
                        letterSpacing: '0.2px',
                        lineHeight: 1.5,
                        marginRight: '5px'

                    }}>
                        <li style={{fontSize: '8px', color: '#6B7984', lineHeight: '24px', marginBottom: '9px'}}><span className="fa-li"><i className="fa fa-circle"></i></span><p style={{fontSize:"16px",marginLeft:'14px'}}>Pegang KTP dibawah dagu</p></li>
                        <li style={{fontSize: '8px', color: '#6B7984', lineHeight: '24px', marginBottom: '9px'}}><span className="fa-li"><i className="fa fa-circle"></i></span><p style={{fontSize:"16px",marginLeft:'14px'}}>Pastikan foto KTP menghadap ke kamera dan tidak menutupi wajah</p></li>
                    </ul> */}

                    <Button size="large"  style={{
                          marginTop: "10px",
                          textTransform: "none",
                          marginBottom: "10px",
                          marginLeft:"4vw",
                          fontFamily: "Nunito",
                          fontSize: "16px",
                          fontWeight: "700",
                          alignItems:'center',
                          width: '91%',
                          backgroundColor: "rgba(0,87,231,1)",
                          marginInline: '0px'
                        }} disableElevation variant="contained" color="primary"
                            onClick={this.getMyPhotoPlease}>
                        {/* <CameraAltIcon style={{marginRight: '5px'}}/> */}
                          Ambil Foto
                    </Button>
                </Box>
        </Box>
       
      </div>
    );
  }
}

export default compose(withStyles(useStyles))(uploadKtp);
