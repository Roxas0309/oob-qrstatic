import React, { Component } from "react";
import Link from "@material-ui/core/Link";
import ArrowBackIcon from "@material-ui/icons/ArrowBack";

import {
  AppBar,
  Backdrop,
  Box,
  Button,
  CircularProgress,
  Fade,
  Grid,
  IconButton,
  TextareaAutosize,
  TextField,
  Toolbar,
  Typography,
  withStyles,
} from "@material-ui/core";
import { ErrorMessage, Field, Form, Formik } from "formik";
import * as Yup from "yup";

import { compose } from "recompose";
import sukse from "../../../public/img/success.gif";
import gagal from "../../../public/img/unapproved.gif";
import { createBrowserHistory } from "history";
import { TIMES_OUT } from "../../master/masterComponent";

const useStyles = (theme) => ({
  root: {
    width: "100%",
    "& > * + *": {
      marginTop: theme.spacing(2),
    },
  },
  placeholder: {
    height: "auto",
    justifyContent: "center",
    alignItems: "center",
    display: "flex",
  },
  backdrop: {
    zIndex: theme.zIndex.drawer + 1,
    color: "#fff",
  },
  paper: {
    backgroundColor: theme.palette.background.paper,
    border: "1px solid lightgray",
    boxShadow: theme.shadows[5],
    padding: theme.spacing(2, 4, 3),
    textAlign: "center",
    borderRadius: "5px",
    alignItems: "center",
    justifyContent: "center",
    minWidth: "150px",
  },
});


class inputDataOwner extends Component {
  constructor(props) {
    super(props);
    this.state = {
      openAlert: false,
      query: "idle",
      proses: "",
    };
  }
  
  componentWillUnmount(){
    //location.href = "/inputDataOwner"
    clearTimeout(this.timer)
    location.reload()
  }

  render() {
    const { loading, classes } = this.props;
    const alpha = /^[a-zA-Z ,.~!@#$%^&*()=+'-/"/:/;//]+( [a-zA-Z ,.~!@#$%^&*()=+'-/"/:/;//]+)*$/;
    const notSpace = /^\S.*[^._@!?#$%*\s]$/;
    const numb = /^[0-9\b]+$/;

    const formTextField = (props) => {
      const {
        form: { setFieldValue, setFieldTouched },
        field: { name },
      } = props;

      const onChange = React.useCallback(
        (event) => {
          const { value } = event.target;
          if(name === "email")
          {  
            if(value.length<52)
                {
                  const { value } = event.target;
                  setFieldTouched(name, true);
                  setFieldValue(name, value ? value : "");
               }
          }
          else{
          const { value } = event.target;
          setFieldTouched(name, true);
          setFieldValue(name, value ? value : "");
          }
        },
        [setFieldValue, setFieldTouched, name]
      );

      const onBlur = React.useCallback(
        (event) => {
          const { value } = event.target;
          setFieldTouched(name, true);
          setFieldValue(name, value ? value : "");
        },
        [setFieldValue, setFieldTouched, name]
      );

      return <TextField {...props} onChange={onChange} onBlur={onBlur} />;
    };

    const formTextFieldArea = (props) => {
      const {
        form: { setFieldValue, setFieldTouched },
        field: { name },
      } = props;

      const onChange = React.useCallback(
        (event) => {
          const { value } = event.target;

          if (name == "nama") {
            localStorage.setItem("owner_nama", value);
          } else if (name == "almaat") {
            localStorage.setItem("owner_alamat", value);
          } else if (name == "email") {
            localStorage.setItem("owner_email", value);
          } else {
          }

          setFieldValue(name, value ? value : "");
        },
        [setFieldValue, name]
      );

      const onBlur = React.useCallback(
        (event) => {
          const { value } = event.target;
          setFieldTouched(name, value ? false : true);
        },
        [setFieldValue, setFieldTouched, name]
      );

      return (
        <TextareaAutosize {...props} onChange={onChange} onBlur={onBlur} />
      );
    };

    const klikLanjut = () => {
      if (
        new URLSearchParams(this.props.location.search).get("_onCorrection") ===
        "yes"
      ) {
        this.props.history.push('/reviewData');
      } else {
        this.props.history.push('/verifSukses', {from: '/inputDataOwner'});
      }
      location.reload();
    };

    return (
      <div>
        {/* -----------------alert----------- */}
        <Backdrop className={classes.backdrop} open={this.state.openAlert}>
          <div className={classes.placeholder}>
            {this.state.query === "success" ? (
              <Box className={classes.paper}>
                <Typography component={"span"} variant={"body2"}>
                  <span>
                    <h4 style={{ color: "black" }}>Berhasil Terkirim</h4>
                  </span>
                  <img src={sukse} style={{ height: 50, width: 50 }}></img>
                </Typography>
                {/* <Button
                    onClick={klikLanjut}
                    variant="contained"
                    style={{ width: "100%", marginTop: "20px" }}
                    color="primary"
                    disableElevation
                  >
                    OK
                  </Button> */}
              </Box>
            ) : this.state.query == "failed" ? (
              <Typography
                component={"span"}
                variant={"body2"}
                className={classes.paper}
              >
                <span>
                  <h4 style={{ color: "black" }}>
                    Gagal terkirim silahkan coba kembali
                  </h4>
                </span>
                <img src={gagal} style={{ height: 50, width: 50 }}></img>
              </Typography>
            ) : (
              <Fade
                in={this.state.query === "progress"}
                style={{
                  transitionDelay:
                    this.state.query === "progress" ? "500ms" : "0ms",
                }}
                unmountOnExit
              >
                <div className={classes.paper}>
                  <span>
                    <h4 style={{ color: "black" }}>Mengirim Data</h4>
                  </span>
                  <CircularProgress />
                </div>
              </Fade>
            )}
          </div>
        </Backdrop>
        {/* --------------------------------- */}
        <AppBar style={{backgroundColor: 'transparent', position: 'absolute', 
                boxShadow: 'unset'}}>
            <Toolbar style={{minHeight: '60px'}}>
                <Link style={{position: 'absolute',
                            alignItems: 'center', display: 'flex'}} onClick={klikLanjut}>
                    <ArrowBackIcon/>
                </Link>
                <div style={{width: '100%', textAlign: 'center'}}>
                    <Typography style={{fontFamily: 'Nunito', fontSize: '14px',
                fontWeight: '400', lineHeight: '21px', color: '#303B4A'}}>Langkah 1 dari 4</Typography>
                </div>
            </Toolbar>
        </AppBar>

        <Grid className="inputOwner"
          style={{
            padding: "16px",
            justifyContent: "center",
            alignItems: "center",
            textAlign: "center",
          }}
        >
          <h4
            style={{
              fontFamily: "Montserrat",
              fontSize: "18px",
              textAlign: "center",
              marginTop: "70px",
              fontWeight: "700",
              lineHeight: "27px",
            }}
          >
            Informasi Pemilik Usaha
          </h4>

          <Box
            style={{
              textAlign: "center"
            }}
          >
            <Formik
              initialValues={{
                nama: localStorage.getItem("rekInfo_ownerName")
                  ? localStorage.getItem("rekInfo_ownerName")
                  : "",
                alamat: localStorage.getItem("address")
                  ? localStorage.getItem("address")
                  : "",
                email: localStorage.getItem("owner_email")
                  ? localStorage.getItem("owner_email")
                  : "",
              }}
              validationSchema={Yup.object().shape({
                nama: Yup.string()
                  .matches(alpha, {
                    message: "Nama tidak valid",
                    excludeEmptyString: true,
                  })
                  .required("Harus diisi"),
                alamat: Yup.string()
                  .matches(notSpace, {
                    message: "Alamat tidak valid",
                    excludeEmptyString: true,
                  })
                  .required("Harus diisi"),
                email: Yup.string()
                  .email("Gunakan format: contoh@email.com")
                  .required("Harus diisi")
                  .nullable()
                  .max(50, 'Maksimal Email terdiri dari 50 karakter'),
              })}
              onSubmit={({ nama, alamat, email }, actions) => {
                this.setState({ openAlert: true });

                //clearTimeout(this.timer);
                if (this.state.query !== "idle") {
                  this.setState({ query: "idle" });
                  return;
                }

                this.setState({ proses: "progress" });
                this.setState({ query: this.state.proses });

                
                this.setState({ proses: "success" });
                  this.setState({ query: this.state.proses });
                this.timer = setTimeout(() => {
                  localStorage.setItem("owner_email", email);
                  
                  if (
                    new URLSearchParams(this.props.location.search).get("_onCorrection") ===
                    "yes"
                  ) {
                    this.props.history.push('/reviewData');
                  } else {
                    this.props.history.push('/startKartuOwner', {from: '/inputDataOwner'});
                  }
                  location.reload();
                  actions.setSubmitting(false);
                }, 3000);
              }}
            >
              {({ values, isValid, errors, touched, isSubmitting }) => (
                <Form autoComplete="off">
                  {console.log('isValid: ', errors)}
                  <Box className="inputOwnerDis">
                    <Field
                      disabled
                      component={formTextField}
                      id="nama"
                      name={"nama"}
                      label="Nama"
                      value={values.nama}
                      variant="filled"
                      margin="normal"
                      fullWidth
                      error={errors.nama && touched.nama}
                      className={
                        "form-control" +
                        (errors.nama && touched.nama ? " is-invalid" : "")
                      }
                    />
                    <ErrorMessage
                      name="nama"
                      component="div"
                      className="invalid-feedback"
                    />
                  </Box>
                  <Box className="inputOwnerDis"
                    // style={{
                    //   marginInlineStart: "10px",
                    //   marginInlineEnd: "10px",
                    // }}
                  >
                    <Field
                      disabled
                      component={formTextField}
                      multiline
                      rows={3}
                      id="alamat"
                      value={values.alamat}
                      name={"alamat"}
                      label="Alamat"
                      variant="filled"
                      margin="normal"
                      fullWidth
                      error={errors.alamat && touched.alamat}
                      className={
                        "form-control" +
                        (errors.alamat && touched.alamat ? " is-invalid" : "")
                      }
                    />
                    <ErrorMessage
                      name="alamat"
                      component="div"
                      className="invalid-feedback"
                    />
                  </Box>
                  <Box
                    // style={{
                    //   marginInlineStart: "10px",
                    //   marginInlineEnd: "10px",
                    // }}
                  >
                    <Field
                      component={formTextField}
                      id="email"
                      name={"email"}
                      label="Email"
                      variant="filled"
                      value={values.email}
                      margin="normal"
                      fullWidth
                      error={errors.email && touched.email}
                      className={
                        "form-control" +
                        (errors.email && touched.email ? " is-invalid" : "")
                      }
                    />
                    <ErrorMessage
                      name="email"
                      component="div"
                      className="invalid-feedback"
                    />
                    <p
                      style={{
                        marginTop: "2px",
                        color: "rgba(48, 59, 74, 0.72)",
                        marginBottom: "2px",
                        fontSize: "12px",
                        fontWeight: '400',
                        lineHeight: '14px',
                        textAlign: "left",
                        marginLeft: "16px"
                      }}
                    >
                      Informasi dari Bank Mandiri dan notifikasi akun akan <br/>
                      dikirim ke email ini.
                    </p>
                  </Box>
                  <Button
                    type="submit"
                    variant="contained"
                    disableElevation
                    disabled={(values.email.length==0)}
                    size="large"
                    color="primary"
                    style={{width: "100%", marginTop: '10px', marginInline: '0px', marginInlineStart:'0vw',
                            textTransform: 'none', marginBottom: '10px', fontFamily: 'Nunito', 
                                fontSize: '16px', fontWeight: '700', position: 'relative', marginTop: '80px'}}
                  >
                    Lanjutkan
                  </Button>
                  {console.log("isSubmitting = " + isSubmitting + " isValid = "+ isValid +" error = " + Object.keys(errors).length)}
                </Form>
              )}
            </Formik>
          </Box>
        </Grid>
      </div>
    );
  }
}

export default compose(withStyles(useStyles))(inputDataOwner);
