import React, { Component } from 'react'
import Link from '@material-ui/core/Link';
import ArrowBackIcon from '@material-ui/icons/ArrowBack';
import { Box, Button, Grid, withStyles } from '@material-ui/core';
import { compose } from 'recompose';
import { createBrowserHistory } from 'history';
import NPWP from '../../../public/img/npwp.png'

const useStyles = theme => ({
    appBar: {
      position: 'relative',
      height: '40px',
      justifyContent: 'center'
    },
    title: {
      flex: 1,
      fontSize: '15px',
      textAlign: 'center',
    },
    formControl: {
      margin: theme.spacing(1),
      minWidth: 50,
      position: 'relative',
      zIndex: '1'
    }

  });

class uploadNpwp extends Component {

    constructor(props){
        super(props);

        this.state = {
            dataUri: '',
            openDialog: false,
            open: false,
            cam: '',
            imgFile: null,
            imgBlob: '',
            openFile: false,
        }

        this.takePhoto = React.createRef();
        this.handleOpen = React.createRef();
        this.handleClose = React.createRef();
        this.handleSlect = React.createRef();
        this.handleCloseSub = React.createRef();
        this.handleOpenSub = React.createRef();
        this.saveLocalStorage = React.createRef();
        this.saveLocalStorageFoto = React.createRef();
        this.onImageChange = this.onImageChange.bind(this)
        this.handleOpenFile = React.createRef();
        this.handleCloseFile = React.createRef();

    }

    componentDidMount(){
        window.onpopstate = function(event) {
          window.location.reload();
      };
      }
  

    getMyPhotoPlease = () => {
        localStorage.removeItem('pathImg')
        this.props.history.push({pathname : '/imageMaker',search : "?_query=imgNpwp&_canvasNumber=1&_originForm=uploadKartuOwner", from: '/uploadNpwp'});
        location.reload();
      }

      onImageChange = (event) => {
        if (event.target.files && event.target.files[0]) {
        //   this.setState({
        //     imgFile: URL.createObjectURL(event.target.files[0]), openFile: true,
        //     imgBlob: event.target.files[0]
        //   });

          let rd = new FileReader();
            var imgBlob = event.target.files[0]
            var val
            rd.readAsDataURL(imgBlob);
            rd.onload = () => {
                val = rd.result
                localStorage.setItem('pathImg', val)
            }

            this.props.history.push({pathname : '/imageMaker',search : "?_query=imgNpwp&_canvasNumber=1&_originForm=uploadKartuOwner", from: '/uploadNpwp'});
            location.reload();

          
        }
       }

    
  
    render() {
      
        
        return (
            <div className="constructor-uploader">
          <div className="header-color-white">
              <div className="incubator-panel">
                  <Link href="/uploadKartuOwner" style={{width:'3.620vw', height:'3.620vw'}} >
                      <ArrowBackIcon  />
                  </Link>
              </div>
          </div>

          <div className="header-judul">
          Foto Kartu NPWP
          </div>

          <div className="image-judul">
          <img src={NPWP} style={{ height: '33.059vw', width:'77.968vw' }} />
          </div>

          <div className="body-wording">
            <div className="li-wording">
              <div className="blue-dot"></div>
              <div className="wording-me">Pastikan foto NPWP tidak terpotong dan tulisan bisa terbaca jelas</div>
            </div>
          </div>

          <div className="button-footer-klik-npwp"  onClick={this.getMyPhotoPlease}>
            <span>Ambil Foto</span>
            </div>

             <Button style={{ width: "93%", height: '12vw', position:'fixed',bottom:'5vh',borderRadius:'1.810vw',
                        textTransform: 'none', backgroundColor: '#E8F3FC', color: '#0057E7',boxShadow:'none'
                        }}  component="label">
                            <p style={{fontSize:'3.620vw'}}>Pilih Dari Galeri</p>
                              
                              <input type="file" accept="image/*" onChange={this.onImageChange} style={{display: 'none'}} />
            </Button>
         </div>
         
        )
    }
}

export default compose(
    withStyles(useStyles)
) (uploadNpwp)
