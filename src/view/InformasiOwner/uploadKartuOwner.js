import React, { Component, createRef } from 'react'
import PropTypes from 'prop-types';
import ArrowBackIcon from '@material-ui/icons/ArrowBack';
import { AppBar, Backdrop, Box, Button, Link, Checkbox, CircularProgress, Container, Fade, FormControl, FormControlLabel, Grid, InputLabel, TextField, Toolbar, Typography, withStyles, FilledInput, Input } from '@material-ui/core';
import Skeleton from '@material-ui/lab/Skeleton';   
import CameraAltIcon from '@material-ui/icons/CameraAlt';
import CheckBoxOutlineBlankIcon from '@material-ui/icons/CheckBoxOutlineBlank';
import CheckBoxIcon from '@material-ui/icons/CheckBox';
import EditIcon from '@material-ui/icons/Edit';
import Cleave from 'cleave.js/react';
import { ErrorMessage, Field, Form, Formik } from 'formik';
import * as Yup from 'yup';
import { compose } from 'recompose';
import sukse from '../../../public/img/success.gif';
import gagal from '../../../public/img/unapproved.gif';
import camera from '../../../public/img/Camera 1.png';
import edit from '../../../public/img/Edit.png';
import edit2 from '../../../public/img/iconEdit.png';
import { createBrowserHistory } from 'history';
import { TIMES_OUT } from '../../master/masterComponent';
import { Col, Row } from 'react-bootstrap';
import { id } from 'date-fns/locale';

const ITEM_HEIGHT = 48;
const ITEM_PADDING_TOP = 8;
const MenuProps = {
  PaperProps: {
    style: {
      maxHeight: ITEM_HEIGHT * 4.5 + ITEM_PADDING_TOP,
      width: 250,
    },
  },
};

const useStyles = (theme) => ({
    errorImg: {
        border: '1px solid red',
    },
    suksesImg: {
        border: 'none',
    },
    addShrink: {
        'label ':'{ & .MuiInputLabel-shrink}'
    },
    root: {
        width: '100%',
        '& > * + *': {
          marginTop: theme.spacing(2),
        },
      },
      placeholder: {
          height: 'auto',
          justifyContent: 'center',
          alignItems: 'center',
          display: 'flex'
      },
      backdrop: {
          zIndex: theme.zIndex.drawer + 1,
          color: '#fff',
        },
      paper: {
      backgroundColor: theme.palette.background.paper,
      border: '1px solid lightgray',
      boxShadow: theme.shadows[5],
      padding: theme.spacing(2, 4, 3),
      textAlign: 'center',
      borderRadius: '5px',
      alignItems: 'center',
      justifyContent: 'center',
      minWidth: '150px'
  
      },
  });

class uploadKartuOwner extends Component {

    constructor(props){
        super(props);

        this.state = { 
            imgKtp: null,
            imgKtpWajah: null,
            imgNpwp: null,
            npwp: '',
            isErrorImgNpwp: false,
            isErrorImgKtp: false,
            isErrorImgKtpWajah: false,
            errorMsgNpwp: "",
            errorMsgImgNpwp: "",
            errorMsgImgKtp: "",
            errorMsgImgKtpWajah: "",
            errorClassNpwp: '',
            errorClassImgNpwp: '',
            errorClassImgKtp: '',
            errorClassImgKtpWajah: '',
            openAlert: false,
            query: 'idle',
            proses: '',
            cekNpwp: false,
            disBut: false,
            klik: false,
            isNpwpValid : false
        }

        this.textRef = React.createRef();

    }

    renderCleaveField = (props) => {
        const { inputRef, ...other } = props;
        return <Cleave {...other} name="npwp" ref={(ref) => {inputRef(ref ? ref.inputElement : null)}} 
        options={{numericOnly: true, delimiters:['.','.','.','-','.'], blocks: [2,3,3,1,3,3]}} 
        value={localStorage.getItem('npwp') ? localStorage.getItem('npwp') : ''}/>
    }


    componentDidMount(){

       
        if(localStorage.getItem('imgKtp')){
            this.setState({imgKtp: localStorage.getItem('imgKtp')})
        }
        if(localStorage.getItem('imgKtpWajah')){
            this.setState({imgKtpWajah: localStorage.getItem('imgKtpWajah')})
        }
        if(localStorage.getItem('imgNpwp')){
            this.setState({imgNpwp: localStorage.getItem('imgNpwp')})
        }
        
        

        var cek = (localStorage.getItem('cekNpwp'))
        if(cek === 'true'){
            this.setState({cekNpwp: true})
        }else{
            this.setState({cekNpwp: false})
        }

        if(cek){
            if(localStorage.getItem('imgKtp') === null ||
                localStorage.getItem('imgKtpWajah') === null){
                this.setState({ disBut: true})
            }else{
                this.setState({ disBut: false})
            }
                
        }else{
            if(localStorage.getItem('imgKtp') === null || 
                localStorage.getItem('imgKtpWajah') === null){
                this.setState({ disBut: true})
            }else {
                this.setState({ disBut: false})
                // if(localStorage.getItem('imgNpwp')){
                //     this.setState({ disBut: true})
                // }else{
                //     this.setState({ disBut: false})
                // }
                
            }
        }

    }

    componentWillUnmount(){
        location.reload()
    }

    


    render() {
        const {loading = false, classes} = this.props;

        
        // const classImgWajah = classNames(
        //     this.state.imgKtpWajah ? "imgBlue" : "imgGray"
        //   );
        const formTextField = (props) => {
            const {
                form: {setFieldValue, setFieldTouched},
                field: {name},
            } = props;

            const onChange = React.useCallback((event) => {
                const {value} = event.target;
                localStorage.setItem('npwp', !this.state.cekNpwp ? value : '')
                setFieldValue(name, value && !this.state.cekNpwp ? value : '');
                setFieldTouched(name, value ? true : false);
                console.log("length : " +  localStorage.getItem('npwp').length);


                // if((localStorage.getItem('npwp') != null || localStorage.getItem('npwp') != "") 
                //     && localStorage.getItem('npwp').length >=20 &&  localStorage.getItem('npwp').length <=20){
                //     this.setState({isNpwpValid:true});
                // }

            },[setFieldValue, setFieldTouched, name])

            const onBlur = React.useCallback((event) => {
                const {value} = event.target;
                setFieldTouched(name, true);
            },[setFieldValue, setFieldTouched, name])
            // return (
            //     <FormControl variant="filled" fullWidth>
            //         <InputLabel htmlFor="npwp">NPWP</InputLabel>
            //         <Input {...props} value={localStorage.getItem('npwp')} id="npwp"
            //             onChange={onChange} onBlur={onBlur} value = {localStorage.getItem('npwp')}
            //             inputComponent={renderCleaveField} />
            //     </FormControl>
            // )
            return <TextField {...props} onChange={onChange} onBlur={onBlur}
                    InputProps={{inputComponent: renderCleaveField}}
                    
                    InputLabelProps={{ shrink: true }}
                    />
        }

        const renderCleaveField = (props) => {
            const { inputRef, ...other } = props;
            return <Cleave {...other} 
            ref={(ref) => {inputRef(ref ? ref.inputElement : null)}} 
            options={{numericOnly: true, delimiters:['.','.','.','-','.'], blocks: [2,3,3,1,3,3]}} />
        }

        const submitNonNpwp = (val) => {
            localStorage.removeItem('imgNpwp')
            localStorage.removeItem('npwp')
            localStorage.setItem('cekNpwp', this.state.cekNpwp)

            if(this.state.imgKtp === null){
                this.setState({ errorMsgImgKtp: "Foto KTP harus diisi", errorClassImgKtp: classes.errorImg, 
                        isErrorImgKtp: true})
                return false;

            }else{
                this.setState({ errorMsgImgKtp: "", errorClassImgKtp: classes.suksesImg, 
                        isErrorImgKtp: false})
            }

            if(this.state.imgKtpWajah === null){
                this.setState({ errorMsgImgKtpWajah: "Foto KTP harus diisi", 
                            errorClassImgKtpWajah: classes.errorImg, isErrorImgKtpWajah: true})
                return false;

            }else{
                this.setState({ errorMsgImgKtpWajah: "", 
                            errorClassImgKtpWajah: classes.suksesImg, isErrorImgKtpWajah: false})
            }

            this.setState({openAlert: true})

            clearTimeout(this.timer);
            if(this.state.query !== 'idle'){
                this.setState({query: 'idle'});
                return;
            }
    
            this.setState({query: 'progress'});
            this.timer = setTimeout(() => {
                this.setState({proses: 'success'})
                this.setState({query: this.state.proses});
                if( new URLSearchParams(this.props.location.search).get("_onCorrection")==="yes"){  
                    this.props.history.push('/reviewData') 
                }
                else{
                    this.props.history.push('/informasiUsaha', {from: '/uploadKartuOwner'})
                }
                
                location.reload();
            }, 3000);

        }

        const klikLanjut = () => {
            if( new URLSearchParams(this.props.location.search).get("_onCorrection")==="yes"){  
                this.props.history.push('/reviewData') 
            }
            else{
                this.props.history.push('/startKartuOwner', {from: '/uploadKartuOwner'})
            }
            
            location.reload();
        }

        const handleChangeCek = (event) => {
            
            this.setState({ cekNpwp: event.target.checked });

            if(event.target.checked){
                if(localStorage.getItem('imgKtp') === null ||
                    localStorage.getItem('imgKtpWajah') === null){
                    this.setState({ disBut: true})
                }else{
                    if(this.state.cekNpwp){
                        this.setState({ disBut: true})
                    }else{
                        this.setState({ disBut: false})
                    }
                    
                }
                    
            }else{
                if(localStorage.getItem('imgKtp') === null || 
                    localStorage.getItem('imgKtpWajah') === null){
                    this.setState({ disBut: true})
                }else {
                    if(this.state.cekNpwp){
                        this.setState({ disBut: true})
                    }else{
                        this.setState({ disBut: false})
                    }
                    // this.setState({ disBut: false})
                    // console.log('disableeee4: ', this.state.disBut)
                    // if(localStorage.getItem('imgNpwp')){
                    //     this.setState({ disBut: true})
                    // }else{
                    //     this.setState({ disBut: false})
                    // }
                    
                }
            }


            // if(event.target.checked && (localStorage.getItem('imgKtp') &&
            //     localStorage.getItem('imgKtpWajah'))){
            //     this.setState({ disBut: false, imgNpwp: null})
            // }

            // if(!event.target.checked){
                
                
            // }

            // if(!event.target.checked && (localStorage.getItem('imgKtp') &&
            //     localStorage.getItem('imgKtpWajah'))){
            //     this.setState({ disBut: true, imgNpwp: null})
            // }

            // if(localStorage.getItem('imgKtp') === null || 
            //     localStorage.getItem('imgKtpWajah') === null || 
            //     localStorage.getItem('imgNpwp') === null ){
            //     this.setState({disBut: true})
            // }else{
            //     this.setState({disBut: false})
            // }

            // if(!event.target.checked && (this.state.imgNpwp === null || this.state.imgKtp === null || this.state.imgKtpWajah === null)){
            //     this.setState({ disBut: true})
            // }

          };

          


        return (
            <div>

                {/* -----------------alert----------- */}
                <Backdrop className={classes.backdrop} open={this.state.openAlert} >
                    <div className={classes.placeholder}>

                    {
                        this.state.query === 'success' ? 
                                (<Box className={classes.paper}>
                                    <Typography component={'span'} variant={'body2'} >
                                        <span><h4 style={{color: 'black'}}>Berhasil Disimpan</h4></span>
                                        <img src={sukse}
                                        style={{ height: 50, width: 50 }}></img>
                                    </Typography>
                                    {/* <Link>
                                        <Button onClick={klikLanjut} variant="contained" 
                                            style={{width: '100%', marginTop: '20px'}} color="primary" 
                                            disableElevation>
                                            OK
                                        </Button>
                                    </Link> */}
                                </Box>
                                
                                ) :

                        this.state.query == 'failed' ? 
                             (<Typography component={'span'} variant={'body2'} className={classes.paper}>
                             <span><h4 style={{color: 'black'}}>Gagal!! silahkan coba kembali</h4></span>
                             <img src={gagal}
                             style={{ height: 50, width: 50 }}></img>
                         </Typography>) :
                         (<Fade in={this.state.query === 'progress'}
                            style={{transitionDelay: this.state.query === 'progress' ? '500ms' : '0ms'}}
                            unmountOnExit
                            >
                                <div className={classes.paper}>
                                    <span><h4 style={{color: 'black'}}>
                                        Menyimpan Data</h4></span>
                                    <CircularProgress />
                                </div>
                            </Fade>)
                        }

                    </div>
                </Backdrop>
                {/* --------------------------------- */}
                {/* <div style={{position: 'fixed', display: 'flex', minWidth: '100%', justifyContent: 'center', alignItems: 'center'}}>
                    <Link href="/startKartuOwner" style={{left: '20px', position: 'fixed', top: '20px'}}>
                        <ArrowBackIcon style={{left: '20px'}}/>
                    </Link>
                    <span style={{display: 'flex', textAlign: 'center', 
                        justifyContent: 'center', alignItems: 'center', 
                        position: 'fixed', top: '21px', fontSize: '14px', color: '#303B4A'}}>Langkah 2 dari 4</span>
                </div> */}

                <AppBar style={{backgroundColor: 'transparent', position: 'absolute', 
                        boxShadow: 'unset'}}>
                    <Toolbar style={{minHeight: '60px'}}>
                        <Link style={{position: 'absolute',
                            alignItems: 'center', display: 'flex'}} onClick={klikLanjut}>
                            <ArrowBackIcon/>
                        </Link>
                        <div style={{width: '100%', textAlign: 'center'}}>
                            <Typography style={{fontFamily: 'Nunito', fontSize: '14px',
                        fontWeight: '400', lineHeight: '21px', color: '#303B4A'}}>Langkah 2 dari 4</Typography>
                        </div>
                    </Toolbar>
                </AppBar>


                <Box style={{marginTop: '70px', marginInline: '16px'}}>
                <h4 style={{textAlign: 'center', color: '#121518', marginTop: '0px', fontSize: '18px', 
                    lineHeight: '24px', fontWeight: 'bold', fontFamily: 'Montserrat', marginBottom: '59px'}}>Foto Kartu KTP dan NPWP</h4>
                </Box>
                
                <Grid container spacing={2}>
                    <Grid item xs={8}>
                        {this.state.isErrorImgKtp ? 
                            <span style={{marginLeft: '30px', fontSize: '16px', color: 'red', lineHeight: '24px', fontWeight: 'bold'}}>
                                    Foto KTP harus diisi</span> :
                            <span style={{marginLeft: '30px', fontSize: '16px', color: '#3A4D5B', lineHeight: '24px', fontWeight: 'bold'}}>Foto KTP</span>
                        }
                    </Grid>
                    {this.state.imgKtp != null && 
                    <Grid item xs={3}>
                        <Link onClick={() => this.props.history.push('/uploadKtp', {from: 'uploadKartuOwner'})}
                            style={{display: 'flex', flexDirection: 'row', justifyContent: 'center', alignItems: 'center'}}>
                            {/* <EditIcon style={{fontSize: "1rem", paddingRight: '3px'}} />  */}
                            <img src={edit2} height={20} width={20} style={{marginRight:'5.35px'}}/>
                            <p style={{margin: '0px'}}>Ubah</p>
                        </Link>
                    </Grid>
                    }
                </Grid>
                
                <Grid container wrap="nowrap"  style={{justifyContent: 'center', alignItems: 'center', 
                        display: 'flex', marginTop: '13px', marginBottom: '30px'}}>
                    {loading ? (<Skeleton animation="wave" variant="rect" height={200}></Skeleton>) : 
                    (
                    this.state.imgKtp == null ? <Box className={this.state.errorClassImgKtp} style={{justifyContent: 'center', textAlign: 'center', alignItems: 'center', 
                                marginTop: '5px', minHeight: '200px', backgroundColor: '#e2f2ff', 
                                marginInlineStart: '30px', marginInlineEnd: '30px', width: '100%', 
                                borderRadius: '10px', display: 'flex'}}>
                        <Link onClick={() => this.props.history.push('/uploadKtp', {from: 'uploadKartuOwner'})}>
                            {/* <CameraAltIcon /> */}
                            <img src={camera}/>
                            <span><p style={{marginTop: '0px'}}>Ambil Foto KTP</p></span>
                        </Link>
                    </Box>
                    :
                    <Box style={{justifyContent: 'center', textAlign: 'center', alignItems: 'center', 
                                marginTop: '5px', minHeight: '200px', backgroundColor: '#e2f2ff', 
                                marginInlineStart: '30px', marginInlineEnd: '30px', width: '100%', 
                                borderRadius: '10px', display: 'flex'}}>
                        <img src={this.state.imgKtp} style={{height: '200px', width: '100%', minHeight: '200px', 
                            boxShadow: '0 2px 0 rgba(90,97,105,.11), 0 4px 8px rgba(90,97,105,.12), 0 10px 10px rgba(90,97,105,.06), 0 7px 70px rgba(90,97,105,.1)',
                            borderRadius: '.625rem'}}/>
                    </Box>
                    
                    )}
                </Grid>
                <br/>
                <Grid container spacing={2}>
                    <Grid item xs={8} style={{paddingRight: '0px'}}>
                        {this.state.isErrorImgKtpWajah ? 
                            <span style={{marginLeft: '30px', fontSize: '16px', color: 'red', lineHeight: '24px', fontWeight: 'bold'}}>
                                    Foto Wajah dengan KTP harus diisi</span> :
                            <span style={{marginLeft: '30px', fontSize: '16px', color: '#3A4D5B', 
                                lineHeight: '24px', fontWeight: 'bold'}}>Foto diri dengan KTP</span>
                        }
                    </Grid>
                    {this.state.imgKtpWajah != null && 
                    <Grid item xs={3}>
                        <Link onClick={() => this.props.history.push('/uploadWajahKtp', {from: 'uploadKartuOwner'})}
                            style={{display: 'flex', flexDirection: 'row', justifyContent: 'center', alignItems: 'center'}}>
                            {/* <EditIcon style={{fontSize: "1rem", paddingRight: '3px'}} />  */}
                            <img src={edit2} height={20} width={20} style={{marginRight:'5.35px'}}/>
                            <p style={{margin: '0px'}}>Ubah</p>
                        </Link>
                    </Grid>
                    }
                </Grid>
                <Grid container wrap="nowrap"  style={{justifyContent: 'center', alignItems: 'center', 
                        display: 'flex', marginTop: '13px', marginBottom: '30px'}}>
                    {loading ? (<Skeleton animation="wave" variant="rect" height={200}></Skeleton>) : 
                    (
                        this.state.imgKtpWajah == null ? <Box className={this.state.errorClassImgKtpWajah} style={{justifyContent: 'center', textAlign: 'center', alignItems: 'center', 
                        marginTop: '5px', minHeight: '200px', backgroundColor: '#e2f2ff', 
                        marginInlineStart: '30px', marginInlineEnd: '30px', width: '100%', 
                        borderRadius: '10px', display: 'flex'}}>
                            {/* className={classImgWajah}  */}
                            <Link onClick={() => this.props.history.push('/uploadWajahKtp', {from: 'uploadKartuOwner'})}>
                                {/* <CameraAltIcon /> */}
                                <img src={camera}/>
                                <span><p style={{marginTop: '0px'}}>Ambil Foto diri dengan KTP</p></span>
                            </Link>
                        </Box>
                        :
                        <Box style={{justifyContent: 'center', textAlign: 'center', alignItems: 'center', 
                        marginTop: '5px', minHeight: '200px', backgroundColor: '#E5E8EA', 
                        marginInlineStart: '30px', marginInlineEnd: '30px', width: '100%', 
                        borderRadius: '10px', display: 'flex'}}>
                            <img src={this.state.imgKtpWajah} style={{height: '200px', width: '45%',  minHeight: '200px',
                            boxShadow: '0 2px 0 rgba(90,97,105,.11), 0 4px 8px rgba(90,97,105,.12), 0 10px 10px rgba(90,97,105,.06), 0 7px 70px rgba(90,97,105,.1)'}}/>
                        </Box>
                    )}
                </Grid>
                <br/>
                <Grid container spacing={2}>
                    <Grid item xs={8}>
                        {this.state.isErrorImgNpwp ? 
                            <span style={{marginLeft: '30px', fontSize: '16px', color: 'red', lineHeight: '24px', fontWeight: 'bold'}}>
                                    Foto NPWP harus diisi</span> :
                            <span style={{marginLeft: '30px', fontSize: '16px', color: '#3A4D5B', lineHeight: '24px', fontWeight: 'bold'}}>Foto NPWP</span>
                        }
                    </Grid>
                    {this.state.imgNpwp != null && 
                    <Grid item xs={3}>
                        <Link onClick={() => this.props.history.push('/uploadNpwp', {from: 'uploadKartuOwner'})}
                            style={{display: 'flex', flexDirection: 'row', justifyContent: 'center', alignItems: 'center'}}>
                            {/* <EditIcon style={{fontSize: "1rem", paddingRight: '3px'}} />  */}
                            <img src={edit2} height={20} width={20} style={{marginRight:'5.35px'}}/>
                            <p style={{margin: '0px'}}>Ubah</p>
                        </Link>
                    </Grid>
                    }
                </Grid>
                <Grid container wrap="nowrap" style={{justifyContent: 'center', alignItems: 'center', 
                        display: 'flex', marginTop: '13px'}}>
                    {loading ? (<Skeleton animation="wave" variant="rect" height={200}></Skeleton>) : 
                    (
                        this.state.imgNpwp == null || this.state.cekNpwp ? <Box className={this.state.errorClassImgNpwp} style={{justifyContent: 'center', textAlign: 'center', alignItems: 'center', 
                        marginTop: '5px', minHeight: '200px', backgroundColor: '#e2f2ff', 
                        marginInlineStart: '30px', marginInlineEnd: '30px', width: '100%', 
                        borderRadius: '10px', display: 'flex'}}>
                            <Link onClick={() => this.props.history.push('/uploadNpwp', {from: 'uploadKartuOwner'})}>
                                {/* <CameraAltIcon /> */}
                                <img src={camera}/>
                                <span><p style={{marginTop: '0px'}}>Ambil Foto NPWP</p></span>
                            </Link>
                        </Box>
                        :
                        <Box style={{justifyContent: 'center', textAlign: 'center', alignItems: 'center', 
                                marginTop: '5px', minHeight: '200px', backgroundColor: '#e2f2ff', 
                                marginInlineStart: '30px', marginInlineEnd: '30px', width: '100%', 
                                borderRadius: '10px', display: 'flex'}}>
                        <img src={this.state.imgNpwp} style={{height: '200px', width: '100%',
                            boxShadow: '0 2px 0 rgba(90,97,105,.11), 0 4px 8px rgba(90,97,105,.12), 0 10px 10px rgba(90,97,105,.06), 0 7px 70px rgba(90,97,105,.1)',
                            borderRadius: '.625rem'}}/>
                    </Box>
                    )}
                </Grid>

                {!this.state.imgNpwp && <FormControlLabel style={{position: 'relative', marginInlineStart: '10px', marginTop: '18px', 
                                fontSize: '14px', lineHeight: '21px', color: '#303B4A',
                                opacity: 'unset'}}
                    control={
                    <Checkbox
                        checked={this.state.cekNpwp}
                        onChange={handleChangeCek}
                        icon={<CheckBoxOutlineBlankIcon fontSize="small" />}
                        checkedIcon={<CheckBoxIcon fontSize="small" />}
                        name="npwp"
                    />
                    }
                    label="Saya tidak punya atau tidak membawa NPWP"
                    />}

                    {this.state.imgNpwp === null && !this.state.cekNpwp && 
                        <Box style={{marginTop: '100px', marginBottom: '32px', 
                            position: 'absolute', width: '100vw', left: '20px'}}>
                            <Button onClick={submitNonNpwp} type="button" style={{position: 'absolute', position: 'absolute', bottom: '10px', 
                                    marginInlineStart: '0px'}} 
                            variant="contained" disableElevation disabled={this.state.disBut} size="large" 
                            color="primary">
                                Kirim
                            </Button>
                        </Box>}
                    
                    {this.state.cekNpwp ?
                    this.state.imgNpwp === null && 

                    <Box style={{marginTop: '100px', marginBottom: '32px', 
                        position: 'absolute', width: '100vw', left: '20px'}}>
                        <Button onClick={submitNonNpwp} type="button" style={{position: 'absolute', position: 'absolute', bottom: '10px', 
                                    marginInlineStart: '0px'}} 
                            variant="contained" disableElevation disabled={this.state.disBut} size="large" 
                            color="primary">
                                Kirim
                        </Button>
                    </Box>

                    // <Box style={{marginInlineStart: '20px', marginInlineEnd: '20px', marginTop: '75px', marginBottom: '32px'}}>
                    //     <Button disabled={this.state.disBut} style={{position: 'absolute', bottom: 'unset', marginBottom: '30px', marginInlineStart: 'unset'}} 
                    //             onClick={submitNonNpwp} type="button" variant="contained" disableElevation size="large" color="primary">
                    //             Kirim
                    //     </Button>
                    // </Box> 
                    : 
                    // ////////////////////////////////////////////////////////////////////////
                    this.state.imgNpwp && <Formik 
                    initialValues={{
                        npwp: localStorage.getItem('npwp') && !this.state.cekNpwp ? localStorage.getItem('npwp') : ''
                    }}
                    validationSchema={Yup.object().shape({
                        // npwp: Yup.string().required('Harus diisi')
                        // .
                        // min(15, 'No NPWP harus 15 digit').
                        // max(15, 'No NPWP harus 15 digit')
                    })}
                    onSubmit={({npwp}, actions) => {
        
                        localStorage.setItem('cekNpwp', this.state.cekNpwp)

                    if(this.state.imgNpwp === null){
                        actions.setSubmitting(false);
                        this.setState({ errorMsgImgNpwp: "Foto NPWP harus diisi", 
                            errorClassImgNpwp: classes.errorImg, isErrorImgNpwp: true})
                        return false;
                    }else{
                        this.setState({isErrorImgNpwp: false, errorClassImgNpwp: classes.suksesImg, 
                            errorMsgNpwp: ""})
                    }

                    if(this.state.imgKtp === null){
                        actions.setSubmitting(false);
                        this.setState({ errorMsgImgKtp: "Foto KTP harus diisi", errorClassImgKtp: classes.errorImg, 
                                isErrorImgKtp: true})
                        return false;

                    }else{
                        this.setState({ errorMsgImgKtp: "", errorClassImgKtp: classes.suksesImg, 
                                isErrorImgKtp: false})
                    }

                    if(this.state.imgKtpWajah === null){
                        actions.setSubmitting(false);
                        this.setState({ errorMsgImgKtpWajah: "Foto KTP dengan Wajah harus diisi", 
                                    errorClassImgKtpWajah: classes.errorImg, isErrorImgKtpWajah: true})
                        return false;
                      
                    }else{
                        this.setState({ errorMsgImgKtpWajah: "", 
                                    errorClassImgKtpWajah: classes.suksesImg, isErrorImgKtpWajah: false})
                    }

                    this.setState({openAlert: true})

                    clearTimeout(this.timer);
                    if(this.state.query !== 'idle'){
                        this.setState({query: 'idle'});
                        return;
                    }
            
                    this.setState({query: 'progress'});
                    this.timer = setTimeout(() => {
                        this.setState({proses: 'success'})
                        this.setState({query: this.state.proses});
                        actions.setSubmitting(false);
                        if( new URLSearchParams(this.props.location.search).get("_onCorrection")==="yes"){  
                           this.props.history.push('/reviewData', {from: '/uploadKartuOwner'}) 
                        }
                        else{
                            this.props.history.push('/informasiUsaha', {from: '/uploadKartuOwner'})
                        }
                        
                        location.reload();
                    }, 3000);
    
                    }}
                    >
                        {({values, errors, isSubmitting, touched}) => (
                           
                            <Form autoComplete="off" style={{marginInlineStart: '30px', marginInlineEnd: '30px', marginTop: '16px'}}>
                                <Field
                                    component={formTextField}
                                    id="idnpwp"
                                    label="NPWP"
                                    name="npwp"
                                    variant="filled"
                                    value={this.state.cekNpwp ? '' : values.npwp}
                                    fullWidth
                                    //inputProps={{name: 'idnpwp', id: "idnpwp", value: localStorage.getItem('npwp')}}
                                    //className={values.npwp ? classes.addShrink : ''}
                                    //focused = {values.npwp ? true : false}
                                    //InputLabelProps={{ shrink: values.npwp || localStorage.getItem('npwp') ? true : false }}
                                    // InputLabelProps={{ classes: {label: localStorage.getItem('npwp') ? 'MuiInputLabel-shrink' : ''}
                                        
                                    // }}
                                     error ={localStorage.getItem('npwp') == null ? false :   localStorage.getItem('npwp').length == 20 ? false : true}
                                    // className={'form-control' + ( ? ' is-invalid' : '')}
                                />
                                 {localStorage.getItem('npwp') == null ? <div></div> : 
                                   localStorage.getItem('npwp').length == 20 ? <div></div> :
                                  <div className="invalid-feedback" > Panjang Karakter Npwp Harus 15 karakter</div>
                                 }

                                {/* {console.log('focused: ', focused)} */}
                                <ErrorMessage name="npwp" component="div" className="invalid-feedback" />
                                <Box style={{marginTop: '100px', marginBottom: '32px', 
                                    position: 'absolute', width: '100vw', left: '20px'}}>
                                    <Button style={{position: 'relative', bottom: '10px', 
                                                marginInlineStart: '0px'}} 
                                        type="submit" variant="contained" disableElevation disabled={isSubmitting || this.state.disBut} size="large" 
                                        color="primary">
                                            Kirim
                                    </Button>
                                </Box>

                                
                            </Form>
                        )}

                    </Formik>}
                    
                
                
                
            </div>
        )
    }
}

export default compose(
    withStyles(useStyles)
)(uploadKartuOwner)
