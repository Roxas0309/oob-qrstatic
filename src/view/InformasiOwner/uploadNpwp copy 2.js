import React, { Component } from 'react'
import Link from '@material-ui/core/Link';
import ArrowBackIcon from '@material-ui/icons/ArrowBack';
import { AppBar, Box, Button, Card, Dialog, FormControl, Grid, IconButton, MenuItem, Select, TextField, Toolbar, Typography, withStyles } from '@material-ui/core';
import Skeleton from '@material-ui/lab/Skeleton';
import CameraAltIcon from '@material-ui/icons/CameraAlt';
import Camera, { FACING_MODES } from '../../components/Camera';
import CloseIcon from '@material-ui/icons/Close';
import { compose } from 'recompose';
import { createBrowserHistory } from 'history';
import NPWP from '../../../public/img/npwp.png'
import Bingkai from '../../../public/img/bingkai.png'

const useStyles = theme => ({
    appBar: {
      position: 'relative',
      height: '40px',
      justifyContent: 'center'
    },
    title: {
      flex: 1,
      fontSize: '15px',
      textAlign: 'center',
    },
    formControl: {
      margin: theme.spacing(1),
      minWidth: 50,
      position: 'relative',
      zIndex: '1'
    }

  });

  const history = createBrowserHistory();

class uploadNpwp extends Component {

    constructor(props){
        super(props);

        this.state = {
            dataUri: '',
            openDialog: false,
            open: false,
            cam: '',
            imgFile: null,
            imgBlob: '',
            openFile: false,
        }

        this.takePhoto = React.createRef();
        this.handleOpen = React.createRef();
        this.handleClose = React.createRef();
        this.handleSlect = React.createRef();
        this.handleCloseSub = React.createRef();
        this.handleOpenSub = React.createRef();
        this.saveLocalStorage = React.createRef();
        this.saveLocalStorageFoto = React.createRef();
        this.onImageChange = this.onImageChange.bind(this)
        this.handleOpenFile = React.createRef();
        this.handleCloseFile = React.createRef();

    }

    getMyPhotoPlease = () => {
        localStorage.removeItem('pathImg')
        history.push({pathname : '/imageMaker',search : "?_query=imgNpwp&_canvasNumber=1&_originForm=uploadKartuOwner"});
        location.reload();
      }

      onImageChange = (event) => {
        if (event.target.files && event.target.files[0]) {
        //   this.setState({
        //     imgFile: URL.createObjectURL(event.target.files[0]), openFile: true,
        //     imgBlob: event.target.files[0]
        //   });

          let rd = new FileReader();
            var imgBlob = event.target.files[0]
            var val
            rd.readAsDataURL(imgBlob);
            rd.onload = () => {
                val = rd.result
                localStorage.setItem('pathImg', val)
            }

            history.push({pathname : '/imageMaker',search : "?_query=imgNpwp&_canvasNumber=1&_originForm=uploadKartuOwner"});
            location.reload();

          
        }
       }

    
  
    render() {
      
        
        return (
            <div>
                <div style={{marginLeft:'20px', marginTop:'20px'}}>
                    <Link href="/uploadKartuOwner">
                        <ArrowBackIcon/>
                    </Link>
                </div>
                <h4 style={{textAlign: 'center', color: '#121518', marginTop: '20px', fontSize: '18px', 
                    lineHeight: '27px', fontWeight: 'bold', fontFamily: 'Montserrat', marginBottom: '59px'}}>
                        Foto Kartu NPWP</h4>
                <Grid container wrap="nowrap" style={{justifyContent: 'center', alignItems: 'center', 
                        display: 'flex', marginTop: '30px'}}>
                   
                    <Box style={{textAlign: 'center', position: 'fixed', display: 'contents'}}>
                        <img src={NPWP} style={{ height: 120 }} />
                    </Box>
                </Grid>
                <Box style={{marginTop: '66px', marginInlineStart: '16px', marginInlineEnd: '16px'}}>
                    <ul className="fa-ul" style={{
                        fontSize: 'large',
                        letterSpacing: '0.2px',
                        lineHeight: 1.5,
                        marginRight: '10px'

                    }}>
                        <li style={{fontSize: '16px', color: '#6B7984', lineHeight: '24px', marginBottom: '9px'}}><span className="fa-li"><i className="fa fa-circle"></i></span>Pastikan foto NPWP tidak terpotong dan tulisan bisa terbaca jelas</li>
                    </ul>
                </Box>



                <Box style={{position: 'fixed', bottom: '20px', marginInlineStart: '20px', minWidth: '90%'}}>
                    <Button size="large" style={{width: "-webkit-fill-available", marginTop: '10px', height: '56px',
                                textTransform: 'none', marginBottom: '10px'}} variant="contained" color="primary"
                            onClick={this.getMyPhotoPlease}    >
                            {/* <CameraAltIcon style={{marginRight: '5px'}}/> */}
                              Ambil Foto
                        </Button>
                    <Button style={{ width: "-webkit-fill-available", marginTop: '10px', height: '56px',
                        textTransform: 'none', marginBottom: '10px', backgroundColor: '#E8F3FC', color: '#0057E7'
                        }} variant="contained" component="label">
                              Pilih Dari Galeri
                              <input type="file" accept="image/*" onChange={this.onImageChange} style={{display: 'none'}} />
                    </Button>
                </Box>
                
            </div>
        )
    }
}

export default compose(
    withStyles(useStyles)
) (uploadNpwp)
