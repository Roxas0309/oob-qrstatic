import React, { Component } from 'react'
import Link from '@material-ui/core/Link';
import ArrowBackIcon from '@material-ui/icons/ArrowBack';
import { AppBar, Box, Button, Grid, Toolbar, withStyles } from '@material-ui/core';
import Skeleton from '@material-ui/lab/Skeleton';
import { compose } from 'recompose';
import { blue } from '@material-ui/core/colors';
import Wajah  from '../../../public/img/iklan/wajah-ktp-new.png'

const useStyles = theme => ({
    appBar: {
      position: 'relative',
      height: '40px',
      justifyContent: 'center'
    },
    title: {
      flex: 1,
      fontSize: '15px',
      textAlign: 'center',
    },
    formControl: {
      margin: theme.spacing(1),
      width: '75%',
      position: 'relative',
      zIndex: '1'
    },
    fab: {
      position: 'absolute',
      bottom: theme.spacing(2),
      right: theme.spacing(2),
    },
    fabBlue: {
      color: theme.palette.common.white,
      backgroundColor: blue[500],
      '&:hover': {
        backgroundColor: blue[600],
      }
    }

  });

class uploadWajahKtp extends Component {

    constructor(props){
        super(props);

        this.state = {
            dataUri: '',
            openDialog: false,
            open: false,
            cam: '',
            disableBtn: true
        }

        
        this.takePhoto = React.createRef();
        this.handleOpen = React.createRef();
        this.handleClose = React.createRef();
        this.handleSlect = React.createRef();
        this.handleCloseSub = React.createRef();
        this.handleOpenSub = React.createRef();
        this.saveLocalStorage = React.createRef();
    }

    componentDidMount(){
      window.onpopstate = function(event) {
        window.location.reload();
    };
    }

    getMyPhotoPlease = () => {
        this.props.history.push({pathname : '/imageMaker',search : "?_query=imgKtpWajah&_fullScreenshoot=yes&_canvasNumber=2&_originForm=uploadKartuOwner", from: '/uploadWajahKtp'});
        location.reload();
      }
    render() {
        const {loading = false, classes} = this.props;

        this.saveLocalStorage = () => {
            
            localStorage.setItem('imgWajahKtp', this.state.dataUri);
            this.props.history.push('/uploadKartuOwner', {from: '/uploadWajahKtp'})
  
          }
  
          this.handleSlect = (event) => {
            this.setState({cam: event.target.value})
          }
  
          this.fotoUlang = () => {
            this.setState({dataUri: '', open: false, openDialog: true })
            localStorage.removeItem('imgWajahKtp')
            
          }
  
          this.takePhoto = (data) => {
              this.setState({dataUri: data, openDialog: true, open: false});
          }
          
          this.handleClose = () => {
              this.setState({open: false})
              localStorage.removeItem('imgWajahKtp')
          }
      
          this.handleOpen = () => {
              this.setState({open: true})
          }
  
          this.handleCloseSub = () => {
            this.setState({openDialog: false})
            localStorage.removeItem('imgWajahKtp')
        }
    
        this.handleOpenSub = () => {
            this.setState({openDialog: true})
        }

       

        return (
            <div style={{paddingInline: '16px', overflow: 'hidden'}}>
                <AppBar style={{backgroundColor: 'transparent', position: 'absolute', 
                    boxShadow: 'unset'}}>
                    <Toolbar onClick={() => 
                    {
                    this.props.history.push('/uploadKartuOwner', {from: '/uploadWajahKtp'})
                    location.reload();
                  }
                  } 
                      style={{minHeight: '60px'}}>
                        <Link style={{position: 'absolute'}} >
                            <ArrowBackIcon/>
                        </Link>
                    </Toolbar>
                </AppBar>
            
                <h4 style={{textAlign: 'center', color: '#121518', marginTop: '70px', fontSize: '18px', 
                    lineHeight: '27px', fontWeight: 'bold', fontFamily: 'Montserrat', marginBottom: '59px'}}>Foto Wajah dengan KTP</h4>
                <Grid container wrap="nowrap" style={{justifyContent: 'center', alignItems: 'center', 
                        display: 'flex', marginTop: '30px'}}>
                   
                    {loading ? (<Skeleton animation="wave" variant="rect" height={150}></Skeleton>) : 
                    (<Box style={{textAlign: 'center', position: 'fixed', display: 'contents'}}>
                        <img src={Wajah} style={{ height: 150 }} />
                    </Box>)}

                </Grid>
                <Box style={{marginTop: '40px'}}>
                    <ul className="fa-ul" style={{
                        fontSize: 'large',
                        letterSpacing: '0.2px',
                        lineHeight: 1.5,
                        marginRight: '5px'

                    }}>
                        <li style={{fontSize: '8px', color: '#6B7984', lineHeight: '24px', marginBottom: '9px'}}><span className="fa-li"><i className="fa fa-circle"></i></span>              <p style={{fontSize:"16px",marginLeft:'14px'}}>
Pegang KTP dibawah dagu</p></li>
                        <li style={{fontSize: '8px', color: '#6B7984', lineHeight: '24px', marginBottom: '9px'}}><span className="fa-li"><i className="fa fa-circle"></i></span>
                        <p style={{fontSize:"16px",marginLeft:'14px'}}>
Pastikan foto KTP menghadap ke kamera dan tidak menutupi wajah </p></li>
                    </ul>

                   
                    <Button size="large"  style={{
                          marginTop: "10px",
                          textTransform: "none",
                          marginBottom: "10px",
                          marginLeft:"4vw",
                          fontFamily: "Nunito",
                          fontSize: "16px",
                          fontWeight: "700",
                          alignItems:'center',
                          width: '91%',
                          backgroundColor: "rgba(0,87,231,1)",
                          marginInline: '0px'
                        }} disableElevation variant="contained" color="primary"
                            onClick={this.getMyPhotoPlease}>
                        {/* <CameraAltIcon style={{marginRight: '5px'}}/> */}
                          Ambil Foto
                    </Button>
                </Box>

            </div>
        )
    }
}

export default compose(
    withStyles(useStyles)
) (uploadWajahKtp)
