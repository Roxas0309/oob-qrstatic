import React from "react";
import * as cocoSsd from "@tensorflow-models/coco-ssd";
import * as tf from '@tensorflow/tfjs';
import * as mobilenet from "@tensorflow-models/mobilenet";
import "./tensor-style.css";
import Webcam from "react-webcam";
import * as knnClassifier from "@tensorflow-models/knn-classifier";
import predictator1 from  '../../../public/img/ktpprediction/predictator1.jpg';

export default class tensorflow extends React.Component {
  constructor(props){
    super(props)
    
    this.state = {
      baseUrl : null
    }
  }
  videoRef = React.createRef();
  canvasValidatorRef = React.createRef();
  componentDidMount() {
    if (navigator.mediaDevices && navigator.mediaDevices.getUserMedia) {
      var canvasValidator = document.querySelector('#canvas-validator');
      var refvid = this.videoRef.current;

      canvasValidator.style.top = ((window.innerHeight - (window.innerHeight / 2)) / 2) + "px";
      canvasValidator.style.left = "20px";

      const webCamPromise = navigator.mediaDevices.getUserMedia({
        audio: false,
        video: {
          facingMode: "environment",
          //width : window.innerWidth,
          height: window.innerHeight
        },
      })
        .then(stream => {
          window.stream = stream;

          this.videoRef.current.srcObject = stream;
          return new Promise((resolve, reject) => {
            this.videoRef.current.onloadedmetadata = () => {
              resolve();
            };
          });
        });
    }

     


    //this.loadClassifierAndModel();
  }

  async loadClassifierAndModel() {
    this.knn = knnClassifier.create();
    this.mobilenetModule = await mobilenet.load();
    console.log("model loaded");

  }

  //full screenshoot
  screenShoot = () => {

    var img = document.querySelector('img') || document.createElement('img');
    var video = document.querySelector('video');
    var width = video.offsetWidth
      , height = video.offsetHeight;
    var canvas = canvas || document.createElement('canvas');
    canvas.width = window.innerWidth;
    canvas.height = height;
    var context = canvas.getContext('2d');


    context.drawImage(video, 0, 0,  width, height);
    var baseurl = canvas.toDataURL('image/png');
    this.setState({ baseurl: baseurl });
  }

  screenShootCanvas = async () => {
    var img = document.querySelector('img') || document.createElement('img');
    var canvasValidator = document.querySelector('#canvas-validator');
    var video = document.querySelector('video');
    var width = video.offsetWidth
      , height = video.offsetHeight;
    var canvas = canvas || document.createElement('canvas');
    canvas.width = canvasValidator.offsetWidth;
    canvas.height = canvasValidator.offsetHeight;
    var context = canvas.getContext('2d');
    context.drawImage(video,  canvasValidator.offsetLeft,  
      canvasValidator.offsetTop,canvasValidator.width,canvasValidator.height,0,0
      ,canvas.width, canvas.height);
    var baseurl = canvas.toDataURL('image/png');
    this.setState({ baseurl: baseurl });

    var canvasValidator = document.querySelector('#canvas-validator');
    var img = document.createElement('img');
    img.width = canvasValidator.offsetWidth;
    img.height = canvasValidator.offsetHeight;
    img.src = predictator1;
    this.loadAllPredictorImage();
  }


  predictimage = async () => {
   var img = document.createElement('img');
    img.width = canvasValidator.offsetWidth;
    img.height = canvasValidator.offsetHeight;
    img.src = predictator1;
    debugger;
    console.log("Model loading...");
    const model = await mobilenet.load();
    console.log("Model is loaded!")
    const predictions = await model.classify(img);
    console.log('Predictions: ', predictions);
    alert('Predictions: ' + JSON.stringify(predictions));
    document.getElementById("#tested-id").appendChild(img);
  }

  loadAllPredictorImage = async() =>{
    const classifier = knnClassifier.create();
    const mobilenetss = await mobilenet.load();

    var canvasValidator = document.querySelector('#canvas-validator');
    var img = document.createElement('img');
    img.width = window.innerWidth - 40;
    img.height = window.innerHeight/3;
    img.src = predictator1;

    const tfImg = tf.browser.fromPixels(img);
    classifier.addExample(mobilenetss.infer(tfImg,'conv_preds'),0);
    classifier.addExample(mobilenetss.infer(tfImg,'conv_preds'),1);
    console.log('finish classifier'); 


    var imgBaru = document.getElementById('image-save');
    const tfImgBaru = tf.browser.fromPixels(imgBaru);
  
    const xlogits = await mobilenetss.infer(tfImgBaru, 'conv_preds');
    console.log('Predictions:');
    
    const predictions = await mobilenetss.classify(tfImgBaru);
    console.log('Predictions: ', predictions);
    // var exampleCount = await this.classifier.predictClass(xlogits);
    // console.log(exampleCount);
  }


  videoPhotoShoot = () => {
    if(this.state.baseurl==null || this.state.baseurl=='' ){
    return <span> <video
      onClick={this.screenShootCanvas}
      className="size"
      autoPlay
      muted
      ref={this.videoRef}
    />
      <canvas id="canvas-validator"
        onClick={this.screenShootCanvas}
        className="size-validator"
        ref={this.canvasValidatorRef}
        width={window.innerWidth - 40}
        height={window.innerHeight / 3}
      />
    </span>
    }else{
      return <div>
      <img id="image-save" src={this.state.baseurl}></img>
      </div>
    }
  }


  render() {
    return (
      <div id="tested-id">
        <this.videoPhotoShoot></this.videoPhotoShoot>
        {/* <section   className="size">
  <button className="button">Left</button>

  <button className="button">Right</button>

  <button className="button">Test</button>
</section> */}
      </div>

    );
  }
}
