import React from "react";
import * as cocoSsd from "@tensorflow-models/coco-ssd";
import "@tensorflow/tfjs";
import "./tensor-style.css";
import Webcam from "react-webcam";


export default class tensorflow extends React.Component {
 
    videoRef = React.createRef();
    canvasRef = React.createRef();
    canvasValidatorRef = React.createRef();
    componentDidMount() {
      if (navigator.mediaDevices && navigator.mediaDevices.getUserMedia) {
       var canvasValidator = document.querySelector('#canvas-validator');
       var refvid = this.videoRef.current;
     
       canvasValidator.style.top = ((window.innerHeight - (window.innerHeight/2))/2)  +"px";
       canvasValidator.style.left = "20px"; 
     

        const webCamPromise = navigator.mediaDevices
          .getUserMedia({
            audio: false,
            video: {
              facingMode: "environment",
         //     width : window.innerWidth,
              height : window.innerHeight
            },
          })
          .then(stream => {
            window.stream = stream;
          
            this.videoRef.current.srcObject = stream;
            return new Promise((resolve, reject) => {
              this.videoRef.current.onloadedmetadata = () => {
                resolve();
              };
            });
          });
        const modelPromise = cocoSsd.load();
        Promise.all([modelPromise, webCamPromise])
          .then(values => {
            this.detectFrame(this.videoRef.current, values[0]);
            var cal = values[0];
          })
          .catch(error => {
            console.error(error);
          });
      }
    }
  
    detectFrame = (video, model) => {
      model.detect(video).then(predictions => {
        this.renderPredictions(predictions);
        requestAnimationFrame(() => {
          this.detectFrame(video, model);
        });
      });
    };
  
    renderPredictions = predictions => {
      const ctx = this.canvasRef.current.getContext("2d");
      ctx.clearRect(0, 0, ctx.canvas.width, ctx.canvas.height);
      // Font options.
      const font = "16px sans-serif";
      ctx.font = font;
      ctx.textBaseline = "top";
      predictions.forEach(prediction => {
        const x = prediction.bbox[0];
        const y = prediction.bbox[1];
        const width = prediction.bbox[2];
        const height = prediction.bbox[3];
       // debugger;
        // Draw the bounding box.
        ctx.strokeStyle = "#0080ff";
        ctx.lineWidth = 4;
        ctx.strokeRect(x, y, width, height);
        // Draw the label background.
        ctx.fillStyle = "#0080ff";
        const textWidth = ctx.measureText(prediction.class).width;
        const textHeight = parseInt(font, 10); // base 10
        ctx.fillRect(x, y, textWidth + 4, textHeight + 4);
      });
  
      predictions.forEach(prediction => {
        const x = prediction.bbox[0];
        const y = prediction.bbox[1];
        // Draw the text last to ensure it's on top.
        ctx.fillStyle = "#000000";
        ctx.fillText(prediction.class, x, y);
      });
    };

    screenShoot = () =>{
      var img = document.querySelector('img') || document.createElement('img');
      var video = document.querySelector('video');
      var width = video.offsetWidth
      , height = video.offsetHeight;
      var canvas = canvas || document.createElement('canvas');
      canvas.width = width;
      canvas.height = height;
      var context = canvas.getContext('2d');
      
      context.drawImage(video, 0, 0, width, height);
      var baseurl = canvas.toDataURL('image/png');
      this.setState({baseurl:baseurl});
    }


    constructor(props){
      super(props);
      this.state = {
        baseurl : ""
      }
    }
  
    render() {
      return (
        <div>
          <video
            onClick={this.screenShoot}
            className="size"
            autoPlay
            muted
            ref={this.videoRef}
           
          />
          <canvas
            onClick={this.screenShoot}
            className="size"
            ref={this.canvasRef}
            width={ window.innerWidth}
            height={ window.innerHeight}
          />

          <canvas id="canvas-validator"
            onClick={this.screenShoot}
            className="size-validator"
            ref={this.canvasValidatorRef}
            width={ window.innerWidth-40}
            height={ window.innerHeight/3}
          />
          <button onClick={this.screenShoot}>klik me</button>

          <img style={{position:"relative"}}
          src={this.state.baseurl} />
        </div>
      );
    }
  }
  