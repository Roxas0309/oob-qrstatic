import React, { Fragment, useRef } from 'react';
import Webcam from "react-webcam";

const videoConstraints = {
  width: 300,
  height: 300,
  facingMode: "user"
};
 
const WebcamCapture = () => {

  

  const webcamRef = React.useRef(null);
  var imageSrc = null;
  const capture = React.useCallback(
    () => {
       imageSrc = webcamRef.current.getScreenshot();
      debugger;
    },
    [webcamRef]
  );
 
  return (
    <>
      <Webcam
        audio={false}
        height={300}
        ref={webcamRef}
        screenshotFormat="image/jpeg"
        width={300}
        videoConstraints={videoConstraints}
      />
      <button onClick={capture}>Capture photo</button>

      <img src = {imageSrc}>
      </img>
    </>
  );
};

export default class testingpicture extends React.Component {
 constructor(props){
   super(props);
   this.state = {
     imagedb : ''
   }
 }


  capture = () => {
    const base64 = this.webcam.getScreenshot();
    this.setState({imagedb : base64})
  }

  componentDidMount(){
    setInterval(this.capture, 1000);
  }
  

  render() {

    return (
        <div>
        <Webcam
        onChange = {this.capture}
        onClick = {this.capture}
        audio={false}
        height={window.innerHeight}
        ref={webcam => this.webcam = webcam}
        screenshotFormat="image/jpeg"
        width={window.innerWidth}
        videoConstraints={{
          width: window.innerWidth,
          height: window.innerHeight,
          facingMode: "user"
        }}
      />
     <button onClick={this.capture}>Capture photo</button>

<img src = {this.state.imagedb}>
</img>

        </div>
    )
  }
}