import React, { Fragment, useRef } from 'react';
import ReactDOM from 'react-dom';
import { Camera } from 'react-cam';


export default class testingpicture extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      imgs: ""
    }
  }


  capture = (imgSrc) => {
    this.setState({
      imgs: imgSrc
    })
  }

  render() {

    return (
      <div>

        <Fragment >
          <Camera
            front={false}
            capture={this.capture}
            style={{ height: 1000, width: 1000 }}
            focusWidth="100%"
            focusHeight="50%"
            ref={(cam) => {
              this.camera = cam;
            }}
          />
          <button onClick={img => this.camera.capture(img)}>Take image</button>
        </Fragment>


        <img
          style={style.captureImage}
          src={this.state.imgs} />
      </div>


    )
  }
}



const style = {
  preview: {
    position: 'relative',
  },
  captureContainer: {
    display: 'flex',
    position: 'absolute',
    justifyContent: 'center',
    zIndex: 1,
    bottom: 0,
    width: '100%'
  },
  captureButton: {
    backgroundColor: '#fff',
    borderRadius: '50%',
    height: 56,
    width: 56,
    color: '#000',
    margin: 20
  },
  captureImage: {
    width: '100%',
  }
};