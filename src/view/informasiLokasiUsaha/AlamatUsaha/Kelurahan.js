import React, { useEffect, useState } from "react";
import {
  Grid,
  CircularProgress,
  List,
  ListItem,
  ListItemText,
  Link,
  Box,
  InputBase,
  ListItemSecondaryAction,
  IconButton,
} from "@material-ui/core";
import ChevronRightIcon from '@material-ui/icons/ChevronRight';
import { fade, makeStyles } from "@material-ui/core/styles";
import { toFirstCharUppercase } from "../../../constant/constants";
import SearchIcon from "@material-ui/icons/Search";
import axios from "axios";
import ArrowBackIcon from '@material-ui/icons/ArrowBack';
import Skeleton from "@material-ui/lab/Skeleton";
import { BACK_END_POINT, HEADER_AUTH, GET,PUT, TIMES_OUT, wsWithBody, wsWithoutBody } from "../../../master/masterComponent";

const useStyles = makeStyles((theme) => ({
  search: {
    border: 'solid 0.5px darkgray',
    marginInlineStart: '20px',
    width: '90%',
    position: 'relative',
    borderRadius: theme.shape.borderRadius,
    backgroundColor: fade(theme.palette.common.white, 0.15),
    '&:hover': {
      backgroundColor: fade(theme.palette.common.white, 0.25),
    },
    [theme.breakpoints.up('sm')]: {
      marginLeft: theme.spacing(3),
      width: 'auto',
    },
  },
  searchIcon: {
    padding: theme.spacing(0, 2),
    height: '100%',
    position: 'absolute',
    pointerEvents: 'none',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
  },
  inputRoot: {
    color: 'inherit',
  },
  inputInput: {
    padding: theme.spacing(1, 1, 1, 0),
    // vertical padding + font size from searchIcon
    paddingLeft: `calc(1em + ${theme.spacing(4)}px)`,
    transition: theme.transitions.create('width'),
    width: '100%',
    [theme.breakpoints.up('md')]: {
      width: '20ch',
    },
  },
}));

const Kelurahan = (props) => {
  const classes = useStyles();
  const { match, history } = props;
  const { params } = match;
  const { kecId, kecName, kotId, kotName, provId, provName } = params;
  const [kelData, setkelData] = useState({});
  const [filter, setFilter] = useState("");
  var kelName = "";
  var kelIdKey = "";
  var val = kecName.toString();
  var valKec = val.replaceAll('%20',' '); 


  useEffect(() => {
      wsWithoutBody(BACK_END_POINT+"/Alamat/FindAll/Kelurahan/ByIdKecamatan?id_kecamatan="+kecId, GET, HEADER_AUTH)
      .then(response=>{
        const { data } = response;
        console.log('data kel: ', data)
        const newkelData = {};
        data.kelurahan.forEach((kel, index) => {
          newkelData[index + 1] = {
            id: kel.id,
            nama: kel.nama
          };
        });
        setkelData(newkelData);        
      })
  }, []);

  const handleSearchChange = (e) => {
    setFilter(e.target.value);
  };

  const handleNext = () => {
    var kelnm = kelName.replaceAll('%20',' ')
    var kotnm = kotName.replaceAll('%20',' ')
    var provnm = provName.replaceAll('%20',' ')

    localStorage.setItem('localProv', provnm)
    localStorage.setItem('localKot', kotnm)
    localStorage.setItem('localKec', valKec)
    localStorage.setItem('localKel', kelnm)
    localStorage.setItem('localKelId',kelIdKey)
    history.push('/informasiLokasiUsaha');
    location.reload();
  };


  const getKecList = (kelId) => {
    const { id, nama } = kelData[kelId];
    kelName = nama;
    kelIdKey = id;
    return (
      <List key={kelId}
            style={{marginInlineEnd: '20px'}}>
        <ListItem style={{border: 'solid 0.5px darkgray',borderRadius: '10px', marginInlineStart: '10px'}}>
          <ListItemText primary={toFirstCharUppercase(nama)}  onClick={handleNext} />
          <ListItemSecondaryAction style={{right: '0px'}}>
            <IconButton onClick={handleNext}  edge="end" aria-label="delete">
              <ChevronRightIcon />
            </IconButton>
          </ListItemSecondaryAction>
        </ListItem>
      </List>
    );
  };

  return (
    <>
      <div style={{position: 'fixed', display: 'flex', minWidth: '100%', justifyContent: 'center', alignItems: 'center'}}>
          <Link onClick={() => window.location.pathname=`/kecamatan/${kotId}/${kotName}/${provId}/${provName}`} style={{left: '10px', position: 'fixed', top: '10px'}}>
              <ArrowBackIcon style={{left: '20px'}}/>
          </Link>
          <span style={{display: 'flex', textAlign: 'center', 
              justifyContent: 'center', alignItems: 'center', 
              position: 'fixed', top: '10px', fontSize: 'smaller', color: 'lightslategray'}}>{valKec}</span>
      </div>
      <Box style={{marginTop: '50px', marginBottom: '20px'}}>
        <h3 style={{textAlign: 'center', marginTop: '0px', marginInlineStart: '10px', 
              marginInlineEnd: '10px'}}>Kelurahan apa di {valKec} ?</h3>
          <div className={classes.search}>
              <div className={classes.searchIcon}>
                  <SearchIcon />
              </div>
              <InputBase
                  onChange={handleSearchChange}
                  placeholder="Cari..."
                  classes={{
                      root: classes.inputRoot,
                      input: classes.inputInput,
                  }}
                  inputProps={{ 'aria-label': 'search' }}
              />
          </div>
      </Box>
      {kelData ? (
        <Grid>
          {Object.keys(kelData).map(
            (id) =>
              kelData[id].nama.includes(filter) &&
              getKecList(id)
          )}
        </Grid>
      ) : (
        <div style={{marginInlineStart: '20px', marginInlineEnd: '20px'}}>
          <Skeleton variant="text" height={100} />
          <Skeleton variant="text" height={100} />
          <Skeleton variant="text" height={100} /> 
          <Skeleton variant="text" height={100} />
          <Skeleton variant="text" height={100} />
          <Skeleton variant="text" height={100} />
          <Skeleton variant="text" height={100} />
          <Skeleton variant="text" height={100} />
        </div>
        // <CircularProgress style={{width: '350px', height: '70px'}} />
      )}
    </>
  );
};

export default Kelurahan;
