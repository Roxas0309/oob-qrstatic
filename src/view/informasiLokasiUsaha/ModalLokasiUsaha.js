import React, { useEffect } from 'react';
import { AppBar, Toolbar, Dialog, Link, IconButton, InputAdornment, InputLabel, List, ListItem, ListItemText, OutlinedInput, Typography } from '@material-ui/core';
import CloseIcon from '@material-ui/icons/Close';
import SearchIcon from '@material-ui/icons/Search';
import { BACK_END_POINT, GET, HEADER_AUTH, wsWithoutBody } from '../../master/masterComponent';
   
function ModalLokasiUsaha(props) {
    const { isShowModal, toggleClose, handler, valuePer} = props
    const [filter, setFilter] = React.useState("");

    const dataLok = (id) => {

        const { idName, nama } = valuePer[id]
            return (
                <List className="listModal" key={id} >
                        <ListItem button type="button" value={idName, nama}
                            onClick={() => {handler(idName, nama); toggleClose()}} >
                            <ListItemText value={nama} primary={
                                 <Typography 
                                 style={{ fontFamily: 'Nunito',fontSize: '16px', fontWeight: '400', 
                                 lineHeight: '24px', color: '#1A1A1A' }}>{nama}</Typography>
                                }/>
                    </ListItem> 
                        
                </List>
                )
    
    }

    const closeJenis = () => {
        return toggleClose, setFilter("")
    }

    return (
        <Dialog fullScreen style={{height: '460px', width: '100%', position: 'fixed',top: 'unset' ,bottom: '0px'}} onClose={toggleClose} open={isShowModal}>
            
            <AppBar style={{backgroundColor: 'transparent', position: 'relative', 
                        boxShadow: 'unset'}}>
                    <Toolbar style={{minHeight: '60px'}}>
                        <div style={{width: '100%', textAlign: 'left'}}>
                            <Typography style={{fontFamily: 'Nunito', fontSize: '16px',
                        fontWeight: '700', lineHeight: '24px', color: '#000000'}}>Lokasi Usaha</Typography>
                        </div>
                        <Link onClick={toggleClose} style={{position: 'absolute',
                            alignItems: 'center', display: 'flex', right: '26px'}}>
                            <CloseIcon/>
                        </Link>
                    </Toolbar>
                </AppBar>
            {/* <IconButton onClick={toggleClose} edge="start" color="inherit" aria-label="close"
                style={{position:'absolute', width: '20px', right: '20px', zIndex: 1, top: '10px', color: '#ADA6AE'}}>
                <CloseIcon />
            </IconButton>
            <Box style={{height: '50px', justifyContent: 'left', alignItems: 'center', 
                display: 'flex', marginBottom: '20px'}}>
                <p style={{fontSize: '16px', lineHeight: '24px', fontWeight: '700', 
                    marginTop: '20px', marginBottom: '0px', marginLeft: '16px'}}>Lokasi Usaha</p>
            </Box> */}
            {/* <FormControl variant="outlined" style={{margin: '10px'}}>
                <InputLabel htmlFor="outlined-adornment">Lokasi Usaha</InputLabel>
                <OutlinedInput
                    id="outlined-adornment"
                    placeholder="Cari Jenis Usaha"
                    label="Jenis Usaha"
                    autoFocus={true}
                    value={filter}
                    onChange={handleSearchChange}
                    startAdornment={<InputAdornment position="start"><SearchIcon /></InputAdornment>}
                    labelWidth={60}
                />
            </FormControl> */}

            {/* {isLoading ? 
                <CircularProgress style={{justifyContent: 'center', alignItems: 'center', alignSelf: 'center'}}/>
                : */}
                {valuePer ?
                (Object.keys(valuePer).map((id) => valuePer[id].nama.includes(filter) &&
                    dataLok(id)))
                    :
                <List >
                    <ListItem  >
                        <ListItemText primary={isAlert}/>
                    </ListItem> 
                </List>}
            {/* } */}
        </Dialog>
    )
}

export default ModalLokasiUsaha
