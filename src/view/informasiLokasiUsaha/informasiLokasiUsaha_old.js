import React, { Component } from "react";
import {
  AppBar,
  Box,
  Button,
  Chip,
  CircularProgress,
  Dialog,
  fade,
  FormControl,
  IconButton,
  Input,
  InputBase,
  InputLabel,
  Link,
  List,
  ListItem,
  ListItemIcon,
  ListItemSecondaryAction,
  ListItemText,
  Select,
  TextareaAutosize,
  TextField,
  Toolbar,
  Typography,
  withStyles,
} from "@material-ui/core";
import TidakPermanen from "../../../public/img/LogoTokoNonPermanen.png";
import Permanen from "../../../public/img/LogoTokoPermanen.png";
import ArrowBackIcon from "@material-ui/icons/ArrowBack";
import CloseIcon from "@material-ui/icons/Close";
import { createBrowserHistory } from "history";
import ChevronRightIcon from "@material-ui/icons/ChevronRight";
import SearchIcon from "@material-ui/icons/Search";
import { compose } from "recompose";
import { Autocomplete } from "@material-ui/lab";
import {
  BACK_END_POINT,
  GET,
  HEADER_AUTH,
  wsWithoutBody,
} from "../../master/masterComponent";
import ModalLokasiUsahaNon from "./ModalLokasiUsahaNon";
import ArrowDropDownIcon from "@material-ui/icons/ArrowDropDown";

const ITEM_HEIGHT = 48;
const ITEM_PADDING_TOP = 8;
const MenuProps = {
  PaperProps: {
    style: {
      maxHeight: ITEM_HEIGHT * 4.5 + ITEM_PADDING_TOP,
      width: 250,
    },
  },
};

const lokasiUsahaNonPermanen = [
  "Gerobak",
  "Tenda Kaki Lima",
  "Pedagang Keliling",
  "Pedagang Lapak",
  "Pasar Malam /Pagi",
];

const history = createBrowserHistory();

const useStyles = (theme) => ({
  search: {
    border: "solid 0.5px darkgray",
    marginInlineStart: "20px",
    width: "90%",
    position: "relative",
    borderRadius: theme.shape.borderRadius,
    backgroundColor: fade(theme.palette.common.white, 0.15),
    "&:hover": {
      backgroundColor: fade(theme.palette.common.white, 0.25),
    },
    [theme.breakpoints.up("sm")]: {
      marginLeft: theme.spacing(3),
      width: "auto",
    },
  },
  searchIcon: {
    padding: theme.spacing(0, 2),
    height: "100%",
    position: "absolute",
    pointerEvents: "none",
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
  },
  inputRoot: {
    color: "inherit",
  },
  inputInput: {
    padding: theme.spacing(1, 1, 1, 0),
    // vertical padding + font size from searchIcon
    paddingLeft: `calc(1em + ${theme.spacing(4)}px)`,
    transition: theme.transitions.create("width"),
    width: "100%",
    [theme.breakpoints.up("md")]: {
      width: "20ch",
    },
  },
});
class informasiLokasiUsaha extends Component {
  constructor(props) {
    super(props);

    this.state = {
      selIdx: 0,
      openLokasi: false,
      openJenis: false,
      openProvinsi: false,
      loadingProvinsi: false,
      openKota: false,
      loadingKota: false,
      openKelurahan: false,
      loadingKelurahan: false,
      openKecamatan: false,
      loadingKecamatan: false,
      valLokasiUsahaNonPermanen: "",
      provKel: false,
      localProv: "",
      localProvId: "",
      localKot: "",
      localKotId: "",
      localKec: "",
      localKecId: "",
      localKel: "",
      localKelId: "",
      valProv: "",
      lokasiUsahaPermanen: "",
      listProvinsi: [],
      listProvinsiSearch: [],
      listKota: [],
      listKotaSearch: [],
      listKelurahan: [],
      listKelurahanSearch: [],
      listKecamatan: [],
      listKecamatanSearch: [],
      listLokasiUsahaPermanen: [],
      listLokasiUsahaNonPermanen: [],
    };

    this.handleGetDataLokasi = this.handleGetDataLokasi.bind(this);
  }

  componentDidMount() {
    if (localStorage.getItem("kategorUsahaIdx") == null) {
      this.setState({ selIdx: 0 });
      localStorage.setItem("kategorUsahaIdx", 0);
    } else {
      var x = localStorage.getItem("kategorUsahaIdx");
      this.setState({ selIdx: parseInt(x, 10) });
    }

    if (localStorage.getItem("lokasiUsahaPermanen")) {
      this.setState({
        lokasiUsahaPermanen: localStorage.getItem("lokasiUsahaPermanen"),
      });
    }
    // this.setState({ lokasiUsahaPermanen: localStorage.getItem('lokasiUsahaPermanen') });

    if (localStorage.getItem("valLokasiUsahaNonPermanen")) {
      this.setState({
        valLokasiUsahaNonPermanen: localStorage.getItem(
          "valLokasiUsahaNonPermanen"
        ),
      });
    }

    if (localStorage.getItem("localProv") != null) {
      this.setState({ localProv: localStorage.getItem("localProv") });
    }

    if (localStorage.getItem("localKot") != null) {
      this.setState({ localKot: localStorage.getItem("localKot") });
    }
    if (localStorage.getItem("localKec") != null) {
      this.setState({ localKec: localStorage.getItem("localKec") });
    }
    if (localStorage.getItem("localKel") != null) {
      this.setState({ localKel: localStorage.getItem("localKel") });
    }

    wsWithoutBody(
      BACK_END_POINT + "/usahaProperties/lokasiPermanenUsaha",
      GET,
      HEADER_AUTH
    )
      .then((response) => {
        this.setState({ listLokasiUsahaPermanen: response.data });
      })
      .catch((error) => {
        alert(error.response.data.message);
      });

    wsWithoutBody(
      BACK_END_POINT + "/usahaProperties/lokasiNonPermanenUsaha",
      GET,
      HEADER_AUTH
    )
      .then((response) => {
        this.setState({ listLokasiUsahaNonPermanen: response.data });
      })
      .catch((error) => {
        alert(error.response.data.message);
      });
  }

  handleGetDataLokasi = (key, value) => {
    console.log("handle value: ", value);
    this.setState({ valLokasiUsahaNonPermanen: value });
    localStorage.setItem("valLokasiUsahaNonPermanenKey", key);
    localStorage.setItem("valLokasiUsahaNonPermanen", value);
  };

  render() {
    const { loading = false, classes } = this.props;

    const handleListItemClick = (event, index) => {
      this.setState({ selIdx: index });
      localStorage.setItem("kategorUsahaIdx", index);
    };

    let dataNon;

    if (!this.state.valLokasiUsahaNonPermanen) {
      dataNon = "";
    } else {
      dataNon = this.state.valLokasiUsahaNonPermanen;
    }

    const handleOpenLokasi = () => {
      this.setState({ openLokasi: true });
    };

    const handleCloseLokasi = () => {
      this.setState({ openLokasi: false });
    };
    const handleCloseJenis = () => {
      this.setState({ openJenis: false });
    };

    const handleCloseAllDaerah = () => {
      this.setState({
        openProvinsi: false,
        openKota: false,
        openKecamatan: false,
        openKelurahan: false,
      });
    };

    const handleOpenJenisUsaha = () => {
      this.setState({ openJenis: true });
    };

    const handleChange = (event) => {
      this.setState({ valLokasiUsahaNonPermanen: event.target.value });
    };

    const handleOpenProv = () => {
      this.setState({ openProvinsi: true, loadingProvinsi: true });
      wsWithoutBody(
        BACK_END_POINT + "/Alamat/FindAll/Provinsi",
        GET,
        HEADER_AUTH
      ).then((response) => {
        const { data } = response;
        this.setState({
          listProvinsi: data.provinsi,
          listProvinsiSearch: data.provinsi,
          loadingProvinsi: false,
        });
      });
    };

    const renderValue = (value) => {
      return value;
    };

    const handleGetDataJenis = (key, value) => {
      //localStorage.setItem('jenisUsahaLokasi', value)
      this.setState({ lokasiUsahaPermanen: value });
      localStorage.setItem("lokasiUsahaPermanenKey", key);
      localStorage.setItem("lokasiUsahaPermanen", value);
    };

    const handleGetProvinsi = (key, value) => {
      this.setState({
        loadingKota: true,
        localProv: value,
        localProvId: key,
        openKota: true,
        openProvinsi: false,
      });
      wsWithoutBody(
        BACK_END_POINT +
          "/Alamat/FindAll/KabupatenOrKota/ByIdProvinsi?id_provinsi=" +
          key,
        GET,
        HEADER_AUTH
      ).then((response) => {
        const { data } = response;
        this.setState({
          listKota: data.kota_kabupaten,
          listKotaSearch: data.kota_kabupaten,
          loadingKota: false,
        });
      });
    };

    const handleGetKota = (key, value) => {
      this.setState({
        loadingKecamatan: true,
        localKot: value,
        localKotId: key,
        openKecamatan: true,
        openKota: false,
      });
      wsWithoutBody(
        BACK_END_POINT +
          "/Alamat/FindAll/Kecamatan/ByIdKabupatenOrKota?id_kabupaten=" +
          key,
        GET,
        HEADER_AUTH
      ).then((response) => {
        const { data } = response;
        this.setState({
          listKecamatan: data.kecamatan,
          listKecamatanSearch: data.kecamatan,
          loadingKecamatan: false,
        });
      });
    };

    const backToDaerah = (typeDaerah) => {
      handleCloseAllDaerah();
      if (typeDaerah === "provinsi") {
        handleOpenProv();
      }

      if (typeDaerah === "kota") {
        handleGetProvinsi(this.state.localProvId, this.state.localProv);
      }

      if (typeDaerah === "kecamatan") {
        handleGetKota(this.state.localKotId, this.state.localKot);
      }
    };

    const handleGetKecamatan = (key, value) => {
      this.setState({
        loadingKelurahan: true,
        localKec: value,
        localKecId: key,
        openKelurahan: true,
        openKecamatan: false,
      });
      wsWithoutBody(
        BACK_END_POINT +
          "/Alamat/FindAll/Kelurahan/ByIdKecamatan?id_kecamatan=" +
          key,
        GET,
        HEADER_AUTH
      ).then((response) => {
        const { data } = response;
        this.setState({
          listKelurahan: data.kelurahan,
          listKelurahanSearch: data.kelurahan,
          loadingKelurahan: false,
        });
      });
    };

    const handleGetKelurahan = (key, value) => {
      this.setState({ openKelurahan: false, localKel: value });
      localStorage.setItem("localProv", this.state.localProv);
      localStorage.setItem("localKot", this.state.localKot);
      localStorage.setItem("localKec", this.state.localKec);
      localStorage.setItem("localKel", value);
      localStorage.setItem("localKelId", key);
    };

    const searchDaerah = (event, typeDaerah) => {
      var src = event.target.value.toUpperCase();
      if (typeDaerah === "provinsi") {
        var valueList = this.state.listProvinsiSearch.filter((provinsi) =>
          provinsi.nama.toUpperCase().includes(src)
        );
        this.setState({ listProvinsi: valueList });
      }

      if (typeDaerah === "kota") {
        var valueList = this.state.listKotaSearch.filter((kota) =>
          kota.nama.toUpperCase().includes(src)
        );
        this.setState({ listKota: valueList });
      }

      if (typeDaerah === "kelurahan") {
        var valueList = this.state.listKelurahanSearch.filter((kelurahan) =>
          kelurahan.nama.toUpperCase().includes(src)
        );
        this.setState({ listKelurahan: valueList });
      }

      if (typeDaerah === "kecamatan") {
        var valueList = this.state.listKecamatanSearch.filter((kecamatan) =>
          kecamatan.nama.toUpperCase().includes(src)
        );
        this.setState({ listKecamatan: valueList });
      }
    };

    return (
      <div>
        <div
          style={{
            position: "fixed",
            display: "flex",
            minWidth: "100%",
            justifyContent: "center",
            alignItems: "center",
          }}
        >
          <Link
            href="/uploadInformasiUsaha"
            style={{ left: "20px", position: "fixed", top: "20px" }}
          >
            <ArrowBackIcon style={{ left: "20px" }} />
          </Link>
          <span
            style={{
              display: "flex",
              textAlign: "center",
              justifyContent: "center",
              alignItems: "center",
              position: "fixed",
              top: "22px",
              fontSize: "14px",
              color: "#303B4A",
            }}
          >
            Langkah 4 dari 4
          </span>
        </div>
        <Box style={{ marginTop: "80px", marginBottom: "40px" }}>
          <h3 style={{ textAlign: "center", marginTop: "27px" }}>
            Informasi Lokasi Usaha
          </h3>
        </Box>
        <h5 style={{ marginLeft: "20px" }}>Pilih kategori yang sesuai</h5>
        <List
          className="listLokasiUsaha"
          component="nav"
          aria-label="main mailbox folders"
          style={{ marginInlineStart: "20px", marginInlineEnd: "20px" }}
        >
          <ListItem
            button
            selected={this.state.selIdx === 0}
            onClick={(event) => handleListItemClick(event, 0)}
            style={{ border: "solid 0.5px lightgray", marginBottom: "10px" }}
          >
            <ListItemIcon
              style={{ justifyContent: "center", alignSelf: "flex-start" }}
            >
              <img
                src={Permanen}
                style={{
                  width: "50px",
                  marginTop: "10px",
                  marginRight: "10px",
                }}
              />
            </ListItemIcon>
            <ListItemText
              className="itemNon"
              primary="Permanen"
              secondary="Usaha bertempat di bangunan tetap dan tidak berpindah-pindah. Usaha jenis ini harus memiliki izin kepemilikan atau penyewaan tertulis"
            />
          </ListItem>
          <ListItem
            button
            selected={this.state.selIdx === 1}
            onClick={(event) => handleListItemClick(event, 1)}
            style={{ border: "solid 0.5px lightgray" }}
          >
            <ListItemIcon
              style={{ justifyContent: "center", alignSelf: "flex-start" }}
            >
              <img
                src={TidakPermanen}
                style={{
                  width: "50px",
                  marginTop: "10px",
                  marginRight: "10px",
                }}
              />
            </ListItemIcon>
            <ListItemText
              primary="Tidak Permanen"
              secondary="Usaha Lokasi usaha yang perpindah-pindah karena dibatasi oleh waktu"
            />
          </ListItem>
        </List>
        {this.state.selIdx === 1 && (
          <Box
            style={{
              marginInlineStart: "20px",
              marginInlineEnd: "20px",
              display: "inline-block",
              position: "relative",
              width: "-webkit-fill-available",
            }}
          >
            <form autoComplete="off">
              <ArrowDropDownIcon
                style={{
                  position: "absolute",
                  right: 10,
                  top: 15,
                  width: 20,
                  height: 20,
                  zIndex: 2,
                }}
              />
              <TextField
                fullWidth
                id="lokasiNon"
                variant="filled"
                name="lokasiNon"
                label="Lokasi Usaha"
                value={dataNon}
                onChange={handleChange}
                onClick={handleOpenLokasi}
                InputLabelProps={{
                  shrink: true,
                }}
              ></TextField>
            </form>

            {/* <FormControl variant="filled" style={{ width: '-webkit-fill-available' }}>
                            <InputLabel htmlFor="filled-jenisLokasi">Lokasi Usaha</InputLabel>
                            <Select style={{ marginBottom: '10px', width: '-webkit-fill-available' }}
                                value={this.state.valLokasiUsahaNonPermanen}
                                renderValue={() => renderValue(this.state.valLokasiUsahaNonPermanen)}
                                MenuProps={MenuProps}
                                onOpen={handleOpenLokasi}
                                inputProps={{
                                    name: "jenisLokasi",
                                    id: "jenisLokasi"
                                }}
                            >

                                

                                 {<Dialog  fullScreen style={{height: '460px', width: '100%', position: 'absolute',top: 'unset' ,bottom: '0px'}} 
                                        open={this.state.openLokasi} onClose={handleCloseLokasi}>
                                    <IconButton edge="start" color="inherit" onClick={handleCloseLokasi} aria-label="close"
                                            style={{ position: 'fixed', width: '20px', right: '20px' }}>
                                            <CloseIcon />
                                    </IconButton>
                                    <Box style={{height: '50px', justifyContent: 'left', alignItems: 'center',  
                                    display: 'flex', marginBottom: '20px'}}>
                                    <p style={{fontSize: '16px', lineHeight: '24px', fontWeight: '700', 
                                        marginTop: '20px', marginBottom: '0px', marginLeft: '16px'}}>Lokasi Usaha</p>
                                    </Box>
                                    {this.state.listLokasiUsahaNonPermanen.map((name) => (
                                        <List className="listModal" key={name}>
                                            <ListItem onClick={() => handleGetDataLokasi(name.idName, name.nama)} value={name} >
                                                <ListItemText primary={name.nama} />
                                            </ListItem>
                                        </List>

                                    ))}
                                </Dialog>
                            </Select>
                        </FormControl> */}
          </Box>
        )}
        <ModalLokasiUsahaNon
          isShowModalNon={this.state.openLokasi}
          valueNon={this.state.listLokasiUsahaNonPermanen}
          toggleCloseNon={handleCloseLokasi}
          handlerNon={this.handleGetDataLokasi}
        />
        {/* ---------------------------------PERMANEN---------------------- */}
        {this.state.selIdx === 0 && (
          <Box style={{ marginInlineStart: "20px", marginInlineEnd: "20px" }}>
            <FormControl
              variant="filled"
              style={{ width: "-webkit-fill-available" }}
            >
              <InputLabel htmlFor="filled-jenisUsaha">Lokasi Usaha</InputLabel>
              <Select
                style={{
                  marginBottom: "10px",
                  width: "-webkit-fill-available",
                }}
                value={this.state.lokasiUsahaPermanen}
                renderValue={() => renderValue(this.state.lokasiUsahaPermanen)}
                MenuProps={MenuProps}
                onOpen={handleOpenJenisUsaha}
                inputProps={{
                  name: "jenisUsaha",
                  id: "filled-jenisUsaha",
                }}
              >
                {
                  <Dialog
                    fullScreen
                    role="presentation"
                    tabIndex="-1"
                    open={this.state.openJenis}
                    onClose={handleCloseJenis}
                  >
                    <AppBar style={{ height: "50px" }}>
                      <Toolbar style={{ height: "50px" }}>
                        <IconButton
                          edge="start"
                          color="inherit"
                          onClick={handleCloseJenis}
                          aria-label="close"
                          style={{
                            position: "fixed",
                            width: "20px",
                            right: "20px",
                          }}
                        >
                          <CloseIcon />
                        </IconButton>
                        <Typography
                          variant="h6"
                          style={{
                            width: "100%",
                            textAlign: "center",
                            marginRight: "50px",
                          }}
                        >
                          Lokasi
                        </Typography>
                      </Toolbar>
                    </AppBar>
                    <br />
                    <br />
                    <br />
                    {this.state.listLokasiUsahaPermanen.map((name) => (
                      <List key={name} style={{ width: "95%" }}>
                        <ListItem
                          onClick={() =>
                            handleGetDataJenis(name.idName, name.nama)
                          }
                          value={name}
                          style={{
                            border: "solid 0.5px deepskyblue",
                            borderRadius: "10px",
                            marginInlineStart: "10px",
                          }}
                        >
                          <ListItemText primary={name.nama} />
                        </ListItem>
                      </List>
                    ))}
                  </Dialog>
                }
              </Select>
            </FormControl>

            <TextField
              style={{ width: "-webkit-fill-available", marginBottom: "10px" }}
              label="Alamat Saat Ini"
              variant="filled"
              multiline
              value={localStorage.getItem("alamatLokasiUsaha")}
              onChange={(event) => {
                localStorage.setItem("alamatLokasiUsaha", event.target.value);
              }}
              rows={3}
            />

            <FormControl
              variant="filled"
              style={{ width: "-webkit-fill-available" }}
            >
              <InputLabel htmlFor="provkelkec" style={{ fontSize: "small" }}>
                Provinsi, Kota, Kecamatan, Kelurahan, Kode Pos
              </InputLabel>
              <Select
                style={{
                  marginBottom: "10px",
                  width: "-webkit-fill-available",
                }}
                value={
                  this.state.localProv +
                  ", " +
                  this.state.localKot +
                  ", " +
                  this.state.localKec +
                  ", " +
                  this.state.localKel
                }
                renderValue={() =>
                  renderValue(
                    this.state.localProv +
                      ", " +
                      this.state.localKot +
                      ", " +
                      this.state.localKec +
                      ", " +
                      this.state.localKel
                  )
                }
                inputProps={{
                  name: "prov",
                  id: "provkelkec",
                }}
                MenuProps={MenuProps}
                onOpen={handleOpenProv}
              ></Select>
              <Dialog
                fullScreen
                role="presentation"
                tabIndex="-1"
                open={this.state.openProvinsi}
              >
                <div
                  style={{
                    position: "fixed",
                    display: "flex",
                    minWidth: "100%",
                    justifyContent: "center",
                    alignItems: "center",
                  }}
                >
                  <span
                    style={{
                      display: "flex",
                      textAlign: "center",
                      justifyContent: "center",
                      alignItems: "center",
                      position: "fixed",
                      top: "10px",
                      fontSize: "smaller",
                      color: "lightslategray",
                    }}
                  >
                    Provinsi
                  </span>
                </div>
                <IconButton
                  edge="start"
                  color="inherit"
                  onClick={() => handleCloseAllDaerah()}
                  aria-label="close"
                  style={{ position: "fixed", width: "20px", right: "20px" }}
                >
                  <CloseIcon />
                </IconButton>
                <Box style={{ marginTop: "50px", marginBottom: "20px" }}>
                  <h3
                    style={{
                      textAlign: "center",
                      marginTop: "0px",
                      marginInlineStart: "10px",
                      marginInlineEnd: "10px",
                    }}
                  >
                    Di provinsi mana Anda tinggal ?
                  </h3>
                  <div className={classes.search}>
                    <div className={classes.searchIcon}>
                      <SearchIcon />
                    </div>
                    <InputBase
                      placeholder="Cari..."
                      classes={{
                        root: classes.inputRoot,
                        input: classes.inputInput,
                      }}
                      onChange={(event) => searchDaerah(event, "provinsi")}
                      inputProps={{ "aria-label": "search" }}
                    />
                  </div>
                </Box>
                {this.state.loadingProvinsi ? (
                  <CircularProgress />
                ) : (
                  this.state.listProvinsi.map((provinsi) => (
                    <List key={provinsi.id} style={{ width: "95%" }}>
                      <ListItem
                        onClick={() =>
                          handleGetProvinsi(provinsi.id, provinsi.nama)
                        }
                        value={provinsi}
                        style={{
                          border: "solid 0.5px deepskyblue",
                          borderRadius: "10px",
                          marginInlineStart: "10px",
                        }}
                      >
                        <ListItemText primary={provinsi.nama} />
                        <ListItemSecondaryAction style={{ right: "0px" }}>
                          <IconButton
                            onClick={() =>
                              handleGetProvinsi(provinsi.id, provinsi.nama)
                            }
                            edge="end"
                            aria-label="delete"
                          >
                            <ChevronRightIcon />
                          </IconButton>
                        </ListItemSecondaryAction>
                      </ListItem>
                    </List>
                  ))
                )}
              </Dialog>
              <Dialog
                fullScreen
                role="presentation"
                tabIndex="-1"
                open={this.state.openKota}
              >
                <div
                  style={{
                    position: "fixed",
                    display: "flex",
                    minWidth: "100%",
                    justifyContent: "center",
                    alignItems: "center",
                  }}
                >
                  <span
                    style={{ left: "10px", position: "fixed", top: "10px" }}
                  >
                    <ArrowBackIcon
                      style={{ left: "20px" }}
                      onClick={() => {
                        backToDaerah("provinsi");
                      }}
                    />
                  </span>
                  <span
                    style={{
                      display: "flex",
                      textAlign: "center",
                      justifyContent: "center",
                      alignItems: "center",
                      position: "fixed",
                      top: "10px",
                      fontSize: "smaller",
                      color: "lightslategray",
                    }}
                  >
                    Kota/Kabupaten
                  </span>
                </div>
                <IconButton
                  edge="start"
                  color="inherit"
                  onClick={() => handleCloseAllDaerah()}
                  aria-label="close"
                  style={{ position: "fixed", width: "20px", right: "20px" }}
                >
                  <CloseIcon />
                </IconButton>
                <Box style={{ marginTop: "50px", marginBottom: "20px" }}>
                  <h3
                    style={{
                      textAlign: "center",
                      marginTop: "0px",
                      marginInlineStart: "10px",
                      marginInlineEnd: "10px",
                    }}
                  >
                    Kota / Kabupaten apa di {this.state.localProv} ?
                  </h3>
                  <div className={classes.search}>
                    <div className={classes.searchIcon}>
                      <SearchIcon />
                    </div>
                    <InputBase
                      placeholder="Cari..."
                      classes={{
                        root: classes.inputRoot,
                        input: classes.inputInput,
                      }}
                      onChange={(event) => searchDaerah(event, "kota")}
                      inputProps={{ "aria-label": "search" }}
                    />
                  </div>
                </Box>
                {this.state.loadingKota ? (
                  <CircularProgress />
                ) : (
                  this.state.listKota.map((kota) => (
                    <List key={kota.id} style={{ width: "95%" }}>
                      <ListItem
                        onClick={() => handleGetKota(kota.id, kota.nama)}
                        value={kota}
                        style={{
                          border: "solid 0.5px deepskyblue",
                          borderRadius: "10px",
                          marginInlineStart: "10px",
                        }}
                      >
                        <ListItemText primary={kota.nama} />
                        <ListItemSecondaryAction style={{ right: "0px" }}>
                          <IconButton
                            onClick={() => handleGetKota(kota.id, kota.nama)}
                            edge="end"
                            aria-label="delete"
                          >
                            <ChevronRightIcon />
                          </IconButton>
                        </ListItemSecondaryAction>
                      </ListItem>
                    </List>
                  ))
                )}
              </Dialog>

              <Dialog
                fullScreen
                role="presentation"
                tabIndex="-1"
                open={this.state.openKecamatan}
              >
                <div
                  style={{
                    position: "fixed",
                    display: "flex",
                    minWidth: "100%",
                    justifyContent: "center",
                    alignItems: "center",
                  }}
                >
                  <span
                    style={{ left: "10px", position: "fixed", top: "10px" }}
                  >
                    <ArrowBackIcon
                      style={{ left: "20px" }}
                      onClick={() => {
                        backToDaerah("kota");
                      }}
                    />
                  </span>
                  <span
                    style={{
                      display: "flex",
                      textAlign: "center",
                      justifyContent: "center",
                      alignItems: "center",
                      position: "fixed",
                      top: "10px",
                      fontSize: "smaller",
                      color: "lightslategray",
                    }}
                  >
                    Kecamatan
                  </span>
                </div>

                <IconButton
                  edge="start"
                  color="inherit"
                  onClick={() => handleCloseAllDaerah()}
                  aria-label="close"
                  style={{ position: "fixed", width: "20px", right: "20px" }}
                >
                  <CloseIcon />
                </IconButton>
                <Box style={{ marginTop: "50px", marginBottom: "20px" }}>
                  <h3
                    style={{
                      textAlign: "center",
                      marginTop: "0px",
                      marginInlineStart: "10px",
                      marginInlineEnd: "10px",
                    }}
                  >
                    Kecamatan apa di {this.state.localKot} ?
                  </h3>
                  <div className={classes.search}>
                    <div className={classes.searchIcon}>
                      <SearchIcon />
                    </div>
                    <InputBase
                      placeholder="Cari..."
                      classes={{
                        root: classes.inputRoot,
                        input: classes.inputInput,
                      }}
                      onChange={(event) => searchDaerah(event, "kecamatan")}
                      inputProps={{ "aria-label": "search" }}
                    />
                  </div>
                </Box>
                {this.state.loadingKecamatan ? (
                  <CircularProgress />
                ) : (
                  this.state.listKecamatan.map((kecamatan) => (
                    <List key={kecamatan.id} style={{ width: "95%" }}>
                      <ListItem
                        onClick={() =>
                          handleGetKecamatan(kecamatan.id, kecamatan.nama)
                        }
                        value={kecamatan}
                        style={{
                          border: "solid 0.5px deepskyblue",
                          borderRadius: "10px",
                          marginInlineStart: "10px",
                        }}
                      >
                        <ListItemText primary={kecamatan.nama} />
                        <ListItemSecondaryAction style={{ right: "0px" }}>
                          <IconButton
                            onClick={() =>
                              handleGetKecamatan(kecamatan.id, kecamatan.nama)
                            }
                            edge="end"
                            aria-label="delete"
                          >
                            <ChevronRightIcon />
                          </IconButton>
                        </ListItemSecondaryAction>
                      </ListItem>
                    </List>
                  ))
                )}
              </Dialog>

              <Dialog
                fullScreen
                role="presentation"
                tabIndex="-1"
                open={this.state.openKelurahan}
              >
                <div
                  style={{
                    position: "fixed",
                    display: "flex",
                    minWidth: "100%",
                    justifyContent: "center",
                    alignItems: "center",
                  }}
                >
                  <span
                    style={{ left: "10px", position: "fixed", top: "10px" }}
                  >
                    <ArrowBackIcon
                      style={{ left: "20px" }}
                      onClick={() => {
                        backToDaerah("kecamatan");
                      }}
                    />
                  </span>
                  <span
                    style={{
                      display: "flex",
                      textAlign: "center",
                      justifyContent: "center",
                      alignItems: "center",
                      position: "fixed",
                      top: "10px",
                      fontSize: "smaller",
                      color: "lightslategray",
                    }}
                  >
                    Kelurahan
                  </span>
                </div>
                {/* <IconButton
                  edge="start"
                  color="inherit"
                  onClick={() => handleCloseAllDaerah()}
                  aria-label="close"
                  style={{ position: "fixed", width: "20px", right: "20px" }}
                >
                  <CloseIcon />
                </IconButton> */}
                <Box style={{ marginTop: "50px", marginBottom: "20px" }}>
                  <h3
                    style={{
                      textAlign: "center",
                      marginTop: "0px",
                      marginInlineStart: "10px",
                      marginInlineEnd: "10px",
                    }}
                  >
                    Kelurahan apa di {this.state.localKec} ?
                  </h3>
                  <div className={classes.search}>
                    <div className={classes.searchIcon}>
                      <SearchIcon />
                    </div>
                    <InputBase
                      placeholder="Cari..."
                      classes={{
                        root: classes.inputRoot,
                        input: classes.inputInput,
                      }}
                      onChange={(event) => searchDaerah(event, "kelurahan")}
                      inputProps={{ "aria-label": "search" }}
                    />
                  </div>
                </Box>
                {this.state.loadingKelurahan ? (
                  <CircularProgress />
                ) : (
                  this.state.listKelurahan.map((kelurahan) => (
                    <List key={kelurahan.id} style={{ width: "95%" }}>
                      <ListItem
                        onClick={() =>
                          handleGetKelurahan(kelurahan.id, kelurahan.nama)
                        }
                        value={kelurahan}
                        style={{
                          border: "solid 0.5px deepskyblue",
                          borderRadius: "10px",
                          marginInlineStart: "10px",
                        }}
                      >
                        <ListItemText primary={kelurahan.nama} />
                        <ListItemSecondaryAction style={{ right: "0px" }}>
                          <IconButton
                            onClick={() =>
                              handleGetKelurahan(kelurahan.id, kelurahan.nama)
                            }
                            edge="end"
                            aria-label="delete"
                          >
                            <ChevronRightIcon />
                          </IconButton>
                        </ListItemSecondaryAction>
                      </ListItem>
                    </List>
                  ))
                )}
              </Dialog>
            </FormControl>
          </Box>
        )}

        {this.state.selIdx === 0 && (
          <Box
            style={{
              bottom: "20px",
              position: "relative",
              marginInlineStart: "20px",
              minWidth: "90%",
              marginInlineEnd: "30px",
              marginTop: "50px",
            }}
          >
            <Link href="/reviewData">
              <Button
                size="large"
                style={{
                  borderRadius: "8px",
                  width: "-webkit-fill-available",
                  marginTop: "10px",
                  textTransform: "none",
                  marginBottom: "10px",
                  height: "56px",
                }}
                variant="contained"
                color="primary"
              >
                Simpan
              </Button>
            </Link>
          </Box>
        )}
        {this.state.selIdx === 1 && (
          <Box
            style={{
              bottom: "20px",
              position: "fixed",
              marginInlineStart: "20px",
              minWidth: "90%",
              marginInlineEnd: "30px",
              marginTop: "50px",
            }}
          >
            <Link href="/reviewData">
              <Button
                size="large"
                style={{
                  borderRadius: "8px",
                  width: "100%",
                  height: "56px",
                  marginTop: "10px",
                  textTransform: "none",
                  marginBottom: "32px",
                  // display: "flex",
                  fontFamily: "Nunito",
                  fontStyle: "normal",
                  fontWeight: "bold",
                  fontSize: "16px",
                  lineHeight: "24px",
                  /* identical to box height, or 150% */
                  alignItems: "center",
                  textAlign: "center",
                }}
                variant="contained"
                color="primary"
              >
                Simpan
              </Button>
            </Link>
          </Box>
        )}
      </div>
    );
  }
}

export default compose(withStyles(useStyles))(informasiLokasiUsaha);
