import React, { Component } from 'react'
import { connect } from 'react-redux';
import { AppBar, Box, Button, Paper, CircularProgress, Dialog, fade, Grid, IconButton, Input, InputBase, 
    InputLabel, Link, List, ListItem, ListItemIcon, ListItemSecondaryAction, ListItemText, Select, 
    TextareaAutosize, TextField, Toolbar, Typography, withStyles, InputAdornment } from '@material-ui/core'
import TidakPermanen from '../../../public/img/LogoTokoNonPermanen.png';
import Permanen from '../../../public/img/LogoTokoPermanen.png';
import ArrowBackIcon from '@material-ui/icons/ArrowBack';
import CloseIcon from '@material-ui/icons/Close';
import ChevronRightIcon from '@material-ui/icons/ChevronRight';
import SearchIcon from '@material-ui/icons/Search';
import { compose } from 'recompose';
import { Autocomplete } from '@material-ui/lab';
import { BACK_END_POINT, GET, HEADER_AUTH, wsWithoutBody } from '../../master/masterComponent';
import ModalLokasiUsahaNon from './ModalLokasiUsahaNon';
import ArrowDropDownIcon from '@material-ui/icons/ArrowDropDown';
import ModalLokasiUsaha from './ModalLokasiUsaha';
import { registrasiActions } from '../../actions';

const ITEM_HEIGHT = 48;
const ITEM_PADDING_TOP = 8;
const MenuProps = {
    PaperProps: {
        style: {
            maxHeight: ITEM_HEIGHT * 4.5 + ITEM_PADDING_TOP,
            width: 250,
        },
    },
};


const lokasiUsahaNonPermanen = [
    'Gerobak',
    'Tenda Kaki Lima',
    'Pedagang Keliling',
    'Pedagang Lapak',
    'Pasar Malam /Pagi'
];


const useStyles = theme => ({

    root: {
        padding: '2px 4px',
        display: 'flex',
        alignItems: 'center',
        width: 400,
      },

    search: {
        //border: 'solid 0.5px darkgray',
        marginInlineStart: '20px',
        width: '90%',
        position: 'relative',
        borderRadius: '32px',
        height: '48px',
        backgroundColor: '#F7F6F6',
        '&:hover': {
            backgroundColor: fade('#F7F6F6', 0.25),
        },
        [theme.breakpoints.up('sm')]: {
            marginLeft: theme.spacing(3),
            width: 'auto',
        },
    },
    searchIcon: {
        padding: theme.spacing(0, 2),
        height: '-webkit-fill-available',
        position: 'absolute',
        pointerEvents: 'none',
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
    },
    inputRoot: {
        color: 'inherit',
    },
    inputInput: {
        padding: theme.spacing(2, 2, 2, 0),
        // vertical padding + font size from searchIcon
        paddingLeft: `calc(1em + ${theme.spacing(4)}px)`,
        transition: theme.transitions.create('width'),
        width: '100%',
        [theme.breakpoints.up('md')]: {
            width: '20ch',
        },
    },

})
class informasiLokasiUsaha extends Component {

    constructor(props) {
        super(props);

        this.state = {
            selIdx: 0,
            openLokasi: false,
            openJenis: false,
            openProvinsi: false,
            loadingJenis: false,
            loadingLokasi: false,
            loadingProvinsi: false,
            openKota: false,
            loadingKota: false,
            openKelurahan: false,
            loadingKelurahan: false,
            openKecamatan: false,
            loadingKecamatan: false,
            valLokasiUsahaNonPermanen: '',
            provKel: false,
            localProv: '',
            localProvId: '',
            localKot: '',
            localKotId: '',
            localKec: '',
            localKecId: '',
            localKel: '',
            localKelId: '',
            valProv: '',
            lokasiUsahaPermanen: '',
            listProvinsi: [],
            listProvinsiSearch: [],
            listKota: [],
            listKotaSearch: [],
            listKelurahan: [],
            listKelurahanSearch: [],
            listKecamatan: [],
            listKecamatanSearch: [],
            listLokasiUsahaPermanen: [],
            listLokasiUsahaNonPermanen: [],
            disableBut: false,
            alamatNow: '',
            isValid: false
        }

        this.handleGetDataLokasi = this.handleGetDataLokasi.bind(this)
        this.handleGetDataJenis =this.handleGetDataJenis.bind(this)


    }

    componentDidMount() {
        const { getLokasiUsaha, getLokasiUsahaNon} = this.props

        getLokasiUsaha();
        getLokasiUsahaNon();

        if (localStorage.getItem('kategorUsahaIdx') == null) {
            this.setState({ selIdx: 0 });
            localStorage.setItem('kategorUsahaIdx', 0);
        } else {
            var x = localStorage.getItem('kategorUsahaIdx');
            this.setState({ selIdx: parseInt(x, 10) });

        }

        if(localStorage.getItem('alamatLokasiUsaha')){
            this.setState({alamatNow: localStorage.getItem('alamatLokasiUsaha')})
        }

        if(localStorage.getItem('lokasiUsahaPermanen')){
            this.setState({ lokasiUsahaPermanen: localStorage.getItem('lokasiUsahaPermanen') });
        }
       // this.setState({ lokasiUsahaPermanen: localStorage.getItem('lokasiUsahaPermanen') });

        if(localStorage.getItem('valLokasiUsahaNonPermanen')){
            this.setState({ valLokasiUsahaNonPermanen: localStorage.getItem('valLokasiUsahaNonPermanen') });
        }
       
        if (localStorage.getItem('localProv') != null) {
            this.setState({ localProv: localStorage.getItem('localProv') })
        }

        if (localStorage.getItem('localKot') != null) {
            this.setState({ localKot: localStorage.getItem('localKot') })
        }
        if (localStorage.getItem('localKec') != null) {
            this.setState({ localKec: localStorage.getItem('localKec') })
        }
        if (localStorage.getItem('localKel') != null) {
            this.setState({ localKel: localStorage.getItem('localKel') })
        }

            console.log('index: ', localStorage.getItem('kategorUsahaIdx'))
            console.log('lokasiUsahaPermanen: ', localStorage.getItem('lokasiUsahaPermanen'))
            console.log('alamatLokasiUsaha: ', localStorage.getItem('alamatLokasiUsaha'))
            console.log('localKel: ', localStorage.getItem('localKel'))

        if(localStorage.getItem('kategorUsahaIdx') === '0'){
            if(localStorage.getItem('lokasiUsahaPermanen') && 
            (localStorage.getItem('alamatLokasiUsaha') || localStorage.getItem('alamatLokasiUsaha') !== '') 
            && localStorage.getItem('localKel')){
                this.setState({isValid: true})
            }else{
                this.setState({isValid: false})
            }
        }else{
            if(localStorage.getItem('valLokasiUsahaNonPermanen')){
                this.setState({isValid: true})
            }else{
                this.setState({isValid: false})
            }
        }

       
        

        
    }

    componentWillUnmount(){
        location.reload()
    }

    handleGetDataLokasi = (key, value) => {
        this.setState({ valLokasiUsahaNonPermanen: value })
        localStorage.setItem("valLokasiUsahaNonPermanenKey", key)
        localStorage.setItem("valLokasiUsahaNonPermanen", value)
        
        if(localStorage.getItem('valLokasiUsahaNonPermanen') && this.state.selIdx === 1){
            this.setState({isValid: true})
        }
    }

    handleGetDataJenis = (key, value) => {
        this.setState({ lokasiUsahaPermanen: value });
        localStorage.setItem("lokasiUsahaPermanenKey", key)
        localStorage.setItem("lokasiUsahaPermanen", value)

        if(localStorage.getItem('lokasiUsahaPermanen') && 
            (localStorage.getItem('alamatLokasiUsaha') || localStorage.getItem('alamatLokasiUsaha') !== '')
            && localStorage.getItem('localKel')){
                this.setState({isValid: true})
            }else{
                this.setState({isValid: false})
            }
    }

    render() {
        const { loading = false, classes, registrasi } = this.props;

        let dataLokasiUsaha = registrasi.loksaiUsahaData
        let loadingLokasiUsaha = registrasi.lokasiUsahaLoading
        let alertLokasiUsaha = registrasi.lokasiUsahaAlert

        let dataLokasiUsahaNon = registrasi.lokasiUSahaNonData
        let loadingLokasiUsahaNon = registrasi.lokasiUSahaNonLoading
        let alertLokasiUsahaNon = registrasi.lokasiUSahaNonAlert

        let dataPermanen = []
        let dataNonPermanen = []

        if (Array.isArray(dataLokasiUsaha)) {
            dataPermanen.push(dataLokasiUsaha)
        }

        if (Array.isArray(dataLokasiUsahaNon)) {
            dataNonPermanen.push(dataLokasiUsaha)
        }


        const handleListItemClick = (event, index) => {
            this.setState({ selIdx: index });
            localStorage.setItem('kategorUsahaIdx', index);

            console.log('index: ', index)
            console.log('lokasiUsahaPermanen: ', localStorage.getItem('lokasiUsahaPermanen'))
            console.log('alamatLokasiUsaha: ', localStorage.getItem('alamatLokasiUsaha'))
            console.log('localKel: ', localStorage.getItem('localKel'))

            if(index === 0){
                if(localStorage.getItem('lokasiUsahaPermanen') && 
                (localStorage.getItem('alamatLokasiUsaha') || localStorage.getItem('alamatLokasiUsaha') !== '') 
                && localStorage.getItem('localKel')){
                    this.setState({isValid: true})
                }else{
                    this.setState({isValid: false})
                }
            }else{
                if(localStorage.getItem('valLokasiUsahaNonPermanen')){
                    this.setState({isValid: true})
                }else{
                    this.setState({isValid: false})
                }
            }

            
        

        };

        let dataNon

        if(this.state.valLokasiUsahaNonPermanen){
            dataNon = this.state.valLokasiUsahaNonPermanen
        }
        // else{
        //     dataNon = this.state.valLokasiUsahaNonPermanen
        // }

        let dataJenis

        if(!this.state.lokasiUsahaPermanen){
            dataJenis = ''
        }else{
            dataJenis = this.state.lokasiUsahaPermanen
        }

        let dataDaerah

        if(!this.state.localProv || !this.state.localKot || !this.state.localKec || !this.state.localKel){
            dataDaerah = ''
        }else{
            dataDaerah = this.state.localProv + ", " + this.state.localKot + ", " + this.state.localKec + ", " + this.state.localKel
        }

        
        const handleCloseLokasi = () => {
            this.setState({ openLokasi: false })
        }
        const handleCloseJenis = () => {
            this.setState({ openJenis: false })

        }

        const handleCloseAllDaerah = () => {
            this.setState({ openProvinsi: false, openKota: false, openKecamatan: false, openKelurahan: false })
        }


        const handleOpenJenisUsaha = () => {
            this.setState({ openJenis: true})
            
            wsWithoutBody(BACK_END_POINT + "/usahaProperties/lokasiPermanenUsaha", GET, HEADER_AUTH).then(response => {

                this.setState({ listLokasiUsahaPermanen: response.data });
            }).catch(error => {
                alert(error.response.data.message);
            })
        }

        const handleOpenLokasi = () => {
            this.setState({ openLokasi: true})
            wsWithoutBody(BACK_END_POINT + "/usahaProperties/lokasiNonPermanenUsaha", GET, HEADER_AUTH).then(response => {
    
                this.setState({ listLokasiUsahaNonPermanen: response.data });
    
            }).catch(error => {
                alert(error.response.data.message);
            })
            
        }

        const handleChangeNon = (event) => {
            this.setState({ valLokasiUsahaNonPermanen: event.target.value })
        }

        const handleChange = (event) => {
            this.setState({ lokasiUsahaPermanen: event.target.value })
        }

        const handleChangeAlamat = (event) => {

          if(event.target.value.length<=256){  
            localStorage.setItem('alamatLokasiUsaha', event.target.value)
            this.setState({ alamatNow: event.target.value })

            if(localStorage.getItem('lokasiUsahaPermanen') && 
                (localStorage.getItem('alamatLokasiUsaha') || localStorage.getItem('alamatLokasiUsaha') !== '') 
                && localStorage.getItem('localKel')){
                this.setState({isValid: true})
            }else{
                this.setState({isValid: false})
            }
           }
        }

        const handleOpenProv = () => {
            this.setState({ openProvinsi: true, loadingProvinsi: true })
            wsWithoutBody(BACK_END_POINT + "/Alamat/FindAll/Provinsi", GET, HEADER_AUTH)
                .then(response => {
                    const { data } = response;
                    this.setState({
                        listProvinsi: data.provinsi,
                        listProvinsiSearch: data.provinsi,
                        loadingProvinsi: false
                    });
                })
        }

        const renderValue = (value) => {
            let dataDaerah
            if(!this.state.localProv || !this.state.localKot || !this.state.localKec || !this.state.localKel){
                dataDaerah = ''
            }else{
                dataDaerah = value
            }
            return dataDaerah;
        }

        
        const handleGetProvinsi = (key, value) => {
            this.setState({ loadingKota: true, localProv: value, localProvId: key, openKota: true, openProvinsi: false });
            wsWithoutBody(BACK_END_POINT + "/Alamat/FindAll/KabupatenOrKota/ByIdProvinsi?id_provinsi=" + key, GET, HEADER_AUTH)
                .then(response => {
                    const { data } = response;
                    this.setState({
                        listKota: data.kota_kabupaten,
                        listKotaSearch: data.kota_kabupaten
                        , loadingKota: false
                    });
                })
        }


        const handleGetKota = (key, value) => {
            this.setState({
                loadingKecamatan: true, localKot: value, localKotId: key, openKecamatan: true,
                openKota: false
            });
            wsWithoutBody(BACK_END_POINT + "/Alamat/FindAll/Kecamatan/ByIdKabupatenOrKota?id_kabupaten=" + key, GET, HEADER_AUTH)
                .then(response => {
                    const { data } = response;
                    this.setState({
                        listKecamatan: data.kecamatan,
                        listKecamatanSearch: data.kecamatan,
                        loadingKecamatan: false
                    });
                })
        }

        const backToDaerah = (typeDaerah) =>{
            handleCloseAllDaerah();
            if(typeDaerah === 'provinsi'){
                handleOpenProv();
            }

            if(typeDaerah === 'kota'){
                handleGetProvinsi(this.state.localProvId, this.state.localProv);
            }

            if(typeDaerah === 'kecamatan'){
                handleGetKota(this.state.localKotId, this.state.localKot);
            }
        }

        const handleGetKecamatan = (key, value) => {
            this.setState({ loadingKelurahan: true, localKec: value, localKecId: key, openKelurahan: true, openKecamatan: false });
            wsWithoutBody(BACK_END_POINT + "/Alamat/FindAll/Kelurahan/ByIdKecamatan?id_kecamatan=" + key, GET, HEADER_AUTH)
                .then(response => {
                    const { data } = response;
                    this.setState({
                        listKelurahan: data.kelurahan,
                        listKelurahanSearch: data.kelurahan,
                        loadingKelurahan: false
                    });
                })
        }

        const handleGetKelurahan = (key, value) => {
            this.setState({ openKelurahan: false, localKel: value });
            localStorage.setItem('localProv', this.state.localProv)
            localStorage.setItem('localKot', this.state.localKot)
            localStorage.setItem('localKec', this.state.localKec)
            localStorage.setItem('localKel', value)
            localStorage.setItem('localKelId', key)

            if(localStorage.getItem('lokasiUsahaPermanen') && 
            (localStorage.getItem('alamatLokasiUsaha') || localStorage.getItem('alamatLokasiUsaha') !== '') 
            && localStorage.getItem('localKel')){
                this.setState({isValid: true})
            }else{
                this.setState({isValid: false})
            }
        }

        const resetVal = (name) => {
            document.getElementById(name).value = ''
            if(name === "cariProv"){
                var valueList = this.state.listProvinsiSearch.filter(provinsi =>
                    provinsi.nama.toUpperCase().includes(''))
                this.setState({listProvinsi: valueList})

            }else if(name === "cariKota"){
                var valueList = this.state.listKotaSearch.filter(kota =>
                    kota.nama.toUpperCase().includes(''))
                this.setState({ listKota: valueList });
            }else if(name === "cariKec"){
                var valueList = this.state.listKecamatanSearch.filter(kecamatan =>
                    kecamatan.nama.toUpperCase().includes(''))
                this.setState({ listKecamatan: valueList });
            }else{
                var valueList = this.state.listKelurahanSearch.filter(kelurahan  =>
                    kelurahan.nama.toUpperCase().includes(''))
                this.setState({ listKelurahan: valueList });
            }
        }

        const searchDaerah = (event, typeDaerah) => {
            var src = event.target.value.toUpperCase();
            if (typeDaerah === 'provinsi') {
                var valueList = this.state.listProvinsiSearch.filter(provinsi =>
                    provinsi.nama.toUpperCase().includes(src))
                this.setState({ listProvinsi: valueList });
            }

            if (typeDaerah === 'kota') {
                var valueList = this.state.listKotaSearch.filter(kota =>
                    kota.nama.toUpperCase().includes(src))
                this.setState({ listKota: valueList });
            }

            if (typeDaerah === 'kelurahan') {
                var valueList = this.state.listKelurahanSearch.filter(kelurahan  =>
                    kelurahan.nama.toUpperCase().includes(src))
                this.setState({ listKelurahan: valueList });
            }

            if (typeDaerah === 'kecamatan') {
                var valueList = this.state.listKecamatanSearch.filter(kecamatan =>
                    kecamatan.nama.toUpperCase().includes(src))
                this.setState({ listKecamatan: valueList });
            }
        }

        const klikLanjut = () => {
            if (
              new URLSearchParams(this.props.location.search).get("_onCorrection") ===
              "yes"
            ) {
              this.props.history.push('/reviewData');
            } else {
              this.props.history.push('/uploadInformasiUsaha', {from: '/informasiLokasiUsaha'});
            }
            location.reload();
          };


        return (
            <div>
                <AppBar style={{backgroundColor: 'transparent', position: 'absolute', 
                        boxShadow: 'unset'}}>
                    <Toolbar style={{minHeight: '60px'}}>
                        <Link onClick={klikLanjut} style={{position: 'absolute',
                            alignItems: 'center', display: 'flex'}}>
                            <ArrowBackIcon />
                        </Link>
                        <div style={{width: '100%', textAlign: 'center'}}>
                            <Typography style={{fontFamily: 'Nunito', fontSize: '14px',
                        fontWeight: '400', lineHeight: '21px', color: '#303B4A'}}>Langkah 4 dari 4</Typography>
                        </div>
                    </Toolbar>
                </AppBar>
                
                <Box style={{ marginTop: '70px', marginBottom: '40px' }}>
                    <h3 style={{fontFamily: 'Montserrat', fontSize: '18px', textAlign: 'center', marginTop: '27px',
                        fontWeight: '700', lineHeight: '27px'}}>Informasi Lokasi Usaha</h3>
                </Box>
                <h5 style={{ marginLeft: '20px' }}>Pilih kategori yang sesuai</h5>
                <List className="listLokasiUsaha" component="nav" aria-label="main mailbox folders" style={{ marginInlineStart: '20px', marginInlineEnd: '20px' }}>
                    <ListItem button
                        selected={this.state.selIdx === 0}
                        onClick={(event) => handleListItemClick(event, 0)}
                        style={{ border: 'solid 0.5px lightgray', marginBottom: '10px' }}>
                        <ListItemIcon style={{ justifyContent: 'center', alignSelf: 'flex-start' }}>
                            <img src={Permanen} style={{ width: '50px', marginTop: '10px', marginRight: '10px' }} />
                        </ListItemIcon>
                        <ListItemText className= "itemNon" primary="Permanen" secondary="Lokasi usaha di bangunan tetap dan tidak berpindah-pindah. Biasanya disertai dengan surat kepemilikan atau penyewaan tempat." />
                    </ListItem>
                    <ListItem button
                        selected={this.state.selIdx === 1}
                        onClick={(event) => handleListItemClick(event, 1)}
                        style={{ border: 'solid 0.5px lightgray' }}>
                        <ListItemIcon style={{ justifyContent: 'center', alignSelf: 'flex-start' }}>
                            <img src={TidakPermanen} style={{ width: '50px', marginTop: '10px', marginRight: '10px' }} />
                        </ListItemIcon>
                        <ListItemText primary="Tidak Permanen" secondary="Lokasi usaha berpindah-pindah dan/atau dibatasi waktu (contoh: siang atau malam saja)." />
                    </ListItem>
                </List>
                {this.state.selIdx === 1 &&
                    <Box className="classInfoUsaha" style={{ marginInlineStart: '20px', marginInlineEnd: '20px', display: 'inline-block', 
                                position: 'relative', width: '-webkit-fill-available', marginTop: '16px'}} >
                    <form autoComplete="off">
                        <ArrowDropDownIcon style={{position: 'absolute', right: 10, top: 15, width: 20, height: 20, zIndex: 2}}/>
                        <TextField  
                            fullWidth
                            id="lokasiNon"
                            variant="filled"
                            name="lokasiNon"
                            label="Lokasi Usaha"
                            value ={dataNon}
                            onChange={handleChangeNon}
                            onClick={handleOpenLokasi}
                            InputLabelProps={{
                                shrink: dataNon ? true : false,
                              }}
                            InputProps={{
                                readOnly: true,
                            }}
                              >

                        </TextField>
                    </form>
                      
                    </Box>
                }
                  <ModalLokasiUsahaNon isShowModalNon={this.state.openLokasi} valueNon={this.state.listLokasiUsahaNonPermanen} 
                                toggleCloseNon={handleCloseLokasi} handlerNon={this.handleGetDataLokasi}/>
                {/* ---------------------------------PERMANEN---------------------- */}
                {this.state.selIdx === 0 &&
                <div>
                    <Grid className="infolokus" style={{ marginInlineStart: '20px', marginInlineEnd: '20px', marginTop: '16px', marginBottom: '16px',display: 'inline-block', 
                                position: 'relative', width: '-webkit-fill-available' }}>

                        <form autoComplete="off">
                            <ArrowDropDownIcon style={{position: 'absolute', right: 10, top: 15, width: 20, height: 20, zIndex: 2}}/>
                            <TextField  
                                fullWidth
                                id="filled-jenisUsaha"
                                variant="filled"
                                name="jenisUsaha"
                                label="Lokasi Usaha"
                                value ={dataJenis}
                                onChange={handleChange}
                                onClick={handleOpenJenisUsaha}
                                InputLabelProps={{
                                    shrink: dataJenis ? true : false,
                                }}
                                InputProps={{
                                    readOnly: true,
                                }}
                                >

                            </TextField>
                        </form>

                        </Grid>
                        <ModalLokasiUsaha isShowModal={this.state.openJenis} valuePer = {this.state.listLokasiUsahaPermanen} 
                                isLoading={loadingLokasiUsaha} isAlert={alertLokasiUsaha}
                                toggleClose={handleCloseJenis} handler={this.handleGetDataJenis}/>

                        <Grid className="infolokus" style={{ marginInlineStart: '20px', marginInlineEnd: '20px', marginBottom: '16px' }}>
                            <TextField style={{ width: '-webkit-fill-available', marginBottom: '10px' }}
                                label="Alamat Saat Ini"
                                variant="filled"
                                multiline
                                value={this.state.alamatNow}
                                onChange={handleChangeAlamat}
                                rows={3}
                            />
                            {this.state.alamatNow.length <= 255 ? <div></div> :
                             <div className="invalid-feedback" >Panjang Alamat maksimal 255 karakter</div>
                            }
                        </Grid>

                        <Grid className="lokus" style={{ marginInlineStart: '20px', marginInlineEnd: '20px', marginBottom: '16px', marginBottom: '16px',display: 'inline-block', 
                                position: 'relative', width: '-webkit-fill-available' }}>
                            <ArrowDropDownIcon style={{position: 'absolute', right: 10, top: 50, width: 20, height: 20, zIndex: 2}}/>
                            <TextField style={{ width: '-webkit-fill-available', marginBottom: '10px' }}
                                label="Provinsi, Kota, Kecamatan, Kelurahan, Kode Pos"
                                variant="filled"
                                multiline
                                value={dataDaerah}
                                inputProps={{
                                    name: "prov",
                                    id: "provkelkec",
                                    readOnly: true
                                }}
                                // onChange={(event) => {
                                //     localStorage.setItem("alamatLokasiUsaha", event.target.value)
                                // }}
                                onClick={handleOpenProv}
                                rows={4}
                            />
                        {/* <FormControl variant="filled" style={{ width: '-webkit-fill-available' }}>
                            <InputLabel htmlFor="provkelkec" style={{ fontSize: 'small' }}>
                                Provinsi, Kota, Kecamatan, Kelurahan, Kode Pos</InputLabel>
                            <Select style={{ marginBottom: '10px', width: '-webkit-fill-available' }}
                                value={dataDaerah}
                                renderValue={() => renderValue(this.state.localProv + ", " + this.state.localKot + ", " + this.state.localKec + ", " + this.state.localKel)}
                                inputProps={{
                                    name: "prov",
                                    id: "provkelkec"
                                }}
                                MenuProps={MenuProps}
                                onOpen={handleOpenProv}
                            >
                            </Select>
                            </FormControl> */}
                            <Dialog fullScreen role="presentation" tabIndex="-1" open={this.state.openProvinsi}>
                                <div style={{ position: 'absolute', display: 'flex', minWidth: '-webkit-fill-available', justifyContent: 'center', alignItems: 'center' }}>
                                    <span style={{ left: '16px', position: 'absolute', top: '20px' }}>
                                        <ArrowBackIcon style={{ left: '16px', color: '#0057E7' }} onClick={()=>{handleCloseAllDaerah()}}/>
                                    </span>
                                    <span style={{
                                        display: 'flex', textAlign: 'center',
                                        justifyContent: 'center', alignItems: 'center',
                                        position: 'absolute', top: '20px', fontSize: '14px', color: '#9DA7AE'
                                    }}>Provinsi</span>
                                </div>
                                {/* <IconButton edge="start" color="inherit" onClick={() => handleCloseAllDaerah()} aria-label="close"
                                    style={{ position: 'fixed', width: '20px', right: '20px' }}>
                                    <CloseIcon />
                                </IconButton> */}
                                <Box style={{ marginTop: '80px', marginBottom: '20px' }}>
                                    <h3 style={{
                                        textAlign: 'left', marginTop: '0px', marginInlineStart: '16px',
                                        marginInlineEnd: '10px'
                                    }}>Provinsi Manakah anda tinggal ?</h3>
                                     <div className={classes.search}>
                                        <div className={classes.searchIcon}>
                                            <SearchIcon />
                                        </div>
                                        <InputBase
                                            placeholder="Cari..."
                                            classes={{
                                                root: classes.inputRoot,
                                                input: classes.inputInput,
                                            }}
                                            onChange={(event) => searchDaerah(event, 'provinsi')}
                                            inputProps={{ 'aria-label': 'search' }}
                                            id="cariProv"
                                            endAdornment={
                                                <InputAdornment position="end">
                                                    <IconButton color="inherit" onClick={() => resetVal("cariProv")} aria-label="close">
                                                        <CloseIcon className="close-logo-custom" />
                                                    </IconButton>
                                                </InputAdornment>
                                            }
                                        />
                                    </div>
                                </Box>
                                {
                                    this.state.loadingProvinsi ?
                                        <CircularProgress style={{justifyContent: 'center', alignItems: 'center', alignSelf: 'center'}}/>
                                        :
                                        this.state.listProvinsi.length > 0 ?
                                        this.state.listProvinsi.map((provinsi) => (
                                            <List className="listModalUsaha" key={provinsi.id} >
                                                <ListItem onClick={() => handleGetProvinsi(provinsi.id, provinsi.nama)} value={provinsi} >
                                                    <ListItemText primary={
                                                        <Typography 
                                                        style={{ fontFamily: 'Nunito',fontSize: '16px', fontWeight: '400', 
                                                        lineHeight: '24px', color: '#1A1A1A' }}>{provinsi.nama}</Typography>
                                                        } />
                                                    <ListItemSecondaryAction style={{ right : '6vw' }}>
                                                        <IconButton onClick={() => handleGetProvinsi(provinsi.id, provinsi.nama)} edge="end" aria-label="delete">
                                                            <ChevronRightIcon />
                                                        </IconButton>
                                                    </ListItemSecondaryAction>
                                                </ListItem>
                                            </List>
                                        ))
                                        :
                                        <List className="listModalUsaha">
                                            <ListItem>
                                                <ListItemText>
                                                <Typography 
                                                        style={{ fontFamily: 'Nunito',fontSize: '16px', fontWeight: '700', 
                                                        lineHeight: '24px', color: '#1A1A1A', textAlign: 'center' }}>Data Tidak Ditemukan</Typography>
                                                         
                                                </ListItemText>
                                            </ListItem>
                                        </List>
                                        
                                        }
                            </Dialog>
                            <Dialog fullScreen role="presentation" tabIndex="-1" open={this.state.openKota}>
                                <div style={{ position: 'fixed', display: 'flex', minWidth: '-webkit-fill-available', justifyContent: 'center', alignItems: 'center' }}>
                                    <span style={{ left: '16px', position: 'fixed', top: '20px' }}>
                                        <ArrowBackIcon style={{ left: '16px', color: '#0057E7' }} onClick={()=>{backToDaerah('provinsi')}}/>
                                    </span>
                                    <span style={{
                                        display: 'flex', textAlign: 'center',
                                        justifyContent: 'center', alignItems: 'center',
                                        position: 'fixed', top: '20px', fontSize: '14px', color: '#9DA7AE'
                                    }}>Kota/Kabupaten</span>
                                </div>
                                {/* <IconButton edge="start" color="inherit" onClick={() => handleCloseAllDaerah()} aria-label="close"
                                    style={{ position: 'fixed', width: '20px', right: '20px' }}>
                                    <CloseIcon />
                                </IconButton> */}
                                <Box style={{ marginTop: '80px', marginBottom: '20px' }}>
                                    <h3 style={{
                                        textAlign: 'left', marginTop: '0px', marginInlineStart: '16px',
                                        marginInlineEnd: '10px'
                                    }}>Kota / Kabupaten apa di {this.state.localProv} ?</h3>
                                    <div className={classes.search}>
                                        <div className={classes.searchIcon}>
                                            <SearchIcon />
                                        </div>
                                        <InputBase
                                            placeholder="Cari..."
                                            classes={{
                                                root: classes.inputRoot,
                                                input: classes.inputInput,
                                            }}
                                            onChange={(event) => searchDaerah(event, 'kota')}
                                            inputProps={{ 'aria-label': 'search' }}
                                            id="cariKota"
                                            endAdornment={
                                                <InputAdornment position="end">
                                                    <IconButton color="inherit" onClick={() => resetVal("cariKota")} aria-label="close">
                                                        <CloseIcon className="close-logo-custom"/>
                                                    </IconButton>
                                                </InputAdornment>
                                            }
                                        />
                                    </div>
                                </Box>
                                {
                                    this.state.loadingKota ?
                                        <CircularProgress style={{justifyContent: 'center', alignItems: 'center', alignSelf: 'center'}}/>
                                        :
                                        this.state.listKota.length > 0 ?
                                        this.state.listKota.map((kota) => (
                                            <List className="listModalUsaha" key={kota.id}>
                                                <ListItem onClick={() => handleGetKota(kota.id, kota.nama)} value={kota} >
                                                    <ListItemText primary={
                                                        <Typography 
                                                        style={{ fontFamily: 'Nunito',fontSize: '16px', fontWeight: '400', 
                                                        lineHeight: '24px', color: '#1A1A1A' }}>{kota.nama}</Typography>
                                                        } />
                                                    <ListItemSecondaryAction style={{ right : '6vw' }}>
                                                        <IconButton onClick={() => handleGetKota(kota.id, kota.nama)} edge="end" aria-label="delete">
                                                            <ChevronRightIcon />
                                                        </IconButton>
                                                    </ListItemSecondaryAction>
                                                </ListItem>
                                            </List>
                                        )):
                                        <List className="listModalUsaha">
                                            <ListItem>
                                                <ListItemText>
                                                <Typography 
                                                        style={{ fontFamily: 'Nunito',fontSize: '16px', fontWeight: '700', 
                                                        lineHeight: '24px', color: '#1A1A1A', textAlign: 'center' }}>Data Tidak Ditemukan</Typography>
                                                         
                                                </ListItemText>
                                            </ListItem>
                                        </List>
                                    }
                            </Dialog>

                            <Dialog fullScreen role="presentation" tabIndex="-1" open={this.state.openKecamatan}>
                                <div style={{ position: 'fixed', display: 'flex', minWidth: '-webkit-fill-available', justifyContent: 'center', alignItems: 'center' }}>
                                    <span style={{ left: '16px', position: 'fixed', top: '20px' }}>
                                        <ArrowBackIcon style={{ left: '16px', color: '#0057E7' }} onClick={()=>{backToDaerah('kota')}} />
                                    </span>
                                    <span style={{
                                        display: 'flex', textAlign: 'center',
                                        justifyContent: 'center', alignItems: 'center',
                                        position: 'fixed', top: '20px', fontSize: '14px', color: '#9DA7AE'
                                    }}>Kecamatan</span>
                                </div>

                                {/* <IconButton edge="start" color="inherit" onClick={() => handleCloseAllDaerah()} aria-label="close"
                                    style={{ position: 'fixed', width: '20px', right: '20px' }}>
                                    <CloseIcon />
                                </IconButton> */}
                                <Box style={{ marginTop: '80px', marginBottom: '20px' }}>
                                    <h3 style={{
                                        textAlign: 'left', marginTop: '0px', marginInlineStart: '16px',
                                        marginInlineEnd: '10px'
                                    }}>Kecamatan apa di {this.state.localKot} ?</h3>
                                    <div className={classes.search}>
                                        <div className={classes.searchIcon}>
                                            <SearchIcon />
                                        </div>
                                        <InputBase
                                            placeholder="Cari..."
                                            classes={{
                                                root: classes.inputRoot,
                                                input: classes.inputInput,
                                            }}
                                            onChange={(event) => searchDaerah(event, 'kecamatan')}
                                            inputProps={{ 'aria-label': 'search' }}
                                            id="cariKec"
                                            endAdornment={
                                                <InputAdornment position="end">
                                                    <IconButton color="inherit" onClick={() => resetVal("cariKec")} aria-label="close">
                                                        <CloseIcon className="close-logo-custom"/>
                                                    </IconButton>
                                                </InputAdornment>
                                            }
                                        />
                                    </div>
                                </Box>
                                {
                                    this.state.loadingKecamatan ?
                                        <CircularProgress style={{justifyContent: 'center', alignItems: 'center', alignSelf: 'center'}}/>
                                        :
                                        this.state.listKecamatan.length >0 ? 
                                        this.state.listKecamatan.map((kecamatan) => (
                                            <List className="listModalUsaha" key={kecamatan.id}>
                                                <ListItem onClick={() => handleGetKecamatan(kecamatan.id, kecamatan.nama)} value={kecamatan} >
                                                    <ListItemText primary={
                                                        <Typography 
                                                        style={{ fontFamily: 'Nunito',fontSize: '16px', fontWeight: '400', 
                                                        lineHeight: '24px', color: '#1A1A1A' }}>{kecamatan.nama}</Typography>
                                                        } />
                                                    <ListItemSecondaryAction style={{ right : '6vw' }}>
                                                        <IconButton onClick={() => handleGetKecamatan(kecamatan.id, kecamatan.nama)} edge="end" aria-label="delete">
                                                            <ChevronRightIcon />
                                                        </IconButton>
                                                    </ListItemSecondaryAction>
                                                </ListItem>
                                            </List>
                                        )):
                                        <List className="listModalUsaha">
                                        <ListItem>
                                            <ListItemText>
                                            <Typography 
                                                    style={{ fontFamily: 'Nunito',fontSize: '16px', fontWeight: '700', 
                                                    lineHeight: '24px', color: '#1A1A1A', textAlign: 'center' }}>Data Tidak Ditemukan</Typography>
                                                     
                                            </ListItemText>
                                        </ListItem>
                                    </List>
                                     }
                            </Dialog>

                            <Dialog fullScreen role="presentation" tabIndex="-1" open={this.state.openKelurahan}>
                                <div style={{ position: 'fixed', display: 'flex', minWidth: '-webkit-fill-available', justifyContent: 'center', alignItems: 'center' }}>
                                    <span style={{ left: '16px', position: 'fixed', top: '20px' }}>
                                        <ArrowBackIcon style={{ left: '16px', color: '#0057E7' }}  onClick={()=>{backToDaerah('kecamatan')}} />
                                    </span>
                                    <span style={{
                                        display: 'flex', textAlign: 'center',
                                        justifyContent: 'center', alignItems: 'center',
                                        position: 'fixed', top: '20px', fontSize: '14px', color: '#9DA7AE'
                                    }}>Kelurahan</span>
                                </div>
                                {/* <IconButton edge="start" color="inherit" onClick={() => handleCloseAllDaerah()} aria-label="close"
                                    style={{ position: 'fixed', width: '20px', right: '20px' }}>
                                    <CloseIcon />
                                </IconButton> */}
                                <Box style={{ marginTop: '80px', marginBottom: '20px' }}>
                                    <h3 style={{
                                        textAlign: 'left', marginTop: '0px', marginInlineStart: '16px',
                                        marginInlineEnd: '10px'
                                    }}>Kelurahan apa di {this.state.localKec} ?</h3>
                                    <div className={classes.search}>
                                        <div className={classes.searchIcon}>
                                            <SearchIcon />
                                        </div>
                                        <InputBase
                                            placeholder="Cari..."
                                            classes={{
                                                root: classes.inputRoot,
                                                input: classes.inputInput,
                                            }}
                                            onChange={(event) => searchDaerah(event, 'kelurahan')}
                                            inputProps={{ 'aria-label': 'search' }}
                                            id="cariKel"
                                            endAdornment={
                                                <InputAdornment position="end">
                                                    <IconButton color="inherit" onClick={() => resetVal("cariKel")} aria-label="close">
                                                        <CloseIcon className="close-logo-custom"/>
                                                    </IconButton>
                                                </InputAdornment>
                                            }
                                        />
                                    </div>
                                </Box>
                                {
                                    this.state.loadingKelurahan ?
                                        <CircularProgress style={{justifyContent: 'center', alignItems: 'center', alignSelf: 'center'}}/>
                                        :
                                        this.state.listKelurahan.length>0 ?
                                        this.state.listKelurahan.map((kelurahan) => (
                                            <List className="listModalUsaha" key={kelurahan.id}>
                                                <ListItem onClick={() => handleGetKelurahan(kelurahan.id, kelurahan.nama)} value={kelurahan} >
                                                    <ListItemText primary={
                                                        <Typography 
                                                        style={{ fontFamily: 'Nunito',fontSize: '16px', fontWeight: '400', 
                                                        lineHeight: '24px', color: '#1A1A1A' }}>{kelurahan.nama}</Typography>
                                                        } />
                                                    <ListItemSecondaryAction style={{ right : '6vw' }}>
                                                        <IconButton onClick={() => handleGetKelurahan(kelurahan.id, kelurahan.nama)} edge="end" aria-label="delete">
                                                            <ChevronRightIcon />
                                                        </IconButton>
                                                    </ListItemSecondaryAction>
                                                </ListItem> 
                                            </List>
                                        )) :
                                        <List className="listModalUsaha">
                                        <ListItem>
                                            <ListItemText>
                                            <Typography 
                                                    style={{ fontFamily: 'Nunito',fontSize: '16px', fontWeight: '700', 
                                                    lineHeight: '24px', color: '#1A1A1A', textAlign: 'center' }}>Data Tidak Ditemukan</Typography>
                                                     
                                            </ListItemText>
                                        </ListItem>
                                    </List>}
                            </Dialog>
                        
                    </Grid>
                    </div>}

                    {this.state.selIdx === 0 &&
                        <Box style={{
                            bottom: '20px', position: 'relative',
                            marginInlineStart: '20px', minWidth: '90%', marginInlineEnd: '30px',
                            marginTop: '50px'
                        }}>
                            
                            <Button disabled={!this.state.isValid} onClick={() => this.props.history.push('/reviewData', {from: '/informasiLokasiUsaha'})}
                                size="large" style={{ borderRadius: '8px',
                                    width: "-webkit-fill-available", marginTop: '10px',
                                    textTransform: 'none', marginBottom: '10px', height: '56px'
                                }} variant="contained" color="primary">
                                    Simpan
                                </Button>
                        </Box>
                    }
                    {this.state.selIdx === 1 &&
                        <Box className="butLokUs" style={{
                            bottom: '20px',
                            marginInlineStart: '20px', minWidth: '90%', marginInlineEnd: '30px',
                            marginTop: '50px'
                        }}>

                            
                                <Button onClick={() => this.props.history.push('/reviewData', {from: '/informasiLokasiUsaha'})}
                                    disabled={!this.state.isValid} size="large" style={{ borderRadius: '8px',
                                    width: "-webkit-fill-available", marginTop: '10px',
                                    textTransform: 'none', marginBottom: '10px', height: '56px'
                                }} variant="contained" color="primary">
                                    Simpan
                                </Button>
                        </Box>
                    }

                
            </div>
        )
    }
}

export default compose(
    withStyles(useStyles),
    connect(
        (state) => ({
            registrasi: state.registrasi
        }),
        {
            getLokasiUsaha: registrasiActions.getLokasiUsaha,
            getLokasiUsahaNon: registrasiActions.getLokasiUsahaNon
        }
    )
)(informasiLokasiUsaha)