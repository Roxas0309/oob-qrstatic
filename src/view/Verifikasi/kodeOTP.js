import React, { Component } from "react";
import { connect } from "react-redux";
import Link from "@material-ui/core/Link";
import ArrowBackIcon from "@material-ui/icons/ArrowBack";
import {
  AppBar,
  Backdrop,
  Box,
  Button,
  Card,
  CircularProgress,
  Fade,
  Grid,
  TextField,
  Toolbar,
  Typography,
  withStyles,
} from "@material-ui/core";
import logoTlpOtp from "../../../public/img/logoTlpOtp.png";
import { Skeleton } from "@material-ui/lab";
import {
  BACK_END_POINT,
  HEADER_AUTH,
  PUT,
  POST,
  TIMES_OUT,
  wsWithBody,
  wsWithoutBody,
} from "../../master/masterComponent";
import { compose } from "recompose";
import sukse from "../../../public/img/success.gif";
import gagal from "../../../public/img/unapproved.gif";
import { createBrowserHistory } from "history";
import { registrasiActions } from "../../actions";

const useStyles = (theme) => ({
  paper: {
    backgroundColor: theme.palette.background.paper,
    border: "1px solid lightgray",
    boxShadow: theme.shadows[5],
    padding: theme.spacing(2, 4, 3),
    textAlign: "center",
    borderRadius: "5px",
    alignItems: "center",
    justifyContent: "center",
    minWidth: "150px",
  },
  placeholder: {
    height: "auto",
    justifyContent: "center",
    alignItems: "center",
    display: "flex",
  },
  backdrop: {
    zIndex: theme.zIndex.drawer + 1,
    color: "#fff",
  },
});
//const history = createBrowserHistory();
class kodeOTP extends Component {
  timer = null;
  constructor(props) {
    super(props);

    this.state = {
      loading: true,
      phone: "",
      open: false,
      query: "idle",
      proses: "",
    };
  }

  componentDidMount() {
    

    // console.log('his2: ', this.props.history(el => {
    //   console.log('hisFuncOtp1: ', el)
    // }))
    
    if (localStorage.getItem("noTelpOtp")) {
      this.setState({ phone: localStorage.getItem("noTelpOtp") });
      this.setState({ loading: false });
    }
  }

  componentWillUnmount(){
    //location.reload() 
    //location.href = "/kodeOTP";
    
    location.reload()


  }

  render() {
    const { isLoadingSendOtp, sendOtp, classes } = this.props;
    
    const nextSendOtp = () => {
      this.setState({ open: true });
      if (this.state.query !== "idle") {
        this.setState({ query: "idle" });
        return;
      }

      this.setState({ query: "progress" });

      sendOtp({ cifInEncrypt: localStorage.getItem("cifEncryptor") }).then(
        (res) => {
          isLoadingSendOtp && console.log("responseOtpSend: ", res);
          if (res.status === undefined) {
            this.setState({ proses: "lost" });
            this.setState({ query: this.state.proses });
            this.setState({
              messageError: "Koneksi terputus silahkan coba kembali",
            });
            actions.setSubmitting(false);
            throw res;
          }

          if (res.status === 406) {
            this.setState({ proses: "lost" });
            this.setState({ query: this.state.proses });
            this.setState({ messageError: res.message });
            throw res;
          }

          if (res.result.otpStatusError) {
            this.setState({ proses: "lost" });
            this.setState({ query: this.state.proses });
            this.setState({ messageError: res.message });
          } else {
            this.setState({
              phone: res.result.phoneNumber,
              encrypValue: res.result.encrpyptValidationOtp,
            });
            this.setState({ proses: "success" });
            this.setState({ query: this.state.proses });
            this.timer = setTimeout(() => {
              this.setState({ loading: false });
              this.setState({ query: "idle" });
              this.setState({ open: false });
              localStorage.setItem(
                "encrypValue",
                res.result.encrpyptValidationOtp
              );
              this.props.history.push("/inputKodeOTP", {from: '/kodeOTP'});
              location.reload();
            }, 4000);
          }
        },
        (error) => {
          console.log("errorr respon:", error);
          if (error.message === "Network Error") {
            alert("Oops jaringan internet terputus silahkan coba kembali");
            this.setState({
              open: false,
              proses: "",
              query: "idle",
            });
            location.reload();
          } else {
            this.setState({ proses: "failed" });
            this.setState({ query: this.state.proses });
            this.setState({ messageError: error.message });
            // window.localStorage.clear();
            location.reload();
            // localStorage.removeItem('rekInfo_ibuKandung')
            // localStorage.removeItem('rekInfo_ownerName')
            // localStorage.removeItem('rekInfo_noKtp')
            // localStorage.removeItem('rekInfo_noRekMandiri')
          }
        }
      );

      // wsWithBody(BACK_END_POINT + "/otpCtl/sendMyOTp/forRegistration",
      // POST, {cifInEncrypt : localStorage.getItem('cifEncryptor')}, HEADER_AUTH)
      // .then(response => {
      //         if(response.data.result.otpStatusError){
      //             this.setState({ proses: 'failed' });
      //             this.setState({ query: this.state.proses });
      //             this.setState({ messageError: response.data.message });
      //             location.reload()
      //         }
      //         else{
      //             this.setState({phone : response.data.result.phoneNumber,
      //             encrypValue:response.data.result.encrpyptValidationOtp})
      //             this.setState({proses: 'success'})
      //             this.setState({query: this.state.proses});
      //             this.timer = setTimeout(() => {
      //             this.setState({loading:false});
      //             this.setState({query: 'idle'})
      //             this.setState({open: false})
      //             localStorage.setItem('encrypValue', response.data.result.encrpyptValidationOtp)
      //             history.push('/inputKodeOTP')
      //             location.reload();

      //         }, 4000);
      //     }
      // }).catch(error => {

      //     console.log('errorr respon:',error);
      //     if(error.message === "Network Error"){
      //         alert('Oops jaringan internet terputus silahkan coba kembali')
      //         this.setState({
      //             open: false,
      //             proses: '',
      //             query: 'idle'
      //         })
      //         location.reload()
      //     }else{
      //         this.setState({ proses: 'failed' });
      //         this.setState({ query: this.state.proses });
      //         this.setState({ messageError: error.response.data.message })
      //        // window.localStorage.clear();
      //         location.reload()
      //         // localStorage.removeItem('rekInfo_ibuKandung')
      //         // localStorage.removeItem('rekInfo_ownerName')
      //         // localStorage.removeItem('rekInfo_noKtp')
      //         // localStorage.removeItem('rekInfo_noRekMandiri')
      //     }

      // })
    };

    return (
      <div>
        {/* <div style={{ marginLeft: "20px", marginTop: "20px" }}>
          <Link href="/infoRekening">
            <ArrowBackIcon />
          </Link>
        </div> */}
        

        <Backdrop className={classes.backdrop} open={this.state.open}>
          <div className={classes.placeholder}>
            {this.state.query === "success" ? (
              <Typography
                component={"span"}
                variant={"body2"}
                className={classes.paper}
              >
                <span>
                  <h4 style={{ color: "black" }}>Kode OTP Berhasil terkirim</h4>
                </span>
                <img src={sukse} style={{ height: 50, width: 50 }}></img>
              </Typography>
            ) : this.state.query === "lost" ? (
              <Box className={classes.paper}>
                <Typography component={"span"} variant={"body2"}>
                  <span>
                    <p style={{ color: "black", width: 220 }}>
                      {" "}
                      {this.state.messageError}{" "}
                    </p>
                  </span>
                  <img src={sukses} style={{ height: 50, width: 50 }}></img>
                </Typography>
                <Link>
                  <Button
                    onClick={klikClose}
                    variant="contained"
                    style={{ width: "100%", marginTop: "20px" }}
                    color="primary"
                    disableElevation
                  >
                    Close
                  </Button>
                </Link>
              </Box>
            ) : this.state.query == "failed" ? (
              <Typography
                component={"span"}
                variant={"body2"}
                className={classes.paper}
              >
                <span>
                  <h4 style={{ color: "black" }}>{this.state.messageError}</h4>
                </span>
                <img src={gagal} style={{ height: 50, width: 50 }}></img>
              </Typography>
            ) : (
              <Fade
                in={this.state.query === "progress"}
                style={{
                  transitionDelay:
                    this.state.query === "progress" ? "500ms" : "0ms",
                }}
                unmountOnExit
              >
                <div className={classes.paper}>
                  <span>
                    <h4 style={{ color: "black" }}>Sedang mengirim kode OTP</h4>
                  </span>
                  <CircularProgress />
                </div>
              </Fade>
            )}
          </div>
        </Backdrop>
        <AppBar style={{backgroundColor: 'transparent', position: 'absolute', 
            boxShadow: 'unset'}}>
            <Toolbar style={{minHeight: '60px'}}>
                <Link href="/infoRekening" style={{position: 'absolute', display: 'flex'}} >
                    <ArrowBackIcon/>
                </Link>
            </Toolbar>
        </AppBar>
        {this.state.loading ? (
          <Skeleton
            animation="wave"
            variant="rect"
            height={500}
            width={"90%"}
            style={{ marginLeft: "20px", marginRight: "20px" }}
          ></Skeleton>
        ) : (
          <Grid className="butOtp"
            style={{
              padding: "16px",
              justifyContent: "center",
              alignItems: "center",
              textAlign: "center",
              marginTop: "70px",
            }}
          >
            <h4
              style={{
                textAlign: "center",
                marginTop: "0px",
                fontFamily: "Montserrat",
                fontSize: "18px",
                fontWeight: "700",
                lineHeight: "27px",
              }}
            >
              Kode Verifikasi
            </h4>
            <p
              style={{
                marginTop: "5px",
                color: "#9F9F9F",
                marginBottom: "48px",
                fontSize: "16px",
                lineHeight: "24px",
                marginInlineStart: "22px",
                marginInlineEnd: "22px",
              }}
            >
              Kami akan mengirimkan SMS berisi kode verifikasi ke nomor yang
              terdaftar di Bank Mandiri ({this.state.phone}).
            </p>

            <Box
              style={{
                textAlign: "center",
                position: "fixed",
                display: "contents",
              }}
            >
              <img src={logoTlpOtp} height={259} />
            </Box>

            <p
              style={{
                width: '309px',
                height: '63px',
                color: "#9F9F9F",
                marginBottom: "10px",
                fontSize: "14px",
                lineHeight: "21px",
                marginInlineStart: "22px",
                marginInlineEnd: "22px",
              }}
            >
              Biaya akan dibebankan oleh operator. <br/>Pastikan kartu Anda memiliki
              cukup pulsa dan berada dalam masa aktif.
            </p>

              <Button
                style={{
                  width:'100%',
                  height:'56px',
                  backgroundColor: "rgba(0,87,231,1)",
                  boxShadow: "0px 4px 4px -4px #8CC3F8 0px 4px 8px 2px #C5E1FB",
                  textTransform:'none',
                  marginTop: "38px",
                  marginBottom:'32px',
                  fontFamily: "Nunito",
                  fontStyle: "normal",
                  fontWeight: "bold",
                  fontSize: '16px',
                  lineheight: '24px',
                  alignItems: 'center',
                  textAlign: 'center',
                  color: '#FFFFFF',
                  borderRadius: '8px'
                }}
                disabled={this.state.loading}
                onClick={nextSendOtp}
                size="large"
                disableElevation
                variant="contained"
              >
              Lanjutkan
            </Button>
          </Grid>
        )}
      </div>
    );
  }
}

export default compose(
  withStyles(useStyles),
  connect(
    (state) => ({
      isLoadingSendOtp: state.registrasi.isLoadingSendOtp,
      isLoading: state.submit.isLoading,
      isPreparing: state.submit.isPreparing,
    }),
    {
      sendOtp: registrasiActions.sendOtp,
    }
  )
)(kodeOTP);
