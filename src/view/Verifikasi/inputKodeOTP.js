import React, { Component } from 'react'
import { connect } from 'react-redux';
import Link from '@material-ui/core/Link';
import ArrowBackIcon from '@material-ui/icons/ArrowBack';
import { AppBar, Box, Button, CircularProgress, Grid, Toolbar } from '@material-ui/core';
import OtpInput from 'react-otp-input';
import compose from 'recompose/compose';
import { withStyles } from '@material-ui/core/styles';
import Fade from '@material-ui/core/Fade';
import Backdrop from '@material-ui/core/Backdrop';
import Typography from '@material-ui/core/Typography';
import { createBrowserHistory } from 'history';
import sukse from '../../../public/img/success.gif';
import gagal from '../../../public/img/unapproved.gif';
import { blue } from '@material-ui/core/colors';
import clsx from 'clsx';
import { Alert } from '@material-ui/lab';
import { registrasiActions } from '../../actions';
import moment from "moment";

const useStyles = (theme) => ({
    containerOtp: {
        display: 'inline-flex !important',
        alignItems: 'center',
        width: '40px',
        height: '40px',
        justifyContent:'center',
        textAlign: 'center'
    },
    inputOtpIos: {
        textAlign: 'center',
        width:'8vw !important',
        //maxWidth: '12vw !important',
        //marginRight: '10px',
        height: '40px',
        fontSize: '4.326vw',
        fontWeight:'700',
        margin: '5px',
        border: 'none',
        borderRadius: '5px',
        color: 'slategray',
        backgroundColor: "#F6F7F8"
    },
    inputOtpErrorIos: {
        textAlign: 'center',
        width:'8vw !important',
        //maxWidth: '12vw !important',
        height: '40px',
        fontSize: '4.326vw',
        fontWeight:'700',
        margin: '5px',
        border: 'solid 1px red',
        borderRadius: '5px',
        color: 'slategray',
        backgroundColor: "#F6F7F8"
    },
    focusStyle: {
        borderBottom:'solid 4px rgba(0, 87, 231, 1)',
    },
    modal: {
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
      },
      paper: {
        backgroundColor: theme.palette.background.paper,
        border: '1px solid lightgray',
        boxShadow: theme.shadows[5],
        padding: theme.spacing(2, 4, 3),
        textAlign: 'center',
        borderRadius: '5px',
        alignItems: 'center',
        justifyContent: 'center',
        minWidth: '150px'

      },
      placeholder: {
          height: 'auto',
          justifyContent: 'center',
          alignItems: 'center',
          display: 'flex'
      },
      backdrop: {
        zIndex: theme.zIndex.drawer + 1,
        color: '#fff',
      },
      buttonSuccess: {
        backgroundColor: blue[500],
        '&:hover': {
          backgroundColor: blue[700],
        },
      },
      buttonProgress: {
        color: blue[500],
        display: 'block',
        marginTop: 30,
        marginBottom: 10,
        marginLeft: '46%',
      },
});


class inputKodeOTP extends Component {

    timer = null
    waktu = null
    timerOtp = 0;

    constructor(props){
        super(props);
        this.state = {
            showotpbox : false,
            encrypValue : '',
            cifEnc: '',
            phone : localStorage.getItem('noTelpOtp'),
            otp: '',
            inputOtp: 0,
            open: false,
            count: [],
            query: 'idle',
            kodeOtp: '123456',
            proses: '',
            time: 60,
            do: false,
            complete: false,
            loading: false,
            sukses: false,
            failureOtpWording:null,
            wrongOtpWording:null
        }

       
    }

    callOtp = (cifEnc) => {
        const { isLoadingSendOtp, sendOtp } = this.props

        sendOtp({cifInEncrypt : cifEnc}).then(res => {

            isLoadingSendOtp && 
            console.log('respon kirim ulang:',res);
            if(res.result.otpStatusError){
                this.setState({ failureOtpWording: res.message });
                this.setState({showotpbox:true});
            }else{
                this.setState({phone : res.result.phoneNumber,
                    encrypValue:res.result.encrpyptValidationOtp})
                    this.setState({showotpbox:true});
                    console.log('callOtp: ', res)
                    return this.id = setInterval(this.initial, 1000);
                
            }

        })
    }

    componentDidMount(){
        this.setState({cifEnc: localStorage.getItem('cifEncryptor'), 
        encrypValue: localStorage.getItem('encrypValue'),
        showotpbox: true}),console.log('losCif:', localStorage.getItem('cifEncryptor')) ;
        
        var startTime = moment(new Date()).format("YYYYMMDDHHmmSS")

        var kadaluarsa_date = localStorage.getItem("kadaluarsa_date");
        if(kadaluarsa_date==null||kadaluarsa_date==""){
        localStorage.setItem('kadaluarsa_date',startTime);
        }else{

            var dateKadaluarsa = moment(kadaluarsa_date,"YYYYMMDDHHmmSS");
            const diff = moment(moment(new Date()).format("YYYYMMDDHHmmSS"),"YYYYMMDDHHmmSS").diff(moment(kadaluarsa_date,"YYYYMMDDHHmmSS"));
            const diffDuration = moment.duration(diff);
            console.log("diffduration :" + diffDuration.minutes());

            if(diffDuration.minutes()>=30){
                this.setState({query: 'expired'});
                this.setState({open: true});

                this.timer = setTimeout(() => {
                    this.setState({otp: ''})
                    this.setState({query: 'idle'})
                    this.setState({open: false})
                    this.props.history.push('/', {from: '/inputKodeOTP'})
                    location.reload();
                }, 6000)
            }else{
                localStorage.setItem('kadaluarsa_date',startTime);
            }

        }
        
        return this.id = setInterval(this.initial, 1000);
    }

    componentWillUnmount(){
       // location.href = "/inputKodeOTP"
       clearInterval(this.id);
        location.reload()
    }

    start = () => {
        const { isLoadingSendOtp, sendOtp } = this.props
        if(!this.state.loading){
            
            var kadaluarsa_date = localStorage.getItem("kadaluarsa_date");
            if(kadaluarsa_date==null||kadaluarsa_date==""){
            localStorage.setItem('kadaluarsa_date',startTime);
            }else{
    
                var dateKadaluarsa = moment(kadaluarsa_date,"YYYYMMDDHHmmSS");
                const diff = moment(moment(new Date()).format("YYYYMMDDHHmmSS"),"YYYYMMDDHHmmSS").diff(moment(kadaluarsa_date,"YYYYMMDDHHmmSS"));
                const diffDuration = moment.duration(diff);
                console.log("diffduration :" + diffDuration.minutes());
    
                if(diffDuration.minutes()>=30){
                    this.setState({query: 'expired'});
                    this.setState({open: true});
    
                    this.timer = setTimeout(() => {
                        this.setState({otp: ''})
                        this.setState({query: 'idle'})
                        this.setState({open: false})
                        this.props.history.push('/', {from: '/inputKodeOTP'})
                        location.reload();
                    }, 6000)
                   return;
                }
                else{
                        
                   var startTime = moment(new Date()).format("YYYYMMDDHHmmSS")
                    localStorage.setItem('kadaluarsa_date',startTime);
                }
    
            }


            this.setState({sukses: false, loading: true})
            sendOtp({cifInEncrypt : this.state.cifEnc}).then(res => {

                isLoadingSendOtp && 
                console.log('respon kirim ulang:',res);
                if(res.result.otpStatusError){
                    this.setState({ 
                        failureOtpWording: res.message,
                        showotpbox:true,
                        sukses: true, loading: false,
                        time: 60 
                    });
                }else{
                    this.setState({
                        sukses: true, loading: false,
                        time: 60,

                    });
                    //this.setState({showotpbox:false})
                    this.setState({phone : res.result.phoneNumber,
                        encrypValue:res.result.encrpyptValidationOtp})
                        this.setState({showotpbox:true});
                        console.log('callOtp: ', res)
                        return this.id = setInterval(this.initial, 1000);
                    
                }
    
            })

            // clearTimeout(this.waktu);
            // this.waktu = setTimeout(() => {
            //     this.setState({sukses: true, loading: false});
            //     this.setState({time: 60})
            //     this.setState({showotpbox:false})
            //     this.callOtp(this.state.cifEnc);
            // },1000)

        }
       
      };

    initial = () => {
        if (this.state.time !== 0) {
            this.setState((prevState, prevProps) => ({
              time: prevState.time - 1
            }));
            if (this.state.time === 0) {
              clearInterval(this.id);
              this.setState({ complete: true });
            }
          }
    }

    handleChange = otp => {
        const { validateOtp, isLoadingValOtp } = this.props
        console.log('inputOtpLengt: ', otp)
        this.setState({
            otp: otp,
            count: otp,
            proses: ''
        })
        console.log('count: ', this.state.count.length);
        if(this.state.count.length == 5){
            this.setState({open: true})

            if(this.state.query !== 'idle'){
                this.setState({query: 'idle'});
                return;
            }
    
            this.setState({query: 'progress'});

            validateOtp({encrpyptValidationOtp : this.state.encrypValue, otpValue : otp}).then(res => {

                isLoadingValOtp && 
                console.log('responseValOtp: ', res);
                if(res.status === undefined){
                    this.setState({ proses: 'failed' });
                    this.setState({ query: this.state.proses });
                    this.setState({ messageError: "Koneksi terputus silahkan coba kembali" })
                    throw res;
                    
                }

                if(res.status > 200){
                    console.log('error valOtp 1: ',res);
                    this.setState({wrongOtpWording:res.message})
                    this.setState({proses: 'failed'})
                    this.setState({query: this.state.proses});
                    this.timer = setTimeout(() => {
                        this.setState({otp: ''})
                        this.setState({query: 'idle'})
                        this.setState({open: false})
                    }, 4000)
                    throw res;
                }else{
                    this.setState({proses: 'success'})
                    this.setState({query: this.state.proses});
                    this.timer = setTimeout(() => {
                        this.setState({otp: ''})
                        this.setState({query: 'idle'})
                        this.setState({open: false})
                        this.props.history.push('/verifSukses', {from: '/inputKodeOTP'})
                        location.reload();
                    }, 4000);

                }

            }, err => {
                console.log('error valOtp 1: ',err);
                    this.setState({wrongOtpWording:error.message})
                    this.setState({proses: 'failed'})
                    this.setState({query: this.state.proses});
                    this.timer = setTimeout(() => {
                        this.setState({otp: ''})
                        this.setState({query: 'idle'})
                        this.setState({open: false})
                    }, 4000)
            }).catch(error => {
                console.log('error valOtp 2: ',error);
                this.setState({wrongOtpWording:error.message})
                this.setState({proses: 'failed'})
                this.setState({query: this.state.proses});
                this.timer = setTimeout(() => {
                    this.setState({otp: ''})
                    this.setState({query: 'idle'})
                    this.setState({open: false})
                }, 4000)

            })

                
        }
    };

    handleOpen = () => {
        this.setState({open: true})
    }
    
    handleClose = () => {
        this.setState({open: false})
    }

    render() {
        const { classes, isLoadingSendOtp} = this.props;
        const buttonClassname = clsx({
            [classes.buttonSuccess]: this.state.sukses,
          });

        return (
            <div>
                {/* <div style={{marginLeft:'20px', marginTop:'24px'}}>
                    <Link href="/kodeOTP">
                        <ArrowBackIcon/>
                    </Link>
                </div> */}
                <Backdrop className={classes.backdrop} open={this.state.open} >
                    <div className={classes.placeholder}>

            

                    {
                        this.state.query === 'success' ? 
                                (<Typography component={'span'} variant={'body2'} className={classes.paper}>
                                    <span><h4 style={{color: 'black'}}>Verifikasi Berhasil</h4></span>
                                    <img src={sukse} 
                                    style={{ height: 50, width: 50 }}></img>
                                </Typography>) :
                        this.state.query === 'expired' ? 
                                 (<Typography component={'span'} variant={'body2'} className={classes.paper}>
                                     <span><h4 style={{color: 'black', width:'50vw'}}>Page Ini Sudah Kadaluarsa. 
                                     Anda Secara Otomatis Diarahkan ke Halaman Awal. <br/>[Last Date Visited : {moment(localStorage.getItem("kadaluarsa_date"), "YYYYMMDDHHmmSS").format("YYYY/MM/DD HH:mm")} ]</h4></span>
                                     <img src={gagal}
                             style={{ height: 50, width: 50 }}></img>
                                 </Typography>) :
                        this.state.query == 'failed' ? 
                             (<Typography component={'span'} variant={'body2'} className={classes.paper}>
                             <span><h4 style={{color: 'black'}}>Verifikasi Tidak Berhasil</h4></span>
                             <img src={gagal}
                             style={{ height: 50, width: 50 }}></img>
                         </Typography>) :
                         (<Fade in={this.state.query === 'progress'}
                            style={{transitionDelay: this.state.query === 'progress' ? '500ms' : '0ms'}}
                            unmountOnExit
                            >
                                <div className={classes.paper}>
                                    <span><h4 style={{color: 'black'}}>
                                        Melakukan Verifikasi</h4></span>
                                    <CircularProgress />
                                </div>
                            </Fade>)
                        }

                    </div>
                </Backdrop>
                <AppBar style={{backgroundColor: 'transparent', position: 'absolute', 
                    boxShadow: 'unset'}}>
                    <Toolbar style={{minHeight: '60px'}}>
                        <Link href="/kodeOTP" style={{position: 'absolute', display: 'flex'}} >
                            <ArrowBackIcon/>
                        </Link>
                    </Toolbar>
                </AppBar>
                <Grid style={{padding: '16px', justifyContent: 'center', alignItems: 'center', textAlign: 'center'}}>
                    <h4 style={{textAlign: 'center', 
                                fontFamily: 'Montserrat', 
                                fontStyle: 'normal',
                                fontWeight: 'bold',
                                fontSize: '18px',
                                lineHeight: '150%',
                                marginTop:'60px',
                                marginBottom:'9px',
                                /* identical to box height, or 27px */
                                textAlign: 'center',
                                /* Black - Character / High Emphasis */
                                color: '#121518',
                                }}>
                    { this.state.showotpbox ?
                        'Masukan Kode Verifikasi' : 'Sedang Mengirim Kode Verifikasi' 
                    }
                    </h4>
                    <p style={{
                            textAlign: 'center',
                            fontFamily: 'Nunito', 
                            fontStyle: 'normal',
                            fontWeight: 'normal',
                            fontSize: '16px',
                            lineHeight: '150%',
                            /* identical to box height, or 27px */
                            textAlign: 'center',
                            /* Black - Character / High Emphasis */
                            color: 'rgba(48, 59, 74, 0.72)',
                            //marginBottom:'26px',
                            marginLeft:'30px',
                            marginRight:'30px'
                    }}>
                    Kode verifikasi 6 digit
                    { this.state.showotpbox ?
                    ' telah ' : ' akan '} 
                    dikirimkan ke nomor 
                    { this.state.showotpbox ? ' ' + this.state.phone : ' Anda '}
                    </p>
                    <br/>
                    { this.state.showotpbox ?
                    <Box 
                        style={{ width:'100%', justifyContent:'center', alignItems: 'center', display: 'contents'}}>
                        <OtpInput
                            value={this.state.otp}
                            onChange={this.handleChange}
                            numInputs={6}
                            containerStyle={classes.containerOtp}
                            inputStyle={this.state.proses === "failed" ? classes.inputOtpErrorIos : classes.inputOtpIos}
                            isInputNum={true}
                            hasErrored={true}
                            style={{width:'2vw'}}
                            focusStyle={classes.focusStyle}
                        />
                        {console.log('otp: ', this.state.otp)}
                    
                    {this.state.proses === "failed" && <p style={{color: 'red', fontSize: 'smaller'}}>{this.state.wrongOtpWording}</p>}
                    <br/>
                    {this.state.loading ? <CircularProgress size={24} className={classes.buttonProgress} /> : 
                    <p style={{
                        fontFamily: 'TTInterfaces', 
                        fontSize: '12px',
                        marginTop: '44px', 
                        fontWeight: '500',
                        lineHeight: '15px',
                        color: '#8CC3F8', 
                        marginBottom: '30px'
                    }}>Kirim ulang kode dalam {this.state.time} Detik</p>}
                    {this.state.time == 0 && 
                        <Button style={{
                            width:'100%',
                            height:'56px',
                            backgroundColor: "#2661DA",
                            boxShadow: "0px 4px 4px -4px #8CC3F8 0px 4px 8px 2px #C5E1FB",
                            textTransform:'none',
                            marginTop: "38px",
                            marginBottom:'32px',
                            fontFamily: "Nunito",
                            fontStyle: "normal",
                            fontWeight: "bold",
                            fontSize: '16px',
                            lineheight: '24px',
                            alignItems: 'center',
                            textAlign: 'center',
                            color: '#FFFFFF',
                            borderRadius: '8px'
                        }} 
                        variant="contained" color="primary" onClick={this.start}
                            className={buttonClassname}
                            disabled={this.state.loading}
                        >Kirim Ulang Kode</Button>
                        }
                   {this.state.failureOtpWording !=null ?
                    <Alert variant="filled" severity="error">
                    <h3><strong>{this.state.failureOtpWording}</strong></h3>
                    </Alert>:<div></div>
                   }
                    </Box>
                    : <Box></Box>}
                </Grid>
            </div>
        )
    }
}

export default compose(
    withStyles(useStyles),
    connect(
        (state) => ({
            isLoadingSendOtp: state.registrasi.isLoadingSendOtp,
            isLoadingValOtp: state.registrasi.isLoadingValOtp,
            isLoading: state.submit.isLoading,
            isPreparing: state.submit.isPreparing
        }),
        {
            validateOtp: registrasiActions.validateOtp,
            sendOtp: registrasiActions.sendOtp
        }
    )
)(inputKodeOTP)