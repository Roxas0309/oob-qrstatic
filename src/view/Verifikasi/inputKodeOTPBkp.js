import React, { Component } from 'react'
import Link from '@material-ui/core/Link';
import ArrowBackIcon from '@material-ui/icons/ArrowBack';
import { Box, Button, Card, CircularProgress, Grid, TextField } from '@material-ui/core';
import OtpInput from 'react-otp-input';
import Modal from '@material-ui/core/Modal';
import compose from 'recompose/compose';
import { withStyles } from '@material-ui/core/styles';
import Fade from '@material-ui/core/Fade';
import Backdrop from '@material-ui/core/Backdrop';
import Typography from '@material-ui/core/Typography';
import CheckCircleRoundedIcon from '@material-ui/icons/CheckCircleRounded';
import verifSukses from './verifSukses';
import { createBrowserHistory } from 'history';
import sukse from '../../../public/img/success.gif';
import gagal from '../../../public/img/unapproved.gif';

const useStyles = (theme) => ({
    containerOtp: {
        display: 'inline-flex !important',
        alignItems: 'center',
        width: '40px',
        height: '40px',
        justifyContent:'center',
        textAlign: 'center'

    },
    inputOtp: {
        textAlign: 'center',
        width: '40px !important',
        height: '40px',
        fontSize: 'xx-large',
        margin: '5px',
        border: 'solid 1px lightgray',
        borderRadius: '5px',
        color: 'slategray'
    },
    modal: {
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
      },
      paper: {
        backgroundColor: theme.palette.background.paper,
        border: '1px solid lightgray',
        boxShadow: theme.shadows[5],
        padding: theme.spacing(2, 4, 3),
        textAlign: 'center',
        borderRadius: '5px',
        alignItems: 'center',
        justifyContent: 'center',
        minWidth: '150px'

      },
      placeholder: {
          height: 'auto',
          justifyContent: 'center',
          alignItems: 'center',
          display: 'flex'
      },
      backdrop: {
        zIndex: theme.zIndex.drawer + 1,
        color: '#fff',
      },
});

const history = createBrowserHistory();

class inputKodeOTP extends Component {

    timer = null
    timerOtp = 0;

    constructor(props){
        super(props);
        this.state = {
            otp: '',
            open: false,
            count: [],
            query: 'idle',
            kodeOtp: '123456',
            proses: '',
            time: 60,
            do: false,
            complete: false
        }

       
    }

    componentDidMount(){

        this.id = setInterval(this.initial, 1000)
        
    }

    start = () => {
        this.setState({time: 60})
        return this.id = setInterval(this.initial, 1000);
      };

    initial = () => {
        if (this.state.time !== 0) {
            this.setState((prevState, prevProps) => ({
              time: prevState.time - 1
            }));
            if (this.state.time === 0) {
              clearInterval(this.id);
              this.setState({ complete: true });
            }
          }
    }

    handleChange = otp => {

        this.setState({otp: otp})
        this.setState({count: otp})
        if(this.state.count.length == 5){
            this.setState({open: true})

            clearTimeout(this.timer);
            if(this.state.query !== 'idle'){
                this.setState({query: 'idle'});
                return;
            }
    
            this.setState({query: 'progress'});
            this.timer = setTimeout(() => {

                if(otp == this.state.kodeOtp){
                    this.setState({proses: 'success'})
                    this.setState({query: this.state.proses});
                    this.timer = setTimeout(() => {
                        this.setState({otp: ''})
                        this.setState({query: 'idle'})
                        this.setState({open: false})
                        history.push('/verifSukses')
                        location.reload();
                    }, 3000)

                }else{
                    this.setState({proses: 'failed'})
                    this.setState({query: this.state.proses});
                    this.timer = setTimeout(() => {
                        this.setState({otp: ''})
                        this.setState({query: 'idle'})
                        this.setState({open: false})
                    }, 3000)

                }

                
            }, 5000);
        }
    };

    handleOpen = () => {
        this.setState({open: true})
    }
    
    handleClose = () => {
        this.setState({open: false})
    }

    render() {
        const {loading = false, classes, open = true} = this.props;

        return (
            <div>
                <div style={{marginLeft:'5px', marginTop:'7px'}}>
                    <Link href="/kodeOTP">
                        <ArrowBackIcon/>
                    </Link>
                </div>
                <Backdrop className={classes.backdrop} open={this.state.open} >
                    <div className={classes.placeholder}>

                    {
                        this.state.query === 'success' ? 
                                (<Typography component={'span'} variant={'body2'} className={classes.paper}>
                                    <span><h4 style={{color: 'black'}}>Verifikasi Berhasil</h4></span>
                                    <img src={sukse} 
                                    style={{ height: 50, width: 50 }}></img>
                                </Typography>) :

                        this.state.query == 'failed' ? 
                             (<Typography component={'span'} variant={'body2'} className={classes.paper}>
                             <span><h4 style={{color: 'black'}}>Verifikasi Tidak Berhasil</h4></span>
                             <img src={gagal}
                             style={{ height: 50, width: 50 }}></img>
                         </Typography>) :
                         (<Fade in={this.state.query === 'progress'}
                            style={{transitionDelay: this.state.query === 'progress' ? '500ms' : '0ms'}}
                            unmountOnExit
                            >
                                <div className={classes.paper}>
                                    <span><h4 style={{color: 'black'}}>
                                        Melakukan Verifikasi</h4></span>
                                    <CircularProgress />
                                </div>
                            </Fade>)
                        }

                    </div>
                </Backdrop>
                <Grid style={{padding: '10px', justifyContent: 'center', alignItems: 'center', textAlign: 'center'}}>
                    <h4 style={{textAlign: 'center', marginTop: '0px'}}>Masukan Kode Verifikasi</h4>
                    <p style={{marginTop: '5px', color: 'lightslategray', marginBottom: '10px'}}>
                        Kode verifikasi 6 digit telah dikirimkan ke nomor ********1234
                    </p>
                    <br/>
                    <Box style={{justifyContent:'center', alignItems: 'center', display: 'contents'}}>
                        <OtpInput
                            value={this.state.otp}
                            onChange={this.handleChange}
                            numInputs={6}
                            containerStyle={classes.containerOtp}
                            inputStyle={classes.inputOtp}
                            isInputNum={true}
                            hasErrored={true}

                        />
                        {console.log('otp: ', this.state.otp)}
                    </Box>
                    <br/>
                    <p style={{marginTop: '30px', color: 'deepskyblue', marginBottom: '10px', fontSize: 'small'}}>Kirim ulang dalam {this.state.time} Detik</p>
                    {this.state.time == 0 && <Button variant="contained" color="primary" onClick={this.start}>Kirim Ulang</Button>}
                   {/* <Box>
                    <Link href="/startReg">
                        <Button size="large" style={{width: "-webkit-fill-available", marginTop: '10px', 
                                textTransform: 'none', marginBottom: '10px'}} variant="contained" color="primary">
                            Lanjutkan
                        </Button>
                    </Link>
                   </Box> */}
                </Grid>
            </div>
        )
    }
}

export default compose(
    withStyles(useStyles)
)(inputKodeOTP)
