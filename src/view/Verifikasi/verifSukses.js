import React, { Component } from "react";
import { Box, Button, Grid } from "@material-ui/core";
import Skeleton from "@material-ui/lab/Skeleton";
import verifOk from "../../../public/img/logoVerfifSukses.png";

class verifSukses extends Component {
  componentWillUnmount() {
    //location.href= "/verifSukses"
    location.reload();
  }
  render() {
    const { loading = false } = this.props;
    const lanjut = () => {
      this.props.history.push("/inputDataOwner", { from: "/verifSukses" });
    };
    return (
      <div style={{ overflow: "hidden" }}>
        <Grid
          container
          wrap="nowrap"
          style={{
            justifyContent: "center",
            alignItems: "center",
            display: "flex",
            marginTop: "30px",
          }}
        >
          {loading ? (
            <Skeleton animation="wave" variant="rect" height={200}></Skeleton>
          ) : (
            <Box
              style={{
                textAlign: "center",
                position: "fixed",
                display: "contents",
              }}
            >
              <img src={verifOk} style={{ height: 202, marginTop: "18px" }} />
            </Box>
          )}
        </Grid>
        <Box style={{ marginTop: "50px", marginInline: "16px" }}>
          <h2
            style={{
              textAlign: "center",
              marginTop: "0px",
              fontFamily: "Montserrat",
              fontSize: "18px",
              fontWeight: "700",
              lineHeight: "27px",
            }}
          >
            Verifikasi data berhasil!
          </h2>
          <p
            style={{
              marginTop: "10px",
              color: "#303B4A",
              marginBottom: "10px",
              textAlign: "center",
              marginInlineStart: "17px",
              marginInlineEnd: "16px",
              fontSize: "16px",
              fontWeight: "400",
              lineHeight: "24px",
            }}
          >
            Silahkan lanjutkan registrasi dengan upload KTP, NPWP (jika ada),
            dan lengkapi informasi usaha.
          </p>

          {/* <Button
            onClick={lanjut}
            // disableElevation
            size="large"
            variant="contained"
            color="primary"
            style={{
              width: "100%",
              height: "56px",
              backgroundColor: "rgba(0,87,231,1)",
              boxShadow: "0px 4px 4px -4px #8CC3F8 0px 4px 8px 2px #C5E1FB",
              textTransform: "none",
              fontFamily: "Nunito",
              fontStyle: "normal",
              fontWeight: "bold",
              fontSize: "16px",
              lineheight: "24px",
              alignItems: "center",
              textAlign: "center",
              color: "#FFFFFF",
              borderRadius: "8px",
              marginTop: "19px",
              marginBottom: "10px",
            }}
          >
            Lanjutkan
          </Button> */}

          <Button
            onClick={lanjut}
            size="large"
            style={{
              marginTop: "10px",
              textTransform: "none",
              marginBottom: "10px",
              marginLeft: "4vw",
              fontFamily: "Nunito",
              fontSize: "16px",
              fontWeight: "700",
              alignItems: "center",
              width: "91%",
              backgroundColor: "rgba(0,87,231,1)",
              marginInline: "0px",
            }}
            disableElevation
            variant="contained"
            color="primary"
          >
            {/* <CameraAltIcon style={{marginRight: '5px'}}/> */}
            Lanjutkan
          </Button>
        </Box>
      </div>
    );
  }
}

export default verifSukses;
