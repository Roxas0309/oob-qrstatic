import React, { Component } from "react";
import { connect } from "react-redux";
import Link from "@material-ui/core/Link";
import ArrowBackIcon from "@material-ui/icons/ArrowBack";
import {
  AppBar,
  Backdrop,
  Box,
  Button,
  Card,
  CircularProgress,
  Collapse,
  Fade,
  Grid,
  IconButton,
  Snackbar,
  TextField,
  TextFieldProps,
  Toolbar,
  Typography,
  withStyles,
} from "@material-ui/core";
import { ErrorMessage, Field, Form, Formik } from "formik";
import * as Yup from "yup";
import MomentUtils from "@date-io/moment";
import moment from "moment";
import { MuiPickersUtilsProvider, DatePicker } from "@material-ui/pickers";
import { createBrowserHistory } from "history";
import { compose } from "recompose";
import sukses from "../../../public/img/success.gif";
import gagal from "../../../public/img/unapproved.gif";
import progressGif from "../../../public/img/loading.gif";
import { registrasiActions } from "../../actions";
import Cleave from "cleave.js/react";
import { parse, isDate } from "date-fns";
import NumberFormat from "react-number-format";
import '../Registrasi/registrasiStyleV2.css'
const useStyles = (theme) => ({
  root: {
    width: "100%",
    "& > * + *": {
      marginTop: theme.spacing(2),
    },
  },
  placeholder: {
    height: "auto",
    justifyContent: "center",
    alignItems: "center",
    display: "flex",
  },
  backdrop: {
    zIndex: theme.zIndex.drawer + 1,
    color: "#fff",
  },
  paper: {
    backgroundColor: theme.palette.background.paper,
    border: "1px solid lightgray",
    boxShadow: theme.shadows[5],
    padding: theme.spacing(2, 4, 3),
    textAlign: "center",
    borderRadius: "5px",
    alignItems: "center",
    justifyContent: "center",
    minWidth: "150px",
  },
  placeholder: {
    height: "auto",
    justifyContent: "center",
    alignItems: "center",
    display: "flex",
  },
  backdrop: {
    zIndex: theme.zIndex.drawer + 1,
    color: "#fff",
  },
  paper: {
    backgroundColor: theme.palette.background.paper,
    border: "1px solid lightgray",
    boxShadow: theme.shadows[5],
    padding: theme.spacing(2, 4, 3),
    textAlign: "center",
    borderRadius: "5px",
    alignItems: "center",
    justifyContent: "center",
    minWidth: "150px",
  },
});

//const history = createBrowserHistory();
class infoRekening extends Component {
  timer = null;

  constructor(props) {
    super(props);

    this.state = {
      loading: false,
      openAlert: false,
      query: "idle",
      proses: "",
      messageError: "",
      alertError: false,
      regerxp: /^[0-9\b]+$/,
      ktp: "",
      errorName:false,
      errorTglLahir:false,
      errorNomorKtp:false,
      errorIbuKandung:false,
      errorRekening:false,
      onTouchedMe : false,
      maxLengthNamaPengusaha:50,
      maxLengthNamaIbuKandung:50
    };
  }

  componentDidMount() {
    localStorage.removeItem("onTouchednoRekMandiri");
    localStorage.removeItem("onTouchednoKtpId");
    localStorage.removeItem("onTouchedibuKandung");
    localStorage.removeItem("onTouchedownerName");
    localStorage.removeItem("onTouchedtglLahirId");

    // console.log('his2: ', this.props.history(el => {
    //     console.log('hisFunc2: ', el)
    // }))
  }

  componentWillUnmount() {
    // console.log('his3: ', this.props.history)
    // location.href = "/infoRekening"
    location.reload();
  }

  changeKtp = (e) => {
    const { name, value } = e.target;
    if (e.target.value === "" || this.state.regerxp.test(value)) {
      this.setState({
        ktp: value,
      });
    }
  };

  render() {
    const {
      koneksi,
      classes,
      isLoading,
      isPreparing,
      isLoadingInfoRek,
      validateInfoRekening,
    } = this.props;
    const alpha = /^[a-zA-Z,.~!@#$%^&*()=+'-/"/:/;//_]+( [a-zA-Z,.~!@#$%^&*()=+'-/"/:/;//_]+)*$/;
    const numb = /^[0-9\b]+$/;
    moment.locale("id");

    const formTextFieldDate = (props) => {
      const {
        form: { setFieldValue, setFieldTouched },
        field: { name },
      } = props;

      const onChange = React.useCallback(
        (event) => {
          const { value } = event.target;
          console.log("value tgl: ", props);
          localStorage.setItem("onTouched"+id, "no");
          localStorage.setItem("rekInfo_tglLahir", value);
          setFieldTouched(name, true);
          setFieldValue(name, value ? value : "");
        },
        [setFieldValue, setFieldTouched, name]
      );

      const onBlur = React.useCallback(
        (event) => {
          const { value } = event.target;
          setFieldTouched(name, true);
          setFieldValue(name, value ? value : "");
        },
        [setFieldValue, setFieldTouched, name]
      );

      return (
        <TextField
          {...props}
          onChange={onChange}
          onBlur={onBlur}
          onClick = {(event) =>{
            var id  = event.currentTarget.firstChild.control.id;
            // localStorage.setItem("onTouchednoRekMandiri","no");
            // localStorage.setItem("onTouchednoKtpId","no");
            // localStorage.setItem("onTouchedibuKandung","no");
            // localStorage.setItem("onTouchednoRekMandiri","no");
            // localStorage.setItem("onTouchednoRekMandiri","no");
        
            localStorage.setItem("onTouched"+id, "yes");
           
          }}
          InputProps={{ inputComponent: NumberFormatCustom }}
        />
      );

      //     <FormControl>
      //         <InputLabel htmlFor="tglLahirLabel">Tanggal Lahir</InputLabel>
      //             <Input {...props} onChange={onChange} onBlur={onBlur}
      //             value={localStorage.getItem('rekInfo_tglLahir') ? localStorage.getItem('rekInfo_tglLahir') : ''}
      //             inputComponent={renderCleaveField} />
      //   </FormControl>
    };

    const NumberFormatCustom = (props) => {
      const { inputRef, onChange, ...other } = props;
      console.log("numb: ", props);

      return (
        <NumberFormat
          {...other}
          getInputRef={inputRef}
          onValueChange={(values) => {
            onChange({
              target: {
                name: props.name,
                value: values.value,
              },
            });
          }}
          isNumericString
          format="########"
          placeholder="DDMMYYYY"
          mask={["D", "D", "M", "M", "Y", "Y", "Y", "Y"]}
        />
      );
    };

    const renderCleaveField = (props) => {
      const { inputRef, ...other } = props;
      console.log("cleave other: ", props);
      console.log(
        "dt: ",
        moment(
          moment(localStorage.getItem("rekInfo_tglLahir"), "DDMMYYYY").toDate()
        ).format("DDMMYYYY")
      );
      return (
        <Cleave
          {...other}
          ref={(ref) => {
            inputRef(ref ? ref.inputElement : null);
          }}
          // value={localStorage.getItem('rekInfo_tglLahir') ?
          //     moment(moment(localStorage.getItem('rekInfo_tglLahir'), "DDMMYYYY").toDate()).format("DDMMYYYY") : ''}
          options={{ date: true, delimiter: "", datePattern: ["d", "m", "Y"] }}
        />
      );
    };

    const validasiNumeric = (words) => {
    
      var result = (words=='')||this.state.regerxp.test(words);
      return result;
    }

    const validasiAlphabet = (words) => {
    
      var result = (words=='')||/^[A-Za-z ,.~!@#$%^&*()=+'-/"/:/;//]+$/.test(words);
      return result;
    }

    const formTextField = (props) => {
      const {
        form: { setFieldValue, setFieldTouched, errors },
        field: { name,label },
      } = props;
      const inputRef = React.useRef();

      const onChange = React.useCallback(
        (event) => {
          const { value } = event.target;
          var id  = event.target.id;
          localStorage.setItem("onTouched"+id, "no");
          if(name === "ownerName")
          {  
            if(value.length<(this.state.maxLengthNamaPengusaha+2) && validasiAlphabet(value))
                {
            localStorage.setItem("rekInfo_ownerName", value);
            localStorage.setItem("rekInfo_ownerNameValidity", value);
            setFieldTouched(name, value ? true : false);
            setFieldValue(name, value ? value : "");
               }
          } else if (name === "noKtp") {
            if(value.length<18)
            {
            if (validasiNumeric(value)) {
              console.log("valuee: ", value);
              localStorage.setItem("rekInfo_noKtp", value);
              setFieldTouched(name, value ? true : false);
              setFieldValue(name, value ? value : "");
            }
            }
          } else if (name === "ibuKandung" && validasiAlphabet(value)) {
            if(value.length<(this.state.maxLengthNamaIbuKandung+2))
            {
            localStorage.setItem("rekInfo_ibuKandung", value);
            setFieldTouched(name, value ? true : false);
            setFieldValue(name, value ? value : "");
            }
          } else if (name === "noRekMandiri") {
            
            if(value.length<15)
            {
              //  alert("result " + validasiNumeric(value) + " dengan value " + value);
            if (validasiNumeric(value)) {
              console.log("valuee: ", value);
              localStorage.setItem("rekInfo_noRekMandiri", value);
              setFieldTouched(name, value ? true : false);
              setFieldValue(name, value ? value : "");
            }
          }
          } else {
          }

          if (`errors.${name}`) {
            inputRef.current.focus();
          }
        },
        [setFieldValue, setFieldTouched, name]
      );

      const onBlur = React.useCallback(
        (event) => {
          const { value } = event.target;
          setFieldTouched(name, true);
        },
        [setFieldValue, setFieldTouched, name]
      );

      return (
        <TextField
          {...props}
          onClick = {(event) =>{
            var id  = event.currentTarget.firstChild.control.id;
            // localStorage.setItem("onTouchednoRekMandiri","no");
            // localStorage.setItem("onTouchednoKtpId","no");
            // localStorage.setItem("onTouchedibuKandung","no");
            // localStorage.setItem("onTouchednoRekMandiri","no");
            // localStorage.setItem("onTouchednoRekMandiri","no");
        
            localStorage.setItem("onTouched"+id, "yes");
           
          }}
          onChange={onChange}
          onBlur={onBlur}
          inputRef={inputRef}
        />
      );
    };

    const failedRegistered = () => {
      this.props.history.push("/infoRekeningGagal");
      location.reload();
    };

    const klikClose = () => {
      this.setState({
        openAlert: false,
        proses: "",
        query: "idle",
      });
    };

    const parseDateString = (value, originalValue) => {
      console.log("valParse: ", value);
      console.log("valoriginalValue: ", originalValue);

      const parsedDate = isDate(originalValue)
        ? originalValue
        : parse(
            originalValue,
            "ddMMyyyy",
            moment(new Date(), "DDMMYYYY").toDate()
          );
      console.log("parseDate: ", parsedDate);
      return parsedDate;
    };

    return (
      <div>
        {/* <div style={{ marginLeft: '20px', marginTop: '20px' }}>
                    <Link href="/termsCondition">
                        <ArrowBackIcon />
                    </Link>
                </div> */}
        {/* -----------------alert----------- */}
        <Backdrop className={classes.backdrop} open={this.state.openAlert}>
          <div className={classes.placeholder}>
            {this.state.query === "progress" ? (
              <Box className={classes.paper}>
                <Typography component={"span"} variant={"body2"}>
                  <span>
                    <h4 style={{ color: "black" }}>Data Sedang Diverifikasi</h4>
                  </span>
                  <CircularProgress />
                  {/* <img src={progressGif}
                                            style={{ height: 150, width: 250 }}></img> */}
                </Typography>
              </Box>
            ) : this.state.query === "success" ? (
              <Box className={classes.paper}>
                <Typography component={"span"} variant={"body2"}>
                  <span>
                    <h4 style={{ color: "black" }}>Verifikasi Berhasil</h4>
                  </span>
                  <img src={sukses} style={{ height: 50, width: 50 }}></img>
                </Typography>
                {/* <Link>
                                            <Button onClick={klikLanjut} variant="contained"
                                                style={{ width: '100%', marginTop: '20px' }} color="primary"
                                                disableElevation>
                                                Lanjut
                                        </Button>
                                        </Link> */}
              </Box>
            ) : this.state.query === "lost" ? (
              <Box className={classes.paper}>
                <Typography component={"span"} variant={"body2"}>
                  <span>
                    <h4 style={{ color: "black" }}>
                      Verifikasi Tidak Berhasil{" "}
                    </h4>
                  </span>
                  <span>
                    <p style={{ color: "black", width: 220 }}>
                      {" "}
                      {this.state.messageError}{" "}
                    </p>
                  </span>
                  <img src={gagal} style={{ height: 50, width: 50 }}></img>
                </Typography>
                <Link>
                  <Button
                    onClick={klikClose}
                    variant="contained"
                    style={{ width: "100%", marginTop: "20px" }}
                    color="primary"
                    disableElevation
                  >
                    Tutup
                  </Button>
                </Link>
              </Box>
            ) : (
              <Typography
                component={"span"}
                variant={"body2"}
                className={classes.paper}
              >
                <span>
                  <h4 style={{ color: "black" }}>Verifikasi Tidak Berhasil </h4>
                </span>
                <span>
                  <p style={{ color: "black", width: 220 }}>
                    {" "}
                    {this.state.messageError}{" "}
                  </p>
                </span>
                <img src={gagal} style={{ height: 50, width: 50 }}></img>
                <Link>
                  <Button
                    onClick={failedRegistered}
                    variant="contained"
                    style={{ width: "100%", marginTop: "20px" }}
                    color="primary"
                    disableElevation
                  >
                    OK
                  </Button>
                </Link>
              </Typography>
            )}
          </div>
        </Backdrop>
        {/* --------------------------------- */}
        <AppBar
          style={{
            backgroundColor: "transparent",
            position: "absolute",
            boxShadow: "unset",
          }}
        >
          <Toolbar style={{ minHeight: "60px" }}>
            {/* onClick={this.props.history.push('/termsCondition', {from: '/infoRekening'})} */}
            <Link
              href="/termsCondition"
              style={{ position: "absolute", display: "flex" }}
            >
              <ArrowBackIcon />
            </Link>
          </Toolbar>
        </AppBar>
        <Grid
          style={{
            padding: "16px",
            justifyContent: "center",
            alignItems: "center",
            textAlign: "center",
          }}
        >
          <h4 style={{ textAlign: "center", marginTop: "70px" }}>
            Informasi Rekening
          </h4>
          <p
            style={{
              marginTop: "5px",
              color: "lightslategray",
              marginBottom: "20px",
            }}
          >
            Pastikan data rekening sama dengan KTP.
          </p>

          <Box style={{ textAlign: "center" }}>
            <Formik
              validateOnChange
              initialValues={{
                ownerName: localStorage.getItem("rekInfo_ownerName")
                  ? localStorage.getItem("rekInfo_ownerName")
                  : "",
                tglLahir: localStorage.getItem("rekInfo_tglLahir")
                  ? localStorage.getItem("rekInfo_tglLahir")
                  : "",
                noKtp: localStorage.getItem("rekInfo_noKtp")
                  ? localStorage.getItem("rekInfo_noKtp")
                  : "",
                ibuKandung: localStorage.getItem("rekInfo_ibuKandung")
                  ? localStorage.getItem("rekInfo_ibuKandung")
                  : "",
                noRekMandiri: localStorage.getItem("rekInfo_noRekMandiri")
                  ? localStorage.getItem("rekInfo_noRekMandiri")
                  : "",
              }}
              validationSchema={Yup.object().shape({
                ownerName: Yup.string()
                  .matches(alpha, {
                    message: "Nama tidak valid",
                    excludeEmptyString: true,
                  })
                  .max(this.state.maxLengthNamaPengusaha, "Maksimal hanya bisa "+this.state.maxLengthNamaPengusaha+" karakter")
                  .required("Harus diisi"),
                tglLahir: Yup.date()
                  .transform(parseDateString, "ddMMyyyy")
                  .typeError("Format tanggal harus DDMMYYYY (contoh: 17081925)")
                  .required("Harus diisi")
                  .min(
                    moment(new Date().setFullYear(1920)).toDate(),
                    "Format tanggal harus DDMMYYYY (contoh: 17081925)"
                  ), //Yup.date().transform(parseTglLahir).max(new Date()),
                noKtp: Yup.string()
                  .max(16, "Nomor KTP harus 16 digit")
                  .min(16, "Nomor KTP harus 16 digit")
                  .matches(numb, {
                    message: "Nomor KTP tidak valid",
                    excludeEmptyString: true,
                  })
                  .required("Harus diisi"),
                ibuKandung: Yup.string()
                  .matches(alpha, {
                    message: "Nama Ibu kandung tidak valid",
                    excludeEmptyString: true,
                  })
                  .required("Harus diisi")
                  .max(this.state.maxLengthNamaIbuKandung, "Maksimal hanya bisa "+this.state.maxLengthNamaIbuKandung+" karakter"),
                noRekMandiri: Yup.string()
                  .max(13, "Nomor rekening harus 13 digit")
                  .min(13, "Nomor rekening harus 13 digit")
                  .matches(numb, {
                    message: "Nomor Rekening tidak valid",
                    excludeEmptyString: true,
                  })
                  .required("Harus diisi")
              })}
              onSubmit={(
                { ownerName, tglLahir, noKtp, ibuKandung, noRekMandiri },
                actions
              ) => {
             
                this.setState({ openAlert: true });
                if (this.state.query !== "idle") {
                  this.setState({ query: "idle" });
                  return;
                }
                this.setState({ query: "progress" });
                var validasiInformasiRekening = {
                  dob: tglLahir,
                  namaIbuKandung: ibuKandung,
                  namaPemilikUsaha: ownerName,
                  nomorKtp: noKtp,
                  nomorRekening: noRekMandiri,
                };

                validateInfoRekening({
                  dob: tglLahir,
                  namaIbuKandung: ibuKandung,
                  namaPemilikUsaha: ownerName,
                  nomorKtp: noKtp,
                  nomorRekening: noRekMandiri,
                }).then(
                  (res) => {
                    isLoadingInfoRek && console.log("sukseeee: ", res);

                    if (res.status === undefined) {
                      this.setState({ proses: "lost" });
                      this.setState({ query: this.state.proses });
                      this.setState({
                        messageError: "Internal Service Error",
                      });
                      actions.setSubmitting(false);
                      throw res;
                    }

                    if (res.status === 406) {
                      this.setState({ proses: "lost" });
                      this.setState({ query: this.state.proses });
                      this.setState({ messageError: res.message });
                      actions.setSubmitting(false);
                      throw res;
                    }

                    if(res.status === 404){
                      this.timer = setTimeout(() => {
                          actions.setSubmitting(false);
                          this.setState({open: false})
                          this.props.history.push('/infoRekeningGagal', { from: '/infoRekening'})
                          location.reload();
                      },3000)  
                    }

                    if(res.status === 409){
                      this.timer = setTimeout(() => {
                          actions.setSubmitting(false);
                          this.setState({open: false})
                          this.props.history.push('/infoDataSudahAda', { from: '/infoDataSudahAda'})
                          location.reload();
                      },3000) 
                    }

                 

                    localStorage.setItem(
                      "rekInfo_tglLahir",
                      validasiInformasiRekening.dob
                    );
                    localStorage.setItem(
                      "rekInfo_tglLahirOnDDMMYYYFormat",
                      validasiInformasiRekening.dob
                    );
                    localStorage.setItem(
                      "rekInfo_ibuKandung",
                      validasiInformasiRekening.namaIbuKandung
                    );
                    localStorage.setItem(
                      "rekInfo_ownerName",
                      res.result.namaPemilikUsaha
                    );
                    localStorage.setItem(
                      "rekInfo_noKtp",
                      validasiInformasiRekening.nomorKtp
                    );
                    localStorage.setItem(
                      "rekInfo_noRekMandiri",
                      res.result.noRekeningValid
                    );
                    localStorage.setItem("address", res.result.address);
                    localStorage.setItem(
                      "cifEncryptor",
                      res.result.cifInEncrypt
                    );
                    localStorage.setItem("noTelpOtp", res.result.noTelp);
                    console.log(
                      "values respon ownerName: ",
                      validasiInformasiRekening.namaPemilikUsaha
                    );
                    console.log(
                      "values tglLahir: ",
                      localStorage.getItem("rekInfo_tglLahir")
                    );
                    console.log(
                      "values tglLahir ori: ",
                      validasiInformasiRekening.dob
                    );
                    console.log(
                      "values noKtp: ",
                      validasiInformasiRekening.nomorKtp
                    );
                    console.log(
                      "values ibuKandung: ",
                      validasiInformasiRekening.namaIbuKandung
                    );
                    console.log(
                      "values noRekMandiri: ",
                      validasiInformasiRekening.nomorRekening
                    );

                    this.setState({ proses: "success" });
                    this.setState({ query: this.state.proses });
                    console.log("alert sukse: ", this.state.query);
                    this.timer = setTimeout(() => {
                      actions.setSubmitting(false);
                      this.setState({ open: false });
                      this.props.history.push("/kodeOTP", {
                        from: "/infoRekening",
                      });
                      location.reload();
                    }, 3000);
                  },
                  (err) => {
                    console.log("errorr respon:", err);
                    if (error.message === "Network Error") {
                      this.setState({
                        openAlert: false,
                        proses: "",
                        query: "idle",
                      });
                      actions.setSubmitting(false);
                    } else {
                      this.setState({ proses: "failed" });
                      this.setState({ query: this.state.proses });
                      this.setState({ messageError: error.message });
                      // localStorage.removeItem('rekInfo_ibuKandung')
                      // localStorage.removeItem('rekInfo_ownerName')
                      // localStorage.removeItem('rekInfo_noKtp')
                      // localStorage.removeItem('rekInfo_noRekMandiri')
                    }

                    console.log("alert gagal: ", this.state.query);
                  }
                );
              }}
            >
              {({
                values,
                isValid,
                errors,
                touched,
                setFieldTouched,
                setFieldValue,
                isSubmitting,
                validateField,
              }) => (
                // <MuiPickersUtilsProvider utils={MomentUtils}>
                <Form autoComplete="off">
                  <Box style={{ marginBottom: "20px"}}>
                    <Field
                      component={formTextField}
                      type="text"
                      id="ownerName"
                      name="ownerName"
                      label="Nama Pemilik Usaha"
                      variant="filled"
                      spellcheck="false"
                      value={values.ownerName}
                      fullWidth
                      maxLength={this.state.maxLengthNamaPengusaha}
                      error={errors.ownerName && (localStorage.getItem("onTouchedownerName")=="no")}
                      helperText={
                        errors.ownerName && touched.ownerName
                          ? ""
                          : "Maksimal "+this.state.maxLengthNamaPengusaha+" Karakter (Tanpa Gelar)"
                      }
                      //className={(errors.ownerName) || (touched.ownerName) ? 'is-invalid' : ''}
                    />
                    {/* {console.log('touched: ', touched)}
                                            {console.log('errors: ', errors)} */}
                    {errors.ownerName && touched.ownerName && (
                      <div className="invalid-feedback">{errors.ownerName}</div>
                    )}
                    {/* <ErrorMessage name="ownerName" component="div" className="invalid-feedback" /> */}
                  </Box>
                  <Box style={{ marginBottom: "20px" }}>
                    <Field
                      autoComplete="off"
                      component={formTextFieldDate}
                      id="tglLahirId"
                      name="tglLahir"
                      label="Tanggal Lahir"
                      variant="filled"
                      value={values.tglLahir}
                      fullWidth
                      error={errors.tglLahir  && (localStorage.getItem("onTouchedtglLahirId")=="no")}
                      helperText={
                        errors.tglLahir && touched.tglLahir
                          ? ""
                          : "Format DDMMYYYY (contoh: 17081925)"
                      }
                    />
                    {/* <Field
                                                component={DatePicker}
                                                name="tglLahir"
                                                label="Tanggal Lahir"
                                                onChange={(newVal) => setFieldValue("tglLahir", moment(newVal).format("YYYY/MM/DD"))}
                                                onBlur={() => setFieldTouched("tglLahir", true)}
                                                value={values.tglLahir}
                                                fullWidth
                                                inputVariant="filled"
                                                views={["year", "month", "date"]}
                                                openTo="year"
                                                format="DDMMYYYY"
                                                helperText={errors.tglLahir ? '' : "Format DDMMYYYY (contoh: 17081925)"}
                                                error={errors.tglLahir}
                                            /> */}
                    {/* <ErrorMessage name="tglLahir" component="div" className="invalid-feedback" /> */}
                    {errors.tglLahir && touched.tglLahir && (
                      <div className="invalid-feedback">{errors.tglLahir}</div>
                    )}
                  </Box>
                  <Box style={{ marginBottom: "20px" }}>
                    <Field
                      autoComplete="off"
                      type="text"
                      maxLength="16"
                      component={formTextField}
                      id="noKtpId"
                      name="noKtp"
                      label="Nomor KTP"
                      variant="filled"
                      value={values.noKtp}
                      fullWidth
                      error={errors.noKtp  && (localStorage.getItem("onTouchednoKtpId")=="no")}
                    />
                    {errors.noKtp && touched.noKtp && (
                      <div className="invalid-feedback">{errors.noKtp}</div>
                    )}
                    {/* <ErrorMessage name="noKtp" component="div" className="invalid-feedback" /> */}
                  </Box>
                  <Box style={{ marginBottom: "20px" }}>
                    <Field
                      type="text"
                      component={formTextField}
                      id="ibuKandung"
                      name="ibuKandung"
                      label="Nama Gadis Ibu Kandung"
                      variant="filled"
                      value={values.ibuKandung}
                      fullWidth
                      helperText={
                        errors.ibuKandung && touched.ibuKandung
                          ? ""
                          : "Maksimal "+this.state.maxLengthNamaIbuKandung+" Karakter (Tanpa Gelar)"
                      }
                      //helperText={errors.ibuKandung && touched.ibuKandung ? '' : "Maksimal 25 Karakter"}
                      error={errors.ibuKandung && (localStorage.getItem("onTouchedibuKandung")=="no")}
                    />
                    {errors.ibuKandung && touched.ibuKandung && (
                      <div className="invalid-feedback">
                        {errors.ibuKandung}
                      </div>
                    )}
                    {/* <ErrorMessage name="ibuKandung" component="div" className="invalid-feedback" /> */}
                  </Box>
                  <Box style={{ marginBottom: "20px" }}>
                    <Field
                      type="text"
                      maxLength="13"
                      component={formTextField}
                      id="noRekMandiri"
                      name="noRekMandiri"
                      label="Nomor Rekening Bank Mandiri"
                      variant="filled"
                      value={values.noRekMandiri}
                      fullWidth
                      error={errors.noRekMandiri && (localStorage.getItem("onTouchednoRekMandiri")=="no")}

                      // className={(errors.noRekMandiri) || (touched.noRekMandiri) ? 'is-invalid' : ''}
                    />


                    {errors.noRekMandiri && touched.noRekMandiri && (
                      <div className="invalid-feedback">
                        {errors.noRekMandiri}
                      </div>
                    )}
                    {/* <ErrorMessage name="noRekMandiri" component="div" className="invalid-feedback" /> */}
                  </Box>
                  <Button
                    style={{
                      position: "relative",
                      marginTop: "200px",
                      marginInlineStart: "0vw",
                      marginInline: "0px",
                      width: "100%",
                      backgroundColor:'rgba(0,87,231,1)!important'
                    //   position: "absolute",
                    //   position:"fixed"
                    }}
                    disableElevation
                    disabled={isSubmitting || values.noRekMandiri=="" || values.ibuKandung == "" || values.ownerName == "" || values.noKtp == ""}
                    type="submit"
                    size="large"
                    variant="contained"
                    color="primary"
                  >
                    Lanjutkan
                  </Button>
                </Form>
                // </MuiPickersUtilsProvider>
              )}
            </Formik>
          </Box>
        </Grid>
      </div>
    );
  }
}

export default compose(
  withStyles(useStyles),
  connect(
    (state) => ({
      isLoadingInfoRek: state.registrasi.isLoadingInfoRek,
      isLoading: state.submit.isLoading,
      isPreparing: state.submit.isPreparing,
    }),
    {
      validateInfoRekening: registrasiActions.validateInfoRekening,
    }
  )
)(infoRekening);
