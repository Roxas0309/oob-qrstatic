import React, { Component } from 'react'
import { connect } from 'react-redux';
import Link from '@material-ui/core/Link';
import ArrowBackIcon from '@material-ui/icons/ArrowBack';
import { Backdrop, Box, Button, Card, CircularProgress, Collapse, Fade, Grid, IconButton, Snackbar, TextField, TextFieldProps, Typography, withStyles } from '@material-ui/core';
import { ErrorMessage, Field, Form, Formik } from 'formik';
import * as Yup from 'yup';
import MomentUtils from '@date-io/moment';
import moment from 'moment';
import { parse, isDate, min } from "date-fns";

import { MuiPickersUtilsProvider, DatePicker } from "@material-ui/pickers";
import { createBrowserHistory } from 'history';
import MuiAlert from '@material-ui/lab/Alert';
import { compose } from 'recompose';
import sukses from '../../../public/img/success.gif';
import gagal from '../../../public/img/unapproved.gif';
import progressGif from '../../../public/img/loading.gif';
import { BACK_END_POINT, HEADER_AUTH, PUT, TIMES_OUT, wsWithBody, wsWithoutBody } from '../../master/masterComponent';
import { infoRekeningActions } from '../../actions/infoRekening.action';
import { alertActions } from '../../actions';


const useStyles = theme => ({
    root: {
        width: '100%',
        '& > * + *': {
            marginTop: theme.spacing(2),
        },
    },
    placeholder: {
        height: 'auto',
        justifyContent: 'center',
        alignItems: 'center',
        display: 'flex'
    },
    backdrop: {
        zIndex: theme.zIndex.drawer + 1,
        color: '#fff',
    },
    paper: {
        backgroundColor: theme.palette.background.paper,
        border: '1px solid lightgray',
        boxShadow: theme.shadows[5],
        padding: theme.spacing(2, 4, 3),
        textAlign: 'center',
        borderRadius: '5px',
        alignItems: 'center',
        justifyContent: 'center',
        minWidth: '150px'

    },
});

const history = createBrowserHistory();
class infoRekening extends Component {

    timer = null

    constructor(props) {
        super(props);

        this.state = {
            loading: false,
            openAlert: false,
            query: 'idle',
            proses: '',
            messageError: ''
        }
    }

    componentDidMount() {
        //window.localStorage.clear();
    }


    render() {
        const { isValid, classes } = this.props;
        const loading = () => <div className="animated fadeIn pt-3 text-center">Loading...</div>;
        const sleep = (ms) => new Promise((r) => setTimeout(r, ms));
        const alpha = /^[a-zA-Z_]+( [a-zA-Z_]+)*$/;
        const numb = /^[0-9\b]+$/;
        moment.locale('id');

        const formTextField = (props) => {
            const {
                form: { setFieldValue, setFieldTouched },
                field: { name },
            } = props;

            const onChange = React.useCallback((event) => {
                const { value } = event.target;

                if (name === "ownerName") {
                    localStorage.setItem('rekInfo_ownerName', value)
                } else if (name === "tglLahir") {
                    localStorage.setItem('rekInfo_tglLahir', value)
                } else if (name === "noKtp") {
                    localStorage.setItem('rekInfo_noKtp', value)
                } else if (name === "ibuKandung") {
                    localStorage.setItem('rekInfo_ibuKandung', value)
                } else if (name === "noRekMandiri") {
                    localStorage.setItem('rekInfo_noRekMandiri', value)
                } else {

                }
                setFieldValue(name, value ? value : '');
            }, [setFieldValue, name])

            const onBlur = React.useCallback((event) => {
                const { value } = event.target;
                setFieldTouched(name, value ? false : true);
            }, [setFieldValue, setFieldTouched, name])

            return <TextField {...props} onChange={onChange} onBlur={onBlur} />
        }

        const klikLanjut = () => {
            history.push('/kodeOTP')
            location.reload();
        }

        const failedRegistered = () => {
            history.push('/infoRekeningGagal')
            location.reload();
        }

        const closeSnackBar = () => {
            this.setState({ openAlert: false })
        }

        const Alert = (props) => {
            return <MuiAlert elevation={6} variant="filled" {...props} />;
        }


        return (
            <div>
                <div style={{ marginLeft: '5px', marginTop: '7px' }}>
                    <Link href="/termsCondition">
                        <ArrowBackIcon />
                    </Link>
                </div>
                {/* -----------------alert----------- */}
                <Backdrop className={classes.backdrop} open={this.state.openAlert} >
                    <div className={classes.placeholder}>

                        {

                            this.state.query === 'progress' ?

                                (<Box className={classes.paper}>
                                    <Typography component={'span'} variant={'body2'} >
                                        <span><h4 style={{ color: 'black' }}>Data Sedang Diverifikasi</h4></span>
                                        <img src={progressGif}
                                            style={{ height: 150, width: 250 }}></img>
                                    </Typography>
                                </Box>

                                )
                                :
                                this.state.query === 'success' ?
                                    (<Box className={classes.paper}>
                                        <Typography component={'span'} variant={'body2'} >
                                            <span><h4 style={{ color: 'black' }}>Verifikasi Berhasil</h4></span>
                                            <img src={sukses}
                                                style={{ height: 50, width: 50 }}></img>
                                        </Typography>
                                        <Link>
                                            <Button onClick={klikLanjut} variant="contained"
                                                style={{ width: '100%', marginTop: '20px' }} color="primary"
                                                disableElevation>
                                                Lanjut
                                        </Button>
                                        </Link>
                                    </Box>

                                    ) :

                                    (
                                        <Typography component={'span'} variant={'body2'} className={classes.paper}>
                                            <span><h4 style={{ color: 'black' }}>Verifikasi Tidak Berhasil </h4>
                                            </span>
                                            <span><p style={{ color: 'black', width: 220 }}> {this.state.messageError} </p>
                                            </span>
                                            <img src={gagal}
                                                style={{ height: 50, width: 50 }}></img>
                                            <Link>
                                                <Button onClick={failedRegistered} variant="contained"
                                                    style={{ width: '100%', marginTop: '20px' }} color="primary"
                                                    disableElevation>
                                                    OK
                                        </Button>
                                            </Link>
                                        </Typography>
                                    )
                        }

                    </div>
                </Backdrop>
                {/* --------------------------------- */}
                <Grid style={{ padding: '10px', justifyContent: 'center', alignItems: 'center', textAlign: 'center' }}>
                    <h4 style={{ textAlign: 'center', marginTop: '0px' }}>Informasi Rekening</h4>
                    <p style={{ marginTop: '5px', color: 'lightslategray', marginBottom: '20px' }}>
                        Pastikan data rekening sama dengan KTP.</p>

                    <Box style={{ textAlign: 'center', position: 'fixed', display: 'contents' }}>

                        <Formik
                            initialValues={{

                                ownerName: localStorage.getItem('rekInfo_ownerName') ?
                                    localStorage.getItem('rekInfo_ownerName') : '',
                                tglLahir: localStorage.getItem('rekInfo_tglLahir') ?
                                    moment(localStorage.getItem('rekInfo_tglLahir'), "DD MMMM YYYY").toDate()
                                    : moment(new Date()).format("DD MMMM YYYY"),
                                noKtp: localStorage.getItem('rekInfo_noKtp') ? localStorage.getItem('rekInfo_noKtp') : '',
                                ibuKandung: localStorage.getItem('rekInfo_ibuKandung') ? localStorage.getItem('rekInfo_ibuKandung') : '',
                                noRekMandiri: localStorage.getItem('rekInfo_noRekMandiri') ? localStorage.getItem('rekInfo_noRekMandiri') : ''
                            }}


                            validationSchema={Yup.object().shape({
                                ownerName: Yup.string().matches(alpha, { message: "Nama tidak valid", excludeEmptyString: true })
                                    .max(25, 'Maksimal hanya bisa 25 karakter')
                                    .required('Harus diisi'),
                                tglLahir: Yup.string().required('Harus diisi'),//Yup.date().transform(parseTglLahir).max(new Date()),
                                noKtp: Yup.string().max(16, 'Nomor KTP harus 16 digit')
                                    .min(16, "Nomor KTP harus 16 digit")
                                    .matches(numb, { message: "Nomor KTP tidak valid", excludeEmptyString: true })
                                    .required('Harus diisi'),
                                ibuKandung: Yup.string().matches(alpha, { message: "Nama Ibu kandung tidak valid", excludeEmptyString: true })
                                    .required('Harus diisi'),
                                noRekMandiri: Yup.string().max(13, 'Nomor rekening harus 13 digit')
                                    .min(13, "Nomor rekening harus 13 digit")
                                    .matches(numb, { message: "Nomor Rekening tidak valid", excludeEmptyString: true })
                                    .required('Harus diisi')
                            })}


                            onSubmit={({ ownerName, tglLahir, noKtp, ibuKandung, noRekMandiri }, actions) => {

                                this.setState({ openAlert: true })

                                clearTimeout(this.timer);

                                if (this.state.query !== 'idle') {
                                    this.setState({ query: 'idle' });
                                    return;
                                }
                                var validasiInformasiRekening = {
                                    dob: moment(tglLahir).format('DDMMYYYY'),
                                    namaIbuKandung: ibuKandung,
                                    namaPemilikUsaha: ownerName,
                                    nomorKtp: noKtp,
                                    nomorRekening: noRekMandiri
                                }

                                this.setState({ query: 'progress' });

                                this.timer = setTimeout(() => {
                                    wsWithBody(BACK_END_POINT + "/UserOob/validasiInformasiRekening",
                                        PUT, validasiInformasiRekening, HEADER_AUTH).then(response => {
                                            localStorage.setItem('rekInfo_tglLahir',
                                                moment(validasiInformasiRekening.dob, "DDMMYYYY").format("DD MMMM YYYY")
                                            )
                                            localStorage.setItem('rekInfo_tglLahirOnDDMMYYYFormat',validasiInformasiRekening.dob
                                            )
                                            localStorage.setItem('rekInfo_ibuKandung', validasiInformasiRekening.namaIbuKandung)
                                            localStorage.setItem('rekInfo_ownerName', validasiInformasiRekening.namaPemilikUsaha)
                                            localStorage.setItem('rekInfo_noKtp', validasiInformasiRekening.nomorKtp)
                                            localStorage.setItem('rekInfo_noRekMandiri', validasiInformasiRekening.nomorRekening)
                                            localStorage.setItem('address', response.data.result.address)
                                            localStorage.setItem('cifEncryptor', response.data.result.cifInEncrypt)
                                            localStorage.setItem('noTelpOtp', response.data.result.noTelp)
                                            console.log('values respon ownerName: ', validasiInformasiRekening.namaPemilikUsaha);
                                            console.log('values tglLahir: ', localStorage.getItem('rekInfo_tglLahir'));
                                            console.log('values tglLahir ori: ', validasiInformasiRekening.dob);
                                            console.log('values noKtp: ', validasiInformasiRekening.nomorKtp);
                                            console.log('values ibuKandung: ', validasiInformasiRekening.namaIbuKandung);
                                            console.log('values noRekMandiri: ', validasiInformasiRekening.nomorRekening);
                                            this.setState({ proses: 'success' })
                                            this.setState({ query: this.state.proses });
                                        }).catch(error => {

                                            console.log('errorr respon:', error);
                                            if (error.message === "Network Error") {
                                                alert('Oops jaringan internet terputus silahkan coba kembali')
                                                this.setState({
                                                    openAlert: false,
                                                    proses: '',
                                                    query: 'idle'
                                                })
                                                actions.setSubmitting(false);
                                            } else {
                                                this.setState({ proses: 'failed' });
                                                this.setState({ query: this.state.proses });
                                                this.setState({ messageError: error.response.data.message })
                                               
                                                // localStorage.removeItem('rekInfo_ibuKandung')
                                                // localStorage.removeItem('rekInfo_ownerName')
                                                // localStorage.removeItem('rekInfo_noKtp')
                                                // localStorage.removeItem('rekInfo_noRekMandiri')
                                            }




                                        })
                                }, TIMES_OUT);

                            }}>

                            {({ values, isValid, errors, touched, setFieldTouched, setFieldValue, isSubmitting }) => (
                                <MuiPickersUtilsProvider utils={MomentUtils}>
                                    <Form>
                                        <Box style={{ margin: '10px', marginBottom: '20px' }}>
                                            <Field
                                                component={formTextField}
                                                id="ownerName"
                                                name={"ownerName"}
                                                label="Nama Pemilik Usaha"
                                                variant="outlined"
                                                value={values.ownerName}
                                                fullWidth
                                                maxLength="25"
                                                error={(errors.ownerName && touched.ownerName)}
                                                helperText={errors.ownerName ? '' : "Maksimal 25 Karakter"}
                                                className={'form-control' + (errors.ownerName && touched.ownerName ? ' is-invalid' : '')}
                                            />
                                            <ErrorMessage name="ownerName" component="div" className="invalid-feedback" />
                                        </Box>
                                        <Box style={{ margin: '10px', marginBottom: '20px' }}>
                                            <Field
                                                component={DatePicker}
                                                name={"tglLahir"}
                                                label="Tanggal Lahir"
                                                onChange={(newVal) => setFieldValue("tglLahir", moment(newVal).format("YYYY/MM/DD"))}
                                                onBlur={() => setFieldTouched("tglLahir", true)}
                                                value={values.tglLahir}
                                                fullWidth
                                                inputVariant="outlined"
                                                views={["year", "month", "date"]}
                                                openTo="year"
                                                format="DD MMMM YYYY"
                                                error={(errors.tglLahir && touched.tglLahir)}
                                                className={'form-control' + (errors.tglLahir && touched.tglLahir ? ' is-invalid' : '')}
                                            />
                                            <ErrorMessage name="tglLahir" component="div" className="invalid-feedback" />
                                        </Box>
                                        <Box style={{ margin: '10px', marginBottom: '20px' }}>
                                            <Field
                                                component={formTextField}
                                                id="noKtp"
                                                name={"noKtp"}
                                                label="Nomor KTP"
                                                variant="outlined"
                                                value={values.noKtp}
                                                fullWidth
                                                error={(errors.noKtp && touched.noKtp)}
                                                className={'form-control' + (errors.noKtp && touched.noKtp ? ' is-invalid' : '')}

                                            />
                                            <ErrorMessage name="noKtp" component="div" className="invalid-feedback" />
                                        </Box>
                                        <Box style={{ margin: '10px', marginBottom: '20px' }}>
                                            <Field
                                                component={formTextField}
                                                id="ibuKandung"
                                                name={"ibuKandung"}
                                                label="Nama Gadis Ibu Kandung"
                                                variant="outlined"
                                                value={values.ibuKandung}
                                                fullWidth
                                                error={(errors.ibuKandung && touched.ibuKandung)}
                                            />
                                            <ErrorMessage name="ibuKandung" component="div" className="invalid-feedback" />
                                        </Box>
                                        <Box style={{ margin: '10px', marginBottom: '20px' }}>
                                            <Field
                                                component={formTextField}
                                                id="noRekMandiri"
                                                name={"noRekMandiri"}
                                                label="Nomor Rekening Bank Mandiri"
                                                variant="outlined"
                                                value={values.noRekMandiri}
                                                fullWidth
                                                error={(errors.noRekMandiri && touched.noRekMandiri)}

                                            />
                                            <ErrorMessage name="noRekMandiri" component="div" className="invalid-feedback" />
                                        </Box>
                                        <Button variant="contained"
                                            disableElevation disabled={isSubmitting}
                                            type="submit" size="large"
                                            style={{
                                                width: "-webkit-fill-available", marginTop: '10px',
                                                textTransform: 'none', marginBottom: '10px'
                                            }}
                                            variant="contained" color="primary">
                                            Lanjutkan
                                         </Button>
                                    </Form>
                                </MuiPickersUtilsProvider>
                            )}
                        </Formik>
                    </Box>
                </Grid>



            </div>
        )
    }
}

export default compose(
    withStyles(useStyles),
    connect(
        (state) => ({
            alert: state.alert
          , isLoading: state.submit.isLoading
          , isPreparing: state.submit.isPreparing
        }),
        {
            reset: alertActions.reset,
            validateInfoRekening: infoRekeningActions.validateInfoRekening
        }
    )
)(infoRekening)
