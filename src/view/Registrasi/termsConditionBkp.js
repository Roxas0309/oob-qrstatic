import React, { Component } from "react";
import Link from "@material-ui/core/Link";
import ArrowBackIcon from "@material-ui/icons/ArrowBack";
import { AppBar, Box, Button, Card, Grid, Toolbar, Typography } from "@material-ui/core";

class termsCondition extends Component {

  componentWillUnmount(){
    location.reload();
  }

  render() {
    const { loading = false } = this.props;

    const lanjut = () => {
      this.props.history.push('/infoRekening', {from: '/termsCondition'})
      location.reload()
    }
    return (
      <div>
        {/* <div style={{ marginLeft: "20px", marginTop: "24px" }}>
          <Link href="/startReg">
            <ArrowBackIcon />
          </Link>
        </div> */}
        <AppBar style={{backgroundColor: 'transparent', position: 'absolute', 
                boxShadow: 'unset'}}>
            <Toolbar  style={{minHeight: '60px'}}>
            {/* onClick={this.props.history.push('/startReg', {from: '/termsCondition'})} */}
                <Link href="/startReg" style={{position: 'absolute', display: 'flex'}} >
                    <ArrowBackIcon/>
                </Link>
            </Toolbar>
        </AppBar>
        <Grid
          style={{
            // padding: "10px",
            justifyContent: "center",
            alignItems: "center",
            //marginInline: "20px",
            width: '100% !important',
            color: "#6B7984",
          }}
        >
          <div style={{padding:'16px',paddingBottom:'27vh'}}>
          <h3
            style={{
              marginTop: "70px",
              fontFamily: "Montserrat",
              textAlign: "center",
              fontStyle: "normal",
              fontWeight: "bold",
              fontSize: "18px",
              marginRight:'10vw',
              marginLeft:'10vw',
              lineHeight: "150%",
              color: "#121518",
            }}
          >
            Syarat &amp; Ketentuan
MORIS Mandiri
          </h3>
          <Typography
            variant="subtitle2"
            gutterBottom
            style={{
              fontFamily: "Nunito",
              fontStyle: "normal",
              fontWeight: "bold",
              fontSize: "16px",
              lineHeight: "150%",
              /* identical to box height, or 24px */
              display: "flex",
              alignItems: "flex-end",
              color: "6B7984",
            }}
          >
            A. DEFINISI
          </Typography>
          <ol
            style={{
              marginTop: "8px",
              color: "#6B7984",
              fontFamily: "Nunito",
              fontStyle: "normal",
              fontWeight: "normal",
              fontSize: "16px",
              lineHeight: "150%",
            }}
          >
            <li>Bank adalah PT. Bank Mandiri (Persero) Tbk.</li>
            <li>
            MORIS Mandiri adalah platform atau sarana dalam bentuk progressive web app dibawah naungan Bank untuk mengakomodasi proses pengajuan usaha dan menyediakan dashboard yang menampilkan QR Code berstandar QRIS dan informasi terkait penerimaan Transaksi. 
            </li>
            <li>
            Quick Response Code Indonesian Standard (QRIS) adalah standarisasi QR Code yang ditetapkan oleh Bank Indonesia sehingga seluruh aplikasi pembayaran dapat memindai kode QR tersebut.
            </li>
            <li>
            Quick Response (QR) Code adalah kode dua dimensi yang terdiri atas penanda tiga pola persegi pada sudut kiri bawah, sudut kiri atas, dan sudut kanan atas, memiliki modul hitam berupa persegi titik atau piksel, dan memiliki kemampuan menyimpan data alfanumerik, karakter, dan simbol.
            </li>
            <li>
            QR Code Statis adalah QR Code yang diterbitkan sebelum terdapat Transaksi yang akan diinisiasi dan dapat dipindai berulang kali untuk memfasilitasi berbagai Transaksi pembayaran yang berbeda.
            </li>
            <li>Pemilik Usaha adalah Nasabah yang menjual barang dan/atau jasa dan menerima Transaksi pembayaran menggunakan MORIS Mandiri.</li>
            <li>Nasabah adalah individu atau perorangan yang memiliki rekening tabungan dan/atau giro di Bank.</li>
            <li>Rekening adalah rekening yang ditunjuk oleh Pemilik Usaha sebagai rekening tujuan pengkreditan dana hasil Transaksi.</li>
            <li>Pelanggan adalah pemilik aplikasi pembayaran yang melakukan Transaksi.</li>
            <li>Transaksi adalah transaksi atas pembelian barang dan/atau jasa dari Pemilik Usaha dengan cara memindai QR Code Statis.</li>
            <li>Settlement adalah proses penyelesaian pembayaran Transaksi yang telah dilakukan untuk ditagihkan kepada Bank.</li>
            <li>Merchant Discount Rate (MDR) adalah biaya administrasi Transaksi yang dikenakan oleh Bank kepada Pemilik Usaha atas Transaksi.</li>
          </ol>
          <Typography
            variant="subtitle2"
            gutterBottom
            style={{
                marginTop:'40px',
              fontFamily: "Nunito",
              fontStyle: "normal",
              fontWeight: "bold",
              fontSize: "16px",
              lineHeight: "150%",
              /* identical to box height, or 24px */
              display: "flex",
              alignItems: "flex-end",
              color: "6B7984",
            }}
          > B. HAK DAN KEWAJIBAN PEMILIK USAHA
          </Typography>
          <ol 
                      style={{
                        marginTop: "8px",
                        color: "#6B7984",
                        fontFamily: "Nunito",
                        fontStyle: "normal",
                        fontWeight: "normal",
                        fontSize: "16px",
                        lineHeight: "150%",
                      }}
          >
          
<li>Hak Pemilik Usaha
<ol>
    <li>Menerima dana hasil Transaksi.</li>
    <li>Menerima training/edukasi mengenai tata cara mekanisme Transaksi.</li>
</ol>

</li>

<li>Kewajiban Pemilik Usaha
<ol>
    <li>Pemilik Usaha wajib:
        <ol>
            <li>Menerima Transaksi tanpa memberikan batasan minimum dan memberikan pelayanan sebaik-baiknya kepada Pelanggan.</li>
            <li>Menjamin tidak mengenakan biaya tambahan atas Transaksi.</li>
            <li>Tidak menyimpan data Pelanggan.</li>
        </ol>
    </li>
    <li>Pemilik Usaha wajib menjaga kerahasian data dan informasi dari Pelanggan yang melakukan transaksi. Pemilik Usaha bertanggung jawab sepenuhnya atas semua kerugian yang timbul dari penyalahgunaan dan/atau kebocoran data Pelanggan akibat kelalaian atau kesalahan Pemilik Usaha saat melakukan transaksi di lokasi usaha.</li>
    <li>Pemilik Usaha wajib menyediakan data Transaksi dan informasi lainnya yang dibutuhkan Bank untuk keperluan investigasi jika diperlukan.</li>
    <li>Pemilik Usaha wajib memberikan keterangan yang valid atas aktivitasnya baik secara lisan atau tertulis dan memberikan persetujuan kepada Bank untuk menghubungi pihak manapun guna memperoleh keterangan-keterangan yang dibutuhkan oleh Bank.</li>
    <li>Pemilik Usaha wajib memberitahukan secara tertulis kepada Bank selambat-lambatnya 7 (tujuh) hari kerja jika Pemilik Usaha ingin melakukan perubahan data informasi, seperti nama, nomor telepon, nomor rekening dan lokasi usaha.</li>
    <li>Merchant wajib melaporkan kepada Bank apabila menemukan:
        <ol>
            <li>Transaksi yang dinilai berisiko tinggi dan abnormal.</li>
            <li>Anomali peningkatan kegagalan Transaksi.</li>
        </ol>
    </li>
    <li>
    Pemilik Usaha bertanggung jawab sepenuhnya atas segala kerugian yang timbul akibat fraud/ kecurangan yang timbul kepada Pelanggan maupun kepada Bank yang terbukti dilakukan oleh Pemilik Usaha ataupun pihak yang terafiliasi dengannya.
    </li>
</ol>

</li>

          </ol>


          <Typography
            variant="subtitle2"
            gutterBottom
            style={{
                marginTop:'40px',
              fontFamily: "Nunito",
              fontStyle: "normal",
              fontWeight: "bold",
              fontSize: "16px",
              lineHeight: "150%",
              /* identical to box height, or 24px */
              display: "flex",
              alignItems: "flex-end",
              color: "6B7984",
            }}
          > C. KETENTUAN PENGGUNAAN MORIS MANDIRI
          </Typography>
          <ol 
                      style={{
                        marginTop: "8px",
                        color: "#6B7984",
                        fontFamily: "Nunito",
                        fontStyle: "normal",
                        fontWeight: "normal",
                        fontSize: "16px",
                        lineHeight: "150%",
                      }}
          >

<li>Kecuali dinyatakan lain oleh Bank, untuk dapat menggunakan MORIS Mandiri, Pemilik Usaha harus telah memenuhi semua persyaratan yang ditentukan oleh Bank:
    <ol>
        <li>Merupakan Pemilik Usaha Perorangan.</li>
        <li>Memiliki Rekening di Bank atas nama Pemilik Usaha sebagai rekening penampungan.</li>
        <li>Memenuhi persyaratan dokumen sebagai penerima QR Code berstandar QRIS yang telah diyakini validitasnya.</li>
        <li>Pemilik Usaha tidak bergerak di bidang usaha ilegal seperti porn site/ adult shop, gambling (perjudian), gunshop (jual beli senjata api), jual beli binatang langka, cashing (penarikan tunai), copyrights goods, atau barang yang dilarang di Indonesia.</li>
    </ol>
</li>
<li>Untuk memfasilitasi MORIS Mandiri, Bank akan menyediakan: </li>
<ol>
    <li>User ID (No. HP terdaftar) dan password yang akan digunakan Pemilik Usaha untuk mengakses aplikasi.</li>
    <li>Dashboard yang dapat digunakan Pemilik Usaha untuk:
        <ol>
            <li>Menampilkan, mengunduh dan membagikan QRIS;</li>
            <li>Mengakses informasi terkait Transaksi termasuk namun tidak terbatas pada notifikasi, riwayat Transaksi yang dapat ditampilkan minimal 7 (tujuh) hari dan maksimal 90 (sembilan puluh) hari serta informasi pencairan yang menampilkan nominal dana transaksi yang sudah masuk Rekening.</li>
            <li>Melakukan aktivitas lainnya terkait dengan Transaksi dan pengelolaan data Pemilik Usaha sesuai ketentuan yang berlaku di Bank.</li>
        </ol>
    </li>
</ol>

<li>Pemilik Usaha dilarang mengubah, merusak, atau memodifikasi atau mencoba mengubah, merusak, atau memodifikasi sistem MORIS Mandiri. </li>

<li>Pemilik Usaha wajib dan bertanggung jawab untuk memastikan ketepatan dan kelengkapan instruksi transaksi. Bank tidak bertanggung jawab terhadap segala akibat apapun yang timbul karena kelalaian, ketidaklengkapan, ketidakjelasan data, atau ketidaktepatan instruksi dari Pemilik Usaha.</li>

<li>Bank berhak menghentikan aplikasi MORIS Mandiri untuk jangka waktu tertentu yang ditentukan oleh Bank untuk keperluan pembaharuan, pemeliharaan, atau untuk tujuan lain dengan alasan apapun yang dianggap baik oleh Bank, dan untuk itu Bank tidak berkewajiban mempertanggungjawabkannya kepada siapapun. </li>

          </ol>

          <Typography
            variant="subtitle2"
            gutterBottom
            style={{
                marginTop:'40px',
              fontFamily: "Nunito",
              fontStyle: "normal",
              fontWeight: "bold",
              fontSize: "16px",
              lineHeight: "150%",
              /* identical to box height, or 24px */
              display: "flex",
              alignItems: "flex-end",
              color: "6B7984",
            }}
          > D. KETENTUAN USER ID DAN PASSWORD MORIS MANDIRI
          </Typography>
        <ol>
            <li>User ID dan password bersifat rahasia dan hanya boleh digunakan oleh Pemilik Usaha yang bersangkutan.</li>
        <li>Setelah mendapatkan user ID dan password dari email dan SMS yang telah dikirimkan ke No HP yang terdaftar di sistem Bank maka dalam jangka waktu maksimal 2 (dua) hari kalender sejak penerimaan user ID dan password tersebut, Pemilik Usaha wajib segera mengganti password awal (default) melalui aplikasi MORIS Mandiri. Jika Pemilik Usaha tidak segera mengganti password dalam jangka waktu yang ditentukan di atas, maka password awal akan invalid dan harus menghubungi Call Center HiYokke di hotline 14021 untuk dilakukan pemrosesan lebih lanjut.</li>
       <li>Pemilik Usaha wajib menjaga kerahasiaan user ID dan password dengan cara:</li>
       <ol>
           <li>Tidak memberitahukan kepada orang lain termasuk kepada anggota keluarga atau orang terdekat;</li>
            <li>Tidak menuliskan pada HP dan/atau menyimpan dalam bentuk tertulis pada sarana penyimpanan lainnya, yang memungkinkan untuk diketahui oleh orang lain;</li>
            <li>Tidak memilih password berupa angka dan kata-kata yang umum serta mudah ditebak seperti 1111111, 1234567, tanggal lahir atau kombinasinya, nomor telepon, nama panggilan dan lain-lain;</li>
            <li>Berhati-hati pada saat menggunakan aplikasi agar informasi rahasia tidak terlihat oleh orang lain;</li>
            <li>Melakukan penggantian password secara berkala.</li>
       </ol>
       <li>Dalam hal Pemilik Usaha mengetahui atau menduga user ID dan password telah diketahui oleh orang lain yang tidak berwenang, maka Pemilik Usaha wajib segera melakukan pengamanan dengan melakukan perubahan password.</li>
       <li>Segala penyalahgunaan user ID dan password merupakan tanggung jawab Pemilik Usaha sepenuhnya. Pemilik Usaha dengan ini membebaskan Bank dari segala tuntutan yang timbul, baik dari pihak lain maupun Pemilik Usaha sendiri sebagai akibat penyalahgunaan user ID dan password.</li>
       <li>Penggunaanuser ID dan password pada aplikasi MORIS Mandiri mempunyai kekuatan hukum yang sama dengan perintah tertulis yang ditandatangani oleh Pemilik Usaha, juga merupakan pemberian kuasa kepada Bank untuk melakukan pendebetan rekening Pemilik Usaha yang timbul sehubungan dengan aplikasi MORIS Mandiri.</li>
        </ol>



        <Typography
            variant="subtitle2"
            gutterBottom
            style={{
                marginTop:'40px',
              fontFamily: "Nunito",
              fontStyle: "normal",
              fontWeight: "bold",
              fontSize: "16px",
              lineHeight: "150%",
              /* identical to box height, or 24px */
              display: "flex",
              alignItems: "flex-end",
              color: "6B7984",
            }}
          > E. KETENTUAN PEMROSESAN TRANSAKSI
          </Typography>

          <ol>
<li>Pemilik Usaha wajib menerima setiap Transaksi menggunakan MORIS Mandiri.</li>
<li>Bank akan memberikan bukti Transaksi terhadap setiap transaksi berhasil kepada Pemilik Usaha melalui dashboard aplikasi yang membuktikan adanya Transaksi.</li>
<li>Pemilik Usaha setuju untuk tidak membantah keabsahan, kebenaran atau keaslian bukti Transaksi menggunakan MORIS Mandiri yang ditransmisi secara elektronik antara kedua belah pihak, termasuk dokumen dalam bentuk email, catatan system, tape/ catridge, hasil print out computer, salinan atau bentuk penyimpanan informasi atau data lain yang terdapat pada MORIS Mandiri. Bukti transaksi merupakan alat bukti yang sah dan mengikat atas dasar instruksi Pemilik Usaha yang dijalankan oleh MORIS Mandiri.</li>
<li>Bank dapat menolak Transaksi jika Pemilik Usaha melanggar sebagian dan/atau seluruh Syarat dan Ketentuan ini dan/atau melanggar ketentuan peraturan perundang-undangan yang berlaku.</li>
<li>Pemilik Usaha melepaskan Bank dari segala tuntutan, gugatan, atau permintaan ganti rugi dari Pemilik Usaha, Pelanggan dan/atau pihak manapun terkait dengan penyerahan barang dan/atau jasa oleh Pemilik Usaha kepada Pelanggan tanpa disertai bukti transaksi berhasil.</li>
<li>Pemilik Usaha bersedia dibebankan kembali oleh Bank atas seluruh biaya kerugian yang mungkin timbul dalam Transaksi yang disebabkan oleh adanya kelalaian dan/atau kegagalan Pemilik Usaha dalam menjalankan kewajiban sebagaimana diatur di Syarat dan Ketentuan ini. </li>
          </ol>

          <Typography
            variant="subtitle2"
            gutterBottom
            style={{
                marginTop:'40px',
              fontFamily: "Nunito",
              fontStyle: "normal",
              fontWeight: "bold",
              fontSize: "16px",
              lineHeight: "150%",
              /* identical to box height, or 24px */
              display: "flex",
              alignItems: "flex-end",
              color: "6B7984",
            }}
          > F. MERCHANT DISCOUNT RATE
          </Typography>

          <ol>
<li>Terhadap Penggunaan MORIS Mandiri, Pemilik Usaha wajib membayar Merchant Discount Rate (MDR) sebesar 0.7% (nol koma tujuh persen) dihitung dari seluruh jumlah nominal transaksi dengan ketentuan dan perhitungan untuk Pelanggan yang menggunakan Layanan Mandiri Online untuk melakukan transaksi (Transaksi On Us) dan untuk Pelanggan yang menggunakan uang elektronik lain untuk melakukan transaksi (Transaksi Off Us).</li>
<li>Pemilik Usaha setuju jika Bank sewaktu-waktu berhak untuk mengubah ketentuan, perhitungan dan jumlah MDR atau biaya lainnya. Perubahan biaya tersebut akan diberitahukan oleh Bank kepada Pemilik Usaha dalam bentuk dan melalui sarana apapun sesuai ketentuan hukum yang berlaku.</li>
          </ol>

          <Typography
            variant="subtitle2"
            gutterBottom
            style={{
                marginTop:'40px',
              fontFamily: "Nunito",
              fontStyle: "normal",
              fontWeight: "bold",
              fontSize: "16px",
              lineHeight: "150%",
              /* identical to box height, or 24px */
              display: "flex",
              alignItems: "flex-end",
              color: "6B7984",
            }}
          > G. PENGKREDITAN DATA HASIL TRANSAKSI
          </Typography>

          <ol>
<li>Pemilik Usaha dengan ini memberikan  kuasa kepada Bank untuk memotong dana hasil transaksi yang harus dibayarkan kepada Pemilik Usaha guna melunasi MDR dan/atau biaya lainnya.</li>
<li>Bank akan mengkreditkan dana hasil transaksi (settlement) ke Rekening setelah  dipotong MDR dan/atau biaya lainnya pada hari yang sama atau pada hari kalender selanjutnya sejak tanggal Transaksi. Informasi mengenai nominal dana Transaksi yang sudah terkredit ke Rekening Pemilik Usaha akan tercantum di dashboard dengan ketentuan dari Bank yaitu maksimal release settlement Rp. 500.000,00 (Lima Ratus Ribu Rupiah) per hari.</li>
<li>Jika Bank melakukan kesalahan/ kelalaian dalam melakukan pemotongan Rekening Pemilik Usaha, maka Bank akan melakukan pengkreditan kembali ke Rekening.</li>
<li>Dalam hal terjadi perbedaan data, Pemilik Usaha wajib segera melaporkan perbedaan data tersebut kepada Bank selambat-lambatnya 7 (tujuh) hari kalender terhitung sejak tanggal transaksi. Jika tidak adanya klaim setelah lewat 7 (tujuh) hari kalender terhitung sejak tanggal transaksi, maka yang berlaku adalah jumlah tagihan sesuai data transaksi yang ada pada catatan Bank.</li>
<li>Jika Bank mencurigai bahwa suatu transaksi merupakan transaksi fiktif, rekayasa, terdapat ketidakwajaran atas nilai, jumlah, pola transaksi dan/atau hal-hal terkait dengan transaksi, dilarang oleh ketentuan dan peraturan perundang-undangan yang berlaku, atau tidak sesuai dengan ketentuan dalam Syarat dan Ketentuan ini, dan/atau apabila Pemilik Usaha tidak memenuhi ketentuan dalam Syarat dan Ketentuan ini, maka untuk kepentingan pemeriksaan transaksi dan/atau terlaksananya Layanan ini, Bank berhak tanpa pemberitahuan sebelumnya untuk
    <ol>
        <li>Menangguhkan pembayaran tagihan;</li>
        <li>Menghentikan sementara atau permanen sarana Bank;</li>
        <li>Tindakan-tindakan lainnya yang dianggap perlu oleh Bank untuk pemeriksaan Transaksi.</li>
    </ol>
</li>
<li>Bank tidak bertanggung jawab atas segala kerugian yang timbul sehubungan dengan penghentian sementara atau permanen sarana Bank sebagaimana dimaksud dalam ayat 5 Klausul ini.</li>
          </ol>

          <Typography
            variant="subtitle2"
            gutterBottom
            style={{
                marginTop:'40px',
              fontFamily: "Nunito",
              fontStyle: "normal",
              fontWeight: "bold",
              fontSize: "16px",
              lineHeight: "150%",
              /* identical to box height, or 24px */
              display: "flex",
              alignItems: "flex-end",
              color: "6B7984",
            }}
          > G. PENGKREDITAN DATA HASIL TRANSAKSI
          </Typography>
          <ol>
              <li>MORIS Mandiri ini berlaku efektif terhitung sejak Pemilik Usaha dapat mengakses atau login dashboard dan akan dilakukan penghentian akses atau penutupan akun pada MORIS Mandiri dengan kondisi:
                  <ol>
                      <li>Pemilik Usaha menutup semua Rekening yang dimiliki di Bank.</li>
                      <li>Salah satu pihak atau kedua belah pihak sepakat untuk mengakhiri Perjanjian penggunaan MORIS Mandiri.</li>
                      <li>Pemilik Usaha tidak memenuhi kewajiban-kewajiban yang telah ditentukan baginya berdasarkan Syarat dan Ketentuan ini maupun yang terdapat pada ketentuan perundang-undangan yang berlaku.</li>
                      <li>Diterimanya laporan dari Pemilik Usaha mengenai dugaan atau diketahuinya user ID dan password telah dikuasai oleh orang lain yang tidak berwenang.</li>
                      <li>Tidak adanya aktivitas Transaksi selama 3 (tiga) bulan berturut-turut sejak tanggal Transaksi terakhir atau berdasarkan kebijakan dan/atau analisa risiko Bank yang semata-mata dilakukan dalam rangka melindungi kepentingan Pemilik Usaha.</li>

                  </ol>
              </li>
              <li>Pemilik Usaha melakukan kesalahan dalam memasukkan password sebanyak tiga kali berturut-turut atau penggantian nomor yang terpasang pada HP dapat mengakibatkan MORIS Mandiri tidak dapat diakses dan diperlukan pelaporan sebagaimana pada Klausul Penyelesaian Pengaduan.</li>
              <li>Bank dapat melakukan pengakhiran kerjasama secara sepihak berdasarkan pertimbangannya dengan melakukan pemberitahuan kepada Pemilik Usaha selambat-lambatnya 7 (tujuh) hari kalender sebelum tanggal pengakhiran berlaku efektif.</li>
              <li>Jika terdapat hak dan/atau kewajiban yang telah ada sebelum MORIS Mandiri berakhir namun belum diselesaikan oleh Bank dan/atau Pemilik Usaha, maka hak dan/atau kewajiban tersebut masih dianggap ada sehingga kedua pihak tetap tunduk pada ketentuan yang telah disepakati dalam syarat dan ketentuan ini sampai dengan hak dan kewahiban tersebut selesai terpenuhi.</li>
              <li>Jika MORIS Mandiri ini berakhir, maka pengakhiran MORIS Mandiri ini tidak melepaskan atau membebaskan Pemilik Usaha dari kewajiban Pemilik Usaha atas transaksi yang timbul sebelum dan/atau pada saat pengakhiran aplikasi ini.</li>
          </ol>

          <Typography
            variant="subtitle2"
            gutterBottom
            style={{
                marginTop:'40px',
              fontFamily: "Nunito",
              fontStyle: "normal",
              fontWeight: "bold",
              fontSize: "16px",
              lineHeight: "150%",
              /* identical to box height, or 24px */
              display: "flex",
              alignItems: "flex-end",
              color: "6B7984",
            }}
          > G. PENGKREDITAN DATA HASIL TRANSAKSI
          </Typography>
          <ol>
              <li>Dalam hal Pemilik Usaha melakukan pelanggaran baik sebagian atau seluruh ketentuan dalam Syarat &amp; Ketentuan ini, antara lain:
<ol>
    <li>Memberikan data atau keterangan yang tidak benar.</li>
    <li>Memberlakukan biaya tambahan Transaksi.</li>
    <li>Mengubah nama dan/atau jenis usaha tanpa terlebih dahulu menyampaikan pemberitahuan kepada Bank sesuai jangka waktu yang ditentukan dalam Pasal B.2.d.</li>
    <li>Menyerahkan bukti pendukung Transaksi yang tidak sah.</li>
    <li>Melakukan kerjasama dengan pelaku kejahatan (fraudster).</li>
</ol>
              </li>
              <li>Dalam hal Pemilik Usaha melakukan perbuatan melawan hukum lainnya, maka Bank berhak:
                  <ol>
                      <li>Sewaktu-waktu mengakhiri kerjasama dengan Pemilik Usaha.</li>
                      <li>Menunda atau menolak melakukan penyelesaian dana hasil Transaksi kepada Pemilik Usaha, memotong atau menagih kembali tagihan yang telah dibayarkan kepada Pemilik Usaha.</li>
                      <li>Tindakan-tindakan lainnya yang dianggap perlu oleh Bank.</li>

                  </ol>
              </li>
              <li></li>Segala kerugian yang dialami baik oleh Bank atau Pelanggan sebagai akibat kesalahan atau kelalaian Pemilik Usaha menjadi tanggung jawab Pemilik Usaha sepenuhnya.
          </ol>


          <Typography
            variant="subtitle2"
            gutterBottom
            style={{
                marginTop:'40px',
              fontFamily: "Nunito",
              fontStyle: "normal",
              fontWeight: "bold",
              fontSize: "16px",
              lineHeight: "150%",
              /* identical to box height, or 24px */
              display: "flex",
              alignItems: "flex-end",
              color: "6B7984",
            }}
          > J. PENYELESAIAN PENGADUAN
          </Typography>
<ol>
    <li>Pemilik Usaha dapat mengajukan pengaduan yang berkaitan dengan transaksi dan/atau akses ke aplikasi MORIS Mandiri dengan menghubungi Call Center Hiyokke di hotline 14021. </li>
    <li>Pemilik Usaha wajib melaporkan kepada Kantor Cabang Bank terdekat untuk melakukan pengkinian atau pembaharuan data Nasabah jika terdapat perubahan No. HP atau masalah terkait Rekening,</li>
    <li>Bank atau pihak yang ditunjuk oleh Bank berhak meminta dokumen pendukung lainnya untuk menindaklanjuti keluhan dimaksud.</li>
    <li>Bank atau pihak yang ditunjuk oleh Bank akan menanggapi keluhan sesuai dengan ketentuan hukum yang berlaku.</li>
    <li>Bank atau pihak yang ditunjuk oleh Bank berhak untuk menolak menangani keluhan yang disampaikan setelah 3 (tiga) bulan atau lebih sejak tanggal Transaksi.</li>

</ol>


<Typography
            variant="subtitle2"
            gutterBottom
            style={{
                marginTop:'40px',
              fontFamily: "Nunito",
              fontStyle: "normal",
              fontWeight: "bold",
              fontSize: "16px",
              lineHeight: "150%",
              /* identical to box height, or 24px */
              display: "flex",
              alignItems: "flex-end",
              color: "6B7984",
            }}
          > K. KERAHASIAAN
          </Typography>
          <ol>
<li>Pemilik Usaha wajib menjaga kerahasiaan seluruh informasi dan/atau data Transaksi termasuk tetapi tidak terbatas pada bukti transaksi dan data Pelanggan dengan tidak memberikannya kepada pihak lain selain kepada Bank atau pihak yang ditunjuk oleh Bank.</li>
<li>Pemilik Usaha dengan ini setuju untuk mematuhi setiap peraturan dan/atau ketentuan yang dikeluarkan oleh Bank, lembaga penyelenggara jasa sistem pembayaran yang berlaku dan otoritas yang berwenang.</li>
<li>Ketentuan-ketentuan ini akan tetap berlaku sampai seluruh hak-hak dan kewajiban-kewajiban tersebut akan diselesaikan selambat-lambatnya 14 (empat belas) hari kerja sejak pengakhiran Perjanjian ini.</li>

          </ol>

          <Typography
            variant="subtitle2"
            gutterBottom
            style={{
                marginTop:'40px',
              fontFamily: "Nunito",
              fontStyle: "normal",
              fontWeight: "bold",
              fontSize: "16px",
              lineHeight: "150%",
              /* identical to box height, or 24px */
              display: "flex",
              alignItems: "flex-end",
              color: "6B7984",
            }}
          > L. FORCE MAJEURE
          </Typography>
<ol>
Pemilik Usaha akan membebaskan Bank dari segala macam tuntutan apapun, dalam hal Bank tidak dapat melaksanakan instruksi dari Pemilik Usaha baik sebagian maupun seluruhnya karena kejadian-kejadian atau sebab-sebab diluar kekuasaan atau kemampuan Bank termasuk namun tidak terbatas pada segala gangguan virus atau komponen membahayakan yang dapat mengganggu MORIS Mandiri, web browser atau komputer sistem Bank karena bencana alam, perang, huru-hara, keadaan peralatan, sistem atau transmisi yang tidak berfungsi, gangguan listrik, gangguan telekomunikasi, kebijakan pemerintah, serta kejadian-kejadian atau sebab-sebab lain diluar kekuasaan atau kemampuan Bank.
</ol>

<Typography
            variant="subtitle2"
            gutterBottom
            style={{
                marginTop:'40px',
              fontFamily: "Nunito",
              fontStyle: "normal",
              fontWeight: "bold",
              fontSize: "16px",
              lineHeight: "150%",
              /* identical to box height, or 24px */
              display: "flex",
              alignItems: "flex-end",
              color: "6B7984",
            }}
          > M. LAIN-LAIN
          </Typography>
<ol>
    <li>Bank berhak untuk mengubah Syarat dan Ketentuan ini sewaktu-waktu dengan pemberitahuan kepada Pemilik Usaha dalam bentuk dan melalui sarana yang ditentukan oleh Bank.</li>
    <li>Syarat dan Ketentuan ini berikut perubahannya secara langsung mengikat outlet/toko yang didirikan Pemilik Usaha setelah disetujuinya Syarat dan Ketentuan dan formulir pendaftaran.</li>
    <li>Pemilik Usaha memberikan pernyataan, hak, ijin, persetujuan, kuasa dan wewenang penuh, dengan hak substitusi, kepada Bank untuk:
        <ol>
            <li>Mengumpulkan, menarik, memperoleh, menerima dan/atau mengambil sebagian atau seluruh data pribadi dan informasi lainnya sehubungan dengan penyelenggaraan MORIS Mandiri termasuk registrasi pembukaan akun baik secara lisan, tertulis, elektronik, melalui sistem maupun dengan cara lainnya dan dalam bentuk apapun.</li>
            <li>Memberikan persetujuan pada Bank untuk mempergunakan data pribadi tersebut termasuk namun tidak terbatas pada informasi terkait dengan catatan, keterangan atau laporan terkait dengan data transaksi dengan menggunakan aplikasi MORIS Mandiri untuk tujuan komersial yang didasarkan kerja sama Bank dengan Pihak Lain. Pemilik Usaha menyatakan telah memahami penjelasan dari Bank mengenai tujuan dan konsekuensi dari pemberian dan/atau penyebarluasan data kepada Pihak Lain untuk tujuan komersial tersebut.</li>
            <li>Menyimpan data pribadi Pemilik Usaha selama diperlakukan sesuai ketentuan hukum yang berlaku.</li>
        </ol>
    </li>
    <li>Pemilik Usaha bersedia untuk memberikan informasi dan/atau data yang diperlukan Bank dalam rangka pelaksanaan pemeriksaan oleh Regulator, Bank dan/atau pihak lain yang ditunjuk oleh Bank sewaktu-waktu dapat melakukan pemeriksanaan dengan adanya pemberitahuan terlebih dahulu kepada Pemilik Usaha sehubungan dengan pelaksanaan Layanan.  </li>
    <li>Pemilik Usaha tidak akan memberikan kepada Bank, karyawannya atau pihak yang terafiliasi dengan Bank sesuatu apapun yang dapat dikategorikan sebagai gratifikasi berdasarkan peraturan Perundang-undangan.</li>
    <li>Pemilik Usaha wajib menerapkan prinsip-prinsip Anti Pencucian Uang dan Pencegahan Pendanaan Terorisme (APU-PPT), persaingan usaha yang sehat dan anti monopoli sesuai dengan ketentuan peraturan perundang-undang yang berlaku</li>
    <li>Ketentuan ini tunduk pada hukum Indonesia dan berlaku sebagai Perjanjian bagi Pemilik Usaha dan Bank serta merupakan satu kesatuan yang tidak terpisahkan dengan formulir pendaftaran.</li>
    <li>Dalam hal salah satu ketentuan dalam Perjanjian ini dinyatakan batal berdasarkan suatu peraturan perundang-undangan, maka pernyataan batal tersebut tidak mengurangi keabsahan atau menyebabkan batalnya persyaratan atau ketentuan lain dalam Perjanjian ini dan oleh karenanya dalam hal demikian ketentuan lain dalam Perjanjian ini tetap sah dan mengikat.</li>
    <li>Perjanjian ini telah disesuaikan dengan ketentuan peraturan perundang-undangan termasuk ketentuan peraturan Otoritas Jasa Keuangan.</li>
</ol>


<div style={{width:'92.5%',bottom:'0px', position:'fixed', backgroundColor:'white',paddingLeft:'4%', paddingRight:'4%',left:'0vw'}}>
          <Typography
            variant="body1"
            gutterBottom
            style={{ 
                fontFamily: "Nunito",
                fontStyle: "normal",
                fontWeight: "normal",
                fontSize: "14px",
                lineHeight: "150%",
                /* or 21px */
                display: "flex",
                alignItems: "center",
                textAlign: "center",
                /* Grey/Grey 02 */
                color: "#3A4D5B",
                marginTop:"17px"
            }}
          >
            Dengan melanjutkan, saya telah membaca dan menyetujui syarat dan
            ketentuan di atas.
          </Typography>
          {/* <Box style={{ textAlign: 'center', position: 'fixed', display: 'contents' }}>
                        <img src={TermsA} style={{ height: 520, width: '90%' }} />
                    </Box>
                    <Box style={{ textAlign: 'center', position: 'fixed', display: 'contents' }}>
                        <img src={TermsB} style={{ height: 520, width: '90%' }} />
                    </Box> */}

          <Button onClick={lanjut}
                size="large"
                variant="contained"
                style={{
                width:'100%',
                height:'56px',
                backgroundColor: "#2661DA",
                boxShadow: "0px 4px 4px -4px #8CC3F8 0px 4px 8px 2px #C5E1FB",
                textTransform:'none',
                marginTop: "25px",
                marginBottom:'25px',
                fontFamily: "Nunito",
                fontStyle: "normal",
                fontWeight: "bold",
                fontSize: '16px',
                lineheight: '24px',
                alignItems: 'center',
                textAlign: 'center',
                color: '#FFFFFF',
                borderRadius: '8px'
                }}
              >
                Setuju
              </Button>
              </div>              
          </div>
        </Grid>
      </div>
    );
  }
}

export default termsCondition;
