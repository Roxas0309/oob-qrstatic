import React, { Component } from 'react'
import * as Yup from 'yup';
import { ErrorMessage, Field, Form, Formik } from 'formik';
import { Box, TextField } from '@material-ui/core';

class FormRegPT extends Component {
    render() {
        const { loading } = this.props
        const alpha = /^[a-zA-Z_]+( [a-zA-Z_]+)*$/;
        const numb = /^[0-9\b]+$/;

        const formTextField = (props) => {
            const {
                form: {setFieldValue, setFieldTouched},
                field: {name},
            } = props;

            const onChange = React.useCallback((event) => {
                const {value} = event.target;
                setFieldValue(name, value ? value : '');
            },[setFieldValue, name])

            const onBlur = React.useCallback((event) => {
                const {value} = event.target;
                setFieldTouched(name, value ? false : true);
            },[setFieldValue, setFieldTouched, name])

            return <TextField {...props} onChange={onChange} onBlur={onBlur} />
        }

        return (
            <Formik 
                initialValues={{
                    nama: '',
                    noTlp: '',
                    email: ''
                }}
                validationSchema={Yup.object().shape({
                    nama: Yup.string().matches(alpha, {message: "Nama tidak valid", excludeEmptyString: true})
                        .required('Harus diisi'),
                    noTlp: Yup.string().matches(numb, {message: "Nomor telepon tidak valid", excludeEmptyString: true})
                    .required('Harus diisi'),
                    email: Yup.string().email('Email tidak valid')
                })}
                onSubmit={ ({nama, noTlp, email}) => {

                    React.useEffect(() => {
                        this.timer = setInterval(() => {
                            this.setState({progress: (prog) => (prog >= 100 ? 10 : prog + 10)});
                        }, 9000);
                        return () => {
                            clearInterval(this.timer);
                        };
                    })

                }}
            >

            {({values, isValid, errors, touched, setFieldTouched, setFieldValue, isSubmitting}) => {
            <Form>
                <Box style={{margin: '10px', marginBottom: '20px'}}>
                    <Field
                        component={formTextField}
                        id="nama"
                        name={"nama"}
                        label="Nama"
                        variant="outlined"
                        margin="normal"
                        fullWidth
                        error ={(errors.nama && touched.nama)}
                        helperText={errors.nama ? '' : "Maksimal 25 Karakter"}
                        className={'form-control' + (errors.nama && touched.nama ? ' is-invalid' : '')}
                        
                    />
                    <ErrorMessage name="nama" component="div" className="invalid-feedback" />
                </Box>
                <Box style={{margin: '10px', marginBottom: '20px'}}>
                    <Field
                        component={formTextField}
                        id="noTlp"
                        name={"noTlp"}
                        label="Nomor Telepon"
                        variant="outlined"
                        margin="normal"
                        fullWidth
                        error ={(errors.noTlp && touched.noTlp)}
                        helperText={errors.noTlp ? '' : "Maksimal 25 Karakter"}
                        className={'form-control' + (errors.noTlp && touched.noTlp ? ' is-invalid' : '')}
                        
                    />
                    <ErrorMessage name="noTlp" component="div" className="invalid-feedback" />
                </Box>
                <Box style={{margin: '10px', marginBottom: '20px'}}>
                    <Field
                        component={formTextField}
                        id="email"
                        name={"email"}
                        label="Email (jika ada)"
                        variant="outlined"
                        margin="normal"
                        fullWidth
                        error ={(errors.email && touched.email)}
                        helperText={errors.email ? '' : "Maksimal 25 Karakter"}
                        className={'form-control' + (errors.email && touched.email ? ' is-invalid' : '')}
                        
                    />
                    <ErrorMessage name="email" component="div" className="invalid-feedback" />
                </Box>
            </Form>

            }}
            </Formik>
        )
    }
}

export default FormRegPT
