import React, { Component } from "react";
import Link from "@material-ui/core/Link";
import ArrowBackIcon from "@material-ui/icons/ArrowBack";
import {
  Backdrop,
  Box,
  Button,
  Card,
  CircularProgress,
  Fade,
  Grid,
  LinearProgress,
  TextField,
  Typography,
  withStyles,
} from "@material-ui/core";
import Skeleton from "@material-ui/lab/Skeleton";
import { ErrorMessage, Field, Form, Formik } from "formik";
import * as Yup from "yup";
import { compose } from "recompose";
import sukse from "../../../public/img/success.gif";
import gagal from "../../../public/img/unapproved.gif";
import { createBrowserHistory } from "history";

const useStyles = (theme) => ({
  root: {
    width: "100%",
    "& > * + *": {
      marginTop: theme.spacing(2),
    },
  },
  placeholder: {
    height: "auto",
    justifyContent: "center",
    alignItems: "center",
    display: "flex",
  },
  backdrop: {
    zIndex: theme.zIndex.drawer + 1,
    color: "#fff",
  },
  paper: {
    backgroundColor: theme.palette.background.paper,
    border: "1px solid lightgray",
    boxShadow: theme.shadows[5],
    padding: theme.spacing(2, 4, 3),
    textAlign: "center",
    borderRadius: "5px",
    alignItems: "center",
    justifyContent: "center",
    minWidth: "150px",
  },
});

const history = createBrowserHistory();

class RegPT extends Component {
  // static propTypes = {
  //     value: PropTypes.number.isRequired
  // }

  timer = null;

  constructor(props) {
    super(props);

    this.state = {
      openAlert: false,
      query: "idle",
      proses: "",
    };
  }

  render() {
    const { loading, classes } = this.props;

    const klikLanjut = () => {
      history.push("/regPt-end");
      location.reload();
    };

    const alpha = /^[a-zA-Z_]+( [a-zA-Z_]+)*$/;
    const numb = /^[0-9\b]+$/;

    const formTextField = (props) => {
      const {
        form: { setFieldValue, setFieldTouched },
        field: { name },
      } = props;

      const onChange = React.useCallback(
        (event) => {
          const { value } = event.target;
          if (name === "nama") {
            localStorage.setItem("rekPT_nama", value);
          } else if (name === "noTlp") {
            localStorage.setItem("rekPT_noTlp", value);
          } else if (name === "email") {
            localStorage.setItem("rekPT_email", value);
          } else {
          }

          setFieldValue(name, value ? value : "");
        },
        [setFieldValue, name]
      );

      const onBlur = React.useCallback(
        (event) => {
          const { value } = event.target;
          setFieldTouched(name, value ? false : true);
        },
        [setFieldValue, setFieldTouched, name]
      );

      return <TextField {...props} onChange={onChange} onBlur={onBlur} />;
    };

    return (
      <div>
        <div style={{ marginLeft: "20px", marginTop: "24px" }}>
          <Link href="/startReg">
            <ArrowBackIcon />
          </Link>
        </div>
        {/* -----------------alert----------- */}
        <Backdrop className={classes.backdrop} open={this.state.openAlert}>
          <div className={classes.placeholder}>
            {this.state.query === "success" ? (
              <Box className={classes.paper}>
                <Typography component={"span"} variant={"body2"}>
                  <span>
                    <h4 style={{ color: "black" }}>Berhasil Terkirim</h4>
                  </span>
                  <img src={sukse} style={{ height: 50, width: 50 }}></img>
                </Typography>
                <Link>
                  <Button
                    onClick={klikLanjut}
                    variant="contained"
                    style={{ width: "100%", marginTop: "20px" }}
                    color="primary"
                    disableElevation
                  >
                    OK
                  </Button>
                </Link>
              </Box>
            ) : this.state.query == "failed" ? (
              <Typography
                component={"span"}
                variant={"body2"}
                className={classes.paper}
              >
                <span>
                  <h4 style={{ color: "black" }}>
                    Gagal terkirim silahkan coba kembali
                  </h4>
                </span>
                <img src={gagal} style={{ height: 50, width: 50 }}></img>
              </Typography>
            ) : (
              <Fade
                in={this.state.query === "progress"}
                style={{
                  transitionDelay:
                    this.state.query === "progress" ? "500ms" : "0ms",
                }}
                unmountOnExit
              >
                <div className={classes.paper}>
                  <span>
                    <h4 style={{ color: "black" }}>Mengirim Data</h4>
                  </span>
                  <CircularProgress />
                </div>
              </Fade>
            )}
          </div>
        </Backdrop>
        {/* --------------------------------- */}
        <Grid style={{ padding: "10px"}}>
          {loading ? (
            <Skeleton animation="wave" variant="text"></Skeleton>
          ) : (
            <h3
              style={{
                // width: '343px',
                // height: '46px',
                // left: '0px',
                // top: '0px',
                padding: "0 80px",
                // marginLeft :'16px',
                textAlign: "center",
                marginTop: "14px",
                fontFamily: "TTInterfaces",
                fontStyle: "normal",
                fontWeight: "bold",
                fontSize: "18px",
                lineHeight: "22px",
                textAlign: "center",
                letterSpacing: "0.005em",
                /* Grey/Grey 01 */
                color: "#011A2C",
              }}
            >
              Pendaftaran untuk Usaha Berbadan Hukum
            </h3>
          )}
          <p
            style={{
              marginTop: "8px",
              // color: 'lightslategray',
              // marginBottom:10px',
              textAlign: "center",
              // marginLeft :'16px',
              textAlign: "center",
              marginTop: "14px",
              fontFamily: "TTInterfaces",
              fontStyle: "normal",
              fontWeight: "normal",
              fontSize: "16px",
              lineHeight: "24px",
              textAlign: "center",
              letterSpacing: "0.0015em",
              /* Grey/Grey 01 */
              color: "#6B7984",
            }}
          >
            Silahkan masukan nama dan kontak Anda.
          </p>
          <Box
            style={{
              textAlign: "center",
              position: "fixed",
              display: "contents",
            }}
          >
            <Formik
              initialValues={{
                nama: localStorage.getItem("rekPT_nama")
                  ? localStorage.getItem("rekPT_nama")
                  : "",
                noTlp: localStorage.getItem("rekPT_noTlp")
                  ? localStorage.getItem("rekPT_noTlp")
                  : "",
                email: localStorage.getItem("rekPT_email")
                  ? localStorage.getItem("rekPT_email")
                  : "",
              }}
              validationSchema={Yup.object().shape({
                nama: Yup.string()
                  .matches(alpha, {
                    message: "Nama tidak valid",
                    excludeEmptyString: true,
                  })
                  .required("Harus diisi"),
                noTlp: Yup.string()
                  .matches(numb, {
                    message: "Nomor telepon tidak valid",
                    excludeEmptyString: true,
                  })
                  .required("Harus diisi"),
                email: Yup.string().email("Gunakan format: contoh@email.com"),
              })}
              onSubmit={({ nama, noTlp, email }, actions) => {
                this.setState({ openAlert: true });

                clearTimeout(this.timer);
                if (this.state.query !== "idle") {
                  this.setState({ query: "idle" });
                  return;
                }

                this.setState({ query: "progress" });
                this.timer = setTimeout(() => {
                  if (nama == "Jhon Lenon") {
                    localStorage.setItem("rekPT_nama", nama);
                    localStorage.setItem("rekPT_noTlp", noTlp);
                    localStorage.setItem("rekPT_email", email);
                    this.setState({ proses: "success" });
                    this.setState({ query: this.state.proses });
                    actions.setSubmitting(false);
                  } else {
                    this.setState({ proses: "failed" });
                    this.setState({ query: this.state.proses });
                    this.timer = setTimeout(() => {
                      localStorage.removeItem("rekPT_nama");
                      localStorage.removeItem("rekPT_noTlp");
                      localStorage.removeItem("rekPT_email");
                      this.setState({ openAlert: false });
                      actions.setSubmitting(false);

                      this.setState({ query: "idle" });
                    }, 5000);
                  }
                }, 5000);
              }}
            >
              {({ values, isValid, errors, touched, isSubmitting }) => (
                <Form autoComplete="off">
                  <Box
                    style={{
                      marginTop: "32px",
                      marginInlineStart: "10px",
                      marginInlineEnd: "10px",
                      // textAlign: 'center',
                      // marginTop: '14px',
                      fontStyle: "normal",
                      fontWeight: "normal",
                      fontSize: "16px",
                      lineHeight: "24px",
                      textAlign: "center",
                      letterSpacing: "0.005em",
                      /* Grey/Grey 01 */
                    }}
                  >
                    <Field
                      style={{ backgroundColor: "#F6F7F8" }}
                      component={formTextField}
                      id="nama"
                      name={"nama"}
                      label="Nama"
                      variant="filled"
                      value={values.nama}
                      margin="normal"
                      fullWidth
                      error={errors.nama && touched.nama}
                      className={
                        "form-control" +
                        (errors.nama && touched.nama ? " is-invalid" : "")
                      }
                    />
                    <ErrorMessage
                      name="nama"
                      component="div"
                      className="invalid-feedback"
                    />
                  </Box>
                  <Box
                    style={{
                      margin: "0 2px",
                      marginInlineStart: "10px",
                      marginInlineEnd: "10px",
                    }}
                  >
                    <Field
                      style={{ backgroundColor: "#F6F7F8" }}
                      component={formTextField}
                      id="noTlp"
                      name={"noTlp"}
                      label="Nomor Telepon"
                      variant="filled"
                      value={values.noTlp}
                      margin="normal"
                      fullWidth
                      error={errors.noTlp && touched.noTlp}
                      className={
                        "form-control" +
                        (errors.noTlp && touched.noTlp ? " is-invalid" : "")
                      }
                    />
                    <ErrorMessage
                      name="noTlp"
                      component="div"
                      className="invalid-feedback"
                    />
                  </Box>
                  <Box
                    style={{
                      marginInlineStart: "10px",
                      marginInlineEnd: "10px",
                    }}
                  >
                    <Field
                      style={{ backgroundColor: "#F6F7F8" }}
                      component={formTextField}
                      id="email"
                      name={"email"}
                      label="Email (jika ada)"
                      variant="filled"
                      value={values.email}
                      margin="normal"
                      fullWidth
                      error={errors.email && touched.email}
                      className={
                        "form-control" +
                        (errors.email && touched.email ? " is-invalid" : "")
                      }
                    />
                    <ErrorMessage
                      name="email"
                      component="div"
                      className="invalid-feedback"
                    />
                  </Box>
                  <Button
                    type="submit"
                    variant="contained"
                    disableElevation
                    disabled={isSubmitting}
                    size="large"
                    style={{
                      // width: "-webkit-fill-available",
                      width:'94%',
                      marginTop: "32px",
                      paddingTop: "16px",
                      paddingBottom: "16px",
                      fontFamily: "Nunito",
                      color: "#FFFFFF",
                      fontStyle: "normal",
                      fontWeight: "bold",
                      fontSize: "16px",
                      lineHeight: "24px",
                      alignItem: "center",
                      textAlign: "center",
                      position: "relative",
                      marginBottom: "16px",
                    }}
                    color="primary"
                    variant="contained"

                  >
                    Kirim
                  </Button>
                </Form>
              )}
            </Formik>
          </Box>
          <Box
            style={{
              textAlign: "center",
              fontSize: "smaller",
              color: "lightslategray",
              marginBottom: "32px",
              fontFamily: "TTInterfaces",
              fontStyle: "normal",
              fontWeight: "500",
              fontSize: "14px",
              lineHeight: "150%",
              /* or 21px */
              textAlign: "center",
              letterSpacing: "0.0025em",
              /* Grey/Grey 04 */
              color: "#9DA7AE",
            }}
          >
            <p>
              Atau kunjungi Cabang Bank Mandiri terdekat. <br />
              Staf kami akan membantu proses pendaftaran Anda
            </p>
          </Box>
        </Grid>
            <hr style={{borderBottom: "solid 1px darkgray" , width:'90%'}}/>
        <Grid
          style={{
            padding: "20px",
            backgroundColor: "#F6F7F8",
            marginTop: "32px",
            fontFamily: "TTInterfaces",
          }}
        >
          <h3
            style={{
            //   textAlign: "center",
              marginTop: "24px",
              fontStyle: "normal",
              fontWeight: "500",
              fontSize: "16px",
              lineHeight: "150%",
              /* identical to box height, or 24px */
              letterSpacing: "0.005em",
              color: "#011A2C",
              marginBottom: "16px",
            }}
          >
            Persiapkan data berikut untuk mendaftar
          </h3>
          <Box style={{ borderBottom: "solid 1px darkgray" }}>
            <h4
              style={{
                // textAlign: "center",
                fontStyle: "normal",
                fontWeight: "500",
                fontSize: "14px",
                lineHeight: "150%",
                /* identical to box height, or 24px */
                letterSpacing: "0.005em",
                color: "#011A2C",
                marginBottom: "2px",
              }}
            >
              Rekening Bank Mandiri Pemilik Usaha
            </h4>
            <p
              style={{
                // textAlign: "center",
                fontStyle: "normal",
                fontWeight: "500",
                fontSize: "14px",
                lineHeight: "150%",
                /* identical to box height, or 24px */
                letterSpacing: "0.025em",
                color: "#6B7984",
                marginBottom: "16px",
              }}
            >
              Hasil pembayaran akan otomatis disetor ke rekening ini. Pastikan
              rekening aktif.
            </p>
          </Box>
          <Box style={{ borderBottom: "solid 1px darkgray" }}>
            <h4
              style={{
                // textAlign: "center",
                fontStyle: "normal",
                fontWeight: "500",
                fontSize: "14px",
                lineHeight: "150%",
                /* identical to box height, or 24px */
                letterSpacing: "0.005em",
                color: "#011A2C",
                marginBottom: "2px",
              }}
            >
              KTP dan NPWP Pemilik Usaha
            </h4>
            <p
              style={{
                // textAlign: "center",
                fontStyle: "normal",
                fontWeight: "500",
                fontSize: "14px",
                lineHeight: "150%",
                /* identical to box height, or 24px */
                letterSpacing: "0.025em",
                color: "#6B7984",
                marginBottom: "16px",
              }}
            >
              Pastikan detail yang tertera di KTP sama dengan yang terdaftar di
              Bank Mandiri
            </p>
          </Box>
          <Box style={{ borderBottom: "solid 1px darkgray" }}>
            <h4
              style={{
                // textAlign: "center",
                fontStyle: "normal",
                fontWeight: "500",
                fontSize: "14px",
                lineHeight: "150%",
                /* identical to box height, or 24px */
                letterSpacing: "0.005em",
                color: "#011A2C",
                marginBottom: "2px",
              }}
            >
              Data Lengkap Usaha
            </h4>
            <p
              style={{
                // textAlign: "center",
                fontStyle: "normal",
                fontWeight: "500",
                fontSize: "14px",
                lineHeight: "150%",
                /* identical to box height, or 24px */
                letterSpacing: "0.025em",
                color: "#6B7984",
                marginBottom: "16px",
              }}
            >
              Siapkan foto terbaik tempat dan produk usaha anda.
            </p>
          </Box>
          <Box style={{ borderBottom: "solid 1px darkgray" }}>
            <h4
              style={{
                // textAlign: "center",
                fontStyle: "normal",
                fontWeight: "500",
                fontSize: "14px",
                lineHeight: "150%",
                /* identical to box height, or 24px */
                letterSpacing: "0.005em",
                color: "#011A2C",
                marginBottom: "2px",
              }}
            >
              SIUP/SKU
            </h4>
            <p
              style={{
                // textAlign: "center",
                fontStyle: "normal",
                fontWeight: "500",
                fontSize: "14px",
                lineHeight: "150%",
                /* identical to box height, or 24px */
                letterSpacing: "0.025em",
                color: "#6B7984",
                marginBottom: "16px",
              }}
            >
              Pastikan dokumen diterbitkan oleh instansi berwenang dan masih
              berlaku
            </p>
          </Box>
          <Box style={{}}>
            <h4
              style={{
                // textAlign: "center",
                fontStyle: "normal",
                fontWeight: "500",
                fontSize: "14px",
                lineHeight: "150%",
                /* identical to box height, or 24px */
                letterSpacing: "0.005em",
                color: "#011A2C",
                marginBottom: "2px",
              }}
            >
              TDP
            </h4>
            <p
              style={{
                // textAlign: "center",
                fontStyle: "normal",
                fontWeight: "500",
                fontSize: "14px",
                lineHeight: "150%",
                /* identical to box height, or 24px */
                letterSpacing: "0.025em",
                color: "#6B7984",
                marginBottom: "56px",
              }}
            >
              Lampirkan Akta Pendirian Perusahaan secara lengkap dan informasi
              perubahannya jika ada
            </p>
          </Box>
        </Grid>
      </div>
    );
  }
}

export default compose(withStyles(useStyles))(RegPT);
