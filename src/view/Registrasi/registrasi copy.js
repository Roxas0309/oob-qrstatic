import React, { Component } from "react";
import Link from "@material-ui/core/Link";
import { Box, Button, Card, Dialog, Grid, Paper } from "@material-ui/core";
import Skeleton from "@material-ui/lab/Skeleton";
import GetAppIcon from "@material-ui/icons/GetApp";
import Gerobak from "../../../public/img/logoGerobak.png";
import { BACK_END_POINT, removeAllValueButAuth, wsWithoutBody, GET } from "../../master/masterComponent";
import ButtonComponent from "../../containers/button/ButtonComponent";

const contents = [
  {
    name: "Cuan Tanpa Henti Dengan Satu Aplikasi",
    description:
      "Daftar sekarang untuk terima pembayaran dari GoPay, OVO, ShopeePay, LinkAja, dan lainnya",
  },
  {
    name: "Mau tau lebih lanjut",
    description: "Jangan ragu untuk daftar ayo kapan lagi",
  },
  {
    name: "Masih ragu untuk daftar",
    description: "Silahkan hubungi kami untuk informasi lebih lanjut",
  },
];



class registrasi extends Component {
  constructor(props) {
    super(props);

    this.state = {
      openDrop: false,
      deferredPrompt: "",
      status: 0,
      appInstall: "",
      blobQr : ""
    };
  }


  linkToStatusPendaftaran = () =>{
    this.props.history.push('/cekStatusPendaftaran');
    location.reload();
  }

  componentDidMount() {

 
    wsWithoutBody(BACK_END_POINT+"/base64/getBase64DataQr?mid=0", GET, {}).then(response => {
     
     this.setState({blobQr:response.data});
     alert('blob berhasil didapat');
    }).catch(error => {
        this.getMyData(moment(new Date()).format("YYYYMMDD"), moment(new Date()).format("YYYYMMDD"));
    })



    window.addEventListener("beforeinstallprompt", (e) => {
      e.preventDefault();
      this.state.deferredPrompt = e;
      console.log("deferetPromt befor: ", this.state.deferredPrompt);
      this.setState({
        openDrop: !this.state.openDrop,
        status: this.state.status + 1,
      });
      //console.log('deferetPromt befor: ', this.state.status);
      window.addEventListener("appinstalled", (evt) => {
        console.log("App install befor: ", evt);
        this.setState({
          appInstall: evt,
        });
      });

      if (this.state.appInstall != "") {
        this.setState({
          status: 2,
        });
      }
    });

    removeAllValueButAuth();


  

    
  }

  render() {
    const { loading = false } = this.props;

    this.toogleOpen = () => {
      this.setState({
        openDrop: !this.state.openDrop,
      });
    };

    this.toogleInstall = () => {
      console.log("👍", "butInstall-clicked");
      console.log("deferetPromt clik: ", this.state.deferredPrompt);
      this.setState({
        openDrop: false,
      });

      this.state.deferredPrompt.prompt();
      this.state.deferredPrompt.userChoice.then((choiceResult) => {
        if (choiceResult.outcome === "accepted") {
          this.setState({
            openDrop: false,
          });
          console.log("User accepted the install prompt");
        } else {
          this.setState({
            openDrop: false,
            status: 3,
          });
          console.log("User dismissed the install prompt");
        }
      });

      window.addEventListener("appinstalled", (evt) => {
        //console.log('App install: ', evt);
      });
    };

    return (
      <div>
        <Dialog
          fullScreen
          style={{
            height: "100px",
            width: "100%",
            position: "absolute",
            left: "0px",
            bottom: "0px",
          }}
          open={this.state.openDrop}
          onClose={this.toogleOpen}
        >
          <Box
            style={{
              justifyContent: "center",
              alignItems: "center",
              display: "block",
              textAlign: "center",
            }}
          >
            <p
              style={{
                fontFamily: "Montserrat",
                fontSize: "20px",
                lineHeight: "30px",
                fontWeight: "700",
                margin: "0px",
                marginTop: "15px",
                marginBottom: "5px",
              }}
            >
              Install Aplikasi MORIS
            </p>
            <Button
              variant="contained"
              color="primary"
              startIcon={<GetAppIcon />}
              style={{ textTransform: "none" }}
              onClick={this.toogleInstall}
            >
              Install
            </Button>
          </Box>

          {this.state.status == 2 && <h5>Thank you</h5>}
          {this.state.status == 3 && <h5>Area you sure?</h5>}
        </Dialog>
        {/* ------------------------------------- */}

        <Grid
          container
          wrap="nowrap"
          style={{
            justifyContent: "center",
            alignItems: "center",
            display: "flex",
            marginTop: "20px",
          }}
        >
          {loading ? (
            <Skeleton animation="wave" variant="rect" height={300}></Skeleton>
          ) : (
            <Box
              style={{
                textAlign: "center",
                position: "fixed",
                display: "contents",
                // marginTop:'60.98px',
              }}
            >
              <img src={Gerobak} style={{ height: 300 }} />
            </Box>
          )}
        </Grid>
        <Grid className="cardSlid">
          {loading ? (
            <Skeleton animation="wave" variant="text"></Skeleton>
          ) : (
            <Box>
              {/* style={{ fontFamily:"Montserrat" ,fontWeight:"700"}} */}
              <h2>Cuan Tanpa Henti Dengan Satu Aplikasi</h2>
              <p>
                Daftar sekarang untuk terima pembayaran dari GoPay, OVO, ShopeePay, LinkAja, dan lainnya
              </p>
            </Box>
            // <Carousel autoPlay={false}>
            //     {contents.map((item, i) => (
            //         <Box key={i}>
            //             <h2>{item.name}</h2>
            //             <p>{item.description}</p>
            //         </Box>
            //     ))}
            // </Carousel>
          )}
          <Box style={{ marginTop: "32px" }}>
            <Link href="/startReg" >
              {/* <Button
                size="large"
                style={{
                  width: "100%",
                  height: "56px",
                  textTransform: "none",
                  backgroundColor: "#2661DA",
                  boxShadow: "0px 4px 4px -4px #8CC3F8 0px 4px 8px 2px #C5E1FB",
                  borderRadius: "8px",
                  fontFamily: "Nunito",
                  fontStyle: "normal",
                  fontWeight: "bold",
                  fontSize: "16px",
                  lineHeight: "22px",
                  display: "flex",
                  alignItems: "center",
                  textAlign: "center",
                  color: "#FFFFFF",
                  padding: "17px",
                  Height: "56px",
                }}
                variant="contained"
              >
                Daftar Sekarang
              </Button> */}
              <ButtonComponent children="Daftar Sekarang"/>
            </Link>
            <Link href="/login">
              <Button
                size="large"
                style={{
                  // width: "-webkit-fill-available",
                  width: "100%",
                  height: "56px",
                  marginTop: "16px",
                  textTransform: "none",
                  left: "0px",
                  top: "0px",
                  Height: "56px",
                  background: "#FFFFFF",
                  padding: "17px",
                  fontFamily: "Nunito",
                  fontStyle: "normal",
                  fontWeight: "bold",
                  fontSize: "16px",
                  lineHeight: "22px",
                  /* identical to box height */
                  display: "flex",
                  alignItems: "center",
                  textAlign: "center",
                  color: "#0057E7",
                  borderRadius: "8px",
                }}
                variant="outlined"
                color="primary"
              >
                Sudah Punya Akun? Login di Sini
              </Button>
            </Link>
              <Button
              onClick = {this.linkToStatusPendaftaran}
                size="large"
                style={{
                  width: "100%",
                  height: "56px",
                  marginTop: "16px",
                  textTransform: "none",
                  color: "#0057E7",
                  fontFamily: "Nunito",
                  fontSize: "16px",
                  fontWeight: "bold",
                  lineHeight: "22px",
                  display: "flex",
                  alignItems: "center",
                  textAlign: "center",
                }}
              >
                Cek Status Pendaftaran
              </Button>

 <div className="button-qr last"


                                    onClick={() => {
                                     
                                    const contentType = 'image/png';
                                    const b64Data = this.state.blobQr;
                                    const byteCharacters = atob(b64Data);
                                    const byteArrays = [];
                                    var sliceSize = 512
                                    for (let offset = 0; offset < byteCharacters.length; offset += sliceSize) {
                                      const slice = byteCharacters.slice(offset, offset + sliceSize);

                                      const byteNumbers = new Array(slice.length);
                                      for (let i = 0; i < slice.length; i++) {
                                        byteNumbers[i] = slice.charCodeAt(i);
                                      }

                                      const byteArray = new Uint8Array(byteNumbers);
                                      byteArrays.push(byteArray);
                                    }

                                    const blob = new Blob(byteArrays, {type: contentType});


                                    var file = new File([blob], "test.png", {type: contentType});
                                    var filesArray = [file];

                                      const shareData = {
                                          title: 'MDN',
                                          text: 'Testing',
                                          files: filesArray
                                        }
                                      navigator.share(shareData).then(() => alert('hai') ).catch((e) => alert(e.message) )
                         
   }}

                                    >
                                            <img className="icon-flatten" src="data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHZlcnNpb249IjEuMSIgeG1sbnM6eGxpbms9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkveGxpbmsiIHhtbG5zOnN2Z2pzPSJodHRwOi8vc3ZnanMuY29tL3N2Z2pzIiB3aWR0aD0iNTEyIiBoZWlnaHQ9IjUxMiIgeD0iMCIgeT0iMCIgdmlld0JveD0iMCAwIDUxMiA1MTIiIHN0eWxlPSJlbmFibGUtYmFja2dyb3VuZDpuZXcgMCAwIDUxMiA1MTIiIHhtbDpzcGFjZT0icHJlc2VydmUiPjxnPgo8ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciPgoJPGc+CgkJPHBhdGggZD0iTTQwNiwzMzJjLTI5LjYzNiwwLTU1Ljk2OSwxNC40MDItNzIuMzc4LDM2LjU3MWwtMTQxLjI3LTcyLjE5NUMxOTQuNzIyLDI4OC4zMjQsMTk2LDI3OS44MDksMTk2LDI3MSAgICBjMC0xMS45MzEtMi4zMzktMjMuMzI0LTYuNTc0LTMzLjc1M2wxNDguMDYtODguOTU4QzM1NC4wMDYsMTY3LjY3OSwzNzguNTksMTgwLDQwNiwxODBjNDkuNjI2LDAsOTAtNDAuMzc0LDkwLTkwICAgIGMwLTQ5LjYyNi00MC4zNzQtOTAtOTAtOTBjLTQ5LjYyNiwwLTkwLDQwLjM3NC05MCw5MGMwLDExLjQ3LDIuMTYxLDIyLjQ0Myw2LjA5LDMyLjU0bC0xNDguNDMsODkuMTggICAgQzE1Ny4xNTIsMTkyLjkwMiwxMzIuOTQxLDE4MSwxMDYsMTgxYy00OS42MjYsMC05MCw0MC4zNzQtOTAsOTBjMCw0OS42MjYsNDAuMzc0LDkwLDkwLDkwYzMwLjEyMiwwLDU2LjgzMi0xNC44NzYsNzMuMTc3LTM3LjY2NiAgICBsMTQwLjg2LDcxLjk4NUMzMTcuNDE0LDQwMy43NTMsMzE2LDQxMi43MTQsMzE2LDQyMmMwLDQ5LjYyNiw0MC4zNzQsOTAsOTAsOTBjNDkuNjI2LDAsOTAtNDAuMzc0LDkwLTkwICAgIEM0OTYsMzcyLjM3NCw0NTUuNjI2LDMzMiw0MDYsMzMyeiBNNDA2LDMwYzMzLjA4NCwwLDYwLDI2LjkxNiw2MCw2MHMtMjYuOTE2LDYwLTYwLDYwcy02MC0yNi45MTYtNjAtNjBTMzcyLjkxNiwzMCw0MDYsMzB6ICAgICBNMTA2LDMzMWMtMzMuMDg0LDAtNjAtMjYuOTE2LTYwLTYwczI2LjkxNi02MCw2MC02MHM2MCwyNi45MTYsNjAsNjBTMTM5LjA4NCwzMzEsMTA2LDMzMXogTTQwNiw0ODJjLTMzLjA4NCwwLTYwLTI2LjkxNi02MC02MCAgICBzMjYuOTE2LTYwLDYwLTYwczYwLDI2LjkxNiw2MCw2MFM0MzkuMDg0LDQ4Miw0MDYsNDgyeiIgZmlsbD0iI2ZmZmZmZiIgZGF0YS1vcmlnaW5hbD0iIzAwMDAwMCIgc3R5bGU9IiI+PC9wYXRoPgoJPC9nPgo8L2c+CjxnIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyI+CjwvZz4KPGcgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIj4KPC9nPgo8ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciPgo8L2c+CjxnIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyI+CjwvZz4KPGcgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIj4KPC9nPgo8ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciPgo8L2c+CjxnIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyI+CjwvZz4KPGcgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIj4KPC9nPgo8ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciPgo8L2c+CjxnIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyI+CjwvZz4KPGcgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIj4KPC9nPgo8ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciPgo8L2c+CjxnIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyI+CjwvZz4KPGcgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIj4KPC9nPgo8ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciPgo8L2c+CjwvZz48L3N2Zz4=" />
                                            <div className="wording">Bagikan QRIS</div>
                                    </div>


          </Box>
        </Grid>
        <Box className="footerCap">
          <p
            style={{
              color: "#303B4A, 40%",
              fontFamily: "Nunito",
              fontSize: "12px",
              fontWeight: "bold",
              lineHeight: "14px",
              display: "flex",
              alignItems: "center",
              textAlign: "center",
              marginTop: "25px",
            }}
          >
            PT. Bank Mandiri (Persero) Tbk, terdaftar dan diawasi oleh Otoritas
            Jasa Keuangan (OJK)
          </p>
        </Box>
      </div>
    );
  }
}

export default registrasi;
