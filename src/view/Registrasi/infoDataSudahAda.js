import React, { Component } from "react";
import Link from "@material-ui/core/Link";
import ArrowBackIcon from "@material-ui/icons/ArrowBack";
import { Box, Button, Card, Grid, TextField } from "@material-ui/core";
import Skeleton from "@material-ui/lab/Skeleton";
import verifOk from "../../../public/img/failed.png";

class infoDataSudahAda extends Component {
  render() {
    const { loading = false } = this.props;
    return (
      <div>
        <Grid
          container
          wrap="nowrap"
          style={{
            justifyContent: "center",
            alignItems: "center",
            display: "flex",
            marginTop: "54px",
          }}
        >
          {loading ? (
            <Skeleton animation="wave" variant="rect" height={200}></Skeleton>
          ) : (
            <Box
              style={{
                textAlign: "center",
                position: "fixed",
                display: "contents",
                marginBottom: "32px",
              }}
            >
              <img src={verifOk} style={{ height: 200 }} />
            </Box>
          )}
        </Grid>
        <Box style={{ marginInlineStart: "20px", marginInlineEnd: "20px" }}>
          <h2
            style={{
              fontFamily: "Montserrat",
              fontStyle: "normal",
              fontWeight: "bold",
              fontSize: "18px",
              lineHeight: "150%",
              /* identical to box height, or 27px */
              textAlign: "center",
              /* Black - Character / High Emphasis */
              color: "#121518",
              marginBottom: "40px",
            }}
          >
            Verifikasi data tidak berhasil
          </h2>
          <p
            style={{
                fontFamily: "Nunito",
                fontStyle: "normal",
                fontWeight: "normal",
                fontSize: "16px",
                lineHeight: "150%",
                /* identical to box height, or 27px */
                textAlign: "center",
                /* Black - Character / High Emphasis */
                color: "#121518",
                marginBottom: "30px",
            }}
          >
            Berdasarkan pengecekan, data yang dimasukkan sudah terdaftar atau masih dalam proses verifikasi.
          </p>
  
        </Box>
        <Box
          style={{
            position: "fixed",
            bottom: "20px",
            marginInlineStart: "20px",
            minWidth: "90%",
          }}
        >
          <Link href="/">
            <Button
              size="large"
              style={{
                width: "-webkit-fill-available",
                textTransform: "none",
                marginBottom: "32px",
                fontFamily: "Nunito",
                fontStyle: "normal",
                fontWeight: "bold",
                fontSize: "16px",
                lineHeight: "24px",
                paddingTop:'16px',
                paddingBottom : '16px',
                /* identical to box height, or 27px */
                textAlign: "center",
                /* Black - Character / High Emphasis */
                color: "#FFFFFF",
              }}
              variant="contained"
              color="primary"
            >
              Kembali ke Beranda
            </Button>
          </Link>
        </Box>
      </div>
    );
  }
}

export default infoDataSudahAda;
