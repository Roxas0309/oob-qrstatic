import React, { Component } from "react";
import Link from "@material-ui/core/Link";
import ArrowBackIcon from "@material-ui/icons/ArrowBack";
import { AppBar, Box, Button, Card, Grid, Toolbar } from "@material-ui/core";
import '../Registrasi/registrasiStyleV2.css'
import ImgDaftar from "../../../public/img/logoDaftar.png";

class startReg extends Component {
  // componentWillUnmount(){
  //     //location.href = "/startReg"
  //     location.reload();
  // }

  render() {
    const { loading = false } = this.props;

    const lanjut = () => {
      this.props.history.push("/termsCondition", { from: "/startReg" });
      location.reload();
    };
    return (
      <div style={{ overflow: "hidden" }}>
        <AppBar
          style={{
            backgroundColor: "transparent",
            position: "absolute",
            boxShadow: "unset",
          }}
        >
          <Toolbar style={{ minHeight: "60px" }}>
            {/* onClick={this.props.history.push('/', {from: '/startReg'})} */}
            <Link href="/" style={{ position: "absolute", display: "flex" }}>
              <ArrowBackIcon />
            </Link>
          </Toolbar>
        </AppBar>
        <Grid
          container
          style={{
            justifyContent: "center",
            alignItems: "center",
            display: "flex",
            marginTop: "70px",
          }}
        >
          {loading ? (
            <Skeleton animation="wave" variant="rect" height={200}></Skeleton>
          ) : (
            <Box
              style={{
                textAlign: "center",
                position: "fixed",
                display: "contents",
              }}
            >
              <img
                src={ImgDaftar}
                style={{
                  height: "202px",
                  width: "327px",
                  background: "#FFFFFF",
                }}
              />
            </Box>
          )}
        </Grid>
        <Grid className="butLokUs" style={{ padding: "15px" }}>
          <Box style={{}}>
            <h3
              style={{
                padding: "0 50px 0 50px",
                color: "#121518",
                fontFamily: "Montserrat",
                fontSize: "18px",
                fontWeight: "bold",
                lineHeight: "27px",
                // display: "flex",
                // alignItems: "center",
                textAlign: "center",
                // marginRight:"70px",
                // marginLeft:"70px",
                // width: "367px",
                // height: "54px",
                // margin:"50px",
                // left: "4px",
                // top: "314px",
                // position: "absolute"
              }}
            >
              Cuma perlu data berikut untuk daftar!
            </h3>
          </Box>

          <Box
            style={{
              marginTop: "40px",
              marginBottom: "70px",
            }}
          >
            <ol
              className="fa-ul"
              style={{
                fontSize: "large",
                letterSpacing: "0.2px",
                lineHeight: 1.5,
                marginRight: "5px",
                fontWeight: "600",
              }}
            >
              <li
                style={{
                  color: "#121518",
                  fontFamily: "Nunito",
                  fontSize: "16px",
                  fontWeight: "bold",
                  lineHeight: "24px",
                }}
              >
                {/* <span className="fa-li">
                  <i className="fa fa-circle" style={{ color: "#CFE0F9" }}></i>
                </span> */}
                Nomor rekening Bank Mandiri
                <p
                  style={{
                    fontSize: "16px",
                    fontFamily: "Nunito",
                    color: "#6B7984",
                    lineHeight: "24px",
                    marginTop: "2px",
fontStyle: "normal",
fontWeight: "normal",
/* identical to box height, or 24px */
/* Grey/Grey 03 */
                  }}
                >
                  Jika belum punya, buka rekening{" "}
                  <Link
                    target="_blank"
                    href="https://www.bankmandiri.co.id/faq-pembukaan-rekening-online"
                  >
                    di sini
                  </Link>
                </p>
              </li>
              <li
                style={{
                  marginTop: "24px",
                  color: "#121518",
                  fontFamily: "Nunito",
                  fontSize: "16px",
                  fontWeight: "bold",
                  lineHeight: "24px",
                }}
              >
                {/* <span className="fa-li">
                  <i className="fa fa-circle" style={{ color: "#CFE0F9" }}></i>
                </span> */}
                KTP (Wajib) dan NPWP (jika ada)
              </li>
              <li
                style={{
                  marginTop: "24px",
                  color: "#121518",
                  fontFamily: "Nunito",
                  fontSize: "16px",
                  fontWeight: "bold",
                  lineHeight: "24px",
                }}
              >
                {/* <span className="fa-li">
                  <i className="fa fa-circle" style={{ color: "#CFE0F9" }}></i>
                </span> */}
                Informasi dan Foto Usaha
              </li>
            </ol>
          </Box>
          <Button
            onClick={lanjut}
            size="large"
            className="startReg-button"
            variant="contained"
            color="primary"
            // disableElevation
          >
            Mulai Pendaftaran
          </Button>
          {/* <Button onClick={lanjut} size="large" disableElevation
                        style={{width: "91%", marginTop: '10px', marginInline: '0px',
                        textTransform: 'none', marginBottom: '10px', fontFamily: 'Nunito', 
                            fontSize: '16px', fontWeight: '700'}}
                            variant="contained" color="primary">
                            Mulai Pendaftaran
                      </Button> */}

          {/* <Box style={{ borderBottom: 'solid 1px darkgray' }}>
                        <h4 style={{ marginBottom: '0px' }}>Nomor rekening Bank Mandiri</h4>
                        <p style={{ marginTop: '5px', color: 'lightslategray', marginBottom: '10px' }}>
                            Jika belum punya, buka rekening <Link>di sini</Link>
                    </p>
                    </Box>
                    <Box style={{ borderBottom: 'solid 1px darkgray' }}>
                        <h4 style={{ marginBottom: '0px' }}>KTP dan NPWP Pemilik Usaha</h4>
                        <p style={{ marginTop: '5px', color: 'lightslategray', marginBottom: '10px' }}>
                            Pastikan detail yang tertera di KTP sama dengan yang terdaftar di Bank Mandiri
                        </p>
                    </Box>
                    <Box style={{ borderBottom: 'solid 1px darkgray' }}>
                        <h4 style={{ marginBottom: '0px' }}>Data Lengkap Usaha</h4>
                        <p style={{ marginTop: '5px', color: 'lightslategray', marginBottom: '10px' }}>
                            Siapkan foto terbaik tempat dan produk usaha anda.</p>
                    </Box>
                    <Box style={{ backgroundColor: '#c7eaff', padding: '10px', borderRadius: '6px', marginTop: '15px' }}>
                        <p style={{ margin: '0px' }}>Jika Anda memiliki usaha berbadan hukum, lihat informasi pendaftaran <Link href="/regPt">di sini</Link></p>
                    </Box> */}
          {/* <Box style={{ 
                        backgroundColor: '#FFF8E5', 
                        padding: '12px 16px 12px 16px', 
                        borderRadius: '6px', 
                        marginTop: '79px', 
                        // marginInlineStart: '10px', 
                        // marginInlineEnd: '10px' 
                        }}>
                        <p style={{ 
                            // margin: '0px 10px',
                            color: "#3A4D5B",
                            fontFamily: "TT Interphases",
                            fontSize: "16px",
                            fontWeight: "500",
                            lineHeight: "21px",
                            // padding : "12px 16px 12px 16px",
                            letterSpacing: "0.0025em",
                            Width : "311px",
                            Height : "63px"
                            }}>
                            Jika Anda memiliki usaha berbentuk PT, CV, yayasan, atau koperasi, silahkan lihat<Link href="/regPt"> info lebih lanjut.</Link>
                        </p>
                    </Box>
                    <br /> */}
          {/* <Link href="/termsCondition">
                        
                    </Link> */}
        </Grid>
      </div>
    );
  }
}

export default startReg;
