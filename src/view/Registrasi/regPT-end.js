import React, { Component } from "react";
import Link from "@material-ui/core/Link";
import ArrowBackIcon from "@material-ui/icons/ArrowBack";
import { Box, Button, Card, Grid } from "@material-ui/core";

import ImgAgent from "../../../public/img/logoAgent.png";

class regPTend extends Component {
  componentDidMount() {
  }

  render() {
    const { loading = false } = this.props;
    return (
      <div>
        {/* <div style={{ marginLeft: '5px', marginTop: '7px' }}>
                    <Link href="/startReg">
                        <ArrowBackIcon />
                    </Link>
                </div> */}
        <Grid
          container
          wrap="nowrap"
          style={{
            justifyContent: "center",
            alignItems: "center",
            display: "flex",
          }}
        >
          {loading ? (
            <Skeleton animation="wave" variant="rect" height={300}></Skeleton>
          ) : (
            <Box
              style={{
                textAlign: "center",
                position: "fixed",
                display: "contents",
              }}
            >
              <img src={ImgAgent} style={{ 
                  height: "202px", 
                  width: "327px",
                    marginTop:'54px',
                    marginLeft:'24px',
                    marginRight:'24px'    
            }} />
            </Box>
          )}
        </Grid>
        <Grid style={{ padding: "10px", textAlign: "center" }}>
          <h3 style={{ 
              textAlign: "center", 
              marginTop:"28px", 
              fontFamily: 'TT Interphases',
              fontStyle: 'normal',
              fontWeight: 'bold',
              fontSize: '18px',
              lineHeight: '22.5px',
              /* identical to box height */
              textAlign: 'center',
              letterSpacing: '0.005em',
              /* Grey/Grey 01 */
              color: '#011A2C'       
              /* Inside Auto Layout */
        }}>Terima Kasih</h3>
          <h4
          style={{
              marginTop:"8px",
              textAlign: "center",
              fontFamily: 'TT Interphases',
              fontStyle: 'normal',
              fontWeight: 'bold',
              fontSize: '16px',
              lineHeight: '24px',
              /* identical to box height */
              textAlign: 'center',
              letterSpacing: '0.0015em',
              /* Grey/Grey 01 */
              color: '#6B7984'       
              /* Inside Auto Layout */
          }}
          >Agen kami akan segera menghubungi Anda</h4>
        </Grid>
        <Box
          style={{
            position: "fixed",
            bottom: "20px",
            marginInlineStart: "20px",
            minWidth: "90%",
          }}
        >
          <Link href="/startReg">
            <Button
              size="large"
              style={{
                width: "100%",
                textTransform: "none",
                fontFamily: 'Nunito',
                fontStyle: 'normal',
                fontWeight: 'bold',
                fontSize: '16px',
                lineHeight: '24px',
                /* identical to box height, or 150% */
                display: 'flex',
                alignItems: 'center',
                textAlign: 'center',
                color: '#FFFFFF',
                padding: "16px 0 16px 0",
                marginTop: "380px 16px 32px 16px"
              }}
              variant="contained"
              color="primary"
            >
              Kembali ke Beranda
            </Button>
          </Link>
        </Box>
      </div>
    );
  }
}

export default regPTend;
