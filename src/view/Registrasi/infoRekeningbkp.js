import React, { Component } from 'react'
import Link from '@material-ui/core/Link';
import ArrowBackIcon from '@material-ui/icons/ArrowBack';
import { Backdrop, Box, Button, Card, CircularProgress, Collapse, Fade, Grid, IconButton, Snackbar, TextField, TextFieldProps, Typography, withStyles } from '@material-ui/core';
import { ErrorMessage, Field, Form, Formik } from 'formik';
import * as Yup from 'yup';
import MomentUtils from '@date-io/moment';
import moment from 'moment';
import { parse, isDate } from "date-fns";

import { MuiPickersUtilsProvider, DatePicker } from "@material-ui/pickers";
import { createBrowserHistory } from 'history';
import MuiAlert from '@material-ui/lab/Alert';
import { compose } from 'recompose';
import sukse from '../../../public/img/success.gif';
import gagal from '../../../public/img/unapproved.gif';


const useStyles = theme => ({
    root: {
      width: '100%',
      '& > * + *': {
        marginTop: theme.spacing(2),
      },
    },
    placeholder: {
        height: 'auto',
        justifyContent: 'center',
        alignItems: 'center',
        display: 'flex'
    },
    backdrop: {
        zIndex: theme.zIndex.drawer + 1,
        color: '#fff',
      },
    paper: {
    backgroundColor: theme.palette.background.paper,
    border: '1px solid lightgray',
    boxShadow: theme.shadows[5],
    padding: theme.spacing(2, 4, 3),
    textAlign: 'center',
    borderRadius: '5px',
    alignItems: 'center',
    justifyContent: 'center',
    minWidth: '150px'

    },
  });

const history = createBrowserHistory();
class infoRekening extends Component {

    timer = null

    constructor(props){
        super(props);

        this.state = {
            loading: false,
            openAlert: false,
            query: 'idle',
            proses: ''
        }
    }

    
    render() {
        const { isValid, classes } = this.props;
        const loading = () => <div className="animated fadeIn pt-3 text-center">Loading...</div>;
        const sleep = (ms) => new Promise((r) => setTimeout(r, ms));
        const alpha = /^[a-zA-Z_]+( [a-zA-Z_]+)*$/;
        const numb = /^[0-9\b]+$/;
        moment.locale('id');

        const formTextField = (props) => {
            const {
                form: {setFieldValue, setFieldTouched},
                field: {name},
            } = props;

            const onChange = React.useCallback((event) => {
                const {value} = event.target;
                setFieldValue(name, value ? value : '');
            },[setFieldValue, name])

            const onBlur = React.useCallback((event) => {
                const {value} = event.target;
                setFieldTouched(name, value ? false : true);
            },[setFieldValue, setFieldTouched, name])

            return <TextField {...props} onChange={onChange} onBlur={onBlur} />
        }

        const klikLanjut = () => {
            history.push('/kodeOTP')
            location.reload();
        }

        const closeSnackBar = () => {
            this.setState({openAlert: false})
        }

        const Alert = (props) => {
            return <MuiAlert elevation={6} variant="filled" {...props} />;
          }

        
        return (
            <div>
                <div style={{ marginLeft: '5px', marginTop: '7px' }}>
                    <Link href="/termsCondition">
                        <ArrowBackIcon />
                    </Link>
                </div>
                {/* -----------------alert----------- */}
                <Backdrop className={classes.backdrop} open={this.state.openAlert} >
                    <div className={classes.placeholder}>

                    {
                        this.state.query === 'success' ? 
                                (<Box className={classes.paper}>
                                    <Typography component={'span'} variant={'body2'} >
                                        <span><h4 style={{color: 'black'}}>Verifikasi Berhasil</h4></span>
                                        <img src={sukse}
                                        style={{ height: 50, width: 50 }}></img>
                                    </Typography>
                                    <Link>
                                        <Button onClick={klikLanjut} variant="contained" 
                                            style={{width: '100%', marginTop: '20px'}} color="primary" 
                                            disableElevation>
                                            Lanjut
                                        </Button>
                                    </Link>
                                </Box>
                                
                                ) :

                        this.state.query == 'failed' ? 
                             (<Typography component={'span'} variant={'body2'} className={classes.paper}>
                             <span><h4 style={{color: 'black'}}>Verifikasi Tidak Berhasil</h4></span>
                             <img src={gagal}
                             style={{ height: 50, width: 50 }}></img>
                         </Typography>) :
                         (<Fade in={this.state.query === 'progress'}
                            style={{transitionDelay: this.state.query === 'progress' ? '500ms' : '0ms'}}
                            unmountOnExit
                            >
                                <div className={classes.paper}>
                                    <span><h4 style={{color: 'black'}}>
                                        Melakukan Verifikasi</h4></span>
                                    <CircularProgress />
                                </div>
                            </Fade>)
                        }

                    </div>
                </Backdrop>
                {/* --------------------------------- */}
                <Grid style={{ padding: '10px', justifyContent: 'center', alignItems: 'center', textAlign: 'center' }}>
                    <h4 style={{ textAlign: 'center', marginTop: '0px' }}>Informasi Rekening</h4>
                    <p style={{ marginTop: '5px', color: 'lightslategray', marginBottom: '20px' }}>
                        Pastikan data rekening sama dengan KTP.</p>

                    <Box style={{ textAlign: 'center', position: 'fixed', display: 'contents' }}>
                        
                    <Formik
                    initialValues={{
                        ownerName : '',
                        tglLahir: moment(new Date()).format("DD MMMM YYYY"),
                        noKtp: '',
                        ibuKandung: '',
                        noRekMandiri: ''
                    }}
                    validationSchema={Yup.object().shape({
                        ownerName: Yup.string().matches(alpha, {message: "Nama tidak valid", excludeEmptyString: true})
                                           .max(25, 'Maksimal hanya bisa 25 karakter')
                                           .required('Harus diisi'),
                        tglLahir: Yup.string().required('Harus diisi'),//Yup.date().transform(parseTglLahir).max(new Date()),
                        noKtp: Yup.string().max(16, 'Nomor KTP harus 16 digit')
                        .matches(numb, {message: "Nomor KTP tidak valid", excludeEmptyString: true})
                                           .required('Harus diisi'),
                        ibuKandung: Yup.string().max(16, 'Nomor KTP harus 16 digit')
                        .matches(alpha, {message: "Nama Ibu kandung tidak valid", excludeEmptyString: true})
                        .required('Harus diisi'),
                        noRekMandiri: Yup.string().max(13, 'Nomor rekening harus 16 digit')
                        .matches(numb, {message: "Nomor Rekening tidak valid", excludeEmptyString: true})
                                                  .required('Harus diisi')
                    })}
                    onSubmit={({ ownerName, tglLahir, noKtp, ibuKandung, noRekMandiri }) => {

                        this.setState({openAlert: true})
                
                            clearTimeout(this.timer);
                            if(this.state.query !== 'idle'){
                                this.setState({query: 'idle'});
                                return;
                            }
                    
                            this.setState({query: 'progress'});
                            this.timer = setTimeout(() => {
                
                                if(ownerName == 'Jhon Lenon'){
                                    localStorage.setItem('rekInfo_ownerName', ownerName)
                                    localStorage.setItem('rekInfo_tglLahir', tglLahir)
                                    localStorage.setItem('rekInfo_noKtp', noKtp)
                                    localStorage.setItem('rekInfo_ibuKandung', ibuKandung)
                                    localStorage.setItem('rekInfo_noRekMandiri', noRekMandiri)
                                    this.setState({proses: 'success'})
                                    this.setState({query: this.state.proses});
                
                                }else{
                                    this.setState({proses: 'failed'})
                                    this.setState({query: this.state.proses});
                                    this.timer = setTimeout(() => {
                                        this.setState({query: 'idle'})
                                        history.push('/infoRekeningGagal')
                                        location.reload()
                                        this.setState({openAlert: false})
                                    }, 5000)
                                }
                            }, 5000);
                        ////////////////////////

                        // console.log('value alert start: ', this.state.openAlert);
                        //   this.setState({loading: true})
                          
                        // await sleep(5000).then((val) => {
                            
                        //     localStorage.setItem('rekInfo_ownerName', ownerName)
                        //     localStorage.setItem('rekInfo_tglLahir', tglLahir)
                        //     localStorage.setItem('rekInfo_noKtp', noKtp)
                        //     localStorage.setItem('rekInfo_ibuKandung', ibuKandung)
                        //     localStorage.setItem('rekInfo_noRekMandiri', noRekMandiri)
                        //     console.log('values ownerName: ', ownerName);
                        //     console.log('values tglLahir: ', tglLahir);
                        //     console.log('values noKtp: ', noKtp);
                        //     console.log('values ibuKandung: ', ibuKandung);
                        //     console.log('values noRekMandiri: ', noRekMandiri);
                        //     this.setState({loading: false});
                        //     this.setState({openAlert: true})
                        //     console.log('value alert end: ', this.state.openAlert);
                            
                            
                        // })
                         
                    }}

                    >

                    {({ values, isValid, errors, touched, setFieldTouched, setFieldValue, isSubmitting }) => (
                    <MuiPickersUtilsProvider utils={MomentUtils}>
                    <Form>
                        <Box style={{margin: '10px', marginBottom: '20px'}}>
                            <Field
                                component={formTextField}
                                id="ownerName"
                                name={"ownerName"}
                                label="Nama Pemilik Usaha"
                                variant="outlined"
                                fullWidth
                                maxLength="25"
                                error ={(errors.ownerName && touched.ownerName)}
                                helperText={errors.ownerName ? '' : "Maksimal 25 Karakter"}
                                className={'form-control' + (errors.ownerName && touched.ownerName ? ' is-invalid' : '')}
                                
                            />
                            <ErrorMessage name="ownerName" component="div" className="invalid-feedback" />
                        </Box>
                        <Box style={{margin: '10px', marginBottom: '20px'}}>
                            <Field 
                                component={DatePicker}
                                name={"tglLahir"}
                                label="Tanggal Lahir"
                                onChange={(newVal) => setFieldValue("tglLahir", moment(newVal).format("YYYY/MM/DD"))}
                                onBlur={() => setFieldTouched("tglLahir", true)}
                                value={moment(values.tglLahir).format("YYYY/MM/DD")}
                                fullWidth
                                //TextFieldComponent={renderInput}
                                inputVariant="outlined"
                                views={["year", "month", "date"]}
                                openTo="year"
                                format="DD MMMM YYYY"
                               // helperText="Format: DDMMYYYY (contoh: 17081980)"
                                error ={(errors.tglLahir && touched.tglLahir)}
                                className={'form-control' + (errors.tglLahir && touched.tglLahir ? ' is-invalid' : '')}
                                
                            />
                            <ErrorMessage name="tglLahir" component="div" className="invalid-feedback" />
                        </Box>
                        <Box style={{margin: '10px', marginBottom: '20px'}}>
                            <Field
                                component={formTextField}
                                id="noKtp"
                                name={"noKtp"}
                                label="Nomor KTP"
                                variant="outlined"
                                fullWidth
                                error ={(errors.noKtp && touched.noKtp)}
                                className={'form-control' + (errors.noKtp && touched.noKtp ? ' is-invalid' : '')}
                                
                            />
                            <ErrorMessage name="noKtp" component="div" className="invalid-feedback" />
                        </Box>
                        <Box style={{margin: '10px', marginBottom: '20px'}}>
                            <Field
                                component={formTextField}
                                id="ibuKandung"
                                name={"ibuKandung"}
                                label="Nama Gadis Ibu Kandung"
                                variant="outlined"
                                fullWidth
                                error ={(errors.ibuKandung && touched.ibuKandung)}
                                
                            />
                            <ErrorMessage name="ibuKandung" component="div" className="invalid-feedback" />
                        </Box>
                        <Box style={{margin: '10px', marginBottom: '20px'}}>
                            <Field
                                component={formTextField}
                                id="noRekMandiri"
                                name={"noRekMandiri"}
                                label="Nomor Rekening Bank Mandiri"
                                variant="outlined"
                                fullWidth
                                error ={(errors.noRekMandiri && touched.noRekMandiri)}
                                
                            />
                            <ErrorMessage name="noRekMandiri" component="div" className="invalid-feedback" />
                        </Box>    

                        <Button variant="contained" disableElevation disabled={isSubmitting} type="submit" size="large" style={{width: "-webkit-fill-available", marginTop: '10px', 
                                textTransform: 'none', marginBottom: '10px'}} variant="contained" color="primary">
                                Lanjutkan
                        </Button>
                        
                             {/* {!this.state.openAlert ? <Button disabled={isSubmitting} type="submit" size="large" style={{width: "-webkit-fill-available", marginTop: '10px', 
                                textTransform: 'none', marginBottom: '10px'}} variant="contained" color="primary">
                                Lanjutkan
                            </Button> :
                            <div className={classes.root}>
                                <Snackbar open={this.state.openAlert} onClose={closeSnackBar}>
                                    <Alert severity="success"
                                        action={<IconButton aria-label="close" size="small" 
                                        onClick={()=> klikLanjut} >
                                            Lanjut<ChevronRightIcon fontSize="inherit" />
                                        </IconButton>}
                                    >
                                        Data berhasil disimpan
                                    </Alert>
                                </Snackbar>
                            </div>
                            }
                            {this.state.loading && <CircularProgress size={20} 
                                style={{color: '#3f51b5', position: 'absolute', 
                                    width: '30px', height: '30px', bottom: '10%', right: '50%' }} />} */}

                            

                            {/* {this.state.openAlert && <Collapse in={this.state.openAlert}>
                                <Alert
                                    action={<IconButton aria-label="close" size="small" 
                                    onClick={()=> klikLanjut} >
                                        <ChevronRightIcon fontSize="inherit" />
                                    </IconButton>}
                                >
                                  Data berhasil disimpan      
                                </Alert>
                            </Collapse>} */}

                    </Form>
                    </MuiPickersUtilsProvider>
                )}
            </Formik>
                    </Box>
                </Grid>

                

            </div>
        )
    }
}

export default compose(
    withStyles(useStyles)
)(infoRekening)
