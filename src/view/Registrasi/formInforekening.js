import React from 'react';
import TextField from '@material-ui/core/TextField';
import { Form, Formik } from 'formik';
import * as Yup from 'yup';
import moment from 'moment';
import { parse, isDate } from "date-fns";




class formInfoRekening extends ValidatorComponent {



    render() {
        moment.locale('id');
        const parseTglLahir = (value, originalValue) => {
            const parseDate = isDate(originalValue)
                ? originalValue
                : parse(originalValue, "ddMMyyyy", new Date());
            return parseDate;
        }

        return (
            <Formik
                initialValues={{
                    ownerName : '',
                    tglLahir: moment(new Date()).format("DDMMYYYY"),
                    noKtp: '',
                    ibuKandung: '',
                    noRekMandiri: ''
                }}
                validationSchema={Yup.object().shape({
                    ownerName: Yup.string().max(25, 'Maksimal 25 karakter')
                                           .required('Tidak boleh kosong'),
                    tglLahir: Yup.date().transform(parseTglLahir).max(new Date()),
                    noKtp: Yup.string().max(16, 'Nomor KTP harus 16 digit'),
                    noRekMandiri: Yup.string().max(13, 'Nomor rening harus 16 digit')
                })}
                onSubmit={values => {
                }}
            >

                {({ values, errors, touched, setFieldTouched, setFieldValue, handleChange, handleBlur }) => (
                    <Form>
                        <TextField
                                variant="filled"
                                margin="normal"
                                required
                                fullWidth
                                id="ownerName"
                                label="Nama Pemilik Usaha"
                                name="ownerName"
                                autoComplete="ownerName"
                                autoFocus
                                value={values.ownerName}
                                helperText={(errors.ownerName && touched.ownerName) && errors.ownerName}
                            />
                        <TextField
                                variant="filled"
                                margin="normal"
                                required
                                fullWidth
                                id="tglLahir"
                                type="date"
                                label="Tanggal Lahir"
                                name="tglLahir"
                                autoComplete="tglLahir"
                                autoFocus
                                value={values.tglLahir}
                                helperText={(errors.tglLahir && touched.tglLahir) && errors.tglLahir}
                            />
                        <TextField
                                variant="filled"
                                margin="normal"
                                required
                                fullWidth
                                id="noKtp"
                                label="Nomor KTP"
                                inputProps={{ inputMode: 'numeric', pattern: '[0-9]*' }}
                                name="noKtp"
                                autoComplete="noKtp"
                                autoFocus
                                value={values.noKtp}
                                helperText={(errors.noKtp && touched.noKtp) && errors.noKtp}
                            />
                        <TextField
                                variant="filled"
                                margin="normal"
                                required
                                fullWidth
                                id="ibuKandung"
                                label="Nama Gadis Ibu Kandung"
                                name="ibuKandung"
                                autoComplete="ibuKandung"
                                autoFocus
                                value={values.ibuKandung}
                            />
                        <TextField
                                variant="filled"
                                margin="normal"
                                required
                                fullWidth
                                id="noRekMandiri"
                                label="Nomor Rekening Bank Mandiri"
                                inputProps={{ inputMode: 'numeric', pattern: '[0-9]*' }}
                                name="noRekMandiri"
                                autoComplete="noRekMandiri"
                                autoFocus
                                value={values.noRekMandiri}
                                helperText={(errors.noRekMandiri && touched.noRekMandiri) && errors.noRekMandiri}
                            />
                    </Form>
                )}
                
            </Formik>
        )
    }

}

export default formInfoRekening