import { Box, Button, Grid, Link } from "@material-ui/core";
import Skeleton from "@material-ui/lab/Skeleton";
import { createBrowserHistory } from "history";
import React, { Component } from "react";
import imgSukses from "../../../public/img/LogoReviewSukses.png";

const history = createBrowserHistory();

class responReview extends Component {

  componentDidMount(){

    history.push(null, null, null);
    history.go(1);

  }

  componentWillUnmount(){
    window.onpopstate = null;
    location.reload()
  }

  render() {
    const { loading = false } = this.props;
    return (
      <div
        style={{
          fontFamily: "Calibri",
          fontSize: "large",
          color: "lightslategray",
        }}
      >
        <br />
        <Grid
          container
          wrap="nowrap"
          style={{
            justifyContent: "center",
            alignItems: "center",
            display: "flex",
          }}
        >
          {loading ? (
            <Skeleton animation="wave" variant="rect" height={202}></Skeleton>
          ) : (
            <Box
              style={{
                textAlign: "center",
                position: "fixed",
                display: "contents",
              }}
            >
              <img
                src={imgSukses}
                style={{ height: "70vw", width: "100vw", 
                // marginTop: "53px" 
              }}
              />
            </Box>
          )}
        </Grid>
        <Box
          style={{
            marginInline: "43px",
            textAlign: "center",
          }}
        >
          <h3
            style={{
              fontFamily: "Montserrat",
              fontSize: "18px",
              textAlign: "center",
              marginTop: "36px",
              fontWeight: "700",
              lineHeight: "27px",
            }}
          >
            Pendaftaran Anda Sedang Diproses
          </h3>
          <p
            style={{
              fontSize: "17px",
              textAlign: "center",
              fontWeight: "400",
              lineHeight: "24px",
              marginTop: '15px',
              marginBottom: '5px'
            }}
          >
            Proses pendaftaran :
          </p>
          <p
            style={{
              fontSize: "17px",
              textAlign: "center",
              marginTop: "0px",
              fontWeight: "400",
              lineHeight: "24px",
            }}
          >
            <strong>Maksimum 3 hari kerja.</strong>
          </p>
          <p
            style={{
              fontSize: "17px",
              textAlign: "center",
              marginTop: "20px",
              fontWeight: "400",
              lineHeight: "24px",
              paddingLeft:"5vw",
              paddingRight:"5vw"
            }}
          >
            Cek status pendaftaran dengan klik tombol
            <strong> 'Cek Status Pendaftaran' </strong>
            di halaman login aplikasi ini.
          </p>
          <p
            style={{
              fontSize: "17px",
              paddingRight:"5vw",
              paddingLeft:"5vw",
              textAlign: "center",
              marginTop: "20px",
              fontWeight: "400",
              lineHeight: "24px",
            }}
          >
            Nomor pendaftaran Anda akan dikirim melalui email dan SMS.
          </p>
        </Box>
        <Box style={{ margin: "20px", marginTop: '100px' }}>
          <Link href="/cekStatusPendaftaran">
            <Button
              size="large"
              style={{
                width: "100%",
                height: "56px",
                marginTop: "10px",
                textTransform: "none",
                marginBottom: "60px",
                // display: "flex",
                fontFamily: "Nunito",
                fontStyle: "normal",
                fontWeight: "bold",
                fontSize: "16px",
                lineHeight: "24px",
                /* identical to box height, or 150% */
                alignItems: "center",
                textAlign: "center",
                borderRadius: "8px",
              }}
              variant="contained"
              color="primary"
            >
              Cek Status Pendaftaran
            </Button>
          </Link>
        </Box>
      </div>
    );
  }
}

export default responReview;
