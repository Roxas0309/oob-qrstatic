
import React, { Component } from 'react'
import { AppBar, Backdrop, Box, Button, CircularProgress, Grid, Link, TextField, Toolbar, Typography, withStyles } from '@material-ui/core'
import ArrowBackIcon from '@material-ui/icons/ArrowBack';
import Skeleton from '@material-ui/lab/Skeleton';
import EditIcon from '@material-ui/icons/Edit';

import sukses from '../../../public/img/success.gif';
import gagal from '../../../public/img/unapproved.gif';
import progressGif from '../../../public/img/loading.gif';
import edit from '../../../public/img/iconEdit.png';
import { compose } from 'recompose';
import { BACK_END_POINT, HEADER_AUTH, POST, wsWithBody } from '../../master/masterComponent';
import '../Registrasi/registrasiStyleV2.css'


const useStyles = theme => ({
    root: {
        width: '100%',
        '& > * + *': {
            marginTop: theme.spacing(2),
        },
    },
    placeholder: {
        height: 'auto',
        justifyContent: 'center',
        alignItems: 'center',
        display: 'flex'
    },
    backdrop: {
        zIndex: theme.zIndex.drawer + 1,
        color: '#fff',
    },
    paper: {
        backgroundColor: theme.palette.background.paper,
        border: '1px solid lightgray',
        boxShadow: theme.shadows[5],
        padding: theme.spacing(2, 4, 3),
        textAlign: 'center',
        borderRadius: '5px',
        alignItems: 'center',
        justifyContent: 'center',
        minWidth: '150px'

    },
});


class reviewData extends Component {

    

    constructor(props){
        super(props);
        this.timer = null
        this.state = {
            imgKtp: null,
            imgKtpWajah: null,
            imgNpwp: null,
            tmptUsaha: null,
            brngJasa: null,
            owner: null,
            openAlert : false,
            query : 'progress'
        }
    }

    componentDidMount(){
        this.setState({imgKtp: localStorage.getItem('imgKtp')})
        this.setState({imgKtpWajah: localStorage.getItem('imgKtpWajah')})
        this.setState({imgNpwp: localStorage.getItem('imgNpwp')})
        this.setState({tmptUsaha: localStorage.getItem('tmptUsaha')})
        this.setState({brngJasa: localStorage.getItem('brngJasa')})
        this.setState({owner: localStorage.getItem('fotoOwner')})
    }

    componentWillUnmount(){
        location.reload()
    }

    closeErrorResp = () =>{
        this.setState({openAlert : false,query : 'progress'});
    }

    klikLanjut = () =>{
        window.localStorage.clear();
        this.props.history.push('/responReviewData', {from: '/'});
        location.reload();
    }

    saveMyData = () =>{
        this.setState({openAlert : true})
        var jenisLokasiUsahaVar = '',
        idxKategori = parseInt(localStorage.getItem('kategorUsahaIdx'),10);
        if(idxKategori===0){
            jenisLokasiUsahaVar = localStorage.getItem('lokasiUsahaPermanenKey')
        }
        else{
            jenisLokasiUsahaVar = localStorage.getItem('valLokasiUsahaNonPermanenKey')
        }
        var bodySend = {
            alamatLokasiUsahaSaatIni : localStorage.getItem('alamatLokasiUsaha'),
            alamatPemilikUsaha : localStorage.getItem('address'),
            base64FotoBarangAtauJasa : localStorage.getItem('brngJasa'),
            base64FotoPemilikTempatUsaha : localStorage.getItem('fotoOwner'),
            base64FotoTempatUsaha : localStorage.getItem('tmptUsaha'),
            base64Ktp : localStorage.getItem('imgKtp'),
            base64Npwp : localStorage.getItem('imgNpwp'),
            base64WajahKtp:localStorage.getItem('imgKtpWajah'),
            dob: localStorage.getItem('rekInfo_tglLahirOnDDMMYYYFormat'),
            emailPemilikUsaha : localStorage.getItem('owner_email'),
            idKelurahan : localStorage.getItem('localKelId'),
            jenisLokasiUsaha : jenisLokasiUsahaVar,
            jenisUsaha : localStorage.getItem('jenisUsahaIdName'),
            kategoriLokasiUsaha : idxKategori,
            namaIbuKandung : localStorage.getItem('rekInfo_ibuKandung'),
            namaPemilikUsaha : localStorage.getItem('rekInfo_ownerName'),
            namaPemilikUsahaValidasi : localStorage.getItem('rekInfo_ownerNameValidity'),
            namaUsaha : localStorage.getItem('usahaInfo_namaUsaha'),
            noNpwp : localStorage.getItem('npwp'),
            nomorKtp : localStorage.getItem('rekInfo_noKtp'),
            nomorRekening : localStorage.getItem('rekInfo_noRekMandiri'),
            nomorTelp : localStorage.getItem('usahaInfo_noTlp'),
            omzet : localStorage.getItem('omsetIdName')
        };
         wsWithBody(BACK_END_POINT + "/UserOob/save",POST,bodySend,HEADER_AUTH).then(
             response =>{

                wsWithBody(BACK_END_POINT + '/send-notification/post-my-notif-token-with-idoob', POST, {
                    'tokenPage': localStorage.getItem('token-info'),
                    'idOob' : response.data.id
                  }, { ...HEADER_AUTH}).then(response => {
                    this.setState({ proses: 'success' })
                    this.setState({ query: this.state.proses });  
                    this.timer = setTimeout(() => {
                        this.props.history.push('/responReviewData', {from: '/'});
                        location.reload();
                    }, 3000)
                  }).catch(error=>{
                    this.setState({ proses: 'success' })
                    this.setState({ query: this.state.proses });  
                    this.timer = setTimeout(() => {
                        window.localStorage.clear();
                        this.props.history.push('/responReviewData', {from: '/'});
                        location.reload();
                    }, 3000)
                  }) 
               
             }
         ).catch(error=>{
            console.log('errorr respon:', error);
            if (error.message === "Network Error") {
                alert('Oops jaringan internet terputus silahkan coba kembali')
                this.setState({
                    openAlert: false,
                    proses: '',
                    query: 'idle'
                })
            } else {
                this.setState({ proses: 'failed' });
                this.setState({ query: this.state.proses });
                this.setState({ messageError: error.response.data.message })
              
                // localStorage.removeItem('rekInfo_ibuKandung')
                // localStorage.removeItem('rekInfo_ownerName')
                // localStorage.removeItem('rekInfo_noKtp')
                // localStorage.removeItem('rekInfo_noRekMandiri')
            }
         })
        // this.props.history.push("/responReviewData");
        // location.reload();
    }

    render() {
        const {  isValid, classes,loading = false } = this.props
        return (
            <div>
                {/* -----------------alert----------- */}
                <Backdrop className={classes.backdrop} open={this.state.openAlert} >
                    <div className={classes.placeholder}>

                        {

                            this.state.query === 'progress' ?

                                (<Box className={classes.paper}>
                                    <Typography component={'span'} variant={'body2'} >
                                        <span><h4 style={{ color: 'black' }}>Data Sedang Disimpan</h4></span>
                                        <CircularProgress />
                                    </Typography>
                                </Box>

                                )
                                :
                                this.state.query === 'success' ?
                                    (<Box className={classes.paper}>
                                        <Typography component={'span'} variant={'body2'} >
                                            <span><h4 style={{ color: 'black' }}>Verifikasi Berhasil</h4></span>
                                            <img src={sukses}
                                                style={{ height: 50, width: 50 }}></img>
                                        </Typography>
                                        {/* <Link>
                                            <Button onClick={this.klikLanjut} variant="contained"
                                                style={{ width: '100%', marginTop: '20px' }} color="primary"
                                                disableElevation>
                                                Kembali
                                        </Button>
                                        </Link> */}
                                    </Box>

                                    ) :

                                    (
                                        <Typography component={'span'} variant={'body2'} className={classes.paper}>
                                            <span><h4 style={{ color: 'black' }}>Verifikasi Tidak Berhasil </h4>
                                            </span>
                                            <span><p style={{ color: 'black', width: 220 }}> {this.state.messageError} </p>
                                            </span>
                                            <img src={gagal}
                                                style={{ height: 50, width: 50 }}></img>
                                            <Link>
                                                <Button variant="contained" onClick={this.closeErrorResp}
                                                    style={{ width: '100%', marginTop: '20px' }} color="primary"
                                                    disableElevation>
                                                    Tutup
                                                 </Button>
                                            </Link>
                                        </Typography>
                                    )
                        }

                    </div>
                </Backdrop>
                {/* --------------------------------- */}
             
                <AppBar style={{backgroundColor: 'transparent', position: 'absolute', 
                        boxShadow: 'unset'}}>
                    <Toolbar style={{minHeight: '60px'}}>
                        <Link style={{display: 'flex'}} onClick={() => this.props.history.push('/informasiLokasiUsaha', {from: '/reviewData'})}>
                            <ArrowBackIcon/>
                        </Link>
                    </Toolbar>
                </AppBar>

                <h3 style={{fontFamily: 'Montserrat', fontSize: '18px', textAlign: 'center', marginTop: '70px',
                        fontWeight: '700', lineHeight: '27px'}}>Periksa Data Anda</h3>
{/* ----------------------------- INFORMASI PEMILIK USAHA ----------------------------------------------*/}
                <Box style={{marginInline: '16px', marginTop: '47px'}}>
                    <Grid container spacing={2}>
                        <Grid item xs={9}>
                            <span style={{fontWeight: '700', fontSize: '16px', color: '#1A1A1A', paddingLeft:'16px'}}>
                                Informasi Pemilik Usaha</span>      
                        </Grid>
                        <Grid item xs={2} style={{textAlign: 'end'}}>
                            <Link onClick={() => this.props.history.push('/inputDataOwner?_onCorrection=yes', {from: '/reviewData'})} 
                            // href="/inputDataOwner?_onCorrection=yes" 
                                >
                                <img src={edit} width={20} height={20}/>
                                {/* <EditIcon style={{fontSize: "1rem"}} /> */}
                            </Link>
                        </Grid>
                    </Grid>
                </Box>
                

                <Box className="boxRev" style={{marginInline: '16px', marginTop: '20px', paddingLeft:'4.467vw',paddingRight:'7.467vw', borderBottom: 'solid 0.5px lightgray'}}>
                    <form noValidate>
                        <TextField style={{width: '100%', marginBottom: '25px', backgroundColor:'white'}}
                            fullWidth
                            name="email"
                            label="Email"
                            variant="filled"
                            InputProps={{
                                readOnly: true,
                            }}
                            value = {localStorage.getItem('owner_email')}
                           className="none-background"
                        />
                    </form>
                </Box>
{/* ----------------------------- INFORMASI KTP DAN NPWP ----------------------------------------------*/}
                <Box style={{marginInline: '16px', marginTop: '23px', borderBottom: 'solid 0.5px lightgray'}}>
                    <Grid container spacing={2}>
                        <Grid item xs={9}>
                            <span style={{lineHeight: '24px', fontWeight: '700', fontSize: '16px', color: '#1A1A1A', paddingLeft:'16px'}}>
                            Informasi KTP dan NPWP</span>       
                        </Grid>
                        <Grid item xs={2} style={{textAlign: 'end'}}>
                            <Link onClick={() => this.props.history.push('/uploadKartuOwner?_onCorrection=yes', {from: '/reviewData'})} 
                            
                            // href="/uploadKartuOwner?_onCorrection=yes" 
                            >
                                <img src={edit} width={20} height={20}/>
                            </Link>
                        </Grid>
                    </Grid>
               
                    <Grid container spacing={1} style={{marginTop: '29px'}}>
                        <span style={{marginLeft: '30px', fontFamily: 'TTInterfaces',
                                lineHeight:'12.5px', fontWeight: '500', fontSize: '10px', 
                                letterSpacing: '5.5%', color: '#6B7984'}}>Foto KTP</span>
                    </Grid>
                    <Grid container style={{justifyContent: 'end', alignItems: 'center', 
                            display: 'flex', padding: '10px', paddingLeft:'7.467vw'}}>
                        {loading ? (<Skeleton animation="wave" variant="rect" height={80} width={112}></Skeleton>) : 
                        (
                            <Box style={{justifyContent: 'center', textAlign: 'center', alignItems: 'center', 
                                    height: '80px', backgroundColor: '#e2f2ff', 
                                    width: '112px', borderRadius: '4px', display: 'flex'}}>
                                <img src={this.state.imgKtp} style={{height: '80px', width: '112px',
                                    boxShadow: '0 2px 0 rgba(90,97,105,.11), 0 4px 8px rgba(90,97,105,.12), 0 10px 10px rgba(90,97,105,.06), 0 7px 70px rgba(90,97,105,.1)',
                                    borderRadius: '4px'}}/>
                            </Box>
                        )}
                    </Grid>

                    <Grid container spacing={1} style={{marginTop: '13px'}}>
                        <span style={{marginLeft: '30px', fontFamily: 'TTInterfaces', 
                                lineHeight:'12.5px', fontWeight: '500', fontSize: '10px', 
                                letterSpacing: '5.5%', color: '#6B7984'}}>Foto Wajah dengan KTP</span>
                    </Grid>
                    <Grid container style={{justifyContent: 'end', alignItems: 'center', 
                            display: 'flex', padding: '10px', paddingLeft:'7.467vw'}}>
                        {loading ? (<Skeleton animation="wave" variant="rect" height={80} width={112}></Skeleton>) : 
                        (
                            <Box style={{justifyContent: 'center', textAlign: 'center', alignItems: 'center', 
                                    height: '80px', backgroundColor: '#C4C4C4', 
                                    width: '112px', borderRadius: '4px', display: 'flex'}}>
                                <img src={this.state.imgKtpWajah} style={{height: '80px', width: '61.34px',
                                    boxShadow: '0 2px 0 rgba(90,97,105,.11), 0 4px 8px rgba(90,97,105,.12), 0 10px 10px rgba(90,97,105,.06), 0 7px 70px rgba(90,97,105,.1)'}}/>
                            </Box>
                        )}
                    </Grid>
                    <Grid container spacing={1} style={{marginTop: '13px'}}>
                        <span style={{marginLeft: '30px', fontFamily: 'TTInterfaces', 
                                lineHeight:'12.5px', fontWeight: '500', fontSize: '10px', 
                                letterSpacing: '5.5%', color: '#6B7984'}}>Foto NPWP</span>
                    </Grid>
                    <Grid container style={{justifyContent: 'end', alignItems: 'center', 
                            display: 'flex', padding: '10px', paddingLeft:'7.467vw'}}>
                        {loading ? (<Skeleton animation="wave" variant="rect" height={80} width={112}></Skeleton>) : 
                        (
                            <Box style={{justifyContent: 'center', textAlign: 'center', alignItems: 'center', 
                                    height: '80px', backgroundColor: '#e2f2ff', 
                                    width: '112px', borderRadius: '4px', display: 'flex'}}>
                                <img src={this.state.imgNpwp} style={{height: '80px', width: '112px',
                                    boxShadow: '0 2px 0 rgba(90,97,105,.11), 0 4px 8px rgba(90,97,105,.12), 0 10px 10px rgba(90,97,105,.06), 0 7px 70px rgba(90,97,105,.1)',
                                    borderRadius: '4px'}}/>
                            </Box>
                        )}
                    </Grid>

                    <Box className="boxRevNpwp" style={{marginTop: '10px', paddingLeft:'7.467vw',paddingRight:'7.467vw'}}>
                        <form noValidate>
                            <TextField style={{width: '100%', marginBottom: '20px'}}
                                fullWidth
                                name="npwp"
                                label="NPWP"
                                variant="filled"
                                InputProps={{
                                    readOnly: true,
                                }}
                                value = {localStorage.getItem('npwp')}
                                
                            />
                        </form>
                    </Box>

                </Box>
{/* ----------------------------- INFORMASI USAHA ----------------------------------------------*/}

                <Box style={{marginInline: '16px', borderBottom: 'solid 0.5px lightgray', marginTop: '30px'}}>
                    
                    <Grid container spacing={2}>
                        <Grid item xs={9}>
                            <span style={{lineHeight: '24px', fontWeight: '700', fontSize: '16px', color: '#1A1A1A', paddingLeft:'16px'}}>
                            Informasi Usaha</span>       
                        </Grid>
                        <Grid item xs={2} style={{textAlign: 'end'}}>
                            <Link onClick={() => this.props.history.push('/uploadInformasiUsaha?_onCorrection=yes', {from: '/reviewData'})} 
                            
                            // href="/uploadKartuOwner?_onCorrection=yes" 
                            >
                                <img src={edit} width={20} height={20}/>
                            </Link>
                        </Grid>
                    </Grid>
                    <Box className="boxRev" style={{marginTop: '10px', padding: '10px', paddingLeft: '0px', paddingLeft:'4.467vw',paddingRight:'7.467vw'}}>
                        <form noValidate>
                            <TextField style={{width: '100%', marginBottom: '10px'}}
                                fullWidth
                                name="nama"
                                label="Nama Usaha"
                                InputProps={{
                                    readOnly: true,
                                }}
                                value = {localStorage.getItem('usahaInfo_namaUsaha')}
                                variant="filled"
                            />
                            <TextField style={{width: '100%', marginBottom: '10px'}}
                                fullWidth
                                name="noTlp"
                                label="Nomor Telepon (jika ada)"
                                InputProps={{
                                    readOnly: true,
                                }}
                                value = {localStorage.getItem('usahaInfo_noTlp')}
                                variant="filled"
                            />
                            <TextField style={{width: '100%', marginBottom: '10px'}}
                            fullWidth
                                name="jenis"
                                label="Jenis Usaha"
                                variant="filled"
                                InputProps={{
                                    readOnly: true,
                                }}
                                value = {localStorage.getItem('usahaInfo_jenisUsaha')}
                            />
                            <TextField style={{width: '100%', marginBottom: '10px'}}
                            fullWidth
                                name="omset"
                                label="Rata-rata Omzet per Bulan"
                                variant="filled"
                                InputProps={{
                                    readOnly: true,
                                }}
                                value = {localStorage.getItem('usahaInfo_omset')}
                            />
                        </form>
                    </Box>

                    <Grid container spacing={1}>
                        <span style={{marginLeft: '30px', fontFamily: 'TTInterfaces',
                                lineHeight:'12.5px', fontWeight: '500', fontSize: '10px', 
                                letterSpacing: '5.5%', color: '#6B7984'}}>Foto Tempat Usaha</span>
                    </Grid>
                    <Grid container style={{justifyContent: 'end', alignItems: 'center', 
                            display: 'flex', padding: '10px', paddingLeft:'7.467vw'}}>
                        {loading ? (<Skeleton animation="wave" variant="rect" height={80} width={112}></Skeleton>) : 
                        (
                            <Box style={{justifyContent: 'center', textAlign: 'center', alignItems: 'center', 
                                    height: '80px', backgroundColor: '#e2f2ff', 
                                    width: '112px', borderRadius: '4px', display: 'flex'}}>
                                <img src={this.state.tmptUsaha} style={{height: '80px', width: '112px',
                                    boxShadow: '0 2px 0 rgba(90,97,105,.11), 0 4px 8px rgba(90,97,105,.12), 0 10px 10px rgba(90,97,105,.06), 0 7px 70px rgba(90,97,105,.1)',
                                    borderRadius: '4px'}}/>
                            </Box>
                        )}
                    </Grid>

                    <Grid container spacing={1} style={{marginTop: '13px'}}>
                        <span style={{marginLeft: '30px', fontFamily: 'TTInterfaces', 
                                lineHeight:'12.5px', fontWeight: '500', fontSize: '10px', 
                                letterSpacing: '5.5%', color: '#6B7984'}}>Foto Barang atau Jasa</span>
                    </Grid>
                    <Grid container style={{justifyContent: 'end', alignItems: 'center', 
                            display: 'flex', padding: '10px', paddingLeft:'7.467vw'}}>
                        {loading ? (<Skeleton animation="wave" variant="rect" height={80} width={112}></Skeleton>) : 
                        (
                            <Box style={{justifyContent: 'center', textAlign: 'center', alignItems: 'center', 
                                    height: '80px', backgroundColor: '#e2f2ff', 
                                    width: '112px', borderRadius: '4px', display: 'flex'}}>
                                <img src={this.state.brngJasa} style={{height: '80px', width: '112px',
                                    boxShadow: '0 2px 0 rgba(90,97,105,.11), 0 4px 8px rgba(90,97,105,.12), 0 10px 10px rgba(90,97,105,.06), 0 7px 70px rgba(90,97,105,.1)',
                                    borderRadius: '4px'}}/>
                            </Box>
                        )}
                    </Grid>

                    <Grid container spacing={1} style={{marginTop: '13px'}}>
                        <span style={{marginLeft: '30px', fontFamily: 'TTInterfaces',
                                lineHeight:'12.5px', fontWeight: '500', fontSize: '10px', 
                                letterSpacing: '5.5%', color: '#6B7984'}}>Foto Pemilik di Tempat Usaha</span>
                    </Grid>
                    <Grid container style={{justifyContent: 'end', alignItems: 'center', 
                            display: 'flex', padding: '10px', marginBottom: '25px' ,paddingLeft:'7.467vw'}}>
                        {loading ? (<Skeleton animation="wave" variant="rect" height={80} width={112}></Skeleton>) : 
                        (
                            <Box style={{justifyContent: 'center', textAlign: 'center', alignItems: 'center', 
                                    height: '80px', backgroundColor: '#e2f2ff',
                                    width: '112px', borderRadius: '4px', display: 'flex'}}>
                                <img src={this.state.owner} style={{height: '80px', width: '112px',
                                    boxShadow: '0 2px 0 rgba(90,97,105,.11), 0 4px 8px rgba(90,97,105,.12), 0 10px 10px rgba(90,97,105,.06), 0 7px 70px rgba(90,97,105,.1)',
                                    borderRadius: '4px'}}/>
                            </Box>
                        )}
                    </Grid>

                </Box>
{/* ---------------------------Informasi Lokasi Usaha------------------------------------------- */}

                
                <Box style={{marginInline: '16px', borderBottom: 'solid 0.5px lightgray', marginTop: '30px'}}>
                    
                    <Grid container spacing={2}>
                        <Grid item xs={9}>
                            <span style={{lineHeight: '24px', fontWeight: '700', fontSize: '16px', color: '#1A1A1A', paddingLeft:'16px'}}>
                            Informasi Lokasi Usaha</span>       
                        </Grid>
                        <Grid item xs={2} style={{textAlign: 'end'}}>
                            <Link onClick={() => this.props.history.push('/informasiLokasiUsaha?_onCorrection=yes', {from: '/reviewData'})} 
                            
                            // href="/uploadKartuOwner?_onCorrection=yes" 
                            >
                                <img src={edit} width={20} height={20}/>
                            </Link>
                        </Grid>
                    </Grid>
                    <Box className="boxRev" style={{marginTop: '10px', padding: '10px', paddingLeft: '0px',paddingLeft:'4.467vw',paddingRight:'7.467vw'}}>
                        <form noValidate>
                            { localStorage.getItem('kategorUsahaIdx') === "0"? 
                            <span>
                            <TextField style={{width: '100%', marginBottom: '10px'}}
                            fullWidth
                                name="kategori"
                                InputProps={{
                                    readOnly: true,
                                }}
                                label="Kategori"
                                variant="filled"
                                value={'Permanen'}
                            />
                            <TextField style={{width: '100%', marginBottom: '10px'}}
                            fullWidth
                                name="lokasi"
                                InputProps={{
                                    readOnly: true,
                                }}
                                label="Lokasi"
                                variant="filled"
                                value={localStorage.getItem('lokasiUsahaPermanen')}
                            />
                            <TextField className="custom-textfield-prof" style={{width: '100%', marginBottom: '10px', backgroundColor:'rgba(0,0,0,0)!important'}}
                            fullWidth
                                rows={3}
                                multiline
                                name="alamat"
                                InputProps={{
                                    readOnly: true,
                                }}
                                label="Alamat"
                                variant="filled"
                                value={localStorage.getItem('alamatLokasiUsaha')}
                            />
                            <TextField className="areaProv custom-textfield-prof" style={{width: '100%', marginBottom: '20px'}}
                            fullWidth
                            rows={4}
                            multiline
                            InputProps={{
                                readOnly: true,
                            }}
                                name="daerah"
                                label="Provinsi, Kota, Kecamatan, Kelurahan, Kode Pos"
                                variant="filled"
                                value={localStorage.getItem('localProv')+","+localStorage.getItem('localKot')+","
                                +localStorage.getItem('localKec')+"," +localStorage.getItem('localKel')+","}
                            /> 
                            </span>: <span>
                            <TextField style={{width: '100%', marginBottom: '10px'}}
                            fullWidth
                                name="kategori"
                                label="Kategori"
                                InputProps={{
                                    readOnly: true,
                                }}
                                variant="filled"
                                value={'Non Permanen'}
                            />
                            <TextField style={{width: '100%', marginBottom: '10px'}}
                            fullWidth
                                name="lokasi"
                                label="Lokasi"
                                variant="filled"
                                InputProps={{
                                    readOnly: true,
                                }}
                                value={localStorage.getItem('valLokasiUsahaNonPermanen')}
                            />
                            </span>
                            }
                        </form>
                    </Box>
                    
                </Box>
                

                <Box style={{position: 'relative', bottom: '20px', 
                            marginInlineStart: '20px', minWidth: '90%', marginInlineEnd: '20px',
                            marginTop: '50px'}}>
                        <Button size="large" 
                                onClick = {this.saveMyData}
                                style={{width: "100%", marginTop: '10px', 
                                textTransform: 'none', marginBottom: '10px', height: '56px'}} variant="contained" color="primary">
                            Kirim
                        </Button>
                </Box>

            </div>
        )
    }
}

export default compose(
    withStyles(useStyles)
)(reviewData)