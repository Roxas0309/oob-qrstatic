import React, { Component } from 'react';
import { connect } from 'react-redux';
import compose from 'recompose/compose';
import Avatar from '@material-ui/core/Avatar';
import Button from '@material-ui/core/Button';
import CssBaseline from '@material-ui/core/CssBaseline';
import TextField from '@material-ui/core/TextField';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Checkbox from '@material-ui/core/Checkbox';
import Link from '@material-ui/core/Link';
import Grid from '@material-ui/core/Grid';
import Box from '@material-ui/core/Box';
import LockOutlinedIcon from '@material-ui/icons/LockOutlined';
import Typography from '@material-ui/core/Typography';
import { withStyles } from '@material-ui/core/styles';
import Container from '@material-ui/core/Container';
//import { Link } from 'react-router-dom';
import Gerobak from '../../../public/img/logoGerobak.png'
import { testActions } from '../../actions/test.actions';
import { t } from '../../helpers/translate';
import { BACK_END_POINT, HEADER_AUTH, POST, SESSION_ITEM, TOKEN_AUTH, wsCallingWithBody, wsWithBody } from '../../master/masterComponent';
import { Alert } from '@material-ui/lab';
import '../Login/login.css';
import { relativeTimeRounding } from 'moment';
import { Backdrop, CircularProgress } from '@material-ui/core';
import LoadingMaker from '../../helpers/loadingMaker/LoadingMaker';
const useStyles = theme => ({

  paper: {
    marginTop: theme.spacing(8),
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center'
  },
  containerPaper: {
    width: '90%'
  },
  avatar: {
    margin: theme.spacing(1),
    backgroundColor: theme.palette.secondary.main,
  },
  form: {
    width: '100%', // Fix IE 11 issue.
    marginTop: theme.spacing(1),
  },
  submit: {
    margin: theme.spacing(3, 0, 2),
  },
  containerTextFieldError: {
    fontSize: "4.072vw",
    position: 'relative',
    width: '100%',
    height: '12.670vw',
    marginBottom: '4.357vw',
    borderRadius: '1.357vw',
    backgroundColor: '#F7F8F9',
    animation: 'shake 0.5s',
    animationIterationCount: '1'
  },
  textField: {
    boxSizing: 'border-box',
    paddingLeft: '16px',
    border: 'none',
    height: '100%',
    width: '100%',
    background: 'none',
    textDecoration:'none',
    height: '100%',
    fontWeight: 'bold',
    fontStyle: 'normal',
    fontFamily: 'Nunito, sans-serif',
    textTransform: 'none',
    fontWeight: 500,
    position: 'relative',
    zIndex: 1,
    paddingTop: '16px',
    fontSize: "4.072vw"
  },
  containerTextField: {
    fontSize: "4.072vw",
    position: 'relative',
    width: '100%',
    height: '12.670vw',
    marginBottom: '16px',
    borderRadius: '6px',
    backgroundColor: '#F7F8F9'
  },
  backdrop: {
    zIndex: theme.zIndex.drawer + 1,
    color: '#fff',
  }
});

class Login extends Component {
  constructor(props) {
    super(props);
    this.state = {
      userPhone: '',
      userPassword: '',
      showErrorLogin: false,
      errorMessage: '',
      loginErrorStatus: 0,
      isOnLoading: false,
       errorNoHp: false,
       typeInput : 'password'
    }
  }

  validateCheckNoHp = (words) => {
    var x = words.split("");
    var result = false;

    var i = 0;


    x.map((xs) => {
      var result0 = (i == 0 && xs != 0);
      var result1 = (i == 0 && xs != '+');
      if (result0 && result1) {
        result = true;
      }
      else {
        if (isNaN(xs) && i !== 0) {
          result = true;
        }
      }
      i++;
    });


    if (x[0] == 0) {
      if (words.length >= 2) {
        if (x[1] != 8) {
          result = true;
        }
      }
    }
    else if (x[0] == '+') {
      if (words.length >= 2) {
        if (x[1] != 6) {
          result = true;
        }
      }
      if (words.length >= 3) {
        if (x[1] != 6) {
          result = true;
        }
        if (x[2] != 2) {
          result = true;
        }
      }
      if (words.length >= 4) {
        if (x[1] != 6) {
          result = true;
        }
        if (x[2] != 2) {
          result = true;
        }
        if (x[3] != 8) {
          result = true;
        }
      }
    }

    return result;
  }

 

  componentDidMount() {

//    history.pushState(null, null, location.href);
    window.onpopstate = function (event) {
      event.preventDefault()
      history.go(0);
      window.location.reload();
    };
    const { dispatch } = this.props;
    dispatch(testActions.test());
  }

  login = () => {
    this.setState({ isOnLoading: true });
    wsWithBody(BACK_END_POINT + "/loginCtl/auth", POST, {
      noPhone: this.state.userPhone,
      password: this.state.userPassword,
      sessionLocal:localStorage.getItem(SESSION_ITEM),
      tokenLocal: localStorage.getItem(TOKEN_AUTH)
    }, HEADER_AUTH).then(response => {
      var token = response.data.result.token;
      var isFirstLogin = response.data.result.isFirstLogin;
      localStorage.setItem(TOKEN_AUTH, token);
      this.setState({ isOnLoading: false });
      wsWithBody(BACK_END_POINT + '/send-notification/post-my-notif-token-with-auth', POST, {
        'tokenPage': localStorage.getItem('token-info')
      }, { ...HEADER_AUTH, 'secret-token': token }).then(response => {
        if (isFirstLogin) {
          this.props.history.push('/makePassword');
        } else {
          this.props.history.push('/homeScreen');
        }
       location.reload();
      }).catch(error=>{
        if (isFirstLogin) {
          this.props.history.push('/makePassword');
        } else {
          this.props.history.push('/homeScreen');
        }
        location.reload();
      })
    }).catch(error => {
      this.setState({ isOnLoading: false });
      if (error.response.data.status === 403) {
        localStorage.removeItem(TOKEN_AUTH);
      }
      this.setState({
        showErrorLogin: true,
        loginErrorStatus: error.response.data.status,
        errorMessage: error.response.data.message
      });
    })
  }

  // Copyright() {
  //   return (
  //     <Typography variant="body2" color="textSecondary" align="center">
  //       {'Copyright © '}
  //       <Link color="inherit" href="https://material-ui.com/">
  //         {t('Your Website')}
  //       </Link>{' '}
  //       {new Date().getFullYear()}
  //       {'.'}
  //     </Typography>
  //   );
  // }

  render() {
    const { classes } = this.props;
    return (
      <div className={classes.paper}>


              

        <div className={classes.containerPaper}>
          <Grid container wrap="nowrap"
            style={{
              justifyContent: 'center', alignItems: 'center', display: 'flex',
              width: '100%'
            }}>
            <Box style={{
              textAlign: 'center', position: 'fixed', display: 'contents',
              width: '100%'
            }}>
              <img src={Gerobak} style={{ width: '80%', marginTop: '-50px', marginBottom: '40px' }} />
            </Box>
          </Grid>

          <LoadingMaker onLoading={this.state.isOnLoading}
            loadingWord={'Proses login sedang berlangsung'}
          ></LoadingMaker>



          {
            this.state.showErrorLogin && (this.state.loginErrorStatus === 401 
              || this.state.loginErrorStatus === 404) || this.state.loginErrorStatus === 500?
              <div className="error-label">
                {
                this.state.loginErrorStatus === 401 ?
                  <h5>Login Tidak Berhasil</h5>
                    :
                    this.state.loginErrorStatus === 500 ?
                    <h5>Ups, terdapat gangguan sistem</h5>  : 
                    <h5>Ups, data tidak sesuai</h5> 
                }
                {this.state.loginErrorStatus === 500?
                <p>Proses login belum berhasil. Silahkan coba beberapa saat lagi. [{this.state.errorMessage}]</p>
                :
                <p>{this.state.errorMessage}</p>
                }
                <img src="data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHZlcnNpb249IjEuMSIgeG1sbnM6eGxpbms9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkveGxpbmsiIHhtbG5zOnN2Z2pzPSJodHRwOi8vc3ZnanMuY29tL3N2Z2pzIiB3aWR0aD0iNTEyIiBoZWlnaHQ9IjUxMiIgeD0iMCIgeT0iMCIgdmlld0JveD0iMCAwIDgxLjk5OCA4MS45OTgiIHN0eWxlPSJlbmFibGUtYmFja2dyb3VuZDpuZXcgMCAwIDUxMiA1MTIiIHhtbDpzcGFjZT0icHJlc2VydmUiIGNsYXNzPSIiPjxnPgo8ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciPgoJPHBhdGggZD0iTTQwLjk5OSwwQzE4LjM5MiwwLDAsMTguMzkyLDAsNDAuOTk5YzAsMjIuNjA2LDE4LjM5Miw0MC45OTksNDAuOTk5LDQwLjk5OWMyMi42MDYsMCw0MC45OTktMTguMzkzLDQwLjk5OS00MC45OTkgICBDODEuOTk4LDE4LjM5Miw2My42MDcsMCw0MC45OTksMHogTTQwLjk5OSw3Ny45OThDMjAuNTk4LDc3Ljk5OCw0LDYxLjQsNCw0MC45OTlDNCwyMC41OTgsMjAuNTk4LDQsNDAuOTk5LDQgICBzMzYuOTk5LDE2LjU5OCwzNi45OTksMzYuOTk5Qzc3Ljk5OCw2MS40LDYxLjQsNzcuOTk4LDQwLjk5OSw3Ny45OTh6IE00NS43MDgsMTIuNzQ0YzAuMzc2LDAuMzg3LDAuNTgxLDAuOTA4LDAuNTY3LDEuNDQ3ICAgbC0xLjMxOSw0MS4zNzJjLTAuMDI4LDEuMDg0LTAuOTE1LDEuOTQ5LTIsMS45NDloLTMuOTE1Yy0xLjA4NCwwLTEuOTcyLTAuODY1LTItMS45NDlsLTEuMzE5LTQxLjM3MiAgIGMtMC4wMTQtMC41MzksMC4xOTEtMS4wNjEsMC41NjctMS40NDdjMC4zNzYtMC4zODYsMC44OTMtMC42MDQsMS40MzItMC42MDRoNi41NTVDNDQuODE0LDEyLjE0MSw0NS4zMzIsMTIuMzU4LDQ1LjcwOCwxMi43NDR6ICAgIE00Ni4xOTEsNjMuMDAydjcuNDljMCwxLjEwNC0wLjg5NiwyLTIsMmgtNi4zODVjLTEuMTA0LDAtMi0wLjg5Ni0yLTJ2LTcuNDljMC0xLjEwNCwwLjg5Ni0yLDItMmg2LjM4NSAgIEM0NS4yOTYsNjEuMDAyLDQ2LjE5MSw2MS44OTYsNDYuMTkxLDYzLjAwMnoiIGZpbGw9IiNmMTA1MDUiIGRhdGEtb3JpZ2luYWw9IiMwMDAwMDAiIHN0eWxlPSIiIGNsYXNzPSIiPjwvcGF0aD4KPC9nPgo8ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciPgo8L2c+CjxnIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyI+CjwvZz4KPGcgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIj4KPC9nPgo8ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciPgo8L2c+CjxnIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyI+CjwvZz4KPGcgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIj4KPC9nPgo8ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciPgo8L2c+CjxnIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyI+CjwvZz4KPGcgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIj4KPC9nPgo8ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciPgo8L2c+CjxnIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyI+CjwvZz4KPGcgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIj4KPC9nPgo8ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciPgo8L2c+CjxnIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyI+CjwvZz4KPGcgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIj4KPC9nPgo8L2c+PC9zdmc+" />

              </div> : 
               this.state.showErrorLogin?
                  this.state.loginErrorStatus === 423?
               <    div className="overlay-bottom-login">
                    <div className="wording-partition"> 
                    <div className="wording-header">Akun tidak aktif</div>
                    <div className="wording-footer">Akun tidak digunakan selama 90 hari. Untuk keamanan Anda, silakan ulangi pendaftaran.</div>
                    <div className="wording-action-button" onClick={()=>{
                      this.props.history.push('/startReg');
                      location.reload();
                    }}>Daftar</div>
                    </div>
                  <div className="overLay-background-deflect">
                  </div>
                  </div> 
                :
                this.state.loginErrorStatus === 403?
                <div className="overlay-bottom-login">
                <div className="wording-partition"> 
                <div className="wording-header">Sesi telah berakhir</div>
                <div className="wording-footer">Untuk keamanan, Anda baru saja log out otomatis dari aplikasi MORIS. Login kembali untuk melanjutkan aktivitas.</div>
                <div className="wording-action-button" onClick={()=>{
                  location.reload();
                }}>Oke</div>
                </div>
              <div className="overLay-background-deflect-no-opacity">
              </div>
              </div> 
                :
                <div></div>
               
               :
               <div>

               </div>
          }



          <form className={classes.form} noValidate autoComplete="new-password">
            <div className={this.state.errorNoHp ? classes.containerTextFieldError : classes.containerTextField} >
              <input autoCorrect="off" autoCapitalize="off" spellCheck="false"
              autoComplete="new-password"
              onPaste={
                (e)=>{
                  e.preventDefault()
                  return false;
                }
              }
            
                className={classes.textField}
                value={this.state.userPhone}
                onChange={(event) => 
              
                  {
                    event.preventDefault();
                    if (this.validateCheckNoHp(event.target.value)) {
                      if (!this.state.errorNoHp) {
                        if ("vibrate" in navigator) {
                          window.navigator.vibrate(1000);
                        
                        }
                        
                      }
                      this.setState({ errorNoHp: true, allowClick:false })
                    }
                    else if (event.target.value.length > 13) {
                      if ("vibrate" in navigator) {
                        navigator.vibrate(1000);
                      }
                      this.setState({ errorNoHp: true, allowClick:false  })
                    }
                    else {
                      this.setState({ errorNoHp: false, allowClick:true })
                      this.setState({ userPhone: event.target.value })
                    }
    
                    if (!this.state.errorNoHp) {
                      this.setState({ userPhone: event.target.value })
                    }
                  }
                  }
              >
              </input>
              <label id="label-textBoardLogin" className={this.state.userPhone==='' ? '' : 
            this.state.errorNoHp ? 'validInput error' : 'validInput'}>
            Nomor Handphone</label>
                {this.state.errorNoHp ?
              // <i className="fa fa-exclamation-circle icon-error" aria-hidden="true"></i> 
              <img className="icon-error" src="data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHZlcnNpb249IjEuMSIgeG1sbnM6eGxpbms9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkveGxpbmsiIHhtbG5zOnN2Z2pzPSJodHRwOi8vc3ZnanMuY29tL3N2Z2pzIiB3aWR0aD0iNTEyIiBoZWlnaHQ9IjUxMiIgeD0iMCIgeT0iMCIgdmlld0JveD0iMCAwIDgxLjk5OCA4MS45OTgiIHN0eWxlPSJlbmFibGUtYmFja2dyb3VuZDpuZXcgMCAwIDUxMiA1MTIiIHhtbDpzcGFjZT0icHJlc2VydmUiIGNsYXNzPSIiPjxnPgo8ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciPgoJPHBhdGggZD0iTTQwLjk5OSwwQzE4LjM5MiwwLDAsMTguMzkyLDAsNDAuOTk5YzAsMjIuNjA2LDE4LjM5Miw0MC45OTksNDAuOTk5LDQwLjk5OWMyMi42MDYsMCw0MC45OTktMTguMzkzLDQwLjk5OS00MC45OTkgICBDODEuOTk4LDE4LjM5Miw2My42MDcsMCw0MC45OTksMHogTTQwLjk5OSw3Ny45OThDMjAuNTk4LDc3Ljk5OCw0LDYxLjQsNCw0MC45OTlDNCwyMC41OTgsMjAuNTk4LDQsNDAuOTk5LDQgICBzMzYuOTk5LDE2LjU5OCwzNi45OTksMzYuOTk5Qzc3Ljk5OCw2MS40LDYxLjQsNzcuOTk4LDQwLjk5OSw3Ny45OTh6IE00NS43MDgsMTIuNzQ0YzAuMzc2LDAuMzg3LDAuNTgxLDAuOTA4LDAuNTY3LDEuNDQ3ICAgbC0xLjMxOSw0MS4zNzJjLTAuMDI4LDEuMDg0LTAuOTE1LDEuOTQ5LTIsMS45NDloLTMuOTE1Yy0xLjA4NCwwLTEuOTcyLTAuODY1LTItMS45NDlsLTEuMzE5LTQxLjM3MiAgIGMtMC4wMTQtMC41MzksMC4xOTEtMS4wNjEsMC41NjctMS40NDdjMC4zNzYtMC4zODYsMC44OTMtMC42MDQsMS40MzItMC42MDRoNi41NTVDNDQuODE0LDEyLjE0MSw0NS4zMzIsMTIuMzU4LDQ1LjcwOCwxMi43NDR6ICAgIE00Ni4xOTEsNjMuMDAydjcuNDljMCwxLjEwNC0wLjg5NiwyLTIsMmgtNi4zODVjLTEuMTA0LDAtMi0wLjg5Ni0yLTJ2LTcuNDljMC0xLjEwNCwwLjg5Ni0yLDItMmg2LjM4NSAgIEM0NS4yOTYsNjEuMDAyLDQ2LjE5MSw2MS44OTYsNDYuMTkxLDYzLjAwMnoiIGZpbGw9IiNmMTA1MDUiIGRhdGEtb3JpZ2luYWw9IiMwMDAwMDAiIHN0eWxlPSIiIGNsYXNzPSIiPjwvcGF0aD4KPC9nPgo8ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciPgo8L2c+CjxnIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyI+CjwvZz4KPGcgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIj4KPC9nPgo8ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciPgo8L2c+CjxnIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyI+CjwvZz4KPGcgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIj4KPC9nPgo8ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciPgo8L2c+CjxnIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyI+CjwvZz4KPGcgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIj4KPC9nPgo8ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciPgo8L2c+CjxnIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyI+CjwvZz4KPGcgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIj4KPC9nPgo8ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciPgo8L2c+CjxnIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyI+CjwvZz4KPGcgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIj4KPC9nPgo8L2c+PC9zdmc+" />

              : <span></span>}
            {this.state.errorNoHp ? <div className="lineError"></div> : ''}
            </div>


            
            {this.state.errorNoHp ?
            <span style={{ color: "red", position: 'relative', top: '-4vw', left: '4vw', fontSize: '3vw' }}>
              Mohon gunakan format no Handphone</span>
            : <span></span>
          }

            <div className={classes.containerTextField}>
              <input
               autoComplete="new-password" spellcheck="false"
                type={this.state.typeInput}
                className={classes.textField}
                onChange={(event) => this.setState({ userPassword: event.target.value })}
              >
              </input>
              <label id="label-textBoardLogin" 
              // style={{zIndex:'3'}}
               className=
              {this.state.userPassword !== '' ? 'validInput' : ''}>Password</label>
              <i className="fa fa-eye icon-eye-post" style={{zIndex:'1'}} aria-hidden="true" onClick={()=>{
                var value = this.state.typeInput;
                if(value==='password'){
                this.setState({
                  typeInput:'text'
                })
                }
                else{
                  this.setState({
                    typeInput:'password'
                  })
                }
              }}></i>
            </div>
            <Link href="/changePassword">
              <Button size="large" style={{
                border: 'none',
                width: "100%",
                fontWeight: 'bold',
                fontStyle: 'normal',
                fontFamily: 'Nunito, sans-serif',
                fontSize: "4.072vw",
                color: '#007ACF', textTransform: 'none'
              }} variant="outlined" color="primary">
                Lupa Password?
             </Button>
            </Link>
            <Button size="large"
              style={{
                backgroundColor: '#0057E7',
                width: "100%",
                color: 'white',
                fontSize: "3.572vw",
                height: (window.innerWidth * 0.1266),
                fontWeight: 'bold',
                fontStyle: 'normal',
                fontFamily: 'Nunito, sans-serif',
                marginTop: '7.4%', textTransform: 'none'
              }}
              onClick={this.login}
              variant="outlined" color="primary">
              Login
                        </Button>

            <Link href="/startReg">
              <Button size="large" style={{
                backgroundColor: '#E8F3FC',
                width: "100%",
                marginTop: '5%', textTransform: 'none',
                fontWeight: 'bold',
                borderRadius: '6px',
                border: 'none',
                height: (window.innerWidth * 0.1266),
                fontStyle: 'normal',
                fontFamily: 'Nunito, sans-serif',
                color: '#0057E7',
                fontSize: "3.572vw",
                marginBottom: '30px'
              }} variant="outlined" color="primary">
                Belum Punya Akun? Daftar Disini
                        </Button>
            </Link>
            
          </form>
       
       
       
       
       
        </div>
      </div>
    );
  }
}



function mapStateToProps(state) {
  return {
    test: state.test,
  };
}

export default compose(
  withStyles(useStyles),
  connect(mapStateToProps)
)(Login);