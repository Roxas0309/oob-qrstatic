import React from 'react';
import { connect } from 'react-redux';
import compose from 'recompose/compose';
import Avatar from '@material-ui/core/Avatar';
import Button from '@material-ui/core/Button';
import CssBaseline from '@material-ui/core/CssBaseline';
import TextField from '@material-ui/core/TextField';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Checkbox from '@material-ui/core/Checkbox';
import Link from '@material-ui/core/Link';
import Grid from '@material-ui/core/Grid';
import Box from '@material-ui/core/Box';
import LockOutlinedIcon from '@material-ui/icons/LockOutlined';
import Typography from '@material-ui/core/Typography';
import { withStyles } from '@material-ui/core/styles';
import Container from '@material-ui/core/Container';
//import { Link } from 'react-router-dom';
import successGanti from '../../../public/img/iklan/success-ganti-password.png';
import { testActions } from '../../actions/test.actions';
import { t } from '../../helpers/translate';
import { BACK_END_POINT, HEADER_AUTH, POST, TOKEN_AUTH, wsCallingWithBody, wsWithBody } from '../../master/masterComponent';
import { Alert } from '@material-ui/lab';
import '../Login/login.css';
const useStyles = theme => ({
  paper: {
    marginTop: theme.spacing(8),
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center'
  },
  containerPaper : {
     width : '90%'
  },
  avatar: {
    margin: theme.spacing(1),
    backgroundColor: theme.palette.secondary.main,
  },
  form: {
    width: '100%', // Fix IE 11 issue.
    marginTop: theme.spacing(1),
  },
  submit: {
    margin: theme.spacing(3, 0, 2),
  }, textField: {
    boxSizing : 'border-box',
    paddingLeft: '16px',
    border : 'none',
    borderRadius : '6px',
    width: '100%',
    background : 'none',
    fontSize : "4.072vw",
    height : '100%',
    fontWeight : 'bold',
    fontStyle : 'normal',
    fontFamily : 'Nunito, sans-serif',
    textTransform: 'none',
    fontWeight: 500,
    position : 'relative',
    zIndex : 1,
    paddingTop : '16px'
   },
   containerTextField : {
    position : 'relative',
    width: '100%',
    height : (window.innerWidth* 0.1266),
    marginBottom : '16px',
    backgroundColor : '#F7F8F9',
    borderRadius : '6px',
    
   },
   containerTextFieldError : {
    position : 'relative',
    width: '100%',
    height : (window.innerWidth* 0.1266),
    marginBottom : '16px',
    backgroundColor : '#F7F8F9',
    borderBottom : '2px solid red'
   }
});

class SuccessChangePassword extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      userPhone : '',
      userPassword : '',
      userPasswordConfirm : '',
      showErrorLogin: false,
      errorMessage : '',
      errorPassword : false,
      errorPasswordConfirm : false,
      yesPassword : false,
      yesConfirm : false
    }
  }

  componentDidMount() {
    const { dispatch } = this.props;

    dispatch(testActions.test());
  }

  login = () => {
    this.props.history.push('/login');
            location.reload();
    
  }


  render() {
    const { classes } = this.props;
    return (
        <div className={classes.paper}>
            <div className={classes.containerPaper}>
        </div>

        <Grid container wrap="nowrap" 
          style={{ justifyContent: 'center', alignItems: 'center', display: 'flex', 
                  width : '100%' }}>
            <Box style={{ textAlign: 'center',position: 'fixed', display: 'contents', 
                  width : '100%' }}>
              <img src={successGanti} style={{ width : '80%', marginTop : '-3%', marginBottom : '6.086vw' }} />
            </Box>
          </Grid>

          <Grid container wrap="nowrap" 
            style={{ justifyContent: 'center', alignItems: 'center', display: 'flex', 
                  width : '100%' }}>
             <Box style={{ textAlign: 'center',position: 'fixed', display: 'contents', 
                  width : '100%' }}>
               <span style={{fontWeight : 'lighter', fontFamily : 'Montserrat, sans-serif',
               paddingBottom : '3px ', fontSize : '5.238vw', lineHeight:'8.6vw'}}> 
               <b>Selamat !</b> <br></br> <b>Pembaruan Password Anda</b> <br></br> <b>Sudah Berhasil</b></span> 
             </Box>
          </Grid>

          <Grid container wrap="nowrap" 
            style={{ justifyContent: 'center', alignItems: 'center', display: 'flex', 
                  width : '100%' }}>
             <Box style={{ textAlign: 'center',position: 'fixed', display: 'contents', 
                  width : '100%' }}>
               <span style={{fontWeight : '400', fontFamily : 'Nunito, sans-serif',
               paddingBottom : '80px', fontSize : '4vw', lineHeight:'8.6vw', color:'#615A5A'}}> 
               Silakan melakukan log in kembali</span> 
             </Box>
          </Grid>
        
          <Button size="large" 
            style={{backgroundColor:'#0057E7',
                width: '81%',
                color : 'white',
                fontSize : "3.572vw",
                height : (window.innerWidth* 0.1266),
                fontWeight : 'bold',
                fontStyle : 'normal',
                fontFamily : 'Nunito, sans-serif',
                borderRadius:'1.5vw',
                marginTop: '2.4%', textTransform: 'none'
              }} 
              onClick = {this.login}
              variant="outlined" color="primary">
                Login
             </Button>

        </div>
    );
  }
}



function mapStateToProps(state) {
  return {
    test: state.test,
  };
}

export default compose(
  withStyles(useStyles),
  connect(mapStateToProps)
)(SuccessChangePassword);