import React from 'react';
import { connect } from 'react-redux';
import compose from 'recompose/compose';
import Button from '@material-ui/core/Button';
import Link from '@material-ui/core/Link';
import Grid from '@material-ui/core/Grid';
import Box from '@material-ui/core/Box';
import Typography from '@material-ui/core/Typography';
import { withStyles } from '@material-ui/core/styles';
import { testActions } from '../../actions/test.actions';
import { t } from '../../helpers/translate';
import { BACK_END_POINT, HEADER_AUTH, POST, TOKEN_AUTH, wsCallingWithBody, wsWithBody } from '../../master/masterComponent';
import '../Login/login.css';
import ArrowBackIcon from '@material-ui/icons/ArrowBack';
import LoadingMaker from '../../helpers/loadingMaker/LoadingMaker';
const useStyles = theme => ({

  paper: {
    marginTop: theme.spacing(8),
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center'
  },
  containerPaper: {
    width: '90%'
  },
  avatar: {
    margin: theme.spacing(1),
    backgroundColor: theme.palette.secondary.main,
  },
  form: {
    width: '100%', // Fix IE 11 issue.
    marginTop: theme.spacing(1),
  },
  submit: {
    margin: theme.spacing(3, 0, 2),
  },
  textField: {
    boxSizing: 'border-box',
    paddingLeft: '16px',
    border: 'none',
    height: '100%',
    width: '100%',
    background: 'none',

    height: '100%',
    fontWeight: 'bold',
    fontStyle: 'normal',
    fontFamily: 'Nunito, sans-serif',
    textTransform: 'none',
    fontWeight: 500,
    position: 'relative',
    zIndex: 1,
    paddingTop: '16px',
    fontSize: "4.072vw"
  },
  containerTextFieldError: {
    fontSize: "4.072vw",
    position: 'relative',
    width: '100%',
    height: '12.670vw',
    marginBottom: '4.357vw',
    borderRadius: '1.357vw',
    backgroundColor: '#F7F8F9',
    animation: 'shake 0.5s',
    animationIterationCount: '1'
  },
  containerTextField: {
    fontSize: "4.072vw",
    position: 'relative',
    width: '100%',
    height: '12.670vw',
    marginBottom: '16px',
    borderRadius: '6px',
    backgroundColor: '#F7F8F9'
  },
  backdrop: {
    zIndex: theme.zIndex.drawer + 1,
    color: '#fff',
  }
});

class MakePassword extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      userPhone: '',
      userPassword: '',
      typeInputPass:'password',
      typeInputPassConf:'password',
      userPasswordConfirm: '',
      showErrorLogin: false,
      errorMessage: '',
      errorPassword: false,
      errorPasswordConfirm: false,
      yesPassword: false,
      yesConfirm: false,
      isOnLoading: false,
      typeText: 'password',
      ifNotAlreadyVibrateInPassword: true,
      ifNotAlreadyVibrateInConfirm:true,
      allowClickPassword:false,
      allowClickConfirm:false,
      stopInputPass: false
    }
  }

  componentDidMount() {
    const { dispatch } = this.props;

    dispatch(testActions.test());
  }

  login = () => {
    this.setState({ isOnLoading: true });
    if (this.state.yesConfirm && this.state.yesPassword) {
      this.setState({ errorPassword: false, errorPasswordConfirm: false })
      wsWithBody(BACK_END_POINT + "/loginCtl/changePassword", POST, {
        password: this.state.userPassword
      }, { ...HEADER_AUTH, 'secret-token': localStorage.getItem(TOKEN_AUTH) }).then(response => {
        this.setState({ isOnLoading: false });
        var valid = response.data.result.valid;
        if (valid) {
          this.props.history.push('/login');
          location.reload();
        } else {
          alert('Password Belum Dapat Diganti Mohon Refresh Dan Coba Buat Password Lagi');
        }

      }).catch(error => {
        this.setState({ isOnLoading: false });
        this.setState({ showErrorLogin: true, errorMessage: error.response.data.message, errorPassword: true });
      })
    }
    else if (!this.state.yesPassword) {
      this.setState({ errorPassword: true });
    }
    else if (!this.state.yesConfirm) {
      this.setState({ errorPasswordConfirm: true });
    }
    else {
      this.setState({ errorPassword: true, errorPasswordConfirm: true })
    }
  }

  // Copyright() {
  //   return (
  //     <Typography variant="body2" color="textSecondary" align="center">
  //       {'Copyright © '}
  //       <Link color="inherit" href="https://material-ui.com/">
  //         Your Website
  //       </Link>{' '}
  //       {new Date().getFullYear()}
  //       {'.'}
  //     </Typography>
  //   );
  // }

  checkIsExistUpper = (words) => {
    var x = words.split("");
    var result = false;
    x.map((xs) => {
      if ((xs.match(/[A-Z]/i) !== null) && xs === xs.toUpperCase()) {
        result = true;
      }
    });
    return result;
  }

  checkIsExistNumeric = (words) => {
    var x = words.split("");
    var result = false;

    x.map((xs) => {
      if (!isNaN(xs)) {
        result = true;
      }
    });
    return result;
  }

  checkIsExistLower = (words) => {
    var x = words.split("");
    var result = false;
    x.map((xs) => {
      if ((xs.match(/[a-z]/i) !== null) && xs === xs.toLowerCase()) {
        result = true;
      }
    });
    return result;
  }

  checkIsOnlyAlphaNumeric = (words) => { 
    var regexp = new RegExp(/^[a-zA-Z0-9]+$/i);
    return regexp.test(words);
  }

  render() {
    const { classes } = this.props;
    return (
      <div className={classes.paper}>
        <div className={classes.containerPaper}>

          <div style={{
            display: 'flex', minWidth: '100%', position: 'fixed',
            marginLeft: '0px', top: '5vw'
          }}>
            <Link href="/login" >
              <ArrowBackIcon style={{ left: '20px', fontSize: '4.925vw', color: '#ADA6AE' }} />
            </Link>
          </div>

          {/* {
            this.state.errorPassword ? <div></div> :
              <div className="error-label" style={{ border: '1px solid #0057E7', position: 'relative', marginTop: '-2vw' }}>
                <h5>Buat Password Baru</h5>
                <p>Pastikan password minimum 8 karakter yang
                terdiri dari kombinasi: huruf kapital, huruf kecil, dan angka.
            </p>
              </div>
          } */}

          {
            
              <div className="error-label" style={{ border: '1px solid #0057E7', position: 'relative', marginTop: '-2vw' }}>
                <h5>Buat Password Baru</h5>
                <p>Pastikan password minimum 8 karakter yang
                terdiri dari kombinasi: huruf kapital, huruf kecil, dan angka.
             </p>
              </div>
          }   

          <Grid container wrap="nowrap"
            style={{
              justifyContent: 'center', alignItems: 'center', display: 'flex',
              width: '100%', paddingBottom: '20px'
            }}>
            <Box style={{
              textAlign: 'center', fontFamily: 'Nunito, sans-serif', position: 'fixed', display: 'contents',
              width: '100%'
            }}>
              <span style={{ fontSize: '4.838vw', fontWeight: 'bold' }}> Buat Password</span>
            </Box>
          </Grid>
          <Grid container wrap="nowrap"
            style={{
              justifyContent: 'center', alignItems: 'center', display: 'flex',
              width: '100%'
            }}>
            <Box style={{
              textAlign: 'center', position: 'fixed', display: 'contents',
              width: '100%'
            }}>
              <span style={{
                fontWeight: 'lighter', fontFamily: 'TTInterfaces', width: '65.837vw',
                paddingBottom: '19px', fontSize: '3.838vw', lineHeight: '5.6vw', color: 'rgba(88, 88, 88, 1)'
              }}> Pastikan password Anda memenuhi kriteria keamanan yang ditentukan.  </span>
            </Box>
          </Grid>

          <LoadingMaker
            loadingWord={'Proses pembaharuan password sedang berlangsung'}
            onLoading={this.state.isOnLoading}></LoadingMaker>

          <form className={classes.form} noValidate  autoComplete="new-password">
            <div className={this.state.errorPassword ? classes.containerTextFieldError : classes.containerTextField}>
              <input
               autoComplete="new-password"
                className={classes.textField}
                type={this.state.typeInputPass}
                value={this.state.userPassword}
                onChange={(event) => {
                  var value = event.target.value;
                  var isExistUpper = !this.checkIsExistUpper(value);
                  var isExistLower = !this.checkIsExistLower(value);
                  var isExistNumeric = !this.checkIsExistNumeric(value);
                  var isOnlyAlphaNumeric = this.checkIsOnlyAlphaNumeric(value);
                  console.log('only alphanumeric : ' + isOnlyAlphaNumeric);
                  console.log( (value.length == 8) + '|| ' + isExistUpper + " || " +  isExistLower + " || " + isExistNumeric );
                  if (value.length <= 255) {
                    this.setState({ userPassword: value });
                  }

                  if ( (value.length >= 8 && value.length <= 255) &&!isExistUpper && !isExistLower && !isExistNumeric && isOnlyAlphaNumeric) {
                    this.setState({ errorPassword: false, yesPassword: true,allowClickPassword:true, ifNotAlreadyVibrateInPassword: true })

                    if ( value === this.state.userPasswordConfirm) {
                      this.setState({ errorPasswordConfirm: false, 
                        yesConfirm: true,ifNotAlreadyVibrateInConfirm: true ,allowClickConfirm : true});
                    }
                    else {
                      if(this.state.userPasswordConfirm!=''){
                      if ("vibrate" in navigator && this.state.ifNotAlreadyVibrateInConfirm) {
                        navigator.vibrate(1000);
                        this.setState({ ifNotAlreadyVibrateInConfirm: false })
                      }
                      this.setState({ errorPasswordConfirm: true, allowClickConfirm:false });
                     }
                    }
  
                
                  }
                  else {
                    if (this.state.ifNotAlreadyVibrateInPassword) {
                      if ("vibrate" in navigator) {
                        navigator.vibrate(1000);
                        this.setState({ ifNotAlreadyVibrateInPassword: false })
                      }
                    }
                    this.setState({ errorPassword: true,allowClickPassword:false })
                  }
                }
                }
              >
              </input>
              <label id="label-textBoardLogin" 
                 className={(this.state.userPassword !== '' && !this.state.errorPassword) ?
                'validInput' : this.state.errorPassword ? 'validInput error' : ''}>Password</label>
              {this.state.errorPassword ?
                // <i className="fa fa-exclamation-circle icon-error" aria-hidden="true"></i> 
                <img className="icon-error" 
                onClick={()=>{
                  var value = this.state.typeInputPass;
                  if(value==='password'){
                  this.setState({
                    typeInputPass:'text'
                  })
                  }
                  else{
                    this.setState({
                      typeInputPass:'password'
                    })
                  }
                }}
                src="data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHZlcnNpb249IjEuMSIgeG1sbnM6eGxpbms9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkveGxpbmsiIHhtbG5zOnN2Z2pzPSJodHRwOi8vc3ZnanMuY29tL3N2Z2pzIiB3aWR0aD0iNTEyIiBoZWlnaHQ9IjUxMiIgeD0iMCIgeT0iMCIgdmlld0JveD0iMCAwIDgxLjk5OCA4MS45OTgiIHN0eWxlPSJlbmFibGUtYmFja2dyb3VuZDpuZXcgMCAwIDUxMiA1MTIiIHhtbDpzcGFjZT0icHJlc2VydmUiIGNsYXNzPSIiPjxnPgo8ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciPgoJPHBhdGggZD0iTTQwLjk5OSwwQzE4LjM5MiwwLDAsMTguMzkyLDAsNDAuOTk5YzAsMjIuNjA2LDE4LjM5Miw0MC45OTksNDAuOTk5LDQwLjk5OWMyMi42MDYsMCw0MC45OTktMTguMzkzLDQwLjk5OS00MC45OTkgICBDODEuOTk4LDE4LjM5Miw2My42MDcsMCw0MC45OTksMHogTTQwLjk5OSw3Ny45OThDMjAuNTk4LDc3Ljk5OCw0LDYxLjQsNCw0MC45OTlDNCwyMC41OTgsMjAuNTk4LDQsNDAuOTk5LDQgICBzMzYuOTk5LDE2LjU5OCwzNi45OTksMzYuOTk5Qzc3Ljk5OCw2MS40LDYxLjQsNzcuOTk4LDQwLjk5OSw3Ny45OTh6IE00NS43MDgsMTIuNzQ0YzAuMzc2LDAuMzg3LDAuNTgxLDAuOTA4LDAuNTY3LDEuNDQ3ICAgbC0xLjMxOSw0MS4zNzJjLTAuMDI4LDEuMDg0LTAuOTE1LDEuOTQ5LTIsMS45NDloLTMuOTE1Yy0xLjA4NCwwLTEuOTcyLTAuODY1LTItMS45NDlsLTEuMzE5LTQxLjM3MiAgIGMtMC4wMTQtMC41MzksMC4xOTEtMS4wNjEsMC41NjctMS40NDdjMC4zNzYtMC4zODYsMC44OTMtMC42MDQsMS40MzItMC42MDRoNi41NTVDNDQuODE0LDEyLjE0MSw0NS4zMzIsMTIuMzU4LDQ1LjcwOCwxMi43NDR6ICAgIE00Ni4xOTEsNjMuMDAydjcuNDljMCwxLjEwNC0wLjg5NiwyLTIsMmgtNi4zODVjLTEuMTA0LDAtMi0wLjg5Ni0yLTJ2LTcuNDljMC0xLjEwNCwwLjg5Ni0yLDItMmg2LjM4NSAgIEM0NS4yOTYsNjEuMDAyLDQ2LjE5MSw2MS44OTYsNDYuMTkxLDYzLjAwMnoiIGZpbGw9IiNmMTA1MDUiIGRhdGEtb3JpZ2luYWw9IiMwMDAwMDAiIHN0eWxlPSIiIGNsYXNzPSIiPjwvcGF0aD4KPC9nPgo8ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciPgo8L2c+CjxnIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyI+CjwvZz4KPGcgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIj4KPC9nPgo8ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciPgo8L2c+CjxnIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyI+CjwvZz4KPGcgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIj4KPC9nPgo8ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciPgo8L2c+CjxnIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyI+CjwvZz4KPGcgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIj4KPC9nPgo8ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciPgo8L2c+CjxnIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyI+CjwvZz4KPGcgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIj4KPC9nPgo8ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciPgo8L2c+CjxnIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyI+CjwvZz4KPGcgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIj4KPC9nPgo8L2c+PC9zdmc+" />

                : <i className="fa fa-eye icon-eye-post" aria-hidden="true"
                onClick={()=>{
                  var value = this.state.typeInputPass;
                  if(value==='password'){
                  this.setState({
                    typeInputPass:'text'
                  })
                  }
                  else{
                    this.setState({
                      typeInputPass:'password'
                    })
                  }
                }}
                 ></i>}
              {this.state.errorPassword ? <div className="lineError"></div> : ''}

            </div>
            {this.state.errorPassword ?
               this.state.showErrorLogin ? 
              <span style={{ color: "red", position: 'relative', top: '-2vw', fontSize: '3vw' }}>
              {this.state.errorMessage}</span>
              :
              <span style={{ color: "red", position: 'relative', top: '-2vw', fontSize: '3vw' }}>
                Harus minimum 8 karakter yang terdiri dari kombinasi huruf kapital, 
                huruf kecil, dan angka</span>
            
              : <span></span>
            }

            <div className={this.state.errorPasswordConfirm ? classes.containerTextFieldError: classes.containerTextField}>
              <input
               autoComplete="new-password"
                className={classes.textField}
                type={this.state.typeInputPassConf}
                onChange={(event) => {
                  var value = event.target.value;
                  this.setState({ userPasswordConfirm: value });

                  if (value !== this.state.userPassword) {
                    if ("vibrate" in navigator && this.state.ifNotAlreadyVibrateInConfirm) {
                      navigator.vibrate(1000);
                      this.setState({ ifNotAlreadyVibrateInConfirm: false })
                    }
                    this.setState({ errorPasswordConfirm: true, allowClickConfirm:false });
                  }
                  else {
                    this.setState({ errorPasswordConfirm: false, 
                     yesConfirm: true,ifNotAlreadyVibrateInConfirm: true ,allowClickConfirm : true});
                  }

                  

                }}
              >
              </input>
              <label id="label-textBoardLogin" className={(this.state.userPasswordConfirm !== '' && !this.state.errorPasswordConfirm) ? 'validInput' : this.state.errorPasswordConfirm ? 'validInput error' : ''}>Konfirmasi Password</label>
              {this.state.errorPasswordConfirm ?
                //  <i className="fa fa-exclamation-circle icon-error" aria-hidden="true"></i> 
                <img className="icon-error" 
                onClick={()=>{
                  var value = this.state.typeInputPassConf;
                  if(value==='password'){
                  this.setState({
                    typeInputPassConf:'text'
                  })
                  }
                  else{
                    this.setState({
                      typeInputPassConf:'password'
                    })
                  }
                }}
                src="data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHZlcnNpb249IjEuMSIgeG1sbnM6eGxpbms9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkveGxpbmsiIHhtbG5zOnN2Z2pzPSJodHRwOi8vc3ZnanMuY29tL3N2Z2pzIiB3aWR0aD0iNTEyIiBoZWlnaHQ9IjUxMiIgeD0iMCIgeT0iMCIgdmlld0JveD0iMCAwIDgxLjk5OCA4MS45OTgiIHN0eWxlPSJlbmFibGUtYmFja2dyb3VuZDpuZXcgMCAwIDUxMiA1MTIiIHhtbDpzcGFjZT0icHJlc2VydmUiIGNsYXNzPSIiPjxnPgo8ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciPgoJPHBhdGggZD0iTTQwLjk5OSwwQzE4LjM5MiwwLDAsMTguMzkyLDAsNDAuOTk5YzAsMjIuNjA2LDE4LjM5Miw0MC45OTksNDAuOTk5LDQwLjk5OWMyMi42MDYsMCw0MC45OTktMTguMzkzLDQwLjk5OS00MC45OTkgICBDODEuOTk4LDE4LjM5Miw2My42MDcsMCw0MC45OTksMHogTTQwLjk5OSw3Ny45OThDMjAuNTk4LDc3Ljk5OCw0LDYxLjQsNCw0MC45OTlDNCwyMC41OTgsMjAuNTk4LDQsNDAuOTk5LDQgICBzMzYuOTk5LDE2LjU5OCwzNi45OTksMzYuOTk5Qzc3Ljk5OCw2MS40LDYxLjQsNzcuOTk4LDQwLjk5OSw3Ny45OTh6IE00NS43MDgsMTIuNzQ0YzAuMzc2LDAuMzg3LDAuNTgxLDAuOTA4LDAuNTY3LDEuNDQ3ICAgbC0xLjMxOSw0MS4zNzJjLTAuMDI4LDEuMDg0LTAuOTE1LDEuOTQ5LTIsMS45NDloLTMuOTE1Yy0xLjA4NCwwLTEuOTcyLTAuODY1LTItMS45NDlsLTEuMzE5LTQxLjM3MiAgIGMtMC4wMTQtMC41MzksMC4xOTEtMS4wNjEsMC41NjctMS40NDdjMC4zNzYtMC4zODYsMC44OTMtMC42MDQsMS40MzItMC42MDRoNi41NTVDNDQuODE0LDEyLjE0MSw0NS4zMzIsMTIuMzU4LDQ1LjcwOCwxMi43NDR6ICAgIE00Ni4xOTEsNjMuMDAydjcuNDljMCwxLjEwNC0wLjg5NiwyLTIsMmgtNi4zODVjLTEuMTA0LDAtMi0wLjg5Ni0yLTJ2LTcuNDljMC0xLjEwNCwwLjg5Ni0yLDItMmg2LjM4NSAgIEM0NS4yOTYsNjEuMDAyLDQ2LjE5MSw2MS44OTYsNDYuMTkxLDYzLjAwMnoiIGZpbGw9IiNmMTA1MDUiIGRhdGEtb3JpZ2luYWw9IiMwMDAwMDAiIHN0eWxlPSIiIGNsYXNzPSIiPjwvcGF0aD4KPC9nPgo8ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciPgo8L2c+CjxnIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyI+CjwvZz4KPGcgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIj4KPC9nPgo8ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciPgo8L2c+CjxnIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyI+CjwvZz4KPGcgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIj4KPC9nPgo8ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciPgo8L2c+CjxnIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyI+CjwvZz4KPGcgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIj4KPC9nPgo8ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciPgo8L2c+CjxnIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyI+CjwvZz4KPGcgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIj4KPC9nPgo8ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciPgo8L2c+CjxnIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyI+CjwvZz4KPGcgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIj4KPC9nPgo8L2c+PC9zdmc+" />

                : <i className="fa fa-eye icon-eye-post" aria-hidden="true" 
                onClick={()=>{
                  var value = this.state.typeInputPassConf;
                  if(value==='password'){
                  this.setState({
                    typeInputPassConf:'text'
                  })
                  }
                  else{
                    this.setState({
                      typeInputPassConf:'password'
                    })
                  }
                }}></i>}
              {this.state.errorPasswordConfirm ? <div className="lineError"></div> : ''}
            </div>
            {this.state.errorPasswordConfirm ?
              <span style={{ color: "red", position: 'relative', top: '-2vw', fontSize: '3vw' }}>Password dan konfirmasi password masih belum sama</span>
              : <span></span>
            }

            {this.state.allowClickPassword && this.state.allowClickConfirm ? 
            <Button size="large"
              style={{
                backgroundColor: '#0057E7',
                width: "100%",
                color: 'white',
                fontSize: "3.572vw",
                height: (window.innerWidth * 0.1266),
                fontWeight: 'bold',
                fontStyle: 'normal',
                fontFamily: 'Nunito, sans-serif',
                marginTop: '7.4%', textTransform: 'none'
              }}
              onClick={this.login}
              variant="outlined" color="primary">
              Login
            </Button>
            :
            <Button size="large"
            style={{
              backgroundColor: 'rgba(236, 238, 242, 1)',
              width: "100%",
              color: 'rgba(48, 59, 74, 0.24)',
              fontSize: "3.572vw",
              height: (window.innerWidth * 0.1266),
              fontWeight: 'bold',
              fontStyle: 'normal',
              fontFamily: 'Nunito, sans-serif',
              marginTop: '7.4%', textTransform: 'none'
            }}>
            Login
          </Button>
            }
          </form>
        </div>
      </div>
    );
  }
}



function mapStateToProps(state) {
  return {
    test: state.test,
  };
}

export default compose(
  withStyles(useStyles),
  connect(mapStateToProps)
)(MakePassword);