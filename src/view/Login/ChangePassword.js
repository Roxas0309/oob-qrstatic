import React from 'react';
import { connect } from 'react-redux';
import compose from 'recompose/compose';
import Button from '@material-ui/core/Button';
import Link from '@material-ui/core/Link';
import Grid from '@material-ui/core/Grid';
import Box from '@material-ui/core/Box';
import Typography from '@material-ui/core/Typography';
import { withStyles } from '@material-ui/core/styles';
import { testActions } from '../../actions/test.actions';
import { t } from '../../helpers/translate';
import { BACK_END_POINT, HEADER_AUTH, POST, SESSION_ITEM, TOKEN_AUTH, wsCallingWithBody, wsWithBody } from '../../master/masterComponent';
import '../Login/login.css';
import ArrowBackIcon from '@material-ui/icons/ArrowBack';
import LoadingMaker from '../../helpers/loadingMaker/LoadingMaker';
import { Backdrop } from '@material-ui/core';
import sukses from "../../../public/img/success.gif";
const useStyles = theme => ({

  paper: {
 
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center'
  },
  containerPaper: {
    width: '90%'
  },
  avatar: {
    margin: theme.spacing(1),
    backgroundColor: theme.palette.secondary.main,
  },
  form: {
    width: '100%', // Fix IE 11 issue.
    marginTop: theme.spacing(1),
  },
  submit: {
    margin: theme.spacing(3, 0, 2),
  },
  textField: {
    boxSizing: 'border-box',
    paddingLeft: '16px',
    border: 'none',
    height: '100%',
    width: '100%',
    background: 'none',

    height: '100%',
    fontWeight: 'bold',
    fontStyle: 'normal',
    fontFamily: 'Nunito, sans-serif',
    textTransform: 'none',
    fontWeight: 500,
    position: 'relative',
    zIndex: 1,
    paddingTop: '16px',
    fontSize: "4.072vw"
  },
  containerTextField: {
    fontSize: "4.072vw",
    position: 'relative',
    width: '100%',
    height: '12.670vw',
    marginBottom: '16px',
    borderRadius: '6px',
    backgroundColor: '#F7F8F9'
  },
  containerTextFieldError: {
    fontSize: "4.072vw",
    position: 'relative',
    width: '100%',
    height: '12.670vw',
    marginBottom: '4.357vw',
    borderRadius: '1.357vw',
    backgroundColor: '#F7F8F9',
    animation: 'shake 0.5s',
    animationIterationCount: '1'
  },
  backdrop: {
    zIndex: theme.zIndex.drawer + 1,
    color: '#fff',
  },
  paperDrop: {
    backgroundColor: theme.palette.background.paper,
    border: "1px solid lightgray",
    boxShadow: theme.shadows[5],
    padding: theme.spacing(2, 4, 3),
    textAlign: "center",
    borderRadius: "5px",
    alignItems: "center",
    justifyContent: "center",
    width: "64vw",
    minWidth: "50px",
  }
});

class ChangePassword extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      userPhone: '',
      userPassword: '',
      userPasswordConfirm: '',
      showErrorLogin: false,
      errorMessage: '',
      errorEmail: false,
      errorEmailConfirm: false,
      yesPassword: false,
      yesConfirm: false,
      isOnLoading: false,
      typeText: 'text',
      allowClickEmail: false,
      wordingErrorEmail:'',
      email: '',
      openPopUp:false
    }
  }

  componentDidMount() {
    const { dispatch } = this.props;
    document.body.style.overflow = 'hidden';
    dispatch(testActions.test());
  }

  sendMail = () =>{
    this.setState({ isOnLoading: true });
    wsWithBody(BACK_END_POINT+"/email-maker/send-forgot-password",POST,{
      "email-accept" : this.state.email,
      sessionLocal:localStorage.getItem(SESSION_ITEM)
    }, HEADER_AUTH).then(response=>{
    this.setState({openPopUp:true, isOnLoading: false});
    setTimeout(() => {
         this.props.history.push('/login');
         location.reload();
    }, 3000);
  
    }).catch(error=>{
      if ("vibrate" in navigator && this.state.ifNotAlreadyVibrateInConfirm) {
        navigator.vibrate(1000);
      }
      if(error.response.data.status==500){
        this.setState({ isOnLoading: false, wordingErrorEmail: 'Service provider internal system error', 
          errorEmail : true});
      }else{
      this.setState({ isOnLoading: false, wordingErrorEmail: error.response.data.message, 
        errorEmail : true});
      }
    })
    ;
  }

  checkValidationEmail = (email) => {
    var isValid = new RegExp(/[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,15}/g).test(email);
    return isValid;
  }

  render() {
    const { classes } = this.props;
    return (
      <div className={classes.paper}>
        <div className={classes.containerPaper}>

          <div style={{
            display: 'flex', minWidth: '100%', position: 'fixed',
            marginLeft: '0px', top: '5vw'
          }}>
            <Link href="/login" >
              <ArrowBackIcon style={{ left: '20px', fontSize: '4.925vw', color: '#ADA6AE' }} />
            </Link>
          </div>

                 {/* -----------------alert----------- */}
       <Backdrop className={classes.backdrop} open={this.state.openPopUp}>
          <div className={classes.placeholder}>
              <Box className={classes.paperDrop}>
                <Typography component={"span"} variant={"body2"}>
                  <span>
                    <h4 style={{ color: "black", lineHeight:'20px' }}>Jika alamat email sudah terdaftar di sistem kami, link perubahan password dikirimkan dalam beberapa saat.</h4>
                  </span>
                  <img src={sukses} style={{ height: 50, width: 50 }}></img>
                </Typography>
                {/* <Link>
                                            <Button onClick={klikLanjut} variant="contained"
                                                style={{ width: '100%', marginTop: '20px' }} color="primary"
                                                disableElevation>
                                                Lanjut
                                        </Button>
                                        </Link> */}
              </Box>
            </div>
        </Backdrop>
        {/* --------------------------------- */}



          <LoadingMaker
            loadingWord={'Proses pembaharuan password sedang berlangsung'}
            onLoading={this.state.isOnLoading}></LoadingMaker>

          <Grid container wrap="nowrap"
            style={{
              justifyContent: 'center', alignItems: 'center', display: 'flex',
              width: '100%', paddingBottom: '20px', paddingTop:'140px'
            }}>
            <Box style={{
              textAlign: 'center', fontFamily: 'Nunito, sans-serif', position: 'fixed', display: 'contents',
              width: '100%'
            }}>
              <span style={{ fontSize: '4.838vw', fontWeight: 'bold' }}> Perubahan Password</span>
            </Box>
          </Grid>
          <Grid container wrap="nowrap"
            style={{
              justifyContent: 'center', alignItems: 'center', display: 'flex',
              width: '100%'
            }}>
            <Box style={{
              textAlign: 'center', position: 'fixed', display: 'contents',
              width: '100%'
            }}>
              <span style={{
                fontWeight: 'lighter', fontFamily: 'TTInterfaces', width: '65.837vw',
                paddingBottom: '19px', fontSize: '3.838vw', lineHeight: '5.6vw', color: 'rgba(88, 88, 88, 1)'
              }}>Masukkan alamat email Anda yang terdaftar pada aplikasi MORIS.</span>
            </Box>
          </Grid>

          <LoadingMaker
            loadingWord={'Proses pembaharuan password sedang berlangsung'}
            onLoading={this.state.isOnLoading}></LoadingMaker>

          <form className={classes.form} noValidate>
          <div className={this.state.errorEmail ? classes.containerTextFieldError : classes.containerTextField}>
              <input
                className={classes.textField}
                type={this.state.typeText}
                onChange={(event) => {
                  var value = event.target.value;

                  if(value==null||value==''||value==""){
                    this.setState({
                      email: value
                      , allowClickEmail: false
                    })
                  }
                  else if(!this.checkValidationEmail(value)){
                    if ("vibrate" in navigator && this.state.ifNotAlreadyVibrateInConfirm) {
                      navigator.vibrate(1000);
                    }
                    this.setState({
                      allowClickEmail: false,
                      errorEmail : true,
                      wordingErrorEmail:'Gunakan format email, contoh : email.contoh@email.com'
                    })
                  }
                  else{
                  this.setState({
                    email: value
                    , allowClickEmail: true,
                    errorEmail : false
                  })
                  }

                }}
              >
              </input>
              <label id="label-textBoardLogin" className={(this.state.email !== '' && !this.state.errorEmail) ?
                'validInput' : this.state.errorEmail ? 'validInput error' : ''}>Email</label>
              {this.state.errorEmail ? <div className="lineError"></div> : ''}

            </div>
            {this.state.errorEmail ?
              <span style={{ color: "red", position: 'relative', top: '-2vw', fontSize: '3vw' }}>
                {this.state.wordingErrorEmail}</span>
              : <span></span>
            }

            {this.state.allowClickEmail?
            <div size="large"
              style={{
                backgroundColor: '#0057E7',
                width: "90%",
                color: 'white',
                fontSize: "3.572vw",
                fontWeight: 'bold',
                fontStyle: 'normal',
                textAlign: 'center',
                fontFamily: 'Nunito, sans-serif',
                marginTop: '7.4%', textTransform: 'none',
                paddingTop: '3.661vw',
                paddingBottom: '3.661vw',
                borderRadius: '1.831vw',
                position: 'fixed',
                bottom: '4.543vh'
              }}
              onClick={this.sendMail}
              variant="outlined" color="primary">
              Lanjutkan
              </div>
             :
             <div size="large"
              style={{
                backgroundColor: 'rgba(236, 238, 242, 1)',
                width: "90%",
                color: 'rgba(48, 59, 74, 0.24)',
                fontSize: "3.572vw",
                fontWeight: 'bold',
                fontStyle: 'normal',
                textAlign: 'center',
                fontFamily: 'Nunito, sans-serif',
                marginTop: '7.4%', textTransform: 'none',
                paddingTop: '3.661vw',
                paddingBottom: '3.661vw',
                borderRadius: '1.831vw',
                position: 'fixed',
                bottom: '4.543vh'
              }}>
              Lanjutkan
              </div>
             }
          </form>
        </div>
      </div>
    );
  }
}



function mapStateToProps(state) {
  return {
    test: state.test,
  };
}

export default compose(
  withStyles(useStyles),
  connect(mapStateToProps)
)(ChangePassword);