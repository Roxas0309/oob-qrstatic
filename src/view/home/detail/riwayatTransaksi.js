import React from 'react';
import { BACK_END_POINT, GET, HEADER_AUTH, POST, TOKEN_AUTH, wsWithBody, wsWithoutBody } from '../../../master/masterComponent'
import '../detail/pencairanDetail.scss';
import HeaderInfo from '../../../master/HeaderInfo';
import moment from 'moment';
import ChevronRightIcon from '@material-ui/icons/ChevronRight';
import CalendarMaker from './CalendarMaker';
import HeaderInfoPencairan from '../../../master/HeaderInfoPencairan';
import prog from '../../../../public/img/in-progress-gray.png'
import progBlue from '../../../../public/img/in-progress.png'
import suk from '../../../../public/img/success.png'
import sukIjo from '../../../../public/img/success_ijo.png'
import LoadingMaker from '../../../helpers/loadingMaker/LoadingMaker';
export default class riwayatTransaksi extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            isOnLoading: false,
            isYouKnowExpands: true,
            namaLengkap: '',
            nomorHandphone: true,
            email: '',
            nomorRekening: '',
            tipeAkun: '',
            maksimalPembayaranPerHari: '',
            isOnNotifikasiBar: true,
            isShowDate: false,
            isNotOnPeriode: true,
            isNotOnRincianTransaksi: false,
            isOncheckPeriode: false,
            isNotValidDateFilter: false,
            calendarMerge: {},
            valueCalender: '',
            showCalendarTable: true,
            showMonthTable: false,
            dateObject: moment(),
            allmonths: moment.months(),
            showYearNav: false,
            selectedDate: null,
            selectedDay: null,
            selectedDayInFormat: null,
            selectedDayInWithoutYear: null,
            selectedDayInFormat: null,
            selectedDayInWithoutYearStart: null,
            selectedDayInWithoutYearEnd: null,
            selectedDayInWithoutYearStartSave: null,
            selectedDayInWithoutYearEndSave: null,
            isStartDate: false,
            isEndDate: false,
            startDate: null,
            startDateInFormat: null,
            stopDateInFormat: null,
            stopDate: null,
            isOpenCalendar: false,
            selectedDateDay: null,
            isOpenItemReferensi: false,
            isOpenTotalDiterima: false,
            pointOnDate: 5,
            detailRiwayatTransaksiSelected: null,
            rincianProses: 0,
            headerTrx: null,
            headerTrxDetail: null,
            isOnSeeInformationPendapatan: false,
            isOnSeeInformationPencairan: false,
            isOnPendapatan: true,
            isOnRincian: false,
            isOnRincianChevronUp:false,
            paidDanaMoney:null,
            unpaidDanaMoney:null,
            pencairanHariIni:{},
            wordingIsNotValidPeriode:'',
            startDatePendapatan: moment(new Date()).format("YYYYMMDD"),
            endDatePendapatan: moment(new Date()).format("YYYYMMDD"),
            listRiwayatPencairan : [],
            listDetailRiwayatPencairanaDana : {}
        }
    }

   

    getPaidUnpaidData = (startdate,enddate) => {
        wsWithoutBody(BACK_END_POINT + "/homeScreen/getPaidUnpaidMoney/auth?startDate=" + startdate + "&endDate=" + enddate + "&isLimitValidated=false",
        GET, {
        ...HEADER_AUTH, 'secret-token': localStorage.getItem(TOKEN_AUTH)
    }).then(response => {
        var value = response.data.result;
        this.setState({ paidDanaMoney: value.paidTransaksiRupiah, unpaidDanaMoney:value.unpaidTransaksiInRupiah });
    }).catch(error => {
        this.getPaidUnpaidData(startdate, enddate);
    })
    }


    getMyData = () => {
        if(this.state.isNotOnPeriode){
             wsWithoutBody(BACK_END_POINT + "/homeScreen/getDataTransaksi/auth/homeScreen?startDate=" + this.state.startDatePendapatan + "&endDate=" + this.state.endDatePendapatan + "&isLimitValidated=false",
            GET, {
            ...HEADER_AUTH, 'secret-token': localStorage.getItem(TOKEN_AUTH)
             }).then(response => {
              var value = response.data.result;
            this.setState({ headerTrx: value });
            }).catch(error => {
            this.getMyData();
            })
        }
    }


    getDetailTask = () => {
        console.log(this.state.startDate + ' ' + this.state.stopDate)

        if (this.state.stopDate != null && this.state.startDate != null) {
            this.setState({ isOnLoading: true });
            
            wsWithoutBody(BACK_END_POINT + "/homeScreen/getDataTransaksi/auth/homeScreen?startDate=" + this.state.startDateInFormat + "&endDate=" + this.state.stopDateInFormat + "&isLimitValidated=true",
                GET, {
                ...HEADER_AUTH, 'secret-token': localStorage.getItem(TOKEN_AUTH)
            }).then(response => {
                if(response.data.result.detail==null){
                    this.setState({ isNotValidDateFilter: true, wordingIsNotValidPeriode: 'Transaksi tidak ditemukan'});
                    this.setState({ isOnLoading: false });
                }
                else if(response.data.result.detail.length==0){
                    this.setState({ isNotValidDateFilter: true, wordingIsNotValidPeriode: 'Transaksi tidak ditemukan'});
                    this.setState({ isOnLoading: false });
                }
                else{
                var value = response.data.result;
                this.setState({ isOnLoading: false, isOnFilterTanggal:true });
                this.setState({ headerTrx: value });
                this.setState({ isNotOnPeriode: true, startDatePendapatan: this.state.startDateInFormat, endDatePendapatan:this.state.stopDateInFormat});
                document.body.style = 'background : #F7F8F9!important';
                this.setState({
                    selectedDayInWithoutYearStartSave: this.state.selectedDayInWithoutYearStart,
                    selectedDayInWithoutYearEndSave: this.state.selectedDayInWithoutYearEnd
                })

                }
            }).catch(error => {
                if (error.response.data.status === 406) {
                    this.setState({ isNotValidDateFilter: true, wordingIsNotValidPeriode:
                        'Pencarian transaksi maksimal 3 bulan terakhir' });
                    this.setState({ isOnLoading: false });
                }
                else  if (error.response.data.status === 411) {
                    this.setState({ isNotValidDateFilter: true, wordingIsNotValidPeriode:
                        'Pencarian transaksi tidak dapat dilakukan dengan tanggal mundur.' });
                    this.setState({ isOnLoading: false });
                    //alert(error.response.data.message);
                }
                else if(error.response.data.status === 413){
                    this.setState({ isNotValidDateFilter: true, wordingIsNotValidPeriode:
                        'Periode transaksi yang dapat dipilih 7 hari. ' });
                    this.setState({ isOnLoading: false });
                }
                else {
                    alert('upsss something went wrong');
                    this.setState({ isOnLoading: false });
                }
            })
        }
    }

    weekdayshort = moment.weekdaysShort();

    daysInMonth = () => {
        return this.state.dateObject.daysInMonth();
    };
    year = () => {
        return this.state.dateObject.format("Y");
    };
    currentDay = () => {
        return this.state.dateObject.format("D");
    };
    firstDayOfMonth = () => {
        let dateObject = this.state.dateObject;
        let firstDay = moment(dateObject)
            .startOf("month")
            .format("d"); // Day of week 0...1..5...6
        return firstDay;
    };
    month = () => {
        return this.state.dateObject.format("MMMM");
    };
    showMonth = (e, month) => {
        this.setState({
            showMonthTable: !this.state.showMonthTable,
            showCalendarTable: !this.state.showCalendarTable
        });
    };
    setMonth = month => {
        let monthNo = this.state.allmonths.indexOf(month);
        let dateObject = Object.assign({}, this.state.dateObject);
        dateObject = moment(dateObject).set("month", monthNo);
        this.setState({
            dateObject: dateObject,
            showMonthTable: !this.state.showMonthTable,
            showCalendarTable: !this.state.showCalendarTable
        });
    };
    MonthList = props => {
        let months = [];
        props.data.map(data => {
            months.push(
                <td
                    key={data}
                    className="calendar-month"
                    onClick={e => {
                        this.setMonth(data);
                    }}
                >
                    <span>{data}</span>
                </td>
            );
        });
        let rows = [];
        let cells = [];

        months.forEach((row, i) => {
            if (i % 3 !== 0 || i == 0) {
                cells.push(row);
            } else {
                rows.push(cells);
                cells = [];
                cells.push(row);
            }
        });
        rows.push(cells);
        let monthlist = rows.map((d, i) => {
            return <tr key={i}>{d}</tr>;
        });

        return (
            <table className="calendar-month">
                <thead>
                    <tr>
                        <th colSpan="4">Select a Month</th>
                    </tr>
                </thead>
                <tbody>{monthlist}</tbody>
            </table>
        );
    };
    showYearEditor = () => {
        this.setState({
            showYearNav: true,
            showCalendarTable: !this.state.showCalendarTable
        });
    };

    onPrev = () => {
        let curr = "";
        if (this.state.showMonthTable == true) {
            curr = "year";
        } else {
            curr = "month";
        }
        this.setState({
            dateObject: this.state.dateObject.subtract(1, curr)
        });
    };
    onNext = () => {
        let curr = "";
        if (this.state.showMonthTable == true) {
            curr = "year";
        } else {
            curr = "month";
        }
        this.setState({
            dateObject: this.state.dateObject.add(1, curr)
        });
    };
    setYear = year => {
        // alert(year)
        let dateObject = Object.assign({}, this.state.dateObject);
        dateObject = moment(dateObject).set("year", year);
        this.setState({
            dateObject: dateObject,
            showMonthTable: !this.state.showMonthTable,
            showYearNav: !this.state.showYearNav,
            showMonthTable: !this.state.showMonthTable
        });
    };
    onYearChange = e => {
        this.setYear(e.target.value);
    };
    getDates(startDate, stopDate) {
        var dateArray = [];
        var currentDate = moment(startDate);
        var stopDate = moment(stopDate);
        while (currentDate <= stopDate) {
            dateArray.push(moment(currentDate).format("YYYY"));
            currentDate = moment(currentDate).add(1, "year");
        }
        return dateArray;
    }
    YearTable = props => {
        let months = [];
        let nextten = moment()
            .set("year", props)
            .add("year", 12)
            .format("Y");

        let tenyear = this.getDates(props, nextten);

        tenyear.map(data => {
            months.push(
                <td
                    key={data}
                    className="calendar-month"
                    onClick={e => {
                        this.setYear(data);
                    }}
                >
                    <span>{data}</span>
                </td>
            );
        });
        let rows = [];
        let cells = [];

        months.forEach((row, i) => {
            if (i % 3 !== 0 || i == 0) {
                cells.push(row);
            } else {
                rows.push(cells);
                cells = [];
                cells.push(row);
            }
        });
        rows.push(cells);
        let yearlist = rows.map((d, i) => {
            return <tr key={i}>{d}</tr>;
        });

        return (
            <table className="calendar-month">
                <thead>
                    <tr>
                        <th colSpan="4">Select a Yeah</th>
                    </tr>
                </thead>
                <tbody>{yearlist}</tbody>
            </table>
        );
    };
    onDayClick = (e, d) => {
        var month = this.month().substring(0, 3);
        var mInNumber = '';
        if (month === 'Jan') {
            mInNumber = '01';
        }
        else if (month === 'Feb') {
            mInNumber = '02';
        }
        else if (month === 'Mar') {
            mInNumber = '03';
        }
        else if (month === 'Apr') {
            mInNumber = '04';
        }
        else if (month === 'May') {
            mInNumber = '05';
        }
        else if (month === 'Jun') {
            mInNumber = '06';
        }
        else if (month === 'Jul') {
            mInNumber = '07';
        }
        else if (month === 'Aug') {
            mInNumber = '08';
        }
        else if (month === 'Sep') {
            mInNumber = '09';
        }
        else if (month === 'Oct') {
            mInNumber = '10';
        }
        else if (month === 'Nov') {
            mInNumber = '11';
        }
        else if (month === 'Dec') {
            mInNumber = '12';
        }

        var inFormatedD = '' + d;
        if (inFormatedD.length == 1) {
            inFormatedD = 0 + '' + inFormatedD;
        }
        this.setState(
            {
                selectedDay: d + ' ' + this.month().substring(0, 3) + ' ' + this.year(),
                selectedDate: d,
                selectedDayInFormat: this.year() + '' + mInNumber + inFormatedD,
                selectedDayInWithoutYear: d + ' ' + this.month().substring(0, 3)
            }
        );
    };




    componentDidMount() {

        if (this.state.isOnPendapatan) {
            document.body.style = 'background : #F7F8F9!important';
        }
        else {
            document.body.style = 'background : #F7F8F9!important';
        }

        window.onpopstate = function (event) {
            window.location.reload();
        };
        wsWithoutBody(BACK_END_POINT + "/loginCtl/detailToken", GET, {
            ...HEADER_AUTH, 'secret-token': localStorage.getItem(TOKEN_AUTH)
        }).then(response => {
            var result = response.data.result;
            this.setState({
                namaLengkap: result.userOobTableSuccessDto.namaPemilikUsaha,
                nomorHandphone: result.userOobTableSuccessDto.noHandphone,
                email: result.userOobTableSuccessDto.emailPemilikUsaha,
                nomorRekening: result.userOobTableSuccessDto.nomorRekening,
                tipeAkun: result.userOobMidDtlTableDto.akunOobMidDesc
            })
        });

        wsWithoutBody(BACK_END_POINT + "/date-maker/get-date-now/7", GET, {}).then(response => {
            this.setState({ isShowDate: true, calendarMerge: response.data });
            //  debugger;
        });

        // var startTime = moment(new Date()).format("YYYYMMDD");
        // var endTime = moment(new Date()).format("YYYYMMDD");

        setInterval(this.getMyData,20000);
        this.getMyData();
        // this.getMyData(this.state.startDatePendapatan, this.state.endDatePendapatan);

        // var startTime30Days = moment(new Date()).add(-30, 'day').format('YYYYMMDD');
        // var endTime = moment(new Date()).format("YYYYMMDD");
        // this.getPaidUnpaidData(startTime30Days,endTime);
        var startTime8Days = moment(new Date()).add(-7, 'day').format('YYYYMMDD');
        var startTime1Days = moment(new Date()).add(-1, 'day').format('YYYYMMDD');
        var endTime = moment(new Date()).format("YYYYMMDD");
        this.getListPencairan(startTime8Days,startTime1Days);
        this.getListPencairanNow(endTime,endTime);
    }

    getListPencairan = (startdate,enddate) => {
        wsWithoutBody(BACK_END_POINT + "/homeScreen/getRincianPencairan/auth?startDate=" + startdate + "&endDate=" + enddate + "&isLimitValidated=false",
        GET, {
        ...HEADER_AUTH, 'secret-token': localStorage.getItem(TOKEN_AUTH)
    }).then(response => {
        var value = response.data.result;
        this.setState({ listRiwayatPencairan : value });
    }).catch(error => {
        this.getPaidUnpaidData(startdate, enddate);
    })
    }

    getListPencairanSchd = () => {
        var startTime8Days = moment(new Date()).add(-7, 'day').format('YYYYMMDD');
        var startTime1Days = moment(new Date()).add(-1, 'day').format('YYYYMMDD');
    var url = BACK_END_POINT + "/homeScreen/getRincianPencairan/auth?startDate=" + startTime8Days + "&endDate=" + startTime1Days + "&isLimitValidated=false";
    console.log('start hit list pencairan schd : ' + url);
    wsWithoutBody(url,
        GET, {
        ...HEADER_AUTH, 'secret-token': localStorage.getItem(TOKEN_AUTH)
    }).then(response => {
        var value = response.data.result;
        this.setState({ listRiwayatPencairan : value });
        console.log('header data pencairan schd : ' + JSON.stringify(value));
    }).catch(error => {
        this.getListPencairan(startTime8Days, startTime1Days);
    })
    }

    getListPencairanNow = (startdate,enddate) => {
        wsWithoutBody(BACK_END_POINT + "/homeScreen/getRincianPencairan/auth?startDate=" + startdate + "&endDate=" + enddate + "&isLimitValidated=false",
        GET, {
        ...HEADER_AUTH, 'secret-token': localStorage.getItem(TOKEN_AUTH)
    }).then(response => {
        var value = response.data.result;
        this.setState({ pencairanHariIni : value[0] });
    }).catch(error => {
        this.getPaidUnpaidData(startdate, enddate);
    })
    }

    getListPencairanNowSchd = () => {
        var endTime = moment(new Date()).format("YYYYMMDD");
        var url = BACK_END_POINT + "/homeScreen/getRincianPencairan/auth?startDate=" + endTime + "&endDate=" + endTime + "&isLimitValidated=false"
        wsWithoutBody(url,
        GET, {
        ...HEADER_AUTH, 'secret-token': localStorage.getItem(TOKEN_AUTH)
    }).then(response => {
        var value = response.data.result;
        this.setState({ pencairanHariIni : value[0] });
    }).catch(error => {
        this.getPaidUnpaidData(startdate, enddate);
    })
    }

    // getSelectedDayRiwayat = (selectedDay, selectedInFormat) => {
    //     console.log('selected day : ' + selectedDay);
    //     this.setState({ isOnLoading: true });
    //     wsWithoutBody(BACK_END_POINT + "/homeScreen/getDataTransaksi/auth/homeScreen?startDate=" + selectedInFormat + "&endDate=" + selectedInFormat + "&isLimitValidated=false",
    //     GET, {
    //     ...HEADER_AUTH, 'secret-token': localStorage.getItem(TOKEN_AUTH)
    // }).then(response => {   
    //     var value = response.data.result;
    //     this.setState({ isOnLoading: false });
    //     this.setState({ headerTrx: value });
    // }).catch(error => {

    //     this.getMyData(startdate, enddate);
    // })
    //     this.setState({ selectedDateDay: selectedDay });
    // }

    render() {

        let weekdayshortname = this.weekdayshort.map((day, i) => {
            return <th key={i}>{day.charAt(0)}</th>;
        });
        let blanks = [];
        for (let i = 0; i < this.firstDayOfMonth(); i++) {
            blanks.push(<td className="calendar-day empty">{""}</td>);
        }
        let daysInMonth = [];
        for (let d = 1; d <= this.daysInMonth(); d++) {
            let currentDay = d == this.currentDay() ? "current-today" : "";
            var h = this.currentDay();
            if (this.state.selectedDate == d) {
                currentDay = 'selected-value';
            }

            daysInMonth.push(
                <td key={d} className={currentDay} >
                    <span
                        onClick={e => {
                            this.onDayClick(e, d);
                        }}
                    >
                        {d}
                    </span>
                </td>
            );
        }
        var totalSlots = [...blanks, ...daysInMonth];
        let rows = [];
        let cells = [];

        totalSlots.forEach((row, i) => {
            if (i % 7 !== 0) {
                cells.push(row);
            } else {
                rows.push(cells);
                cells = [];
                cells.push(row);
            }
            if (i === totalSlots.length - 1) {
                // let insertRow = cells.slice();
                rows.push(cells);
            }
        });

        let daysinmonth = rows.map((d, i) => {
            return <tr key={i}>{d}</tr>;
        });
        return (
            <div>

                <LoadingMaker onLoading={this.state.isOnLoading}
                    loadingWord={'Proses login sedang berlangsung'}
                ></LoadingMaker>

                {   (this.state.isNotOnPeriode) ?
                    <div>
                        <HeaderInfoPencairan position={'fixed'} headerName={"Riwayat Transaksi"} urlBack={"/homeScreen"}></HeaderInfoPencairan>

                        {this.state.isOnSeeInformationPendapatan ?
                            <div className="fluid-on-rincian">
                                <div className="overLay-background-deflect"></div>
                                <div className="body-fluid on-custom">
                                    <div className="container-fluid">
                                        <div className="big-header-fluid">Informasi</div>
                                        <div className="small-rincian-fluid">
                                            <p>  <span class="dot"></span> <div className="div-after-dot"> <b>Total transaksi</b> adalah jumlah transaksi yang masuk sebelum dikurangi biaya admin.</div></p>
                                            <p>  <span class="dot"></span> <div className="div-after-dot"> <b>Biaya admin </b>   yang ditetapkan pemerintah sebesar 0% hingga Desember 2021. </div></p>
                                        </div>
                                        <div className="button-rincian-fluid" onClick={() => {
                                            this.setState({ isOnSeeInformationPendapatan: false })
                                        }}>Oke</div>
                                    </div>
                                </div>
                            </div>
                            : <div></div>}

                        {this.state.isOnSeeInformationPencairan ?
                            <div className="fluid-on-rincian">
                                <div className="overLay-background-deflect"></div>
                                <div className="body-fluid on-custom">
                                    <div className="container-fluid">
                                        <div className="big-header-fluid">Total Sementara</div>
                                        <div className="small-rincian-fluid">
                                            <p>- Dana dicairkan otomatis tiap hari kerja pada jam 08:00, 16:00, dan 20:00. </p>
                                            <p> - Jumlah maksimum pencairan adalah
                                                Rp 500.000 /pencairan/hari. Jika pendapatan melebihi jumlah tersebut, sisa dana akan dicairkan pada jadwal atau hari kerja berikutnya.
</p>
<p> - biaya admin yang ditetapkan pemerintah sebesar 0% hingga Desember 2021 </p>
                                        </div>
                                        <div className="button-rincian-fluid" onClick={() => {
                                            this.setState({ isOnSeeInformationPencairan: false })
                                        }}>Oke</div>
                                    </div>
                                </div>
                            </div>
                            : <div></div>}


                        {/* start show rincian --START */}
                        {!this.state.isNotOnRincianTransaksi ?
                            <div></div> :
                            <div className="fluid-on-rincian">
                                <div className="overLay-background-deflect"></div>
                                <div className="body-fluid">
                                    <div className="justify-padding-lr">
                                        <div className="relay-header">
                                            <div className="relay-header-boot">Rincian</div>
                                            <div className="relay-header-boot-x"
                                                onClick={
                                                    () => {
                                                        this.setState({ isNotOnRincianTransaksi: false })
                                                    }
                                                }>X</div>
                                        </div>
                                        <div className="Header-date">{this.state.headerTrxDetail.authDateTime}</div>
                                        <div className="Header-wording">Pembayaran berhasil via {this.state.headerTrxDetail.issuerName}</div>
                                        {/* <div className="Sub-Header-wording">
                                            {
                                                !this.state.headerTrxDetail.isTransferToRek ?
                                                    <img src={progBlue} height={16} width={16} /> :
                                                    <img src={sukIjo} height={16} width={16} />
                                            }

                                            {
                                                !this.state.headerTrxDetail.isTransferToRek ?
                                                    'Proses masuk rekening' :
                                                    'Sudah masuk rekening'
                                            }

                                        </div> */}
                                        <div className="total-diterima">
                                            <div className="wording">Total yang diterima</div>
                                            <div className="referensi">{this.state.headerTrxDetail.transferAmount}</div>
                                            {/* {!this.state.isOpenTotalDiterima ?
                                                <i className="fa fa-chevron-down custom-wagon" onClick={
                                                    () => this.setState({ isOpenTotalDiterima: true })
                                                }></i>
                                                : <i className="fa fa-chevron-up custom-wagon" onClick={
                                                    () => this.setState({ isOpenTotalDiterima: false })
                                                }></i>} */}
                                        </div>
                                        {this.state.isOpenTotalDiterima ?
                                            <div className="chevron-most-list-detail">
                                                <div className="transaksi">
                                                    <div className="transaksiWording">Transaksi</div>
                                                    <div className="transaksiMoney">{this.state.headerTrxDetail.authAmount}</div>
                                                </div>
                                                <div className="biayaAdmin">
                                                    <div className="biayaAdminWording">Biaya Admin ({this.state.headerTrxDetail.percentageFeeAmount})</div>
                                                    <div className="biayaAdminNomor">{this.state.headerTrxDetail.feeAmount}</div>
                                                </div>
                                            </div> : <div></div>
                                        }
                                        <div className="nomor-referensi">
                                            <div className="wording">Nomor Referensi</div>
                                            <div className="number">{this.state.headerTrxDetail.reffNumber}</div>
                                        </div>

                                        <div className="transaksi-detail">
                                            <div className="referensi">
                                                <span><b>Detail Transaksi</b></span>
                                                {!this.state.isOpenItemReferensi ?
                                                    <i className="fa fa-chevron-down custom-wagon"
                                                        onClick={
                                                            () => this.setState({
                                                                isOpenItemReferensi: true
                                                            })
                                                        }></i>
                                                    :
                                                    <i className="fa fa-chevron-up custom-wagon"
                                                        onClick={
                                                            () => this.setState({
                                                                isOpenItemReferensi: false
                                                            })
                                                        }></i>
                                                }
                                            </div>
                                            {this.state.isOpenItemReferensi ?
                                                <span>
                                                    <div className="items-wording">
                                                        <div className="left-item">Nama Penerbit</div>
                                                        <div className="right-item">{this.state.headerTrxDetail.issuerName}</div>
                                                    </div>
                                                    <div className="items-wording">
                                                        <div className="left-item">Nama Customer</div>
                                                        <div className="right-item">{this.state.headerTrxDetail.customerName}</div>
                                                    </div>
                                                    <div className="items-wording">
                                                        <div className="left-item">MPAN</div>
                                                        <div className="right-item">{this.state.headerTrxDetail.mpan}</div>
                                                    </div>
                                                    <div className="items-wording">
                                                        <div className="left-item">TID</div>
                                                        <div className="right-item">{this.state.headerTrxDetail.tid}</div>
                                                    </div>
                                                    <div className="items-wording">
                                                        <div className="left-item">CPAN</div>
                                                        <div className="right-item">{this.state.headerTrxDetail.cpan}</div>
                                                    </div>
                                                </span>
                                                : <div></div>}
                                        </div>
                                    </div>
                                </div>
                            </div>
                        }
                        {/* start show rincian --END */}

                        {this.state.isShowDate ?
                            <div className={this.state.isOnPendapatan ? "riwayat-transaksi-container" : "riwayat-transaksi-container brown-color-fully"}>
                                <div className={this.state.isOnPendapatan ? 'riwayat-date-transaksi' : 'riwayat-date-transaksi inRelativePosition'}>
                                    <div className="notifikasi-me-container edit-riwayat">
                                        <div className="button-anomali" style={{ left: '-1vw', zIndex:'2', position:'fixed', top:'24vw', backgroundColor:'white', width:'101%', paddingTop:'5vw',marginTop:'-5vw', paddingBottom:'1vw' }}>
                                            <div className="container-anomali">
                                                <div
                                                    onClick={() => {
                                                        this.setState({ isOnPendapatan: true })
                                                        document.body.style = 'background : #F7F8F9!important';
                                                    }}
                                                    className={this.state.isOnPendapatan ?
                                                        'anomali edit-riwayat anomali-transaksi active' : 'anomali edit-riwayat anomali-transaksi'
                                                    }>Pendapatan</div>
                                                <div
                                                    onClick={() => {
                                                        document.body.style = 'background : #F7F8F9!important';
                                                        this.setState({ isOnPendapatan: false })
                                                    }}
                                                    className=
                                                    {this.state.isOnPendapatan ?
                                                        'anomali edit-riwayat anomali-informasi' : 'anomali edit-riwayat anomali-informasi active'
                                                    }
                                                >Pencairan Dana</div>
                                            </div>
                                        </div>
                                    </div>

                                    {this.state.isOnPendapatan ?
                                        <span>


                                            <div className="bulan-tahun">
                                                {this.state.headerTrx!=null?
                                                (this.state.selectedDayInWithoutYearStartSave != null
                                                    && this.state.selectedDayInWithoutYearEndSave != null) ? 
                                                    this.state.selectedDayInWithoutYearStartSave+' - '+
                                                    this.state.selectedDayInWithoutYearEndSave: this.state.headerTrx.startDateWithoutYear
                                                 :''}
                                                </div>
                                            
                                            
                                            <div className="periode-button" onClick={
                                                () => {
                                                    document.body.style = 'background : white!important';
                                                    this.setState({ isNotOnPeriode: false, stopDate: null, startDate: null, stopDateInFormat: null, startDateInFormat: null, isNotValidDateFilter: false })
                                                }
                                            }>
                                                {/* <img className="date-flatter" src="data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHZlcnNpb249IjEuMSIgeG1sbnM6eGxpbms9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkveGxpbmsiIHhtbG5zOnN2Z2pzPSJodHRwOi8vc3ZnanMuY29tL3N2Z2pzIiB3aWR0aD0iNTEyIiBoZWlnaHQ9IjUxMiIgeD0iMCIgeT0iMCIgdmlld0JveD0iMCAwIDM2Ni44IDM2Ni44IiBzdHlsZT0iZW5hYmxlLWJhY2tncm91bmQ6bmV3IDAgMCA1MTIgNTEyIiB4bWw6c3BhY2U9InByZXNlcnZlIiBjbGFzcz0iIj48Zz4KPGcgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIj4KCTxnPgoJCTxwYXRoIGQ9Ik0zNTMuNCw3MS42Yy0yLjQtMTEuNi04LTIyLTE2LTMwYy0yLjQtMi40LTUuMi00LjgtOC40LTYuOGwtMC40LTAuNGMtMC40LTAuNC0wLjgtMC40LTEuMi0wLjhjLTEuMi0wLjgtMi40LTEuNi0zLjYtMiAgICBoLTAuNGMtOC40LTQuNC0xNy42LTYuOC0yOC02LjhoLTI0LjhWNy42YzAtNC0zLjItNy42LTcuNi03LjZjLTQsMC03LjYsMy4yLTcuNiw3LjZWMjRIMTExLjhWNy42YzAtNC0zLjYtNy42LTcuNi03LjYgICAgcy03LjYsMy4yLTcuNiw3LjZWMjRINzEuOGMtNy42LDAtMTUuMiwxLjYtMjIsNC40Yy03LjYsMy4yLTE0LjQsNy42LTIwLDEzLjJjLTMuMiwzLjItNiw2LjgtOC40LDEwLjRjLTIuNCw0LTQuNCw4LTYsMTIuNCAgICBjLTAuOCwyLjQtMS42LDQuOC0yLDcuMmMtMC44LDQtMS4yLDgtMS4yLDEydjQwVjMwOGMwLDE2LjQsNi44LDMxLjIsMTcuMiw0MS42QzQwLjIsMzYwLjQsNTUsMzY2LjgsNzEsMzY2LjhoMjI0LjggICAgYzE2LjQsMCwzMS4yLTYuOCw0MS42LTE3LjJjMTAuOC0xMC44LDE3LjItMjUuNiwxNy4yLTQxLjZWMTIzLjZ2LTQwQzM1NC42LDc5LjYsMzU0LjIsNzUuNiwzNTMuNCw3MS42eiBNMjcuOCw4My4yICAgIGMwLTMuMiwwLjQtNiwwLjgtOC44YzAuNC0yLjgsMS42LTUuNiwyLjQtOC40YzEuNi0zLjYsMy42LTYuOCw1LjYtOS42YzEuMi0xLjYsMi40LTIuOCwzLjYtNC40YzIuNC0yLjQsNS4yLTQuNCw4LTYuNCAgICBjNi44LTQsMTQuNC02LjQsMjIuOC02LjRoMjQuOHYxNi40YzAsNCwzLjIsNy42LDcuNiw3LjZjNCwwLDcuNi0zLjIsNy42LTcuNlYzOS4yaDE0NS42djE2LjRjMCw0LDMuMiw3LjYsNy42LDcuNiAgICBjNCwwLDcuNi0zLjIsNy42LTcuNlYzOS4yaDI0LjhjOC40LDAsMTYuNCwyLjQsMjIuOCw2LjRjMi44LDIsNS42LDQsOCw2LjRjNiw2LDEwLjQsMTMuNiwxMiwyMi40YzAuNCwyLjgsMC44LDYsMC44LDguOFYxMTZIMjcuOCAgICBWODMuMnogTTM0MC42LDMwNy42YzAsMTIuNC00LjgsMjMuNi0xMi44LDMxLjZTMzA4LjYsMzUyLDI5Ni42LDM1Mkg3MS44Yy0xMiwwLTIzLjItNC44LTMxLjItMTIuOFMyNy44LDMyMCwyNy44LDMwOFYxMzAuOGgzMTIuOCAgICBWMzA3LjZ6IiBmaWxsPSIjMDA3YWNmIiBkYXRhLW9yaWdpbmFsPSIjMDAwMDAwIiBzdHlsZT0iIiBjbGFzcz0iIj48L3BhdGg+Cgk8L2c+CjwvZz4KPGcgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIj4KPC9nPgo8ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciPgo8L2c+CjxnIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyI+CjwvZz4KPGcgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIj4KPC9nPgo8ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciPgo8L2c+CjxnIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyI+CjwvZz4KPGcgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIj4KPC9nPgo8ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciPgo8L2c+CjxnIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyI+CjwvZz4KPGcgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIj4KPC9nPgo8ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciPgo8L2c+CjxnIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyI+CjwvZz4KPGcgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIj4KPC9nPgo8ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciPgo8L2c+CjxnIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyI+CjwvZz4KPC9nPjwvc3ZnPg==" /> */}
                                                <svg width="3.865vw" height="3.865vw" viewBox="0 0 16 16" fill="none" xmlns="http://www.w3.org/2000/svg">
<path d="M10.6667 1.33331V3.99998M5.33333 1.33331V3.99998M2 6.66665H14M3.33333 2.66665H12.6667C13.403 2.66665 14 3.2636 14 3.99998V13.3333C14 14.0697 13.403 14.6666 12.6667 14.6666H3.33333C2.59695 14.6666 2 14.0697 2 13.3333V3.99998C2 3.2636 2.59695 2.66665 3.33333 2.66665Z" stroke="#0057E7" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
</svg>

                                                <span >
                                                    Pilih Tanggal
                                        </span>
                                            </div>

                                            <div className="header-total">
                                                <div className="total-sementara">Total Transaksi</div>
                                                <div className="before-admin">{this.state.headerTrx!=null?this.state.headerTrx.sumAllTransaksiAfterFee :''}</div>
                                              
                                            {/* <div className="sebelum-biaya">Sebelum biaya admin</div>
                                                <div className="after-admin">{this.state.headerTrx!=null?this.state.headerTrx.sumAllTransaksi:''}</div> */}
                                                <div className="svgs-header" onClick={() => {
                                                        this.setState({ isOnSeeInformationPendapatan: true });
                                                    }}>
                                                        
                                                        <svg width="5.797vw" 
                                                height="6.039vw" viewBox="0 0 24 25" fill="none" xmlns="http://www.w3.org/2000/svg">
<path d="M13 10.0636H11V17.0636H13V10.0636Z" fill="#0057E7"/>
<path d="M11 8.06516C11 7.51286 11.4477 7.06516 12 7.06516C12.5523 7.06516 13 7.51286 13 8.06516C13 8.61746 12.5523 9.06516 12 9.06516C11.4477 9.06526 11 8.61746 11 8.06516Z" fill="#0057E7"/>
<path d="M12 20.0684C7.5888 20.0684 4 16.4796 4 12.0684C4 7.65716 7.5888 4.06836 12 4.06836C16.4112 4.06836 20 7.65716 20 12.0684C20 16.4796 16.4112 20.0684 12 20.0684ZM12 22.0684C17.5229 22.0684 22 17.5913 22 12.0684C22 6.54556 17.5229 2.06836 12 2.06836C6.4771 2.06836 2 6.54556 2 12.0684C2 17.5913 6.4771 22.0684 12 22.0684Z" fill="#0057E7"/>
</svg>
</div>

                                            </div>

                                        </span>
                                        : <span>

                                            <div className="box-wielder-notsettle-new2  padding-none">
                                            
                                            <div className="container-transaksi-detail padding-none">

 <div className="list-pencairan-party-header border-none margin-none">
       <div className="header-tanggal">
           <span className="tanggal"> {this.state.pencairanHariIni.pencairanDateWithoutYear}</span>
           <span className="hari">Hari ini</span>
          
       </div>
       <div className="list-header-gradually">
        {
          this.state.pencairanHariIni.allRiwayats !=null ?   
             this.state.pencairanHariIni.allRiwayats.map((value, i) => {  
                    if(value.pencairanTypeCode==1){
                        return  <div className="pencairan1" onClick={()=>{
                                     this.setState({isOnRincian:true,listDetailRiwayatPencairanaDana:value})
                                }}>
                                    <div className="svgs-header-tips2">
                               
                                    <svg width="3.865vw" height="3.865vw" viewBox="0 0 16 16" fill="none" xmlns="http://www.w3.org/2000/svg">
<path d="M4.66634 6.66665H3.33301V12.6666H4.66634V6.66665Z" fill="#303B4A" fill-opacity="0.72"/>
<path d="M8.66634 6.66665H7.33301V12.6666H8.66634V6.66665Z" fill="#303B4A" fill-opacity="0.72"/>
<path d="M14.6663 13.3333H1.33301V14.6666H14.6663V13.3333Z" fill="#303B4A" fill-opacity="0.72"/>
<path d="M12.6663 6.66665H11.333V12.6666H12.6663V6.66665Z" fill="#303B4A" fill-opacity="0.72"/>
<path d="M7.99967 2.82405L11.6849 4.66665H4.31441L7.99967 2.82405ZM7.99967 1.33331L1.33301 4.66665V5.99998H14.6663V4.66665L7.99967 1.33331Z" fill="#303B4A" fill-opacity="0.72"/>
</svg>

                                    </div>
                                    <div className="tanggal" >
                                    <span className="idle-type">{value.pencairanType}</span> 
                                    <span className="idle-time">{value.pencairanTime}</span>
                                    </div>
                                     <div className="money">
                                         <span className="idle-type">{value.pencairanMoneyInString}</span>
                                         <span className="idle-time"> {value.countTransaksi} Transaksi</span>
                                    </div>
                                 </div>
                    }
                    else   if(value.pencairanTypeCode==2){
                        return  <div className="pencairan2" onClick={()=>{
                            this.setState({isOnRincian:true,listDetailRiwayatPencairanaDana:value})
                       }}>
                           
                           <div className="svgs-header-tips2">
                           
                           <svg width="3.865vw" height="3.865vw" viewBox="0 0 16 16" fill="none" xmlns="http://www.w3.org/2000/svg">
<path d="M4.66634 6.66665H3.33301V12.6666H4.66634V6.66665Z" fill="#303B4A" fill-opacity="0.72"/>
<path d="M8.66634 6.66665H7.33301V12.6666H8.66634V6.66665Z" fill="#303B4A" fill-opacity="0.72"/>
<path d="M14.6663 13.3333H1.33301V14.6666H14.6663V13.3333Z" fill="#303B4A" fill-opacity="0.72"/>
<path d="M12.6663 6.66665H11.333V12.6666H12.6663V6.66665Z" fill="#303B4A" fill-opacity="0.72"/>
<path d="M7.99967 2.82405L11.6849 4.66665H4.31441L7.99967 2.82405ZM7.99967 1.33331L1.33301 4.66665V5.99998H14.6663V4.66665L7.99967 1.33331Z" fill="#303B4A" fill-opacity="0.72"/>
</svg>
                                    </div>
                                    <div className="tanggal" >
                                    <span className="idle-type">{value.pencairanType}</span> 
                                    <span className="idle-time">{value.pencairanTime}</span>
                                    </div>
                                     <div className="money">
                                         <span className="idle-type">{value.pencairanMoneyInString}</span>
                                         <span className="idle-time"> {value.countTransaksi} Transaksi</span>
                                    </div>
                        </div>
                    }
                    else if(value.pencairanTypeCode==3){
                        return  <div className="pencairan3" onClick={()=>{
                            this.setState({isOnRincian:true,listDetailRiwayatPencairanaDana:value})
                       }}>
                        <div className="svgs-header-tips2">
                      
                        <svg width="3.865vw" height="3.865vw" viewBox="0 0 16 16" fill="none" xmlns="http://www.w3.org/2000/svg">
<path d="M4.66634 6.66665H3.33301V12.6666H4.66634V6.66665Z" fill="#303B4A" fill-opacity="0.72"/>
<path d="M8.66634 6.66665H7.33301V12.6666H8.66634V6.66665Z" fill="#303B4A" fill-opacity="0.72"/>
<path d="M14.6663 13.3333H1.33301V14.6666H14.6663V13.3333Z" fill="#303B4A" fill-opacity="0.72"/>
<path d="M12.6663 6.66665H11.333V12.6666H12.6663V6.66665Z" fill="#303B4A" fill-opacity="0.72"/>
<path d="M7.99967 2.82405L11.6849 4.66665H4.31441L7.99967 2.82405ZM7.99967 1.33331L1.33301 4.66665V5.99998H14.6663V4.66665L7.99967 1.33331Z" fill="#303B4A" fill-opacity="0.72"/>
</svg>

                                    </div>
                                    <div className="tanggal" >
                                    <span className="idle-type">{value.pencairanType}</span> 
                                    <span className="idle-time">{value.pencairanTime}</span>
                                    </div>
                                     <div className="money">
                                         <span className="idle-type">{value.pencairanMoneyInString}</span>
                                         <span className="idle-time"> {value.countTransaksi} Transaksi</span>
                                    </div>
                          </div>
                    }
                    else if(value.pencairanTypeCode==-1){
                        return  <div className="pencairan3" onClick={()=>{
                            this.setState({isOnRincian:true,listDetailRiwayatPencairanaDana:value})
                       }}>
                        <div className="svgs-header-tips2">
                      
                        <svg width="3.865vw" height="3.865vw" viewBox="0 0 16 16" fill="none" xmlns="http://www.w3.org/2000/svg">
<path d="M4.66634 6.66665H3.33301V12.6666H4.66634V6.66665Z" fill="#303B4A" fill-opacity="0.72"/>
<path d="M8.66634 6.66665H7.33301V12.6666H8.66634V6.66665Z" fill="#303B4A" fill-opacity="0.72"/>
<path d="M14.6663 13.3333H1.33301V14.6666H14.6663V13.3333Z" fill="#303B4A" fill-opacity="0.72"/>
<path d="M12.6663 6.66665H11.333V12.6666H12.6663V6.66665Z" fill="#303B4A" fill-opacity="0.72"/>
<path d="M7.99967 2.82405L11.6849 4.66665H4.31441L7.99967 2.82405ZM7.99967 1.33331L1.33301 4.66665V5.99998H14.6663V4.66665L7.99967 1.33331Z" fill="#303B4A" fill-opacity="0.72"/>
</svg>

                                    </div>
                                    <div className="tanggal" >
                                    <span class="idle-type">Pencairan {value.pencairanBeforeWithoutYear}</span>
                                    <span className="idle-time">{value.pencairanTime}</span>
                                    </div>
                                     <div className="money">
                                         <span className="idle-type">{value.pencairanMoneyInString}</span>
                                         <span className="idle-time"> {value.countTransaksi} Transaksi</span>
                                    </div>
                          </div>
                    }
                    else if(value.pencairanTypeCode==-2){
                        return  <div className="pencairan3" onClick={()=>{
                            this.setState({isOnRincian:true,listDetailRiwayatPencairanaDana:value})
                       }}>
                        <div className="svgs-header-tips2">
                      
                        <svg width="3.865vw" height="3.865vw" viewBox="0 0 16 16" fill="none" xmlns="http://www.w3.org/2000/svg">
<path d="M4.66634 6.66671H3.33301V12.6667H4.66634V6.66671Z" fill="#303B4A" fill-opacity="0.24"/>
<path d="M8.66634 6.66671H7.33301V12.6667H8.66634V6.66671Z" fill="#303B4A" fill-opacity="0.24"/>
<path d="M14.6663 13.3334H1.33301V14.6667H14.6663V13.3334Z" fill="#303B4A" fill-opacity="0.24"/>
<path d="M12.6663 6.66671H11.333V12.6667H12.6663V6.66671Z" fill="#303B4A" fill-opacity="0.24"/>
<path d="M7.99967 2.82411L11.6849 4.66671H4.31441L7.99967 2.82411ZM7.99967 1.33337L1.33301 4.66671V6.00004H14.6663V4.66671L7.99967 1.33337Z" fill="#303B4A" fill-opacity="0.24"/>
</svg>

                                    </div>
                                    <div className="tanggal" >
                                    <span className="idle-type">{value.pencairanType}</span> 
                                    <span className="idle-time">dijadwalkan hari kerja berikutnya</span>
                                    </div>
                                     <div className="money">
                                         <span className="idle-type">{value.pencairanMoneyInString}</span>
                                         <span className="idle-time"> {value.countTransaksi} Transaksi</span>
                                    </div>
                          </div>
                    }
                })
        
         :''
        }
        </div>
        <div className="pencairan-total">
                <div className="wording">Total sementara</div>
                <div className="money">{this.state.pencairanHariIni.totalPencairanInString}</div>
                <div className="svgs-custom-v3"onClick={() => {
                                            this.setState({ isOnSeeInformationPencairan: true })
                                        }}>
                <svg width="5.797vw" height="5.797vw" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
<path d="M13 9.56356H11V16.5636H13V9.56356Z" fill="#0057E7"/>
<path d="M11 7.56516C11 7.01286 11.4477 6.56516 12 6.56516C12.5523 6.56516 13 7.01286 13 7.56516C13 8.11746 12.5523 8.56516 12 8.56516C11.4477 8.56526 11 8.11746 11 7.56516Z" fill="#0057E7"/>
<path d="M12 19.5684C7.5888 19.5684 4 15.9796 4 11.5684C4 7.15716 7.5888 3.56836 12 3.56836C16.4112 3.56836 20 7.15716 20 11.5684C20 15.9796 16.4112 19.5684 12 19.5684ZM12 21.5684C17.5229 21.5684 22 17.0913 22 11.5684C22 6.04556 17.5229 1.56836 12 1.56836C6.4771 1.56836 2 6.04556 2 11.5684C2 17.0913 6.4771 21.5684 12 21.5684Z" fill="#0057E7"/>
</svg>

                </div>
       </div>
    </div>
    


                                        </div>


                                            </div>

                                        </span>
                                    }
                                </div>


                                {this.state.isOnPendapatan ?
                                    <span>
                                        {/* start show list transaksi --START */}
                                        {this.state.headerTrx != null ?
                                            <div className="container-transaksi-detail">
                                                <div className="total-transaksi-container">
                                        <div className="header">Transaksi {(this.state.selectedDayInWithoutYearStartSave != null
                                                    && this.state.selectedDayInWithoutYearEndSave != null) ? 
                                                    this.state.selectedDayInWithoutYearStartSave+' - '+this.state.selectedDayInWithoutYearEndSave:'hari ini'}</div>
                                                    <div className="money">{this.state.headerTrx.countAllTransaksi} Transaksi</div>
                                                </div>

                                                <div className="detail-big-container">
                                                    {
                                                        this.state.headerTrx.detail.map((value, i) => (
                                                            <div key={i} className="detail-riwayat-container"

                                                                onClick={
                                                                    () => {
                                                                        this.setState({ isNotOnRincianTransaksi: true, rincianProses: value.proses, headerTrxDetail: value.detail })
                                                                    }
                                                                }
                                                            >

                                                                <div className="riwayat-pembayaran">
                                                                    
                                                                    <div className="header">
                                                                        {/* {value.cpanNumberInMask} */}
                                                                        <p> </p>
                                                                    </div>
                                                                    <div className="footer">Pembayaran via {value.issuerPayment} </div>
                                                                    <div className="riwayat-number">{value.number}</div>
                                                                </div>
                                                                <div className="riwayat-money">
                                                                    <div className="header">{value.detail.transferAmount}</div>
                                                                    <div className="footer">{value.time} WIB</div>
                                                                </div>
                                                               

                                                            </div>
                                                        ))
                                                    }
                                                </div>
                                            </div>
                                            : <div className="container-transaksi-detail">


                                            </div>}
                                        {/* start show list transaksi --END */}
                                    </span>
                                    : <span>


                                        <div className="container-transaksi-detail overlay-margin-bottom-pencairan adjust-paddingtop-custom-101vw">
                                        <div className="header-7hari">7 hari terakhir</div>  
{
  this.state.listRiwayatPencairan.map((value, i) => (
    <div className="list-pencairan-party">
       <div className="header-tanggal">
           <span className="tanggal">
           {value.pencairanDateWithoutYear}
           </span>
           <span className="hari">
           {value.namaHari}
           </span>
       </div>
        { value.allRiwayats !=null?
             value.allRiwayats.map((value, i) => {  
                    if(value.pencairanTypeCode==1 || value.pencairanTypeCode==2 || value.pencairanTypeCode==3){
                        return  <div className="pencairan1" onClick={()=>{
                                     this.setState({isOnRincian:true,listDetailRiwayatPencairanaDana:value})
                                }}>
                                         <div className="svgs-header-tips2">
                        <svg width="3.865vw" height="3.865vw" viewBox="0 0 16 16" fill="none" xmlns="http://www.w3.org/2000/svg">
<path d="M4.66634 6.66665H3.33301V12.6666H4.66634V6.66665Z" fill="#303B4A" fill-opacity="0.72"/>
<path d="M8.66634 6.66665H7.33301V12.6666H8.66634V6.66665Z" fill="#303B4A" fill-opacity="0.72"/>
<path d="M14.6663 13.3333H1.33301V14.6666H14.6663V13.3333Z" fill="#303B4A" fill-opacity="0.72"/>
<path d="M12.6663 6.66665H11.333V12.6666H12.6663V6.66665Z" fill="#303B4A" fill-opacity="0.72"/>
<path d="M7.99967 2.82405L11.6849 4.66665H4.31441L7.99967 2.82405ZM7.99967 1.33331L1.33301 4.66665V5.99998H14.6663V4.66665L7.99967 1.33331Z" fill="#303B4A" fill-opacity="0.72"/>
</svg></div>
                                    <div className="tanggal">
                                    <span class="idle-type">{value.pencairanType}</span>
                                    <span class="idle-time">{value.pencairanTime}</span> </div>
                                     <div className="money">
                                     <span class="idle-type"> {value.pencairanMoneyInString}</span>
                                    <span class="idle-time">{value.countTransaksi} transaksi</span>
                                        </div>
                                 </div>
                    }
                    else if(value.pencairanTypeCode==-1){
                        return  <div className="pencairan1" onClick={()=>{
                            this.setState({isOnRincian:true,listDetailRiwayatPencairanaDana:value})
                       }}>
                                <div className="svgs-header-tips2">
               <svg width="3.865vw" height="3.865vw" viewBox="0 0 16 16" fill="none" xmlns="http://www.w3.org/2000/svg">
<path d="M4.66634 6.66665H3.33301V12.6666H4.66634V6.66665Z" fill="#303B4A" fill-opacity="0.72"/>
<path d="M8.66634 6.66665H7.33301V12.6666H8.66634V6.66665Z" fill="#303B4A" fill-opacity="0.72"/>
<path d="M14.6663 13.3333H1.33301V14.6666H14.6663V13.3333Z" fill="#303B4A" fill-opacity="0.72"/>
<path d="M12.6663 6.66665H11.333V12.6666H12.6663V6.66665Z" fill="#303B4A" fill-opacity="0.72"/>
<path d="M7.99967 2.82405L11.6849 4.66665H4.31441L7.99967 2.82405ZM7.99967 1.33331L1.33301 4.66665V5.99998H14.6663V4.66665L7.99967 1.33331Z" fill="#303B4A" fill-opacity="0.72"/>
</svg></div>
                           <div className="tanggal">
                    <span class="idle-type">Pencairan {value.pencairanBeforeWithoutYear}</span>
                           <span class="idle-time">{value.pencairanTime}</span> </div>
                            <div className="money">
                            <span class="idle-type"> {value.pencairanMoneyInString}</span>
                           <span class="idle-time">{value.countTransaksi} transaksi</span>
                               </div>
                        </div>
                    }
                    else if(value.pencairanTypeCode==-2){
                        return  <div className="pencairan1" onClick={()=>{
                            this.setState({isOnRincian:true,listDetailRiwayatPencairanaDana:value})
                       }}>
                                <div className="svgs-header-tips2">
                                <svg width="3.865vw" height="3.865vw" viewBox="0 0 16 16" fill="none" xmlns="http://www.w3.org/2000/svg">
<path d="M4.66634 6.66671H3.33301V12.6667H4.66634V6.66671Z" fill="#303B4A" fill-opacity="0.24"/>
<path d="M8.66634 6.66671H7.33301V12.6667H8.66634V6.66671Z" fill="#303B4A" fill-opacity="0.24"/>
<path d="M14.6663 13.3334H1.33301V14.6667H14.6663V13.3334Z" fill="#303B4A" fill-opacity="0.24"/>
<path d="M12.6663 6.66671H11.333V12.6667H12.6663V6.66671Z" fill="#303B4A" fill-opacity="0.24"/>
<path d="M7.99967 2.82411L11.6849 4.66671H4.31441L7.99967 2.82411ZM7.99967 1.33337L1.33301 4.66671V6.00004H14.6663V4.66671L7.99967 1.33337Z" fill="#303B4A" fill-opacity="0.24"/>
</svg>

</div>
                           <div className="tanggal">
                           <span class="idle-type">{value.pencairanType}</span>
                           <span class="idle-time">dijadwalkan hari kerja berikutnya</span> </div>
                            <div className="money">
                            <span class="idle-type"> {value.pencairanMoneyInString}</span>
                           <span class="idle-time">{value.countTransaksi} transaksi</span>
                               </div>
                        </div>
                    }
                })
        :''
            }
        <div className="pencairan-total">
                <div className="wording">TOTAL</div>
                <div className="money">{value.totalPencairanInString}</div>
       </div>
    </div>
    
  ))
}

                                        </div>

                                        <div className="container-transaksi-detail">
                                            {this.state.isOnRincian ?

                                                <div className="overlay-bottom-rincian z-index-2">
                                                    <div className="overLay-background-deflect">
                                                    </div>
                                                    <div className="rincian-detail-rekening">
                                                        <div className="header">Detail Pencairan Dana</div>
                                                        <div className="topHeader">
                                            <div className="money">{this.state.listDetailRiwayatPencairanaDana.pencairanMoneyInString}</div>
                                            <div className="wording-footer">{this.state.listDetailRiwayatPencairanaDana.pencairanType} - {this.state.listDetailRiwayatPencairanaDana.pencairanDate}, {this.state.listDetailRiwayatPencairanaDana.pencairanTime}</div>
                                                        </div>
                                                        {/* <div className="bottom-divider"></div> */}
                                                        <div className="alowanceHeader">
                                                            <div className="potongan">Pendapatan sebelum dipotong biaya</div>
                                                            <div className="potongan-money">{this.state.listDetailRiwayatPencairanaDana.pencairanMoneyWithoutFeeInString}</div>

                                                           {this.state.isOnRincianChevronUp? 
                                                            <div className="svgs" onClick={()=>{
                                                                this.setState({isOnRincianChevronUp:false})
                                                            }}>
                                                            <svg width="3.467vw" height="2.133vw" viewBox="0 0 13 8" fill="none" xmlns="http://www.w3.org/2000/svg">
<path fill-rule="evenodd" clip-rule="evenodd" d="M10.8849 7.70498L12.2949 6.29498L6.29492 0.294984L0.294923 6.29498L1.70492 7.70498L6.29492 3.12498L10.8849 7.70498Z" fill="#615A5A"/>
</svg>

                                                            </div> :
                                                            <div className="svgs"  onClick={()=>{
                                                                this.setState({isOnRincianChevronUp:true})
                                                            }}>
                                                            <svg width="3.467vw" height="2.133vw" viewBox="0 0 13 8" fill="none" xmlns="http://www.w3.org/2000/svg">
<path fill-rule="evenodd" clip-rule="evenodd" d="M2.11508 0.295013L0.705078 1.70501L6.70508 7.70501L12.7051 1.70501L11.2951 0.295013L6.70508 4.87501L2.11508 0.295013Z" fill="#615A5A"/>
</svg>


                                                            </div>
    }

{this.state.isOnRincianChevronUp? 
                                                        <div className="list-transaksi-dicairkan">
                                                            {this.state.listDetailRiwayatPencairanaDana !=null && 
                                                               this.state.listDetailRiwayatPencairanaDana.riwayatPencairanDtlRincianDtos != null?
                                                               this.state.listDetailRiwayatPencairanaDana.riwayatPencairanDtlRincianDtos.map((value, i) => (
                                                                    <span>
                                                                    <div className="wording">Pembayaran via {value.issuerName}</div>
                                                                       <div className="time">{value.authTime}</div>
                                                                      <div className="money">{value.authNumberInRupiah}</div>
                                                                    </span>
                                                                ))
                                                                : <span></span>
                                                            }
                                                        </div>
                                                        : <div></div>
    }
                                                        </div>
                                                        <div className="biayaHeader">
                                                            <div className="containerStruts">
                                                                <div className="biaya">Biaya Admin</div>
                                                                <div className="biaya-footer">0,7% dari pendapatan</div>
                                                            </div>
                                                            <div className="biaya-money">{this.state.listDetailRiwayatPencairanaDana.feeAmountInString}</div>
                                                        </div>
                                                        <div className="button-close-rincian" onClick={
                                                            () => {
                                                                this.setState({
                                                                    isOnRincian: false
                                                                })
                                                            }
                                                        }>
                                                            <span>
                                                                Oke
                          </span>
                                                        </div>
                                                    </div>

                                                </div> : <div></div>
                                            }

                                        </div>

                                    </span>
                                }
                            </div>
                            :
                            <div></div>
                        }
                    </div>
                    :
                    <div>
                        <div>
                            {!this.state.isOncheckPeriode ?
                                <div className="container-pilih-periode">
                                    <div className="x-container-width" onClick={
                                        () => {
                                            document.body.style = 'background : #F7F8F9!important';
                                            this.setState({ isNotOnPeriode: true })
                                        }
                                    }>X</div>
                                    {this.state.isNotValidDateFilter ?
                                        <div className="error-wording-tanggal">
                                            <div className="header">Data tidak dapat ditampilkan</div>
                                            <div className="body"
                                            >{this.state.wordingIsNotValidPeriode}</div>
                                        </div>
                                        : <div></div>}
                                    <div className="suplementary-div">
                                        <div className="header-periode">Pilih Periode</div>
                                        <div className="subHeader-periode">Pencarian periode transaksi yang dapat dipilih 7 hari dengan maksimal transaksi 3 bulan terakhir</div>
                                        <span>
                                            <div className="tanggal-list ">
                                                <div className={!this.state.isStartDate ? "start" : "start blue-print"}
                                                    onClick={
                                                        () => {
                                                            this.setState({
                                                                isOpenCalendar: true,
                                                                isEndDate: false,
                                                                isStartDate: true
                                                            })
                                                        }
                                                    }
                                                >
                                                    <div className="word">Dari Tanggal</div>
                                                    <div className={this.state.startDate == null ? "date" : "date blue-print"}>
                                                        {this.state.startDate == null ? 'Pilih tanggal' : this.state.startDate}
                                                    </div>
                                                </div>
                                                <i className="fa fa-chevron-right custom-wagon"></i>
                                                <div className={!this.state.isEndDate ? "end" : "end blue-print"}
                                                    onClick={
                                                        () => {
                                                            this.setState({
                                                                isOpenCalendar: true,
                                                                isEndDate: true,
                                                                isStartDate: false
                                                            })
                                                        }
                                                    }
                                                >
                                                    <div className="word">Hingga Tanggal</div>
                                                    <div className={this.state.stopDate == null ? "date" : "date blue-print"}>
                                                        {this.state.stopDate == null ? 'Pilih tanggal' : this.state.stopDate}</div>
                                                </div>
                                            </div>
                                            {this.state.isOpenCalendar ?
                                                <div className="custom-j-calendar">
                                                    <div className="tail-datetime-calendar">
                                                        <i className="fa fa-chevron-left custom-wagon" onClick={this.onPrev}></i>
                                                        {!this.state.showMonthTable && !this.state.showYearEditor && (
                                                            <span
                                                                className="calendar-label-month"
                                                                class="calendar-label"
                                                            >
                                                                {this.month()}
                                                            </span>
                                                        )}
                                                        <span
                                                            className="calendar-label-year"
                                                        >
                                                            {this.year()}
                                                        </span>
                                                        <i className="fa fa-chevron-right custom-wagon" onClick={this.onNext}></i>
                                                        {this.state.showCalendarTable && (
                                                            <div className="calendar-date">
                                                                <table className="calendar-day">
                                                                    <thead>
                                                                        <tr>{weekdayshortname}</tr>
                                                                    </thead>
                                                                    <tbody>{daysinmonth}</tbody>
                                                                </table>
                                                            </div>
                                                        )}
                                                        <span className="batal"
                                                            onClick={
                                                                () => {
                                                                    this.setState({
                                                                        isOpenCalendar: false,
                                                                        isEndDate: false,
                                                                        isStartDate: false
                                                                    })
                                                                }
                                                            }
                                                        >Batal</span>
                                                        <span className="ok" onClick={
                                                            () => {
                                                                if (this.state.isEndDate) {
                                                                    this.setState({
                                                                        stopDateInFormat: this.state.selectedDayInFormat,
                                                                        stopDate: this.state.selectedDay,
                                                                        selectedDayInWithoutYearEnd: this.state.selectedDayInWithoutYear
                                                                    },
                                                                        () => {
                                                                            this.getDetailTask();
                                                                        }
                                                                    );
                                                                }

                                                                if (this.state.isStartDate) {
                                                                    this.setState({
                                                                        startDateInFormat: this.state.selectedDayInFormat,
                                                                        startDate: this.state.selectedDay,
                                                                        selectedDayInWithoutYearStart: this.state.selectedDayInWithoutYear

                                                                    },
                                                                        () => {
                                                                            this.getDetailTask();

                                                                        }
                                                                    );
                                                                }


                                                                this.setState({
                                                                    isOpenCalendar: false,
                                                                    isEndDate: false,
                                                                    isStartDate: false
                                                                })


                                                                console.log('stopDate' + this.state.stopDate + " startdate " + this.state.startDate)

                                                                //this.props.valueCalendar = this.state.selectedDay;
                                                                //localStorage.setItem('selectedDayValue',this.state.selectedDay);
                                                            }
                                                        }>OK</span>
                                                    </div>
                                                </div> : <div></div>
                                            }
                                        </span>
                                    </div>

                                </div>
                                :
                                <div className="container-pilih-periode">
                                    <div>Hai</div>
                                </div>
                            }
                        </div>
                    </div>

                }
            </div>
        );
    }
}