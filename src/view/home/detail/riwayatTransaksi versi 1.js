import React from 'react';
import { BACK_END_POINT, GET, HEADER_AUTH, POST, TOKEN_AUTH, wsWithBody, wsWithoutBody } from '../../../master/masterComponent'
import '../detail/pencairanDetail.scss';
import HeaderInfo from '../../../master/HeaderInfo';
import moment from 'moment';
import ChevronRightIcon from '@material-ui/icons/ChevronRight';
import CalendarMaker from './CalendarMaker';
import HeaderInfoPencairan from '../../../master/HeaderInfoPencairan';
import prog from '../../../../public/img/in-progress-gray.png'
import progBlue from '../../../../public/img/in-progress.png'
import suk from '../../../../public/img/success.png'
import sukIjo from '../../../../public/img/success_ijo.png'
export default class riwayatTransaksi extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            isYouKnowExpands: true,
            namaLengkap: '',
            nomorHandphone: true,
            email: '',
            nomorRekening: '',
            tipeAkun: '',
            maksimalPembayaranPerHari: '',
            isOnNotifikasiBar: true,
            isShowDate: false,
            isNotOnPeriode: true,
            isNotOnRincianTransaksi: false,
            isOncheckPeriode: false,
            calendarMerge: {},
            valueCalender: '',
            showCalendarTable: true,
            showMonthTable: false,
            dateObject: moment(),
            allmonths: moment.months(),
            showYearNav: false,
            selectedDate: null,
            selectedDay: null,
            selectedDayInWithoutYear: null,
            selectedDayInFormat: null,
            selectedDayInWithoutYearStart: null,
            selectedDayInWithoutYearEnd: null,
            isStartDate: false,
            isEndDate: false,
            startDate: null,
            stopDate: null,
            isOpenCalendar: false,
            selectedDateDay: null,
            isOpenItemReferensi : false,
            isOpenTotalDiterima:false,
            pointOnDate: 5,
            detailRiwayatTransaksiSelected: null,
            rincianProses: 0,
            headerTrx: null,
            detailRiwayatTransaksi: [{
                tanggal: 5,
                totalTransaksi: 'Rp 290.000',
                allData: [{
                    number:"05",
                    viaPembayaran: "Pembayaran via OVO (**811)",
                    waktuPembayaran: "11.00",
                    jumlahPembayaran: "Rp 150.000",
                    biayaAdminWording: "Biaya (Rp 350)",
                    proses: 0
                }, {
                    number:"04",
                    viaPembayaran: "Pembayaran via Mandiri (**811)",
                    waktuPembayaran: "12.00",
                    jumlahPembayaran: "Rp 110.000",
                    biayaAdminWording: "Biaya (Rp 350)",
                    proses: 0
                }, {
                    number:"03",
                    viaPembayaran: "Pembayaran via GOPAY (**113)",
                    waktuPembayaran: "12.35",
                    jumlahPembayaran: "Rp 10.000",
                    biayaAdminWording: "Biaya (Rp 50)",
                    proses: 1
                }, {
                    number:"02",
                    viaPembayaran: "Pembayaran via GOPAY (**123)",
                    waktuPembayaran: "14.00",
                    jumlahPembayaran: "Rp 10.000",
                    biayaAdminWording: "Biaya (Rp 50)",
                    proses: 1
                }, {
                    number:"01",
                    viaPembayaran: "Pembayaran via GOPAY (**811)",
                    waktuPembayaran: "14.00",
                    jumlahPembayaran: "Rp 10.000",
                    biayaAdminWording: "Biaya (Rp 50)",
                    proses: 0
                }]
            },

            {
                tanggal: 6,
                totalTransaksi: 'Rp 600.000',
                allData: [{
                    number:"06",
                    viaPembayaran: "Pembayaran via TOP UP (**811)",
                    waktuPembayaran: "11.00",
                    jumlahPembayaran: "Rp 130.000",
                    biayaAdminWording: "Biaya (Rp 350)",
                    proses: 1
                }, {
                    number:"05",
                    viaPembayaran: "Pembayaran via LINK AJA (**811)",
                    waktuPembayaran: "12.00",
                    jumlahPembayaran: "Rp 210.000",
                    biayaAdminWording: "Biaya (Rp 350)",
                    proses: 0
                }, {
                    number:"04",
                    viaPembayaran: "Pembayaran via GOPAY (**113)",
                    waktuPembayaran: "12.35",
                    jumlahPembayaran: "Rp 25.000",
                    biayaAdminWording: "Biaya (Rp 50)",
                    proses: 0
                }, {
                    number:"03",
                    viaPembayaran: "Pembayaran via GOPAY (**123)",
                    waktuPembayaran: "14.00",
                    jumlahPembayaran: "Rp 115.000",
                    biayaAdminWording: "Biaya (Rp 50)",
                    proses: 1
                }, {
                    number:"02",
                    viaPembayaran: "Pembayaran via MANDIRI (**811)",
                    waktuPembayaran: "14.00",
                    jumlahPembayaran: "Rp 14.000",
                    biayaAdminWording: "Biaya (Rp 50)",
                    proses: 0
                },
                , {
                    number:"01",
                    viaPembayaran: "Pembayaran via MANDIRI (**811)",
                    waktuPembayaran: "14.00",
                    jumlahPembayaran: "Rp 18.000",
                    biayaAdminWording: "Biaya (Rp 50)",
                    proses: 1
                }]
            }, 

            {
                tanggal: 7,
                totalTransaksi: 'Rp 1.000.000',
                allData: [{
                    number:"09",
                    viaPembayaran: "Pembayaran via OVO (**811)",
                    waktuPembayaran: "11.00",
                    jumlahPembayaran: "Rp 1.000.000",
                    biayaAdminWording: "Biaya (Rp 350)",
                    proses: 1
                }, {
                    number:"08",
                    viaPembayaran: "Pembayaran via MANDIRI (**811)",
                    waktuPembayaran: "12.00",
                    jumlahPembayaran: "Rp 200.000",
                    biayaAdminWording: "Biaya (Rp 350)",
                    proses: 0
                }, {
                    number:"07",
                    viaPembayaran: "Pembayaran via SERIKAT (**113)",
                    waktuPembayaran: "12.35",
                    jumlahPembayaran: "Rp 15.000",
                    biayaAdminWording: "Biaya (Rp 50)",
                    proses: 1
                }, {
                    number:"06",
                    viaPembayaran: "Pembayaran via GOPAY (**123)",
                    waktuPembayaran: "14.00",
                    jumlahPembayaran: "Rp 115.000",
                    biayaAdminWording: "Biaya (Rp 50)",
                    proses: 0
                }, {
                    number:"05",
                    viaPembayaran: "Pembayaran via MANDIRI (**811)",
                    waktuPembayaran: "14.00",
                    jumlahPembayaran: "Rp 14.000",
                    biayaAdminWording: "Biaya (Rp 50)",
                    proses: 1
                },
                , {
                    number:"04",
                    viaPembayaran: "Pembayaran via MANDIRI (**811)",
                    waktuPembayaran: "14.00",
                    jumlahPembayaran: "Rp 18.000",
                    biayaAdminWording: "Biaya (Rp 50)",
                    proses: 0
                },
                , {
                    number:"03",
                    viaPembayaran: "Pembayaran via MANDIRI (**811)",
                    waktuPembayaran: "14.00",
                    jumlahPembayaran: "Rp 18.000",
                    biayaAdminWording: "Biaya (Rp 50)",
                    proses: 1
                },
                , {
                    number:"02",
                    viaPembayaran: "Pembayaran via MANDIRI (**811)",
                    waktuPembayaran: "14.00",
                    jumlahPembayaran: "Rp 18.000",
                    biayaAdminWording: "Biaya (Rp 50)",
                    proses: 0
                },
                , {
                    number:"01",
                    viaPembayaran: "Pembayaran via MANDIRI (**811)",
                    waktuPembayaran: "14.00",
                    jumlahPembayaran: "Rp 30.000",
                    biayaAdminWording: "Biaya (Rp 50)",
                    proses: 1
                }]
            }

            ]

        }

     

       
    }


    getMyData = (startdate, enddate) => {

        wsWithoutBody(BACK_END_POINT + "/homeScreen/getDataTransaksi/auth?startDate=" + startdate + "&endDate=" + enddate,
            GET, {
            ...HEADER_AUTH, 'secret-token': localStorage.getItem(TOKEN_AUTH)
        }).then(response => {
            var value = response.data.result;
            this.setState({headerTrx:value});
        }).catch(error => {
            this.getMyData(startdate, enddate);
        })
    }


    getDetailTask = () => {
        console.log(this.state.startDate + ' ' + this.state.stopDate)
        if (this.state.stopDate != null && this.state.startDate != null) {
            this.setState({ isNotOnPeriode: true })
        }
    }

    weekdayshort = moment.weekdaysShort();

    daysInMonth = () => {
        return this.state.dateObject.daysInMonth();
    };
    year = () => {
        return this.state.dateObject.format("Y");
    };
    currentDay = () => {
        return this.state.dateObject.format("D");
    };
    firstDayOfMonth = () => {
        let dateObject = this.state.dateObject;
        let firstDay = moment(dateObject)
            .startOf("month")
            .format("d"); // Day of week 0...1..5...6
        return firstDay;
    };
    month = () => {
        return this.state.dateObject.format("MMMM");
    };
    showMonth = (e, month) => {
        this.setState({
            showMonthTable: !this.state.showMonthTable,
            showCalendarTable: !this.state.showCalendarTable
        });
    };
    setMonth = month => {
        let monthNo = this.state.allmonths.indexOf(month);
        let dateObject = Object.assign({}, this.state.dateObject);
        dateObject = moment(dateObject).set("month", monthNo);
        this.setState({
            dateObject: dateObject,
            showMonthTable: !this.state.showMonthTable,
            showCalendarTable: !this.state.showCalendarTable
        });
    };
    MonthList = props => {
        let months = [];
        props.data.map(data => {
            months.push(
                <td
                    key={data}
                    className="calendar-month"
                    onClick={e => {
                        this.setMonth(data);
                    }}
                >
                    <span>{data}</span>
                </td>
            );
        });
        let rows = [];
        let cells = [];

        months.forEach((row, i) => {
            if (i % 3 !== 0 || i == 0) {
                cells.push(row);
            } else {
                rows.push(cells);
                cells = [];
                cells.push(row);
            }
        });
        rows.push(cells);
        let monthlist = rows.map((d, i) => {
            return <tr key={i}>{d}</tr>;
        });

        return (
            <table className="calendar-month">
                <thead>
                    <tr>
                        <th colSpan="4">Select a Month</th>
                    </tr>
                </thead>
                <tbody>{monthlist}</tbody>
            </table>
        );
    };
    showYearEditor = () => {
        this.setState({
            showYearNav: true,
            showCalendarTable: !this.state.showCalendarTable
        });
    };

    onPrev = () => {
        let curr = "";
        if (this.state.showMonthTable == true) {
            curr = "year";
        } else {
            curr = "month";
        }
        this.setState({
            dateObject: this.state.dateObject.subtract(1, curr)
        });
    };
    onNext = () => {
        let curr = "";
        if (this.state.showMonthTable == true) {
            curr = "year";
        } else {
            curr = "month";
        }
        this.setState({
            dateObject: this.state.dateObject.add(1, curr)
        });
    };
    setYear = year => {
        // alert(year)
        let dateObject = Object.assign({}, this.state.dateObject);
        dateObject = moment(dateObject).set("year", year);
        this.setState({
            dateObject: dateObject,
            showMonthTable: !this.state.showMonthTable,
            showYearNav: !this.state.showYearNav,
            showMonthTable: !this.state.showMonthTable
        });
    };
    onYearChange = e => {
        this.setYear(e.target.value);
    };
    getDates(startDate, stopDate) {
        var dateArray = [];
        var currentDate = moment(startDate);
        var stopDate = moment(stopDate);
        while (currentDate <= stopDate) {
            dateArray.push(moment(currentDate).format("YYYY"));
            currentDate = moment(currentDate).add(1, "year");
        }
        return dateArray;
    }
    YearTable = props => {
        let months = [];
        let nextten = moment()
            .set("year", props)
            .add("year", 12)
            .format("Y");

        let tenyear = this.getDates(props, nextten);

        tenyear.map(data => {
            months.push(
                <td
                    key={data}
                    className="calendar-month"
                    onClick={e => {
                        this.setYear(data);
                    }}
                >
                    <span>{data}</span>
                </td>
            );
        });
        let rows = [];
        let cells = [];

        months.forEach((row, i) => {
            if (i % 3 !== 0 || i == 0) {
                cells.push(row);
            } else {
                rows.push(cells);
                cells = [];
                cells.push(row);
            }
        });
        rows.push(cells);
        let yearlist = rows.map((d, i) => {
            return <tr key={i}>{d}</tr>;
        });

        return (
            <table className="calendar-month">
                <thead>
                    <tr>
                        <th colSpan="4">Select a Yeah</th>
                    </tr>
                </thead>
                <tbody>{yearlist}</tbody>
            </table>
        );
    };
    onDayClick = (e, d) => {
        this.setState(
            {
                selectedDay: d + ' ' + this.month().substring(0, 3) + ' ' + this.year(),
                selectedDate: d,
                selectedDayInWithoutYear: d + ' ' + this.month().substring(0, 3)
            }
        );
    };




    componentDidMount() {
        window.onpopstate = function(event) {
            window.location.reload();
        };
        wsWithoutBody(BACK_END_POINT + "/loginCtl/detailToken", GET, {
            ...HEADER_AUTH, 'secret-token': localStorage.getItem(TOKEN_AUTH)
        }).then(response => {
            var result = response.data.result;
            this.setState({
                namaLengkap: result.userOobTableSuccessDto.namaPemilikUsaha,
                nomorHandphone: result.userOobTableSuccessDto.noHandphone,
                email: result.userOobTableSuccessDto.emailPemilikUsaha,
                nomorRekening: result.userOobTableSuccessDto.nomorRekening,
                tipeAkun: result.userOobMidDtlTableDto.akunOobMidDesc
            })
        });

        wsWithoutBody(BACK_END_POINT + "/date-maker/get-date-now/7", GET, {}).then(response => {
            this.setState({ isShowDate: true, calendarMerge: response.data });
            //  debugger;
        });

        var startTime = moment(new Date()).format("YYYYMMDD");
        var endTime = moment(new Date()).format("YYYYMMDD");
        this.getMyData(startTime, endTime);

        this.state.detailRiwayatTransaksi.map((value) => {

            if (value.tanggal == this.state.pointOnDate) {
                this.setState({ detailRiwayatTransaksiSelected: value })
            }
        })

    }

    getSelectedDayRiwayat = (selectedDay,selectedInFormat) =>{
        console.log('selected day : ' + selectedDay);
        this.getMyData(selectedInFormat,selectedInFormat);
        // if((selectedDay%3)==0){
        //     console.log('selected day : ' + selectedDay + " in 5");
        // this.state.detailRiwayatTransaksi.map((value)=>{
        //       if(value.tanggal == 5){
        //           this.setState({
        //               detailRiwayatTransaksiSelected : value
        //           })
        //       }  
        // })}
        // else if((selectedDay%3)==1){
        //     console.log('selected day : ' + selectedDay + " in 6");
        // this.state.detailRiwayatTransaksi.map((value)=>{
        //       if(value.tanggal == 6){
        //           this.setState({
        //               detailRiwayatTransaksiSelected : value
        //           })
        //       }  
        // })}
        // else{
        //    this.state.detailRiwayatTransaksi.map((value)=>{
        //     console.log('selected day : ' + selectedDay + " in 7");
        //         if(value.tanggal == 7){
        //             this.setState({
        //                 detailRiwayatTransaksiSelected : value
        //             })
        //         }  
        //   })
        // }
        // console.log('selected day riwayat : ' + this.state.detailRiwayatTransaksiSelected.allData);
        this.setState({ selectedDateDay: selectedDay });
    }

    render() {
        
        let weekdayshortname = this.weekdayshort.map((day, i) => {
            return <th key={i}>{day.charAt(0)}</th>;
        });
        let blanks = [];
        for (let i = 0; i < this.firstDayOfMonth(); i++) {
            blanks.push(<td className="calendar-day empty">{""}</td>);
        }
        let daysInMonth = [];
        for (let d = 1; d <= this.daysInMonth(); d++) {
            let currentDay = d == this.currentDay() ? "current-today" : "";
            var h = this.currentDay();
            if (this.state.selectedDate == d) {
                currentDay = 'selected-value';
            }

            daysInMonth.push(
                <td key={d} className={currentDay} >
                    <span
                        onClick={e => {
                            this.onDayClick(e, d);
                        }}
                    >
                        {d}
                    </span>
                </td>
            );
        }
        var totalSlots = [...blanks, ...daysInMonth];
        let rows = [];
        let cells = [];

        totalSlots.forEach((row, i) => {
            if (i % 7 !== 0) {
                cells.push(row);
            } else {
                rows.push(cells);
                cells = [];
                cells.push(row);
            }
            if (i === totalSlots.length - 1) {
                // let insertRow = cells.slice();
                rows.push(cells);
            }
        });

        let daysinmonth = rows.map((d, i) => {
            return <tr key={i}>{d}</tr>;
        });
        return (
            <div>
                {   (this.state.isNotOnPeriode) ?
                    <div>
                        <HeaderInfoPencairan position={'fixed'} headerName={"Riwayat Transaksi"} urlBack={"/homeScreen"}></HeaderInfoPencairan>
                        
                        {/* start show rincian --START */}
                        {!this.state.isNotOnRincianTransaksi ?
                        <div></div>:
                        <div className="fluid-on-rincian">
                                <div className="overLay-background-deflect"></div>
                                <div className="body-fluid">
                                    <div className="justify-padding-lr">
                                    <div className="relay-header">
                                       <div className="relay-header-boot">Rincian</div>
                                       <div className="relay-header-boot-x"
                                        onClick={
                                            () => {
                                                this.setState({ isNotOnRincianTransaksi: false })
                                            }
                                        }>X</div>
                                    </div>
                                    <div className="Header-date">29 Agu 2020  10:00</div>
                                    <div className="Header-wording">Pembayaran berhasil via OVO</div>
                                    <div className="Sub-Header-wording">
                                        {/* <i className="fa fa-hourglass"></i> */}
                                        {
                                            this.state.rincianProses === 0 ?
                                            <img src={progBlue} height={16} width={16}/> :
                                            <img src={sukIjo} height={16} width={16}/>
                                        }

                                        {
                                            this.state.rincianProses === 0 ?
                                            'Proses masuk rekening':
                                            'Sudah masuk rekening'
                                        }
                                        
                                    </div>
                                    <div className="total-diterima">
                                        <div className="wording">Total yang diterima</div>
                                        <div className="referensi">Rp 34.999</div>
                                        { !this.state.isOpenTotalDiterima?
                                        <i className="fa fa-chevron-down custom-wagon" onClick={
                                            () => this.setState({isOpenTotalDiterima : true})
                                        }></i>
                                         :  <i className="fa fa-chevron-up custom-wagon"onClick={
                                            () => this.setState({isOpenTotalDiterima : false})
                                        }></i>}
                                    </div>
                                    { this.state.isOpenTotalDiterima?
                                    <div className="chevron-most-list-detail">
                                            <div className="transaksi">
                                            <div className="transaksiWording">Transaksi</div>
                                            <div className="transaksiMoney">Rp 35.000</div>
                                            </div>
                                            <div className="biayaAdmin">
                                            <div className="biayaAdminWording">Biaya Admin (0,7%)</div>
                                            <div className="biayaAdminNomor">1</div>
                                            </div>
                                    </div>:<div></div>
                                    }
                                    <div className="nomor-referensi">
                                        <div className="wording">Nomor Referensi</div>
                                        <div className="number">6517528163nxsjy</div>
                                    </div>
                                    
                                    <div className="transaksi-detail">
                                        <div className="referensi">
                                            <span>Transaksi Detail</span>
                                            { !this.state.isOpenItemReferensi?
                                            <i className="fa fa-chevron-down custom-wagon"
                                            onClick={
                                                () =>this.setState({
                                                    isOpenItemReferensi : true
                                                })
                                            }></i>
                                            :
                                            <i className="fa fa-chevron-up custom-wagon"
                                            onClick={
                                                () =>this.setState({
                                                    isOpenItemReferensi : false
                                                })
                                            }></i>
                                            }
                                        </div>
                                        { this.state.isOpenItemReferensi?
                                        <span>
                                        <div className="items-wording">
                                            <div className="left-item">Nama Penerbit</div>
                                            <div className="right-item">OVO</div>
                                        </div>
                                        <div className="items-wording">
                                            <div className="left-item">Nama Customer</div>
                                            <div className="right-item">Daud</div>
                                        </div>
                                        <div className="items-wording">
                                            <div className="left-item">MPAN</div>
                                            <div className="right-item">9360000786218</div>
                                        </div>
                                        <div className="items-wording">
                                            <div className="left-item">TID</div>
                                            <div className="right-item">A01</div>
                                        </div>
                                        <div className="items-wording">
                                            <div className="left-item">CPAN</div>
                                            <div className="right-item">9360000783334</div>
                                        </div>
                                        </span>
                                        :<div></div> }
                                        </div>
                                    </div>
                                </div>
                            </div>
                        }
                           {/* start show rincian --END */}

                        {this.state.isShowDate ?
                            <div className="riwayat-transaksi-container">
                                <div className="riwayat-date-transaksi">
                                    <div className="bulan-tahun">{this.state.calendarMerge.bulanDesc + " " + this.state.calendarMerge.tahun}</div>
                                    <div className="periode-button" onClick={
                                        () => {
                                            this.setState({ isNotOnPeriode: false })
                                        }
                                       }>
                                    {/* <img className="date-flatter" src="data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHZlcnNpb249IjEuMSIgeG1sbnM6eGxpbms9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkveGxpbmsiIHhtbG5zOnN2Z2pzPSJodHRwOi8vc3ZnanMuY29tL3N2Z2pzIiB3aWR0aD0iNTEyIiBoZWlnaHQ9IjUxMiIgeD0iMCIgeT0iMCIgdmlld0JveD0iMCAwIDM2Ni44IDM2Ni44IiBzdHlsZT0iZW5hYmxlLWJhY2tncm91bmQ6bmV3IDAgMCA1MTIgNTEyIiB4bWw6c3BhY2U9InByZXNlcnZlIiBjbGFzcz0iIj48Zz4KPGcgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIj4KCTxnPgoJCTxwYXRoIGQ9Ik0zNTMuNCw3MS42Yy0yLjQtMTEuNi04LTIyLTE2LTMwYy0yLjQtMi40LTUuMi00LjgtOC40LTYuOGwtMC40LTAuNGMtMC40LTAuNC0wLjgtMC40LTEuMi0wLjhjLTEuMi0wLjgtMi40LTEuNi0zLjYtMiAgICBoLTAuNGMtOC40LTQuNC0xNy42LTYuOC0yOC02LjhoLTI0LjhWNy42YzAtNC0zLjItNy42LTcuNi03LjZjLTQsMC03LjYsMy4yLTcuNiw3LjZWMjRIMTExLjhWNy42YzAtNC0zLjYtNy42LTcuNi03LjYgICAgcy03LjYsMy4yLTcuNiw3LjZWMjRINzEuOGMtNy42LDAtMTUuMiwxLjYtMjIsNC40Yy03LjYsMy4yLTE0LjQsNy42LTIwLDEzLjJjLTMuMiwzLjItNiw2LjgtOC40LDEwLjRjLTIuNCw0LTQuNCw4LTYsMTIuNCAgICBjLTAuOCwyLjQtMS42LDQuOC0yLDcuMmMtMC44LDQtMS4yLDgtMS4yLDEydjQwVjMwOGMwLDE2LjQsNi44LDMxLjIsMTcuMiw0MS42QzQwLjIsMzYwLjQsNTUsMzY2LjgsNzEsMzY2LjhoMjI0LjggICAgYzE2LjQsMCwzMS4yLTYuOCw0MS42LTE3LjJjMTAuOC0xMC44LDE3LjItMjUuNiwxNy4yLTQxLjZWMTIzLjZ2LTQwQzM1NC42LDc5LjYsMzU0LjIsNzUuNiwzNTMuNCw3MS42eiBNMjcuOCw4My4yICAgIGMwLTMuMiwwLjQtNiwwLjgtOC44YzAuNC0yLjgsMS42LTUuNiwyLjQtOC40YzEuNi0zLjYsMy42LTYuOCw1LjYtOS42YzEuMi0xLjYsMi40LTIuOCwzLjYtNC40YzIuNC0yLjQsNS4yLTQuNCw4LTYuNCAgICBjNi44LTQsMTQuNC02LjQsMjIuOC02LjRoMjQuOHYxNi40YzAsNCwzLjIsNy42LDcuNiw3LjZjNCwwLDcuNi0zLjIsNy42LTcuNlYzOS4yaDE0NS42djE2LjRjMCw0LDMuMiw3LjYsNy42LDcuNiAgICBjNCwwLDcuNi0zLjIsNy42LTcuNlYzOS4yaDI0LjhjOC40LDAsMTYuNCwyLjQsMjIuOCw2LjRjMi44LDIsNS42LDQsOCw2LjRjNiw2LDEwLjQsMTMuNiwxMiwyMi40YzAuNCwyLjgsMC44LDYsMC44LDguOFYxMTZIMjcuOCAgICBWODMuMnogTTM0MC42LDMwNy42YzAsMTIuNC00LjgsMjMuNi0xMi44LDMxLjZTMzA4LjYsMzUyLDI5Ni42LDM1Mkg3MS44Yy0xMiwwLTIzLjItNC44LTMxLjItMTIuOFMyNy44LDMyMCwyNy44LDMwOFYxMzAuOGgzMTIuOCAgICBWMzA3LjZ6IiBmaWxsPSIjMDA3YWNmIiBkYXRhLW9yaWdpbmFsPSIjMDAwMDAwIiBzdHlsZT0iIiBjbGFzcz0iIj48L3BhdGg+Cgk8L2c+CjwvZz4KPGcgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIj4KPC9nPgo8ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciPgo8L2c+CjxnIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyI+CjwvZz4KPGcgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIj4KPC9nPgo8ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciPgo8L2c+CjxnIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyI+CjwvZz4KPGcgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIj4KPC9nPgo8ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciPgo8L2c+CjxnIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyI+CjwvZz4KPGcgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIj4KPC9nPgo8ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciPgo8L2c+CjxnIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyI+CjwvZz4KPGcgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIj4KPC9nPgo8ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciPgo8L2c+CjxnIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyI+CjwvZz4KPC9nPjwvc3ZnPg==" /> */}
                                    <svg className="date-flatter" width="5.430vw" height="5.430vw" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
<path d="M19.0004 6V4H17.0004V6H7.0004V4H5.0004V6H2V20H22V6H19.0004ZM20 8V10H4V8H20ZM4 18V12H20V18H4Z" fill="#007ACF"/>
<path fillRule="evenodd" clipRule="evenodd" d="M16.0002 16.3081L15.1165 17.1918L14.0559 16.1312L14.9396 15.2474L14.0557 14.3635L15.1163 13.3028L16.0002 14.1868L16.8843 13.3027L17.9449 14.3634L17.0609 15.2474L17.9448 16.1313L16.8841 17.1919L16.0002 16.3081Z" fill="#007DFE"/>
</svg>
                                        <span >
                                      Periode
                                        </span>
                                    </div>
                                    <div className="tanggal" style={{ position: 'relative', top: '4vw', left:'-2vw' }}>
                                        {(this.state.selectedDayInWithoutYearStart != null
                                            && this.state.selectedDayInWithoutYearEnd != null) ?
                                            <div className="selected-date" >
                                                <div className="start">{this.state.selectedDayInWithoutYearStart}</div>
                                                <div className="strip"> - </div>
                                                <div className="end">{this.state.selectedDayInWithoutYearEnd}</div>
                                            </div>
                                            :
                                            this.state.calendarMerge.calendarDayDateDtos.map((date, i) => {
                                                if (this.state.selectedDateDay == null) {
                                                    if (date.isActiveDay) {
                                                        return <div key={i} className="detail active"
                                                            onClick={ () => this.getSelectedDayRiwayat(date.tanggal, date.formatIn)}
                                                        >
                                                            <div className="tanggal">{date.tanggal}</div>
                                                            <div className="hari">{date.hari}</div>
                                                        </div>
                                                    }
                                                    else {
                                                        return <div key={i} className="detail"
                                                        onClick={ () => this.getSelectedDayRiwayat(date.tanggal,date.formatIn)}>
                                                            <div className="tanggal">{date.tanggal}</div>
                                                            <div className="hari">{date.hari}</div>
                                                        </div>
                                                    }
                                                } else {
                                                    if (this.state.selectedDateDay == date.tanggal) {
                                                        return <div className="detail active"
                                                        onClick={ () => this.getSelectedDayRiwayat(date.tanggal,date.formatIn)}>
                                                            <div className="tanggal">{date.tanggal}</div>
                                                            <div className="hari">{date.hari}</div>
                                                        </div>
                                                    }
                                                    else {
                                                        return <div className="detail"
                                                        onClick={ () => this.getSelectedDayRiwayat(date.tanggal,date.formatIn)}>
                                                            <div className="tanggal">{date.tanggal}</div>
                                                            <div className="hari">{date.hari}</div>
                                                        </div>
                                                    }
                                                }
                                            })
                                        }
                                    </div>
                                </div>


  {/* start show list transaksi --START */}
  {this.state.headerTrx !=null ? 
                                <div className="container-transaksi-detail">
                                    <div className="total-transaksi-container">
                                        <div className="header">TOTAL TRANSAKSI</div>
                                        <div className="money">{this.state.headerTrx.sumAllTransaksi}</div>
                                        <div className="footer">Belum termasuk biaya admin</div>
                                        <div className="divider"></div>
                                    </div>

                                    <div className="detail-big-container">
                                        {
                                            this.state.detailRiwayatTransaksiSelected.allData.map((value, i) => (
                                                <div key={i} className="detail-riwayat-container"
                                                
                                                    onClick={
                                                        () => {
                                                            this.setState({ isNotOnRincianTransaksi: true, rincianProses: value.proses })
                                                        }
                                                    }
                                                >
                                                    
                                                    <div className="riwayat-pembayaran">
                                                    {value.proses === 0 ? 
       
<svg width="4.831vw" height="4.831vw" viewBox="0 0 20 20" fill="none" xmlns="http://www.w3.org/2000/svg">
<circle cx="10" cy="10" r="10" fill="#65768B"/>
<path d="M13.0001 8.94169C13.3531 8.7064 13.5295 8.35346 13.5295 7.94169V5.29463C13.5295 4.64758 13.0001 4.11816 12.3531 4.11816H7.64717C7.00011 4.11816 6.4707 4.64758 6.4707 5.29463V7.94169C6.4707 8.35346 6.64717 8.7064 7.00011 8.94169L8.23541 9.76522L8.64717 10.0593L7.00011 11.177C6.64717 11.3535 6.4707 11.7652 6.4707 12.1182V14.7064C6.4707 15.3535 7.00011 15.8829 7.64717 15.8829H12.3531C13.0001 15.8829 13.5295 15.3535 13.5295 14.7064V12.1182C13.5295 11.7064 13.3531 11.3535 13.0001 11.1182L11.2942 10.0005L11.7648 9.7064L13.0001 8.94169ZM12.3531 12.1182V14.7064H7.64717V12.1182L10.0001 10.5299L10.8236 11.0593L12.3531 12.1182ZM10.0001 9.52993L8.58835 8.58875L7.64717 7.94169V5.29463H12.3531V7.94169L11.4119 8.58875L10.0001 9.52993Z" fill="white"/>
<path d="M11.7648 13.5299H8.23541V14.1182H11.7648V13.5299Z" fill="white"/>
<path d="M8.23541 7.35346V7.75781L8.58835 8.00052L8.8847 8.19805V7.76522H11.1156V8.19805L11.4119 8.00052L11.7648 7.75787V7.35346H8.23541Z" fill="white"/>
<path d="M9.11782 8.35346L10.0001 8.94169L10.8825 8.35346H9.11782Z" fill="white"/>
<path d="M8.8847 7.76522V8.19805L9.11782 8.35346H10.8825L11.1156 8.19805V7.76522H8.8847Z" fill="white"/>
</svg>

                                                       :
<svg width="4.831vw" height="4.831vw" viewBox="0 0 20 20" fill="none" xmlns="http://www.w3.org/2000/svg">
<circle cx="10" cy="10" r="10" fill="white"/>
<path fill-rule="evenodd" clip-rule="evenodd" d="M10 0C4.48 0 0 4.48 0 10C0 15.52 4.48 20 10 20C15.52 20 20 15.52 20 10C20 4.48 15.52 0 10 0ZM8 15L3 9.99996L4.41 8.58996L8 12.17L15.59 4.57996L17 5.99996L8 15Z" fill="#007ACF"/>
</svg>

                                                        }
                                                <div className="header">{value.viaPembayaran}</div>
                                                <div className="footer">{value.waktuPembayaran}</div>
                                                    </div>
                                                    <div className="riwayat-money">
                                                <div className="header">{value.jumlahPembayaran}</div>
                                                <div className="footer">{value.biayaAdminWording}</div>
                                                    </div>
                                                <div className="riwayat-number">{value.number}</div>

                                                </div>
                                            ))
                                        }
                                    </div>
                                </div>
    : <div className="container-transaksi-detail"></div>}
{/* start show list transaksi --END */}
                                <div className="footer-static">
                                    <div className="blue">Lihat Pencairan</div>
                                    <div className="grey">Total yang sudah masuk rekening</div>
                                    <i className="fa fa-chevron-right custom-wagon" onClick={
                                        () => {
                                            this.props.history.push('/pencairanSaya');
                                            location.reload();
                                        }
                                    }></i>
                                </div>

                            </div>
                            :
                            <div></div>
                        }
                    </div>
                    :
                     <div>
                        <div>
                            {!this.state.isOncheckPeriode ?
                                <div className="container-pilih-periode">
                                    <div className="x-container-width" onClick={
                                        () => {
                                            this.setState({ isNotOnPeriode: true })
                                        }
                                    }>X</div>
                                    {/* <div className="error-wording-tanggal">
                                        <div className="header">Data tidak dapat ditampilkan</div>
                                        <div className="body"
                                                            >Pencarian transaksi 
                                                              maksimum 7 hari 
                                                              dalam periode 3 bulan terakhir</div>
                                    </div> */}
                                    <div className="suplementary-div">
                                    <div className="header-periode">Pilih Periode</div>
                                    <div className="subHeader-periode">Transaksi yang ditampilkan adalah
                                                minimal 7 hari dan  maksimal 90 hari</div>
                                    <span>
                                        <div className="tanggal-list ">
                                            <div className={!this.state.isStartDate ? "start" : "start blue-print"}
                                                onClick={
                                                    () => {
                                                        this.setState({
                                                            isOpenCalendar: true,
                                                            isEndDate: false,
                                                            isStartDate: true
                                                        })
                                                    }
                                                }
                                            >
                                                <div className="word">Dari Tanggal</div>
                                                <div className={this.state.startDate == null ? "date" : "date blue-print"}>
                                                    {this.state.startDate == null ? 'Pilih tanggal' : this.state.startDate}
                                                </div>
                                            </div>
                                            <i className="fa fa-chevron-right custom-wagon"></i>
                                            <div className={!this.state.isEndDate ? "end" : "end blue-print"}
                                                onClick={
                                                    () => {
                                                        this.setState({
                                                            isOpenCalendar: true,
                                                            isEndDate: true,
                                                            isStartDate: false
                                                        })
                                                    }
                                                }
                                            >
                                                <div className="word">Hingga Tanggal</div>
                                                <div className={this.state.stopDate == null ? "date" : "date blue-print"}>
                                                    {this.state.stopDate == null ? 'Pilih tanggal' : this.state.stopDate}</div>
                                            </div>
                                        </div>
                                        {this.state.isOpenCalendar ?
                                            <div className="custom-j-calendar">
                                                <div className="tail-datetime-calendar">
                                                    <i className="fa fa-chevron-left custom-wagon" onClick={this.onPrev}></i>
                                                    {!this.state.showMonthTable && !this.state.showYearEditor && (
                                                        <span
                                                            className="calendar-label-month"
                                                            class="calendar-label"
                                                        >
                                                            {this.month()}
                                                        </span>
                                                    )}
                                                    <span
                                                        className="calendar-label-year"
                                                    >
                                                        {this.year()}
                                                    </span>
                                                    <i className="fa fa-chevron-right custom-wagon" onClick={this.onNext}></i>
                                                    {this.state.showCalendarTable && (
                                                        <div className="calendar-date">
                                                            <table className="calendar-day">
                                                                <thead>
                                                                    <tr>{weekdayshortname}</tr>
                                                                </thead>
                                                                <tbody>{daysinmonth}</tbody>
                                                            </table>
                                                        </div>
                                                    )}
                                                    <span className="batal"
                                                        onClick={
                                                            () => {
                                                                this.setState({
                                                                    isOpenCalendar: false,
                                                                    isEndDate: false,
                                                                    isStartDate: false
                                                                })
                                                            }
                                                        }
                                                    >Batal</span>
                                                    <span className="ok" onClick={
                                                        () => {
                                                            if (this.state.isEndDate) {
                                                                this.setState({
                                                                    stopDate: this.state.selectedDay,
                                                                    selectedDayInWithoutYearEnd: this.state.selectedDayInWithoutYear
                                                                },
                                                                    () => {
                                                                        this.getDetailTask();
                                                                    }
                                                                );
                                                            }

                                                            if (this.state.isStartDate) {
                                                                this.setState({
                                                                    startDate: this.state.selectedDay,
                                                                    selectedDayInWithoutYearStart: this.state.selectedDayInWithoutYear
                                                                   
                                                                },
                                                                    () => {
                                                                        this.getDetailTask();

                                                                    }
                                                                );
                                                            }


                                                            this.setState({
                                                                isOpenCalendar: false,
                                                                isEndDate: false,
                                                                isStartDate: false
                                                            })


                                                            console.log('stopDate' + this.state.stopDate + " startdate " + this.state.startDate)

                                                            //this.props.valueCalendar = this.state.selectedDay;
                                                            //localStorage.setItem('selectedDayValue',this.state.selectedDay);
                                                        }
                                                    }>OK</span>
                                                </div>
                                            </div> : <div></div>
                                        }
                                    </span>
                                    </div>
                                
                                </div>
                                :
                                <div className="container-pilih-periode">
                                    <div>Hai</div>
                                </div>
                            }
                        </div>
                    </div>
                      
                    }
            </div>
        );
    }
}