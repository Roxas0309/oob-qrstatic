import React, { Component } from "react";
import Link from "@material-ui/core/Link";
import ArrowBackIcon from "@material-ui/icons/ArrowBack";
import { AppBar, Box, Button, Card, Grid, Toolbar, Typography } from "@material-ui/core";

class kontenMeningkatPenjualan extends Component {

  componentWillUnmount(){
    location.reload();
  }

  render() {
    const { loading = false } = this.props;

  
    return (
      <div>
        {/* <div style={{ marginLeft: "20px", marginTop: "24px" }}>
          <Link href="/startReg">
            <ArrowBackIcon />
          </Link>
        </div> */}
        <AppBar style={{backgroundColor: 'transparent', position: 'absolute', 
                boxShadow: 'unset'}}>
            <Toolbar  style={{minHeight: '60px'}}>
            {/* onClick={this.props.history.push('/startReg', {from: '/termsCondition'})} */}
                <Link href="/homeScreen" style={{position: 'absolute', display: 'flex'}} >
                    <ArrowBackIcon/>
                </Link>
            </Toolbar>
        </AppBar>
        <Grid
          style={{
            // padding: "10px",
            justifyContent: "center",
            alignItems: "center",
            //marginInline: "20px",
            width: '100% !important',
            color: "black",
            textAlign:"justify"
          }}
        >
          <div style={{padding:'16px'}}>
          <h3
            style={{
              marginTop: "70px",
              fontFamily: "Montserrat",
              fontStyle: "normal",
              fontWeight: "bold",
              fontSize: "18px",
              lineHeight: "150%",
              color: "#121518",
            }}
          >
         Selamat Anda telah bergabung di Aplikasi ini.
          </h3>

          <p>Melalui aplikasi ini Anda dapat menikmati kelebihan dari fitur-fitur yang kami berikan:</p>
            
            <ul>
              <li>Anda dapat langsung menampilkan, membagi dan menyimpan QRIS sesuai kebutuhan.</li>
              <li>Transaksi yang masuk bisa dilihat di notifikasi.</li>
              <li>Dana transaksi dapat dilihat di pencairan untuk memantau yang sudah masuk ke rekening Anda.</li>
              <li>Yuk tingkatkan penerimaan transaksimu dan nikmati akun bisnis dengan limitasi pencairan harian lebih besar.</li>
            </ul>
          </div>
        </Grid>
      </div>
    );
  }
}

export default kontenMeningkatPenjualan;
