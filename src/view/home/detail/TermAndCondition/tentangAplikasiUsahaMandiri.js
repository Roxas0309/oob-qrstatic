import React, { Component } from "react";
import Link from "@material-ui/core/Link";
import ArrowBackIcon from "@material-ui/icons/ArrowBack";
import { AppBar, Box, Button, Card, Grid, Toolbar, Typography } from "@material-ui/core";

class tentangAplikasiUsahaMandiri extends Component {

  componentWillUnmount(){
    location.reload();
  }

  render() {
    const { loading = false } = this.props;

  
    return (
      <div>
        {/* <div style={{ marginLeft: "20px", marginTop: "24px" }}>
          <Link href="/startReg">
            <ArrowBackIcon />
          </Link>
        </div> */}
        <AppBar style={{backgroundColor: 'transparent', position: 'absolute', 
                boxShadow: 'unset'}}>
            <Toolbar  style={{minHeight: '60px'}}>
            {/* onClick={this.props.history.push('/startReg', {from: '/termsCondition'})} */}
                <Link href="/profileSaya" style={{position: 'absolute', display: 'flex'}} >
                    <ArrowBackIcon/>
                </Link>
            </Toolbar>
        </AppBar>
        <Grid
          style={{
            // padding: "10px",
            justifyContent: "center",
            alignItems: "center",
            //marginInline: "20px",
            width: '100% !important',
            color: "black",
            textAlign:"justify"
          }}
        >
          <div style={{padding:'16px'}}>
          <h3
            style={{
              marginTop: "70px",
              fontFamily: "Montserrat",
              textAlign: "center",
              fontStyle: "normal",
              fontWeight: "bold",
              fontSize: "18px",
              marginRight:'94px',
              marginLeft:'94px',
              lineHeight: "150%",
              color: "black",
            }}
          >
           TENTANG MORIS
          </h3>

          <p>MORIS menggabungkan proses pengajuan usaha dan penyediaan dashboard guna memantau aktivitas penerimaan Transaksi Anda. Dapatkan kemudahan dan kenyamanan sebagai Pemilik Usaha untuk memperoleh pelayanan dan informasi lengkap seputar Transaksi menggunakan QRIS.</p>
<p style={{color:'#121518', fontWeight:'700'}}>SATU AKSES</p>
<p>Satu user ID dan password untuk akses Web MORIS.</p>
<p style={{color:'#121518', fontWeight:'700'}} >KEUNGGULAN</p>
<p>Hadir dengan inovasi “Cuan Tanpa Henti dengan Satu Aplikasi”, MORIS dapat menerima transaksi dimana pun dan kapan pun. Selain itu, adanya dashboard dengan beragam fitur dapat mempermudah Anda dalam melakukan aktivitas penerimaan Transaksi.</p>
<p>Fitur yang dapat digunakan:</p>
<ol>
    <li>Penerimaan Transaksi menggunakan QRIS</li>
    <li>Riwayat Transaksi</li>
    <li>Notifikasi</li>
    <li>Memantau pencairan dana ke Rekening</li>
    <li>Upgrade akun untuk dapat menikmati kelebihan lainnya</li>
</ol>
<p style={{color:'#121518', fontWeight:'700'}} >KEAMANAN</p>
<p>Pastikan No. HP terdaftar di Bank (digunakan pada saat pembukaan rekening tabungan/ giro sebagai rekening penampungan) untuk dapat menggunakan Layanan ini.</p>
<p>Untuk informasi lebih lanjut hubungi Call Center HiYokke 14021 terkait Layanan ini atau kunjungi Cabang Bank Mandiri terdekat untuk pembaharuan data Nasabah.</p>
          </div>
        </Grid>
      </div>
    );
  }
}

export default tentangAplikasiUsahaMandiri;
