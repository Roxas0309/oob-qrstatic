import React, { Component } from "react";
import Link from "@material-ui/core/Link";
import ArrowBackIcon from "@material-ui/icons/ArrowBack";
import { AppBar, Box, Button, Card, Grid, Toolbar, Typography } from "@material-ui/core";

class kebijakanPrivasiNHukum extends Component {

  componentWillUnmount(){
    location.reload();
  }

  render() {
    const { loading = false } = this.props;

  
    return (
      <div>
        {/* <div style={{ marginLeft: "20px", marginTop: "24px" }}>
          <Link href="/startReg">
            <ArrowBackIcon />
          </Link>
        </div> */}
        <AppBar style={{backgroundColor: 'transparent', position: 'absolute', 
                boxShadow: 'unset'}}>
            <Toolbar  style={{minHeight: '60px'}}>
            {/* onClick={this.props.history.push('/startReg', {from: '/termsCondition'})} */}
                <Link href="/profileSaya" style={{position: 'absolute', display: 'flex'}} >
                    <ArrowBackIcon/>
                </Link>
            </Toolbar>
        </AppBar>
        <Grid
          style={{
            // padding: "10px",
            justifyContent: "center",
            alignItems: "center",
            //marginInline: "20px",
            width: '100% !important',
            color: "black",
            textAlign:"justify"
          }}
        >
          <div style={{padding:'16px'}}>
          <h3
            style={{
              marginTop: "70px",
              fontFamily: "Montserrat",
              textAlign: "center",
              fontStyle: "normal",
              fontWeight: "bold",
              fontSize: "18px",
              lineHeight: "150%",
              color: "black",
            }}
          >
           Kebijakan Privasi
          </h3>
         
         <p>Kebijakan Privasi ini disertakan sebagai bagian yang tidak terpisahkan dengan Syarat dan Ketentuan. </p>

<p style={{fontWeight:'700', color: "#6B7984"}}>KEBIJAKAN PRIVASI</p>
<p>Data Nasabah sebagai Pemilik Usaha, semua transaksi perbankan dan informasi rekening lainnya yang berada di Bank Mandiri merupakan rahasia Bank yang patut dilindungi oleh Bank dan mengacu pada ketentuan perundangan yang berlaku di Indonesia.</p>
<p>Bank Mandiri tidak akan memperlihatkan, menukar atau menjual data yang terkait dengan Nasabah atau pengguna MORIS.</p>
<p>Selain itu Bank Mandiri akan menjaga kerahasian data pengguna MORIS, dan hanya orang tertentu yang berhak untuk mengakses informasi tersebut untuk digunakan sebagaimana mestinya (dalam hal ini Bank Mandiri akan selalu mengingatkan karyawan atau pihak terkait yang membantu dalam pengelolaan acquiring akan pentingnya menjaga kerahasian data Nasabah). </p>
          
<p style={{fontWeight:'700', color: "#6B7984"}}>KEAMANAN</p>
<p>Web site MORIS dijamin kerahasiaan dan keamanannya, dalam hal ini Bank Mandiri menggunakan teknologi enkripsi Secure Socket Layer (SSL) 128 bit, yang akan melindungi akses dan komunikasi antara browser Nasabah dengan server Bank Mandiri. </p>
<p>Bank Mandiri juga tidak secara otomatis mengumpulkan informasi data pengunjung MORIS, hanya beberapa informasi umum yang akan dikumpulkan dan digunakan antara lain :</p>
<p>
    <ol>
        <li>Nama domain yang akan digunakan Nasabah untuk mengakses  MORIS https://qr.bankmandiri.co.id/</li>
        <li>Internet address yang digunakan untuk mengakses website Bank Mandiri</li>
        <li>Browser yang digunakan</li>
        <li>Hari, tanggal &amp; waktu mengakses internet</li>
    </ol>
</p>
<p>Untuk dapat mengakses MORIS, Nasabah harus memasukkan terlebih dahulu User ID dan password.</p>
<p>Mengingat banyaknya variasi internet browser yang ada, sulit untuk menyediakan Layanan yang mengikuti keamanan masing-masing browser. Saat ini Bank Mandiri hanya menyediakan sarana MORIS yang lebih cocok diakses dengan menggunakan browser dengan versi minimal sebagai berikut:</p>
<ol>
    <li>Chrome</li>
    <li>Microsoft Edge</li>
    <li>Firefox : version 16 keatas </li>
    <li>Opera</li>
    <li>Safari</li>
</ol>
<p>
Sedangkan untuk aplikasi MORIS, Operating System telepon seluler yang disarankan adalah:
   <ol>
       <li> Android OS: 4.4</li>
       <li> IOS: 8</li>
   </ol>
  
</p>
          </div>
        </Grid>
      </div>
    );
  }
}

export default kebijakanPrivasiNHukum;
