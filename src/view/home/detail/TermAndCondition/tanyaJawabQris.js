import React, { Component } from "react";
import Link from "@material-ui/core/Link";
import ArrowBackIcon from "@material-ui/icons/ArrowBack";
import { AppBar, Box, Button, Card, Grid, Toolbar, Typography } from "@material-ui/core";

class tanyaJawabQris extends Component {

  componentWillUnmount(){
    location.reload();
  }

  render() {
    const { loading = false } = this.props;

  
    return (
      <div>
        {/* <div style={{ marginLeft: "20px", marginTop: "24px" }}>
          <Link href="/startReg">
            <ArrowBackIcon />
          </Link>
        </div> */}
        <AppBar style={{backgroundColor: 'transparent', position: 'absolute', 
                boxShadow: 'unset'}}>
            <Toolbar  style={{minHeight: '60px'}}>
            {/* onClick={this.props.history.push('/startReg', {from: '/termsCondition'})} */}
                <Link href="/helpSaya" style={{position: 'absolute', display: 'flex'}} >
                    <ArrowBackIcon/>
                </Link>
            </Toolbar>
        </AppBar>
        <Grid
          style={{
            // padding: "10px",
            justifyContent: "center",
            alignItems: "center",
            //marginInline: "20px",
            width: '100% !important',
            color: "#black",
            textAlign:"justify"
          }}
        >
          <div style={{padding:'16px'}}>
          <h3
            style={{
              marginTop: "70px",
              fontFamily: "Montserrat",
              textAlign: "center",
              fontStyle: "normal",
              fontWeight: "bold",
              fontSize: "18px",
              lineHeight: "150%",
              color: "#121518",
            }}
          >
          TANYA JAWAB
          </h3>

  <ul style={{listStyleType:'decimal'}}>
  <li style={{fontWeight:'700', color:'black'}}> Apa itu MORIS Mandiri? </li>
  <p>merupakan platform atau sarana berbasis website (progressive web app/ PWA) dibawah naungan Bank Mandiri untuk mengakomodasi proses pengajuan usaha dan menyediakan dashboard yang menawarkan kemudahan dan kenyamanan bagi Pemilik Usaha untuk memperoleh informasi dan memantau aktivitas transaksi menggunakan QRIS.</p>
  <li style={{fontWeight:'700', color:'black'}}> Benefit apa saja yang ada pada MORIS Mandiri? </li>
  <p>
  <ul style={{listStyleType:'lower-alpha'}}>
    <li style={{fontWeight:'700', color:'black'}}>Akses Mudah</li>
    <p>Pemilik Usaha hanya memerlukan 1 user ID dan 1 password untuk login di MORIS Mandiri.</p>
    <li style={{fontWeight:'700', color:'black'}}>Aman dan Nyaman</li>
    <p>Pada saat proses pengajuan, Pemilik Usaha melakukan verifikasi kesesuaian data di Bank sebagai Nasabah Bank Mandiri. Selain itu, Pemilik Usaha tidak memerlukan verifikasi One Time Password (OTP) setiap kali menerima transaksi dari customer/ pelanggan.</p>
    <li style={{fontWeight:'700', color:'black'}}>Menampilkan QRIS pada dashboard</li>
    <p>QRIS adalah Kode QR Standar Indonesia yang memungkinkan Pemilik Usaha dapat menerima transaksi pembayaran dari seluruh uang/dompet elektronik yang telah terdaftar di Bank Indonesia. 
Dengan adanya standardisasi kode QR Standar Indonesia tersebut, maka satu kode QR (QRIS) dapat digunakan oleh berbagai aplikasi penyedia pembayaran kode QR yang telah terdaftar di Bank Indonesia.</p>
    <li style={{fontWeight:'700', color:'black'}}>Riwayat Transaksi</li>
    <p>Pemilik Usaha dapat melihat riwayat transaksi dalam rentang waktu 7 (tujuh) hari dan maksimal 3 (tiga) bulan.</p>
    <li style={{fontWeight:'700', color:'black'}}>Notifikasi</li>
    <p>Pemilik Usaha dapat menerima pemberitahuan secara langsung atas penerimaan transaksi yang terjadi.</p>
    <li style={{fontWeight:'700', color:'black'}}>Pencairan</li>
    <p>Berisi informasi mengenai status pengkreditan dana transaksi yang diterima oleh Pemilik Usaha dalam rentang waktu 1 (satu) bulan.</p>
  </ul>
  </p>
  <li style={{fontWeight:'700', color:'black'}}> Kriteria Pemilik Usaha yang dapat menggunakan MORIS Mandiri? </li>
  <p>
    <ul>
      <li>Merupakan Nasabah Perorangan.</li>
      <li>Wajib memiliki rekening tabungan/ giro di Bank Mandiri sebagai rekening penampungan.</li>
      <li>Tidak berjualan barang/ jasa yang dilarang oleh Bank.</li>
      <li>Memenuhi persyaratan sesuai yang dicantumkan pada Syarat dan Ketentuan menjadi Merchant/ Pemilik Usaha Bank Mandiri.</li>
    </ul>
  </p>
  <li style={{fontWeight:'700', color:'black'}}> Bagaimana cara Pemilik Usaha untuk mengakses MORIS Mandiri? </li>
 
   <p>Pemilik Usaha dapat mengakses melalui website di alamat XXX dengan menggunakan komputer atau handphone dan tidak terbatas pada sistem operasi yang digunakan.</p>
 
  <li style={{fontWeight:'700', color:'black'}}> Apakah Bank Mandiri pernah atau akan meminta data Pemilik Usaha melalui e-mail atau telepon? </li>
  
  <p>Tidak.Untuk keamanan Pemilik Usaha, Bank Mandiri tidak pernah meminta detail data Pemilik Usaha sebagai Nasabah atau informasi login melalui  e-mail  atau  telepon.  Jika  menerima  e-mail  atau  telepon  yang  meminta  detail  data  terkait produk  perbankan  ataupun  informasi  pribadi  seperti  <i>User ID</i> dan <i>password</i>, mohon jangan  memberikan  data tersebut.</p>
  
  <li style={{fontWeight:'700', color:'black'}}> Bagaimana jika Pemilik Usaha menemukan kendala lain terkait penggunaan aplikasi? </li>
 
  <p>Jika Pemilik Usaha mengalami kendala terkait akses MORIS Mandiri dapat langsung menghubungi kontak berikut:</p>
 <ul style={{listStyleType:'none'}}>
   <li>HiYokke 	: 14021</li>
   <li>E-mail		: hiyokke@yokke.co.id </li>
   <li>WhatApp	: 08163614021</li>
 </ul>
 
  </ul>       
  
          </div>
        </Grid>
      </div>
    );
  }
}

export default tanyaJawabQris;
