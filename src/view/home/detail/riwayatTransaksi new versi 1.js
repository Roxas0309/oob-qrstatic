import React from 'react';
import { BACK_END_POINT, GET, HEADER_AUTH, POST, TOKEN_AUTH, wsWithBody, wsWithoutBody } from '../../../master/masterComponent'
import '../detail/pencairanDetail.scss';
import HeaderInfo from '../../../master/HeaderInfo';
import moment from 'moment';
import ChevronRightIcon from '@material-ui/icons/ChevronRight';
import CalendarMaker from './CalendarMaker';
import HeaderInfoPencairan from '../../../master/HeaderInfoPencairan';
import prog from '../../../../public/img/in-progress-gray.png'
import progBlue from '../../../../public/img/in-progress.png'
import suk from '../../../../public/img/success.png'
import sukIjo from '../../../../public/img/success_ijo.png'
import LoadingMaker from '../../../helpers/loadingMaker/LoadingMaker';
export default class riwayatTransaksi extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            isOnLoading: false,
            isYouKnowExpands: true,
            namaLengkap: '',
            nomorHandphone: true,
            email: '',
            nomorRekening: '',
            tipeAkun: '',
            maksimalPembayaranPerHari: '',
            isOnNotifikasiBar: true,
            isShowDate: false,
            isNotOnPeriode: true,
            isNotOnRincianTransaksi: false,
            isOncheckPeriode: false,
            isNotValidDateFilter: false,
            calendarMerge: {},
            valueCalender: '',
            showCalendarTable: true,
            showMonthTable: false,
            dateObject: moment(),
            allmonths: moment.months(),
            showYearNav: false,
            selectedDate: null,
            selectedDay: null,
            selectedDayInFormat: null,
            selectedDayInWithoutYear: null,
            selectedDayInFormat: null,
            selectedDayInWithoutYearStart: null,
            selectedDayInWithoutYearEnd: null,
            isStartDate: false,
            isEndDate: false,
            startDate: null,
            startDateInFormat: null,
            stopDateInFormat: null,
            stopDate: null,
            isOpenCalendar: false,
            selectedDateDay: null,
            isOpenItemReferensi: false,
            isOpenTotalDiterima: false,
            pointOnDate: 5,
            detailRiwayatTransaksiSelected: null,
            rincianProses: 0,
            headerTrx: null,
            headerTrxDetail: null,
            isOnSeeInformationPendapatan: false,
            isOnSeeInformationPencairan: false,
            isOnPendapatan: true,
            isOnRincian: false,
            isOnRincianChevronUp:false,
            paidDanaMoney:null,
            unpaidDanaMoney:null,
            listRiwayatPencairan : []
        }
    }

    getListPencairan = (startdate,enddate) => {
        wsWithoutBody(BACK_END_POINT + "/homeScreen/getRincianPencairan/auth?startDate=" + startdate + "&endDate=" + enddate + "&isLimitValidated=false",
        GET, {
        ...HEADER_AUTH, 'secret-token': localStorage.getItem(TOKEN_AUTH)
    }).then(response => {
        var value = response.data.result;
        this.setState({ listRiwayatPencairan : value });
    }).catch(error => {
        this.getPaidUnpaidData(startdate, enddate);
    })
    }

    getPaidUnpaidData = (startdate,enddate) => {
        wsWithoutBody(BACK_END_POINT + "/homeScreen/getPaidUnpaidMoney/auth?startDate=" + startdate + "&endDate=" + enddate + "&isLimitValidated=false",
        GET, {
        ...HEADER_AUTH, 'secret-token': localStorage.getItem(TOKEN_AUTH)
    }).then(response => {
        var value = response.data.result;
        this.setState({ paidDanaMoney: value.paidTransaksiRupiah, unpaidDanaMoney:value.unpaidTransaksiInRupiah });
    }).catch(error => {
        this.getPaidUnpaidData(startdate, enddate);
    })
    }


    getMyData = (startdate, enddate) => {

        wsWithoutBody(BACK_END_POINT + "/homeScreen/getDataTransaksi/auth?startDate=" + startdate + "&endDate=" + enddate + "&isLimitValidated=false",
            GET, {
            ...HEADER_AUTH, 'secret-token': localStorage.getItem(TOKEN_AUTH)
        }).then(response => {
            var value = response.data.result;
            this.setState({ headerTrx: value });
        }).catch(error => {
            this.getMyData(startdate, enddate);
        })
    }


    getDetailTask = () => {
        console.log(this.state.startDate + ' ' + this.state.stopDate)

        if (this.state.stopDate != null && this.state.startDate != null) {
            this.setState({ isOnLoading: true });
            wsWithoutBody(BACK_END_POINT + "/homeScreen/getDataTransaksi/auth?startDate=" + this.state.startDateInFormat + "&endDate=" + this.state.stopDateInFormat + "&isLimitValidated=true",
                GET, {
                ...HEADER_AUTH, 'secret-token': localStorage.getItem(TOKEN_AUTH)
            }).then(response => {
                var value = response.data.result;
                this.setState({ isOnLoading: false });
                this.setState({ headerTrx: value });
                this.setState({ isNotOnPeriode: true })
            }).catch(error => {
                if (error.response.data.status === 406) {
                    this.setState({ isNotValidDateFilter: true });
                    this.setState({ isOnLoading: false });
                    //alert(error.response.data.message);
                }
                else {
                    alert('upsss something went wrong');
                }
            })
        }
    }

    weekdayshort = moment.weekdaysShort();

    daysInMonth = () => {
        return this.state.dateObject.daysInMonth();
    };
    year = () => {
        return this.state.dateObject.format("Y");
    };
    currentDay = () => {
        return this.state.dateObject.format("D");
    };
    firstDayOfMonth = () => {
        let dateObject = this.state.dateObject;
        let firstDay = moment(dateObject)
            .startOf("month")
            .format("d"); // Day of week 0...1..5...6
        return firstDay;
    };
    month = () => {
        return this.state.dateObject.format("MMMM");
    };
    showMonth = (e, month) => {
        this.setState({
            showMonthTable: !this.state.showMonthTable,
            showCalendarTable: !this.state.showCalendarTable
        });
    };
    setMonth = month => {
        let monthNo = this.state.allmonths.indexOf(month);
        let dateObject = Object.assign({}, this.state.dateObject);
        dateObject = moment(dateObject).set("month", monthNo);
        this.setState({
            dateObject: dateObject,
            showMonthTable: !this.state.showMonthTable,
            showCalendarTable: !this.state.showCalendarTable
        });
    };
    MonthList = props => {
        let months = [];
        props.data.map(data => {
            months.push(
                <td
                    key={data}
                    className="calendar-month"
                    onClick={e => {
                        this.setMonth(data);
                    }}
                >
                    <span>{data}</span>
                </td>
            );
        });
        let rows = [];
        let cells = [];

        months.forEach((row, i) => {
            if (i % 3 !== 0 || i == 0) {
                cells.push(row);
            } else {
                rows.push(cells);
                cells = [];
                cells.push(row);
            }
        });
        rows.push(cells);
        let monthlist = rows.map((d, i) => {
            return <tr key={i}>{d}</tr>;
        });

        return (
            <table className="calendar-month">
                <thead>
                    <tr>
                        <th colSpan="4">Select a Month</th>
                    </tr>
                </thead>
                <tbody>{monthlist}</tbody>
            </table>
        );
    };
    showYearEditor = () => {
        this.setState({
            showYearNav: true,
            showCalendarTable: !this.state.showCalendarTable
        });
    };

    onPrev = () => {
        let curr = "";
        if (this.state.showMonthTable == true) {
            curr = "year";
        } else {
            curr = "month";
        }
        this.setState({
            dateObject: this.state.dateObject.subtract(1, curr)
        });
    };
    onNext = () => {
        let curr = "";
        if (this.state.showMonthTable == true) {
            curr = "year";
        } else {
            curr = "month";
        }
        this.setState({
            dateObject: this.state.dateObject.add(1, curr)
        });
    };
    setYear = year => {
        // alert(year)
        let dateObject = Object.assign({}, this.state.dateObject);
        dateObject = moment(dateObject).set("year", year);
        this.setState({
            dateObject: dateObject,
            showMonthTable: !this.state.showMonthTable,
            showYearNav: !this.state.showYearNav,
            showMonthTable: !this.state.showMonthTable
        });
    };
    onYearChange = e => {
        this.setYear(e.target.value);
    };
    getDates(startDate, stopDate) {
        var dateArray = [];
        var currentDate = moment(startDate);
        var stopDate = moment(stopDate);
        while (currentDate <= stopDate) {
            dateArray.push(moment(currentDate).format("YYYY"));
            currentDate = moment(currentDate).add(1, "year");
        }
        return dateArray;
    }
    YearTable = props => {
        let months = [];
        let nextten = moment()
            .set("year", props)
            .add("year", 12)
            .format("Y");

        let tenyear = this.getDates(props, nextten);

        tenyear.map(data => {
            months.push(
                <td
                    key={data}
                    className="calendar-month"
                    onClick={e => {
                        this.setYear(data);
                    }}
                >
                    <span>{data}</span>
                </td>
            );
        });
        let rows = [];
        let cells = [];

        months.forEach((row, i) => {
            if (i % 3 !== 0 || i == 0) {
                cells.push(row);
            } else {
                rows.push(cells);
                cells = [];
                cells.push(row);
            }
        });
        rows.push(cells);
        let yearlist = rows.map((d, i) => {
            return <tr key={i}>{d}</tr>;
        });

        return (
            <table className="calendar-month">
                <thead>
                    <tr>
                        <th colSpan="4">Select a Yeah</th>
                    </tr>
                </thead>
                <tbody>{yearlist}</tbody>
            </table>
        );
    };
    onDayClick = (e, d) => {
        var month = this.month().substring(0, 3);
        var mInNumber = '';
        if (month === 'Jan') {
            mInNumber = '01';
        }
        else if (month === 'Feb') {
            mInNumber = '02';
        }
        else if (month === 'Mar') {
            mInNumber = '03';
        }
        else if (month === 'Apr') {
            mInNumber = '04';
        }
        else if (month === 'May') {
            mInNumber = '05';
        }
        else if (month === 'Jun') {
            mInNumber = '06';
        }
        else if (month === 'Jul') {
            mInNumber = '07';
        }
        else if (month === 'Aug') {
            mInNumber = '08';
        }
        else if (month === 'Sep') {
            mInNumber = '09';
        }
        else if (month === 'Oct') {
            mInNumber = '10';
        }
        else if (month === 'Nov') {
            mInNumber = '11';
        }
        else if (month === 'Dec') {
            mInNumber = '12';
        }

        var inFormatedD = '' + d;
        if (inFormatedD.length == 1) {
            inFormatedD = 0 + '' + inFormatedD;
        }
        this.setState(
            {
                selectedDay: d + ' ' + this.month().substring(0, 3) + ' ' + this.year(),
                selectedDate: d,
                selectedDayInFormat: this.year() + '' + mInNumber + inFormatedD,
                selectedDayInWithoutYear: d + ' ' + this.month().substring(0, 3)
            }
        );
    };




    componentDidMount() {

        if (this.state.isOnPendapatan) {
            document.body.style = 'background :white';
        }
        else {
            document.body.style = 'background : #F7F8F9!important';
        }

        window.onpopstate = function (event) {
            window.location.reload();
        };
        wsWithoutBody(BACK_END_POINT + "/loginCtl/detailToken", GET, {
            ...HEADER_AUTH, 'secret-token': localStorage.getItem(TOKEN_AUTH)
        }).then(response => {
            var result = response.data.result;
            this.setState({
                namaLengkap: result.userOobTableSuccessDto.namaPemilikUsaha,
                nomorHandphone: result.userOobTableSuccessDto.noHandphone,
                email: result.userOobTableSuccessDto.emailPemilikUsaha,
                nomorRekening: result.userOobTableSuccessDto.nomorRekening,
                tipeAkun: result.userOobMidDtlTableDto.akunOobMidDesc
            })
        });

        wsWithoutBody(BACK_END_POINT + "/date-maker/get-date-now/7", GET, {}).then(response => {
            this.setState({ isShowDate: true, calendarMerge: response.data });
            //  debugger;
        });

        var startTime = moment(new Date()).format("YYYYMMDD");
        var endTime = moment(new Date()).format("YYYYMMDD");
        this.getMyData(startTime, endTime);

        var startTime30Days = moment(new Date()).add(-30, 'day').format('YYYYMMDD');
        var endTime = moment(new Date()).format("YYYYMMDD");
        this.getPaidUnpaidData(startTime30Days,endTime);
        this.getListPencairan(startTime30Days,endTime);

    }

    getSelectedDayRiwayat = (selectedDay, selectedInFormat) => {
        console.log('selected day : ' + selectedDay);
        this.getMyData(selectedInFormat, selectedInFormat);
        this.setState({ selectedDateDay: selectedDay });
    }

    render() {

        let weekdayshortname = this.weekdayshort.map((day, i) => {
            return <th key={i}>{day.charAt(0)}</th>;
        });
        let blanks = [];
        for (let i = 0; i < this.firstDayOfMonth(); i++) {
            blanks.push(<td className="calendar-day empty">{""}</td>);
        }
        let daysInMonth = [];
        for (let d = 1; d <= this.daysInMonth(); d++) {
            let currentDay = d == this.currentDay() ? "current-today" : "";
            var h = this.currentDay();
            if (this.state.selectedDate == d) {
                currentDay = 'selected-value';
            }

            daysInMonth.push(
                <td key={d} className={currentDay} >
                    <span
                        onClick={e => {
                            this.onDayClick(e, d);
                        }}
                    >
                        {d}
                    </span>
                </td>
            );
        }
        var totalSlots = [...blanks, ...daysInMonth];
        let rows = [];
        let cells = [];

        totalSlots.forEach((row, i) => {
            if (i % 7 !== 0) {
                cells.push(row);
            } else {
                rows.push(cells);
                cells = [];
                cells.push(row);
            }
            if (i === totalSlots.length - 1) {
                // let insertRow = cells.slice();
                rows.push(cells);
            }
        });

        let daysinmonth = rows.map((d, i) => {
            return <tr key={i}>{d}</tr>;
        });
        return (
            <div>

                <LoadingMaker onLoading={this.state.isOnLoading}
                    loadingWord={'Proses login sedang berlangsung'}
                ></LoadingMaker>

                {   (this.state.isNotOnPeriode) ?
                    <div>
                        <HeaderInfoPencairan position={'fixed'} headerName={"Riwayat Transaksi"} urlBack={"/homeScreen"}></HeaderInfoPencairan>

                        {this.state.isOnSeeInformationPendapatan ?
                            <div className="fluid-on-rincian">
                                <div className="overLay-background-deflect"></div>
                                <div className="body-fluid on-custom">
                                    <div className="container-fluid">
                                        <div className="big-header-fluid">Total Transaksi</div>
                                        <div className="small-rincian-fluid">Biaya admin yang diterapkan Pemerintah adalah
                                        0,7% dari tiap transaksi.</div>
                                        <div className="button-rincian-fluid" onClick={() => {
                                            this.setState({ isOnSeeInformationPendapatan: false })
                                        }}>Oke</div>
                                    </div>
                                </div>
                            </div>
                            : <div></div>}

                        {this.state.isOnSeeInformationPencairan ?
                            <div className="fluid-on-rincian">
                                <div className="overLay-background-deflect"></div>
                                <div className="body-fluid on-custom">
                                    <div className="container-fluid">
                                        <div className="big-header-fluid">Belum Dicairkan</div>
                                        <div className="small-rincian-fluid">
                                            <p>- Dana dicairkan otomatis tiap hari kerja pada jam 08:00, 14:00, dan 20:00. </p>
                                            <p>
                                                - Jumlah maksimum pencairan adalah
                                                Rp 500.000/pencairan/hari. Jika pendapatan melebihi jumlah tersebut, sisa dana akan dicairkan pada jadwal atau hari kerja berikutnya.
</p>
                                        </div>
                                        <div className="button-rincian-fluid" onClick={() => {
                                            this.setState({ isOnSeeInformationPencairan: false })
                                        }}>Oke</div>
                                    </div>
                                </div>
                            </div>
                            : <div></div>}


                        {/* start show rincian --START */}
                        {!this.state.isNotOnRincianTransaksi ?
                            <div></div> :
                            <div className="fluid-on-rincian">
                                <div className="overLay-background-deflect"></div>
                                <div className="body-fluid">
                                    <div className="justify-padding-lr">
                                        <div className="relay-header">
                                            <div className="relay-header-boot">Rincian</div>
                                            <div className="relay-header-boot-x"
                                                onClick={
                                                    () => {
                                                        this.setState({ isNotOnRincianTransaksi: false })
                                                    }
                                                }>X</div>
                                        </div>
                                        <div className="Header-date">{this.state.headerTrxDetail.authDateTime}</div>
                                        <div className="Header-wording">Pembayaran berhasil via {this.state.headerTrxDetail.issuerName}</div>
                                        <div className="Sub-Header-wording">
                                            {/* <i className="fa fa-hourglass"></i> */}
                                            {
                                                !this.state.headerTrxDetail.isTransferToRek ?
                                                    <img src={progBlue} height={16} width={16} /> :
                                                    <img src={sukIjo} height={16} width={16} />
                                            }

                                            {
                                                !this.state.headerTrxDetail.isTransferToRek ?
                                                    'Proses masuk rekening' :
                                                    'Sudah masuk rekening'
                                            }

                                        </div>
                                        <div className="total-diterima">
                                            <div className="wording">Total yang diterima</div>
                                            <div className="referensi">{this.state.headerTrxDetail.transferAmount}</div>
                                            {!this.state.isOpenTotalDiterima ?
                                                <i className="fa fa-chevron-down custom-wagon" onClick={
                                                    () => this.setState({ isOpenTotalDiterima: true })
                                                }></i>
                                                : <i className="fa fa-chevron-up custom-wagon" onClick={
                                                    () => this.setState({ isOpenTotalDiterima: false })
                                                }></i>}
                                        </div>
                                        {this.state.isOpenTotalDiterima ?
                                            <div className="chevron-most-list-detail">
                                                <div className="transaksi">
                                                    <div className="transaksiWording">Transaksi</div>
                                                    <div className="transaksiMoney">{this.state.headerTrxDetail.authAmount}</div>
                                                </div>
                                                <div className="biayaAdmin">
                                                    <div className="biayaAdminWording">Biaya Admin ({this.state.headerTrxDetail.percentageFeeAmount})</div>
                                                    <div className="biayaAdminNomor">{this.state.headerTrxDetail.feeAmount}</div>
                                                </div>
                                            </div> : <div></div>
                                        }
                                        <div className="nomor-referensi">
                                            <div className="wording">Nomor Referensi</div>
                                            <div className="number">{this.state.headerTrxDetail.reffNumber}</div>
                                        </div>

                                        <div className="transaksi-detail">
                                            <div className="referensi">
                                                <span><b>Detail Transaksi</b></span>
                                                {!this.state.isOpenItemReferensi ?
                                                    <i className="fa fa-chevron-down custom-wagon"
                                                        onClick={
                                                            () => this.setState({
                                                                isOpenItemReferensi: true
                                                            })
                                                        }></i>
                                                    :
                                                    <i className="fa fa-chevron-up custom-wagon"
                                                        onClick={
                                                            () => this.setState({
                                                                isOpenItemReferensi: false
                                                            })
                                                        }></i>
                                                }
                                            </div>
                                            {this.state.isOpenItemReferensi ?
                                                <span>
                                                    <div className="items-wording">
                                                        <div className="left-item">Nama Penerbit</div>
                                                        <div className="right-item">{this.state.headerTrxDetail.issuerName}</div>
                                                    </div>
                                                    <div className="items-wording">
                                                        <div className="left-item">Nama Customer</div>
                                                        <div className="right-item">{this.state.headerTrxDetail.customerName}</div>
                                                    </div>
                                                    <div className="items-wording">
                                                        <div className="left-item">MPAN</div>
                                                        <div className="right-item">{this.state.headerTrxDetail.mpan}</div>
                                                    </div>
                                                    <div className="items-wording">
                                                        <div className="left-item">TID</div>
                                                        <div className="right-item">{this.state.headerTrxDetail.tid}</div>
                                                    </div>
                                                    <div className="items-wording">
                                                        <div className="left-item">CPAN</div>
                                                        <div className="right-item">{this.state.headerTrxDetail.cpan}</div>
                                                    </div>
                                                </span>
                                                : <div></div>}
                                        </div>
                                    </div>
                                </div>
                            </div>
                        }
                        {/* start show rincian --END */}

                        {this.state.isShowDate ?
                            <div className="riwayat-transaksi-container">
                                 <div className={this.state.isOnPendapatan ? "riwayat-date-transaksi" : "riwayat-date-transaksi  no-box-shadow"}>


                                    <div className="notifikasi-me-container edit-riwayat">
                                        <div className="button-anomali" style={{ left: '-1vw' }}>
                                            <div className="container-anomali">
                                                <div
                                                    onClick={() => {
                                                        this.setState({ isOnPendapatan: true })

                                                    }}
                                                    className={this.state.isOnPendapatan ?
                                                        'anomali edit-riwayat anomali-transaksi active' : 'anomali edit-riwayat anomali-transaksi'
                                                    }>Pendapatan</div>
                                                <div
                                                    onClick={() => {
                                                        this.setState({ isOnPendapatan: false })
                                                    }}
                                                    className=
                                                    {this.state.isOnPendapatan ?
                                                        'anomali edit-riwayat anomali-informasi' : 'anomali edit-riwayat anomali-informasi active'
                                                    }
                                                >Pencairan Dana</div>
                                            </div>
                                        </div>
                                    </div>

                                    {this.state.isOnPendapatan ?
                                        <span>
                                            <div className="bulan-tahun">{this.state.calendarMerge.bulanDesc + " " + this.state.calendarMerge.tahun}</div>
                                            <div className="periode-button" onClick={
                                                () => {
                                                    this.setState({ isNotOnPeriode: false, stopDate: null, startDate: null, stopDateInFormat: null, startDateInFormat: null, isNotValidDateFilter: false })
                                                }
                                            }>
                                                {/* <img className="date-flatter" src="data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHZlcnNpb249IjEuMSIgeG1sbnM6eGxpbms9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkveGxpbmsiIHhtbG5zOnN2Z2pzPSJodHRwOi8vc3ZnanMuY29tL3N2Z2pzIiB3aWR0aD0iNTEyIiBoZWlnaHQ9IjUxMiIgeD0iMCIgeT0iMCIgdmlld0JveD0iMCAwIDM2Ni44IDM2Ni44IiBzdHlsZT0iZW5hYmxlLWJhY2tncm91bmQ6bmV3IDAgMCA1MTIgNTEyIiB4bWw6c3BhY2U9InByZXNlcnZlIiBjbGFzcz0iIj48Zz4KPGcgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIj4KCTxnPgoJCTxwYXRoIGQ9Ik0zNTMuNCw3MS42Yy0yLjQtMTEuNi04LTIyLTE2LTMwYy0yLjQtMi40LTUuMi00LjgtOC40LTYuOGwtMC40LTAuNGMtMC40LTAuNC0wLjgtMC40LTEuMi0wLjhjLTEuMi0wLjgtMi40LTEuNi0zLjYtMiAgICBoLTAuNGMtOC40LTQuNC0xNy42LTYuOC0yOC02LjhoLTI0LjhWNy42YzAtNC0zLjItNy42LTcuNi03LjZjLTQsMC03LjYsMy4yLTcuNiw3LjZWMjRIMTExLjhWNy42YzAtNC0zLjYtNy42LTcuNi03LjYgICAgcy03LjYsMy4yLTcuNiw3LjZWMjRINzEuOGMtNy42LDAtMTUuMiwxLjYtMjIsNC40Yy03LjYsMy4yLTE0LjQsNy42LTIwLDEzLjJjLTMuMiwzLjItNiw2LjgtOC40LDEwLjRjLTIuNCw0LTQuNCw4LTYsMTIuNCAgICBjLTAuOCwyLjQtMS42LDQuOC0yLDcuMmMtMC44LDQtMS4yLDgtMS4yLDEydjQwVjMwOGMwLDE2LjQsNi44LDMxLjIsMTcuMiw0MS42QzQwLjIsMzYwLjQsNTUsMzY2LjgsNzEsMzY2LjhoMjI0LjggICAgYzE2LjQsMCwzMS4yLTYuOCw0MS42LTE3LjJjMTAuOC0xMC44LDE3LjItMjUuNiwxNy4yLTQxLjZWMTIzLjZ2LTQwQzM1NC42LDc5LjYsMzU0LjIsNzUuNiwzNTMuNCw3MS42eiBNMjcuOCw4My4yICAgIGMwLTMuMiwwLjQtNiwwLjgtOC44YzAuNC0yLjgsMS42LTUuNiwyLjQtOC40YzEuNi0zLjYsMy42LTYuOCw1LjYtOS42YzEuMi0xLjYsMi40LTIuOCwzLjYtNC40YzIuNC0yLjQsNS4yLTQuNCw4LTYuNCAgICBjNi44LTQsMTQuNC02LjQsMjIuOC02LjRoMjQuOHYxNi40YzAsNCwzLjIsNy42LDcuNiw3LjZjNCwwLDcuNi0zLjIsNy42LTcuNlYzOS4yaDE0NS42djE2LjRjMCw0LDMuMiw3LjYsNy42LDcuNiAgICBjNCwwLDcuNi0zLjIsNy42LTcuNlYzOS4yaDI0LjhjOC40LDAsMTYuNCwyLjQsMjIuOCw2LjRjMi44LDIsNS42LDQsOCw2LjRjNiw2LDEwLjQsMTMuNiwxMiwyMi40YzAuNCwyLjgsMC44LDYsMC44LDguOFYxMTZIMjcuOCAgICBWODMuMnogTTM0MC42LDMwNy42YzAsMTIuNC00LjgsMjMuNi0xMi44LDMxLjZTMzA4LjYsMzUyLDI5Ni42LDM1Mkg3MS44Yy0xMiwwLTIzLjItNC44LTMxLjItMTIuOFMyNy44LDMyMCwyNy44LDMwOFYxMzAuOGgzMTIuOCAgICBWMzA3LjZ6IiBmaWxsPSIjMDA3YWNmIiBkYXRhLW9yaWdpbmFsPSIjMDAwMDAwIiBzdHlsZT0iIiBjbGFzcz0iIj48L3BhdGg+Cgk8L2c+CjwvZz4KPGcgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIj4KPC9nPgo8ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciPgo8L2c+CjxnIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyI+CjwvZz4KPGcgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIj4KPC9nPgo8ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciPgo8L2c+CjxnIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyI+CjwvZz4KPGcgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIj4KPC9nPgo8ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciPgo8L2c+CjxnIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyI+CjwvZz4KPGcgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIj4KPC9nPgo8ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciPgo8L2c+CjxnIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyI+CjwvZz4KPGcgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIj4KPC9nPgo8ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciPgo8L2c+CjxnIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyI+CjwvZz4KPC9nPjwvc3ZnPg==" /> */}
                                                <svg className="date-flatter" width="3.430vw" height="3.430vw" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                    <path d="M19.0004 6V4H17.0004V6H7.0004V4H5.0004V6H2V20H22V6H19.0004ZM20 8V10H4V8H20ZM4 18V12H20V18H4Z" fill="#007ACF" />
                                                    <path fillRule="evenodd" clipRule="evenodd" d="M16.0002 16.3081L15.1165 17.1918L14.0559 16.1312L14.9396 15.2474L14.0557 14.3635L15.1163 13.3028L16.0002 14.1868L16.8843 13.3027L17.9449 14.3634L17.0609 15.2474L17.9448 16.1313L16.8841 17.1919L16.0002 16.3081Z" fill="#007DFE" />
                                                </svg>
                                                <span >
                                                    Periode
                                        </span>
                                            </div>
                                            <div className="tanggal" style={{ position: 'relative', top: '4vw', left: '-2vw' }}>
                                                {(this.state.selectedDayInWithoutYearStart != null
                                                    && this.state.selectedDayInWithoutYearEnd != null) ?
                                                    <div className="selected-date" >
                                                        <div className="start">{this.state.selectedDayInWithoutYearStart}</div>
                                                        <div className="strip"> - </div>
                                                        <div className="end">{this.state.selectedDayInWithoutYearEnd}</div>
                                                    </div>
                                                    :
                                                    this.state.calendarMerge.calendarDayDateDtos.map((date, i) => {
                                                        if (this.state.selectedDateDay == null) {
                                                            if (date.isActiveDay) {
                                                                return <div key={i} className="detail active"
                                                                    onClick={() => this.getSelectedDayRiwayat(date.tanggal, date.formatIn)}
                                                                >
                                                                    <div className="tanggal">{date.tanggal}</div>
                                                                    <div className="hari">{date.hari}</div>
                                                                </div>
                                                            }
                                                            else {
                                                                return <div key={i} className="detail"
                                                                    onClick={() => this.getSelectedDayRiwayat(date.tanggal, date.formatIn)}>
                                                                    <div className="tanggal">{date.tanggal}</div>
                                                                    <div className="hari">{date.hari}</div>
                                                                </div>
                                                            }
                                                        } else {
                                                            if (this.state.selectedDateDay == date.tanggal) {
                                                                return <div className="detail active"
                                                                    onClick={() => this.getSelectedDayRiwayat(date.tanggal, date.formatIn)}>
                                                                    <div className="tanggal">{date.tanggal}</div>
                                                                    <div className="hari">{date.hari}</div>
                                                                </div>
                                                            }
                                                            else {
                                                                return <div className="detail"
                                                                    onClick={() => this.getSelectedDayRiwayat(date.tanggal, date.formatIn)}>
                                                                    <div className="tanggal">{date.tanggal}</div>
                                                                    <div className="hari">{date.hari}</div>
                                                                </div>
                                                            }
                                                        }
                                                    })
                                                }
                                            </div>
                                        </span>
                                        : <span>

                                            <div className="box-wielder-notsettle">
                                                <div className="box-header">Belum Dicairkan</div>
                                                <div className="box-money">{this.state.unpaidDanaMoney}</div>
                                                <div className="box-footnote">Setelah dikurangi biaya admin </div>
                                                <div className="box-svg" onClick={() => {
                                                    this.setState({
                                                        isOnSeeInformationPencairan: true
                                                    })
                                                }}>
                                                    <svg width="6.400vw" height="6.400vw" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                        <path d="M13 9.56356H11V16.5636H13V9.56356Z" fill="#0057E7" />
                                                        <path d="M11 7.56516C11 7.01286 11.4477 6.56516 12 6.56516C12.5523 6.56516 13 7.01286 13 7.56516C13 8.11746 12.5523 8.56516 12 8.56516C11.4477 8.56526 11 8.11746 11 7.56516Z" fill="#0057E7" />
                                                        <path d="M12 19.5684C7.5888 19.5684 4 15.9796 4 11.5684C4 7.15716 7.5888 3.56836 12 3.56836C16.4112 3.56836 20 7.15716 20 11.5684C20 15.9796 16.4112 19.5684 12 19.5684ZM12 21.5684C17.5229 21.5684 22 17.0913 22 11.5684C22 6.04556 17.5229 1.56836 12 1.56836C6.4771 1.56836 2 6.04556 2 11.5684C2 17.0913 6.4771 21.5684 12 21.5684Z" fill="#0057E7" />
                                                    </svg>

                                                </div>
                                            </div>

                                        </span>
                                    }
                                </div>


                                {this.state.isOnPendapatan ?
                                    <span>
                                        {/* start show list transaksi --START */}
                                        {this.state.headerTrx != null ?
                                            <div className="container-transaksi-detail">
                                                <div className="total-transaksi-container">
                                                    <div className="header">TOTAL TRANSAKSI</div>
                                                    <div className="money">{this.state.headerTrx.sumAllTransaksi}</div>
                                                    <div className="footer">Belum dikurangi biaya admin</div>
                                                    <div style={{ position: 'absolute', top: '8vw', right: '6.4vw' }} onClick={() => {
                                                        this.setState({ isOnSeeInformationPendapatan: true });
                                                    }}>
                                                        <svg width="6.400vw" height="6.400vw" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                            <path d="M13 9.56356H11V16.5636H13V9.56356Z" fill="#0057E7" />
                                                            <path d="M11 7.56516C11 7.01286 11.4477 6.56516 12 6.56516C12.5523 6.56516 13 7.01286 13 7.56516C13 8.11746 12.5523 8.56516 12 8.56516C11.4477 8.56526 11 8.11746 11 7.56516Z" fill="#0057E7" />
                                                            <path d="M12 19.5684C7.5888 19.5684 4 15.9796 4 11.5684C4 7.15716 7.5888 3.56836 12 3.56836C16.4112 3.56836 20 7.15716 20 11.5684C20 15.9796 16.4112 19.5684 12 19.5684ZM12 21.5684C17.5229 21.5684 22 17.0913 22 11.5684C22 6.04556 17.5229 1.56836 12 1.56836C6.4771 1.56836 2 6.04556 2 11.5684C2 17.0913 6.4771 21.5684 12 21.5684Z" fill="#0057E7" />
                                                        </svg>

                                                    </div>
                                                    {/* <div className="divider"></div> */}
                                                </div>

                                                <div className="detail-big-container">
                                                    {
                                                        this.state.headerTrx.detail.map((value, i) => (
                                                            <div key={i} className="detail-riwayat-container"

                                                                onClick={
                                                                    () => {
                                                                        this.setState({ isNotOnRincianTransaksi: true, rincianProses: value.proses, headerTrxDetail: value.detail })
                                                                    }
                                                                }
                                                            >

                                                                <div className="riwayat-pembayaran">
                                                                    {!value.isTransferToRek ?

                                                                        <svg width="4.831vw" height="4.831vw" viewBox="0 0 20 20" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                                            <circle cx="10" cy="10" r="10" fill="#65768B" />
                                                                            <path d="M13.0001 8.94169C13.3531 8.7064 13.5295 8.35346 13.5295 7.94169V5.29463C13.5295 4.64758 13.0001 4.11816 12.3531 4.11816H7.64717C7.00011 4.11816 6.4707 4.64758 6.4707 5.29463V7.94169C6.4707 8.35346 6.64717 8.7064 7.00011 8.94169L8.23541 9.76522L8.64717 10.0593L7.00011 11.177C6.64717 11.3535 6.4707 11.7652 6.4707 12.1182V14.7064C6.4707 15.3535 7.00011 15.8829 7.64717 15.8829H12.3531C13.0001 15.8829 13.5295 15.3535 13.5295 14.7064V12.1182C13.5295 11.7064 13.3531 11.3535 13.0001 11.1182L11.2942 10.0005L11.7648 9.7064L13.0001 8.94169ZM12.3531 12.1182V14.7064H7.64717V12.1182L10.0001 10.5299L10.8236 11.0593L12.3531 12.1182ZM10.0001 9.52993L8.58835 8.58875L7.64717 7.94169V5.29463H12.3531V7.94169L11.4119 8.58875L10.0001 9.52993Z" fill="white" />
                                                                            <path d="M11.7648 13.5299H8.23541V14.1182H11.7648V13.5299Z" fill="white" />
                                                                            <path d="M8.23541 7.35346V7.75781L8.58835 8.00052L8.8847 8.19805V7.76522H11.1156V8.19805L11.4119 8.00052L11.7648 7.75787V7.35346H8.23541Z" fill="white" />
                                                                            <path d="M9.11782 8.35346L10.0001 8.94169L10.8825 8.35346H9.11782Z" fill="white" />
                                                                            <path d="M8.8847 7.76522V8.19805L9.11782 8.35346H10.8825L11.1156 8.19805V7.76522H8.8847Z" fill="white" />
                                                                        </svg>

                                                                        :
                                                                        <svg width="4.831vw" height="4.831vw" viewBox="0 0 20 20" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                                            <circle cx="10" cy="10" r="10" fill="white" />
                                                                            <path fill-rule="evenodd" clip-rule="evenodd" d="M10 0C4.48 0 0 4.48 0 10C0 15.52 4.48 20 10 20C15.52 20 20 15.52 20 10C20 4.48 15.52 0 10 0ZM8 15L3 9.99996L4.41 8.58996L8 12.17L15.59 4.57996L17 5.99996L8 15Z" fill="#007ACF" />
                                                                        </svg>

                                                                    }
                                                                    <div className="header">Pembayaran via {value.issuerPayment} ({value.cpanNumberInMask})</div>
                                                                    <div className="footer">{value.time}</div>
                                                                </div>
                                                                <div className="riwayat-money">
                                                                    <div className="header">{value.authAmount}</div>
                                                                    <div className="footer">Biaya ({value.feeAmount})</div>
                                                                </div>
                                                                <div className="riwayat-number">{value.number}</div>

                                                            </div>
                                                        ))
                                                    }
                                                </div>
                                            </div>
                                            : <div className="container-transaksi-detail"></div>}
                                        {/* start show list transaksi --END */}
                                    </span>
                                    : <span>

                                        <div className="container-transaksi-detail">
                                            <div className="container-belum-dicairkan">
                                                <div className="header">BELUM DICAIRKAN</div>
                                                <div className="foot-string">Pencairan dari transaksi</div>
                                                <div className="foot-date">19 Sep 2020</div>
                                                <div className="money">Rp 100.000</div>
                                                <div className="svgs">
                                                    <svg width="5.333vw" height="5.333vw" viewBox="0 0 20 20" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                        <mask id="mask0" mask-type="alpha" maskUnits="userSpaceOnUse" x="0" y="0" width="20" height="20">
                                                            <path d="M20 10C20 15.5228 15.5228 20 10 20C4.47715 20 0 15.5228 0 10C0 4.47715 4.47715 0 10 0C15.5228 0 20 4.47715 20 10Z" fill="#C4C4C4" />
                                                        </mask>
                                                        <g mask="url(#mask0)">
                                                            <path d="M19.5 10C19.5 15.2467 15.2467 19.5 10 19.5C4.75329 19.5 0.5 15.2467 0.5 10C0.5 4.75329 4.75329 0.5 10 0.5C15.2467 0.5 19.5 4.75329 19.5 10Z" fill="#F7F6F6" stroke="#F0EDED" />
                                                        </g>
                                                        <path d="M7.39567 8.33335H6.354V13.0209H7.39567V8.33335Z" fill="#ADA6AE" />
                                                        <path d="M10.5207 8.33335H9.479V13.0209H10.5207V8.33335Z" fill="#ADA6AE" />
                                                        <path d="M15.2082 13.5417H4.7915V14.5834H15.2082V13.5417Z" fill="#ADA6AE" />
                                                        <path d="M13.6457 8.33335H12.604V13.0209H13.6457V8.33335Z" fill="#ADA6AE" />
                                                        <path d="M9.99984 5.33132L12.879 6.77085H7.12072L9.99984 5.33132ZM9.99984 4.16669L4.7915 6.77085V7.81252H15.2082V6.77085L9.99984 4.16669Z" fill="#ADA6AE" />
                                                    </svg>

                                                </div>
                                            </div>
                                            <div className="container-belum-dicairkan">
                                                <div className="header">BELUM DICAIRKAN</div>
                                                <div className="foot-string">Pencairan dari transaksi</div>
                                                <div className="foot-date">19 Sep 2020</div>
                                                <div className="money">Rp 100.000</div>
                                                <div className="svgs">
                                                    <svg width="5.333vw" height="5.333vw" viewBox="0 0 20 20" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                        <mask id="mask0" mask-type="alpha" maskUnits="userSpaceOnUse" x="0" y="0" width="20" height="20">
                                                            <path d="M20 10C20 15.5228 15.5228 20 10 20C4.47715 20 0 15.5228 0 10C0 4.47715 4.47715 0 10 0C15.5228 0 20 4.47715 20 10Z" fill="#C4C4C4" />
                                                        </mask>
                                                        <g mask="url(#mask0)">
                                                            <path d="M19.5 10C19.5 15.2467 15.2467 19.5 10 19.5C4.75329 19.5 0.5 15.2467 0.5 10C0.5 4.75329 4.75329 0.5 10 0.5C15.2467 0.5 19.5 4.75329 19.5 10Z" fill="#F7F6F6" stroke="#F0EDED" />
                                                        </g>
                                                        <path d="M7.39567 8.33335H6.354V13.0209H7.39567V8.33335Z" fill="#ADA6AE" />
                                                        <path d="M10.5207 8.33335H9.479V13.0209H10.5207V8.33335Z" fill="#ADA6AE" />
                                                        <path d="M15.2082 13.5417H4.7915V14.5834H15.2082V13.5417Z" fill="#ADA6AE" />
                                                        <path d="M13.6457 8.33335H12.604V13.0209H13.6457V8.33335Z" fill="#ADA6AE" />
                                                        <path d="M9.99984 5.33132L12.879 6.77085H7.12072L9.99984 5.33132ZM9.99984 4.16669L4.7915 6.77085V7.81252H15.2082V6.77085L9.99984 4.16669Z" fill="#ADA6AE" />
                                                    </svg>

                                                </div>
                                            </div>
                                            <div className="container-belum-dicairkan">
                                                <div className="header">BELUM DICAIRKAN</div>
                                                <div className="foot-string">Pencairan dari transaksi</div>
                                                <div className="foot-date">19 Sep 2020</div>
                                                <div className="money">Rp 100.000</div>
                                                <div className="svgs">
                                                    <svg width="5.333vw" height="5.333vw" viewBox="0 0 20 20" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                        <mask id="mask0" mask-type="alpha" maskUnits="userSpaceOnUse" x="0" y="0" width="20" height="20">
                                                            <path d="M20 10C20 15.5228 15.5228 20 10 20C4.47715 20 0 15.5228 0 10C0 4.47715 4.47715 0 10 0C15.5228 0 20 4.47715 20 10Z" fill="#C4C4C4" />
                                                        </mask>
                                                        <g mask="url(#mask0)">
                                                            <path d="M19.5 10C19.5 15.2467 15.2467 19.5 10 19.5C4.75329 19.5 0.5 15.2467 0.5 10C0.5 4.75329 4.75329 0.5 10 0.5C15.2467 0.5 19.5 4.75329 19.5 10Z" fill="#F7F6F6" stroke="#F0EDED" />
                                                        </g>
                                                        <path d="M7.39567 8.33335H6.354V13.0209H7.39567V8.33335Z" fill="#ADA6AE" />
                                                        <path d="M10.5207 8.33335H9.479V13.0209H10.5207V8.33335Z" fill="#ADA6AE" />
                                                        <path d="M15.2082 13.5417H4.7915V14.5834H15.2082V13.5417Z" fill="#ADA6AE" />
                                                        <path d="M13.6457 8.33335H12.604V13.0209H13.6457V8.33335Z" fill="#ADA6AE" />
                                                        <path d="M9.99984 5.33132L12.879 6.77085H7.12072L9.99984 5.33132ZM9.99984 4.16669L4.7915 6.77085V7.81252H15.2082V6.77085L9.99984 4.16669Z" fill="#ADA6AE" />
                                                    </svg>

                                                </div>
                                            </div>
                                            <div className="container-telah-dicairkan">
                                                <div className="header">SUDAH DICAIRKAN</div>
                                                <div className="list-data">
                                                        <div className="date-type">19 September</div>
                                                     
                                                <div className="list-in-list">
                                                   <div className="svgs">
                                                    <svg width="5.333vw" height="5.333vw" viewBox="0 0 20 20" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                        <mask id="mask0" mask-type="alpha" maskUnits="userSpaceOnUse" x="0" y="0" width="20" height="20">
                                                            <path d="M20 10C20 15.5228 15.5228 20 10 20C4.47715 20 0 15.5228 0 10C0 4.47715 4.47715 0 10 0C15.5228 0 20 4.47715 20 10Z" fill="#C4C4C4" />
                                                        </mask>
                                                        <g mask="url(#mask0)">
                                                            <path d="M19.5 10C19.5 15.2467 15.2467 19.5 10 19.5C4.75329 19.5 0.5 15.2467 0.5 10C0.5 4.75329 4.75329 0.5 10 0.5C15.2467 0.5 19.5 4.75329 19.5 10Z" fill="#F7F6F6" stroke="#F0EDED" />
                                                        </g>
                                                        <path d="M7.39567 8.33335H6.354V13.0209H7.39567V8.33335Z" fill="#ADA6AE" />
                                                        <path d="M10.5207 8.33335H9.479V13.0209H10.5207V8.33335Z" fill="#ADA6AE" />
                                                        <path d="M15.2082 13.5417H4.7915V14.5834H15.2082V13.5417Z" fill="#ADA6AE" />
                                                        <path d="M13.6457 8.33335H12.604V13.0209H13.6457V8.33335Z" fill="#ADA6AE" />
                                                        <path d="M9.99984 5.33132L12.879 6.77085H7.12072L9.99984 5.33132ZM9.99984 4.16669L4.7915 6.77085V7.81252H15.2082V6.77085L9.99984 4.16669Z" fill="#ADA6AE" />
                                                    </svg>

                                                   </div>
                                                        <div className="sub-word-header">SUDAH DICAIRKAN</div>
                                                        <div className="foot-word">Pencairan pertama</div>
                                                        <div className="foot-time">19 Sep 2020, 08.00</div>
                                                        <div className="foot-money">Rp 25.000</div>
                                                        <div className="svgs-check">

                                                        <svg width="5.333vw" height="5.333vw" viewBox="0 0 20 20" fill="none" xmlns="http://www.w3.org/2000/svg">
<circle cx="10" cy="10" r="10" fill="white"/>
<path fill-rule="evenodd" clip-rule="evenodd" d="M10 0C4.48 0 0 4.48 0 10C0 15.52 4.48 20 10 20C15.52 20 20 15.52 20 10C20 4.48 15.52 0 10 0ZM8 15L3 9.99996L4.41 8.58996L8 12.17L15.59 4.57996L17 5.99996L8 15Z" fill="#007ACF"/>
</svg>

                                                        </div>
                                                        
                                                </div>

                                                <div className="list-in-list">
                                                   <div className="svgs">
                                                    <svg width="5.333vw" height="5.333vw" viewBox="0 0 20 20" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                        <mask id="mask0" mask-type="alpha" maskUnits="userSpaceOnUse" x="0" y="0" width="20" height="20">
                                                            <path d="M20 10C20 15.5228 15.5228 20 10 20C4.47715 20 0 15.5228 0 10C0 4.47715 4.47715 0 10 0C15.5228 0 20 4.47715 20 10Z" fill="#C4C4C4" />
                                                        </mask>
                                                        <g mask="url(#mask0)">
                                                            <path d="M19.5 10C19.5 15.2467 15.2467 19.5 10 19.5C4.75329 19.5 0.5 15.2467 0.5 10C0.5 4.75329 4.75329 0.5 10 0.5C15.2467 0.5 19.5 4.75329 19.5 10Z" fill="#F7F6F6" stroke="#F0EDED" />
                                                        </g>
                                                        <path d="M7.39567 8.33335H6.354V13.0209H7.39567V8.33335Z" fill="#ADA6AE" />
                                                        <path d="M10.5207 8.33335H9.479V13.0209H10.5207V8.33335Z" fill="#ADA6AE" />
                                                        <path d="M15.2082 13.5417H4.7915V14.5834H15.2082V13.5417Z" fill="#ADA6AE" />
                                                        <path d="M13.6457 8.33335H12.604V13.0209H13.6457V8.33335Z" fill="#ADA6AE" />
                                                        <path d="M9.99984 5.33132L12.879 6.77085H7.12072L9.99984 5.33132ZM9.99984 4.16669L4.7915 6.77085V7.81252H15.2082V6.77085L9.99984 4.16669Z" fill="#ADA6AE" />
                                                    </svg>

                                                   </div>
                                                        <div className="sub-word-header">SUDAH DICAIRKAN</div>
                                                        <div className="foot-word">Pencairan pertama</div>
                                                        <div className="foot-time">19 Sep 2020, 08.00</div>
                                                        <div className="foot-money">Rp 25.000</div>
                                                        <div className="svgs-check">

                                                        <svg width="5.333vw" height="5.333vw" viewBox="0 0 20 20" fill="none" xmlns="http://www.w3.org/2000/svg">
<circle cx="10" cy="10" r="10" fill="white"/>
<path fill-rule="evenodd" clip-rule="evenodd" d="M10 0C4.48 0 0 4.48 0 10C0 15.52 4.48 20 10 20C15.52 20 20 15.52 20 10C20 4.48 15.52 0 10 0ZM8 15L3 9.99996L4.41 8.58996L8 12.17L15.59 4.57996L17 5.99996L8 15Z" fill="#007ACF"/>
</svg>

                                                        </div>
                                                        
                                                </div>

                                                </div>
                                            
                                                <div className="list-data">
                                                        <div className="date-type">19 September</div>
                                                     
                                                <div className="list-in-list" onClick={()=>{
                                                    this.setState({
                                                        isOnRincian:true
                                                    })
                                                }}>
                                                   <div className="svgs">
                                                    <svg width="5.333vw" height="5.333vw" viewBox="0 0 20 20" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                        <mask id="mask0" mask-type="alpha" maskUnits="userSpaceOnUse" x="0" y="0" width="20" height="20">
                                                            <path d="M20 10C20 15.5228 15.5228 20 10 20C4.47715 20 0 15.5228 0 10C0 4.47715 4.47715 0 10 0C15.5228 0 20 4.47715 20 10Z" fill="#C4C4C4" />
                                                        </mask>
                                                        <g mask="url(#mask0)">
                                                            <path d="M19.5 10C19.5 15.2467 15.2467 19.5 10 19.5C4.75329 19.5 0.5 15.2467 0.5 10C0.5 4.75329 4.75329 0.5 10 0.5C15.2467 0.5 19.5 4.75329 19.5 10Z" fill="#F7F6F6" stroke="#F0EDED" />
                                                        </g>
                                                        <path d="M7.39567 8.33335H6.354V13.0209H7.39567V8.33335Z" fill="#ADA6AE" />
                                                        <path d="M10.5207 8.33335H9.479V13.0209H10.5207V8.33335Z" fill="#ADA6AE" />
                                                        <path d="M15.2082 13.5417H4.7915V14.5834H15.2082V13.5417Z" fill="#ADA6AE" />
                                                        <path d="M13.6457 8.33335H12.604V13.0209H13.6457V8.33335Z" fill="#ADA6AE" />
                                                        <path d="M9.99984 5.33132L12.879 6.77085H7.12072L9.99984 5.33132ZM9.99984 4.16669L4.7915 6.77085V7.81252H15.2082V6.77085L9.99984 4.16669Z" fill="#ADA6AE" />
                                                    </svg>

                                                   </div>
                                                        <div className="sub-word-header">SUDAH DICAIRKAN</div>
                                                        <div className="foot-word">Pencairan pertama</div>
                                                        <div className="foot-time">19 Sep 2020, 08.00</div>
                                                        <div className="foot-money">Rp 25.000</div>
                                                        <div className="svgs-check">

                                                        <svg width="5.333vw" height="5.333vw" viewBox="0 0 20 20" fill="none" xmlns="http://www.w3.org/2000/svg">
<circle cx="10" cy="10" r="10" fill="white"/>
<path fill-rule="evenodd" clip-rule="evenodd" d="M10 0C4.48 0 0 4.48 0 10C0 15.52 4.48 20 10 20C15.52 20 20 15.52 20 10C20 4.48 15.52 0 10 0ZM8 15L3 9.99996L4.41 8.58996L8 12.17L15.59 4.57996L17 5.99996L8 15Z" fill="#007ACF"/>
</svg>

                                                        </div>
                                                        
                                                </div>

                                                <div className="list-in-list">
                                                   <div className="svgs">
                                                    <svg width="5.333vw" height="5.333vw" viewBox="0 0 20 20" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                        <mask id="mask0" mask-type="alpha" maskUnits="userSpaceOnUse" x="0" y="0" width="20" height="20">
                                                            <path d="M20 10C20 15.5228 15.5228 20 10 20C4.47715 20 0 15.5228 0 10C0 4.47715 4.47715 0 10 0C15.5228 0 20 4.47715 20 10Z" fill="#C4C4C4" />
                                                        </mask>
                                                        <g mask="url(#mask0)">
                                                            <path d="M19.5 10C19.5 15.2467 15.2467 19.5 10 19.5C4.75329 19.5 0.5 15.2467 0.5 10C0.5 4.75329 4.75329 0.5 10 0.5C15.2467 0.5 19.5 4.75329 19.5 10Z" fill="#F7F6F6" stroke="#F0EDED" />
                                                        </g>
                                                        <path d="M7.39567 8.33335H6.354V13.0209H7.39567V8.33335Z" fill="#ADA6AE" />
                                                        <path d="M10.5207 8.33335H9.479V13.0209H10.5207V8.33335Z" fill="#ADA6AE" />
                                                        <path d="M15.2082 13.5417H4.7915V14.5834H15.2082V13.5417Z" fill="#ADA6AE" />
                                                        <path d="M13.6457 8.33335H12.604V13.0209H13.6457V8.33335Z" fill="#ADA6AE" />
                                                        <path d="M9.99984 5.33132L12.879 6.77085H7.12072L9.99984 5.33132ZM9.99984 4.16669L4.7915 6.77085V7.81252H15.2082V6.77085L9.99984 4.16669Z" fill="#ADA6AE" />
                                                    </svg>

                                                   </div>
                                                        <div className="sub-word-header">SUDAH DICAIRKAN</div>
                                                        <div className="foot-word">Pencairan pertama</div>
                                                        <div className="foot-time">19 Sep 2020, 08.00</div>
                                                        <div className="foot-money">Rp 25.000</div>
                                                        <div className="svgs-check">

                                                        <svg width="5.333vw" height="5.333vw" viewBox="0 0 20 20" fill="none" xmlns="http://www.w3.org/2000/svg">
<circle cx="10" cy="10" r="10" fill="white"/>
<path fill-rule="evenodd" clip-rule="evenodd" d="M10 0C4.48 0 0 4.48 0 10C0 15.52 4.48 20 10 20C15.52 20 20 15.52 20 10C20 4.48 15.52 0 10 0ZM8 15L3 9.99996L4.41 8.58996L8 12.17L15.59 4.57996L17 5.99996L8 15Z" fill="#007ACF"/>
</svg>

                                                        </div>
                                                        
                                                </div>

                                                </div>
                                            
                                            
                                            
                                            
                                            </div>
                                      
                                            { this.state.isOnRincian ?
              
              <div className="overlay-bottom-rincian z-index-2">
                   <div className="overLay-background-deflect">
                   </div> 
                  <div className="rincian-detail-rekening">
                      <div className="header">Rincian Sudah Masuk Rekening</div>
                      <div className="topHeader">
                          <div className="money">Rp 595.800</div>
                          <div className="wording-footer">Pencairan terakhir 18 Sep 2020, 12:00</div>
                      </div>
                      {/* <div className="bottom-divider"></div> */}
                      <div className="alowanceHeader">
                          <div className="potongan">Pendapatan sebelum potongan</div>
                          <div className="potongan-money">Rp 600.000</div>
                      </div>
                      <div className="biayaHeader">
                          <div className="containerStruts">
                              <div className="biaya">Biaya 0,7%</div>
                              <div className="biaya-footer">Biaya yang diterapkan oleh pemerintah</div>
                          </div>
                          <div className="biaya-money">Rp1.740</div>
                      </div>
                      <div className="button-close-rincian" onClick={
                          () =>{
                              this.setState({
                                  isOnRincian : false
                              })
                          }
                      }>
                          <span>
                          Oke
                          </span>
                      </div>
                  </div>
                   
              </div> : <div></div>
              }
                                      
                                        </div>

                                    </span>
                                }
                            </div>
                            :
                            <div></div>
                        }
                    </div>
                    :
                    <div>
                        <div>
                            {!this.state.isOncheckPeriode ?
                                <div className="container-pilih-periode">
                                    <div className="x-container-width" onClick={
                                        () => {
                                            this.setState({ isNotOnPeriode: true })
                                        }
                                    }>X</div>
                                    {this.state.isNotValidDateFilter ?
                                        <div className="error-wording-tanggal">
                                            <div className="header">Data tidak dapat ditampilkan</div>
                                            <div className="body"
                                            >Pencarian transaksi
                                                maksimum 7 hari
                                                              dalam periode 3 bulan terakhir</div>
                                        </div>
                                        : <div></div>}
                                    <div className="suplementary-div">
                                        <div className="header-periode">Pilih Periode</div>
                                        <div className="subHeader-periode">Transaksi yang ditampilkan adalah
                                                minimal 7 hari dan  maksimal 90 hari</div>
                                        <span>
                                            <div className="tanggal-list ">
                                                <div className={!this.state.isStartDate ? "start" : "start blue-print"}
                                                    onClick={
                                                        () => {
                                                            this.setState({
                                                                isOpenCalendar: true,
                                                                isEndDate: false,
                                                                isStartDate: true
                                                            })
                                                        }
                                                    }
                                                >
                                                    <div className="word">Dari Tanggal</div>
                                                    <div className={this.state.startDate == null ? "date" : "date blue-print"}>
                                                        {this.state.startDate == null ? 'Pilih tanggal' : this.state.startDate}
                                                    </div>
                                                </div>
                                                <i className="fa fa-chevron-right custom-wagon"></i>
                                                <div className={!this.state.isEndDate ? "end" : "end blue-print"}
                                                    onClick={
                                                        () => {
                                                            this.setState({
                                                                isOpenCalendar: true,
                                                                isEndDate: true,
                                                                isStartDate: false
                                                            })
                                                        }
                                                    }
                                                >
                                                    <div className="word">Hingga Tanggal</div>
                                                    <div className={this.state.stopDate == null ? "date" : "date blue-print"}>
                                                        {this.state.stopDate == null ? 'Pilih tanggal' : this.state.stopDate}</div>
                                                </div>
                                            </div>
                                            {this.state.isOpenCalendar ?
                                                <div className="custom-j-calendar">
                                                    <div className="tail-datetime-calendar">
                                                        <i className="fa fa-chevron-left custom-wagon" onClick={this.onPrev}></i>
                                                        {!this.state.showMonthTable && !this.state.showYearEditor && (
                                                            <span
                                                                className="calendar-label-month"
                                                                class="calendar-label"
                                                            >
                                                                {this.month()}
                                                            </span>
                                                        )}
                                                        <span
                                                            className="calendar-label-year"
                                                        >
                                                            {this.year()}
                                                        </span>
                                                        <i className="fa fa-chevron-right custom-wagon" onClick={this.onNext}></i>
                                                        {this.state.showCalendarTable && (
                                                            <div className="calendar-date">
                                                                <table className="calendar-day">
                                                                    <thead>
                                                                        <tr>{weekdayshortname}</tr>
                                                                    </thead>
                                                                    <tbody>{daysinmonth}</tbody>
                                                                </table>
                                                            </div>
                                                        )}
                                                        <span className="batal"
                                                            onClick={
                                                                () => {
                                                                    this.setState({
                                                                        isOpenCalendar: false,
                                                                        isEndDate: false,
                                                                        isStartDate: false
                                                                    })
                                                                }
                                                            }
                                                        >Batal</span>
                                                        <span className="ok" onClick={
                                                            () => {
                                                                if (this.state.isEndDate) {
                                                                    this.setState({
                                                                        stopDateInFormat: this.state.selectedDayInFormat,
                                                                        stopDate: this.state.selectedDay,
                                                                        selectedDayInWithoutYearEnd: this.state.selectedDayInWithoutYear
                                                                    },
                                                                        () => {
                                                                            this.getDetailTask();
                                                                        }
                                                                    );
                                                                }

                                                                if (this.state.isStartDate) {
                                                                    this.setState({
                                                                        startDateInFormat: this.state.selectedDayInFormat,
                                                                        startDate: this.state.selectedDay,
                                                                        selectedDayInWithoutYearStart: this.state.selectedDayInWithoutYear

                                                                    },
                                                                        () => {
                                                                            this.getDetailTask();

                                                                        }
                                                                    );
                                                                }


                                                                this.setState({
                                                                    isOpenCalendar: false,
                                                                    isEndDate: false,
                                                                    isStartDate: false
                                                                })


                                                                console.log('stopDate' + this.state.stopDate + " startdate " + this.state.startDate)

                                                                //this.props.valueCalendar = this.state.selectedDay;
                                                                //localStorage.setItem('selectedDayValue',this.state.selectedDay);
                                                            }
                                                        }>OK</span>
                                                    </div>
                                                </div> : <div></div>
                                            }
                                        </span>
                                    </div>

                                </div>
                                :
                                <div className="container-pilih-periode">
                                    <div>Hai</div>
                                </div>
                            }
                        </div>
                    </div>

                }
            </div>
        );
    }
}