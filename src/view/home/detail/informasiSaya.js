import React from 'react';
import { BACK_END_POINT, GET, HEADER_AUTH, POST, TOKEN_AUTH, wsWithBody, wsWithoutBody } from '../../../master/masterComponent'
import '../../../assets/css/homeScreen.css';
import HeaderInfo from '../../../master/HeaderInfo';
import ChevronRightIcon from '@material-ui/icons/ChevronRight';
export default class informasiSaya extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            namaLengkap: '',
            nomorHandphone: true,
            email: '',
            nomorRekening: '',
            tipeAkun :'',
            maksimalPembayaranPerHari :''
        }
    }

    componentDidMount(){
        window.onpopstate = function(event) {
            window.location.reload();
        };
        wsWithoutBody(BACK_END_POINT + "/loginCtl/detailToken", GET, {
            ...HEADER_AUTH, 'secret-token': localStorage.getItem(TOKEN_AUTH)
        }).then(response => {
            var result = response.data.result;
            this.setState({
                namaLengkap : result.userOobTableSuccessDto.namaPemilikUsaha,
                nomorHandphone : result.userOobTableSuccessDto.noHandphone,
                email : result.userOobTableSuccessDto.emailPemilikUsaha,
                nomorRekening : result.userOobTableSuccessDto.nomorRekening,
                tipeAkun : result.userOobMidDtlTableDto.akunOobMidDesc
            })

        })
    }

    render() {
        return (
            <div>
                <HeaderInfo headerName={"Tahukah Anda?"} urlBack={"/profileSaya"}></HeaderInfo>
                <div className="header-topper">
                    <p>Keuntungan Akun Bisnis</p>
                </div>
                <div className="body-topper">
                    <p>Dengan akun bisnis, maka Anda bisa melakukan pencairan dengan limit 
                        hingga Rp 10 juta per hari. 
                     </p>   
<p> Limit pencairan untuk tiap akun bisnis dapat berbeda-beda sesuai dengan kebijakan Bank Mandiri.</p>
                </div>
            </div>
        );
    }
}