import React from "react";
import moment from "moment";
import { range } from "moment-range";
import '../detail/CalendarCusso.scss';
import '../detail/pencairanDetail.scss';

export default class CalendarMaker extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            isYouKnowExpands: true,
            namaLengkap: '',
            nomorHandphone: true,
            email: '',
            nomorRekening: '',
            tipeAkun: '',
            maksimalPembayaranPerHari: '',
            isOnNotifikasiBar: true,
            isShowDate: false,
            isNotOnPeriode: false,
            isOncheckPeriode: false,
            calendarMerge: {},
            valueCalender: '',
            showCalendarTable: true,
            showMonthTable: false,
            dateObject: moment(),
            allmonths: moment.months(),
            showYearNav: false,
            selectedDate: null,
            selectedDay: null,
            selectedMonth: null,
            selectedYear: null,
            isStartDate:false,
            isEndDate:false,
            startDate:null,
            stopDate:null,
            isOpenCalendar:false
        }
    }


    weekdayshort = moment.weekdaysShort();

    daysInMonth = () => {
        return this.state.dateObject.daysInMonth();
    };
    year = () => {
        return this.state.dateObject.format("Y");
    };
    currentDay = () => {
        return this.state.dateObject.format("D");
    };
    firstDayOfMonth = () => {
        let dateObject = this.state.dateObject;
        let firstDay = moment(dateObject)
            .startOf("month")
            .format("d"); // Day of week 0...1..5...6
        return firstDay;
    };
    month = () => {
        return this.state.dateObject.format("MMMM");
    };
    showMonth = (e, month) => {
        this.setState({
            showMonthTable: !this.state.showMonthTable,
            showCalendarTable: !this.state.showCalendarTable
        });
    };
    setMonth = month => {
        let monthNo = this.state.allmonths.indexOf(month);
        let dateObject = Object.assign({}, this.state.dateObject);
        dateObject = moment(dateObject).set("month", monthNo);
        this.setState({
            dateObject: dateObject,
            showMonthTable: !this.state.showMonthTable,
            showCalendarTable: !this.state.showCalendarTable
        });
    };
    MonthList = props => {
        let months = [];
        props.data.map(data => {
            months.push(
                <td
                    key={data}
                    className="calendar-month"
                    onClick={e => {
                        this.setMonth(data);
                    }}
                >
                    <span>{data}</span>
                </td>
            );
        });
        let rows = [];
        let cells = [];

        months.forEach((row, i) => {
            if (i % 3 !== 0 || i == 0) {
                cells.push(row);
            } else {
                rows.push(cells);
                cells = [];
                cells.push(row);
            }
        });
        rows.push(cells);
        let monthlist = rows.map((d, i) => {
            return <tr>{d}</tr>;
        });

        return (
            <table className="calendar-month">
                <thead>
                    <tr>
                        <th colSpan="4">Select a Month</th>
                    </tr>
                </thead>
                <tbody>{monthlist}</tbody>
            </table>
        );
    };
    showYearEditor = () => {
        this.setState({
            showYearNav: true,
            showCalendarTable: !this.state.showCalendarTable
        });
    };

    onPrev = () => {
        let curr = "";
        if (this.state.showMonthTable == true) {
            curr = "year";
        } else {
            curr = "month";
        }
        this.setState({
            dateObject: this.state.dateObject.subtract(1, curr)
        });
    };
    onNext = () => {
        let curr = "";
        if (this.state.showMonthTable == true) {
            curr = "year";
        } else {
            curr = "month";
        }
        this.setState({
            dateObject: this.state.dateObject.add(1, curr)
        });
    };
    setYear = year => {
        // alert(year)
        let dateObject = Object.assign({}, this.state.dateObject);
        dateObject = moment(dateObject).set("year", year);
        this.setState({
            dateObject: dateObject,
            showMonthTable: !this.state.showMonthTable,
            showYearNav: !this.state.showYearNav,
            showMonthTable: !this.state.showMonthTable
        });
    };
    onYearChange = e => {
        this.setYear(e.target.value);
    };
    getDates(startDate, stopDate) {
        var dateArray = [];
        var currentDate = moment(startDate);
        var stopDate = moment(stopDate);
        while (currentDate <= stopDate) {
            dateArray.push(moment(currentDate).format("YYYY"));
            currentDate = moment(currentDate).add(1, "year");
        }
        return dateArray;
    }
    YearTable = props => {
        let months = [];
        let nextten = moment()
            .set("year", props)
            .add("year", 12)
            .format("Y");

        let tenyear = this.getDates(props, nextten);

        tenyear.map(data => {
            months.push(
                <td
                    key={data}
                    className="calendar-month"
                    onClick={e => {
                        this.setYear(data);
                    }}
                >
                    <span>{data}</span>
                </td>
            );
        });
        let rows = [];
        let cells = [];

        months.forEach((row, i) => {
            if (i % 3 !== 0 || i == 0) {
                cells.push(row);
            } else {
                rows.push(cells);
                cells = [];
                cells.push(row);
            }
        });
        rows.push(cells);
        let yearlist = rows.map((d, i) => {
            return <tr>{d}</tr>;
        });

        return (
            <table className="calendar-month">
                <thead>
                    <tr>
                        <th colSpan="4">Select a Yeah</th>
                    </tr>
                </thead>
                <tbody>{yearlist}</tbody>
            </table>
        );
    };
    onDayClick = (e, d) => {
        this.setState(
            {
                selectedDay: d + ' ' + this.month().substring(0, 3) + ' ' + this.year(),
                selectedDate: d
            },
            () => {
                console.log("SELECTED DAY: ", this.state.selectedDay);
            }
        );
    };

    render() {
        let weekdayshortname = this.weekdayshort.map(day => {
            return <th key={day}>{day.charAt(0)}</th>;
        });
        let blanks = [];
        for (let i = 0; i < this.firstDayOfMonth(); i++) {
            blanks.push(<td className="calendar-day empty">{""}</td>);
        }
        let daysInMonth = [];
        for (let d = 1; d <= this.daysInMonth(); d++) {
            let currentDay = d == this.currentDay() ? "current-today" : "";
            var h = this.currentDay();
            console.log('current day : ' + h + " d : " + d + " selected date " + this.state.selectedDate);
            console.log('selected day : ' + (this.state.selectedDate === h));
            if (this.state.selectedDate == d) {
                currentDay = 'selected-value';
            }

            daysInMonth.push(
                <td key={d} className={currentDay} >
                    <span
                        onClick={e => {
                            this.onDayClick(e, d);
                        }}
                    >
                        {d}
                    </span>
                </td>
            );
        }
        var totalSlots = [...blanks, ...daysInMonth];
        let rows = [];
        let cells = [];

        totalSlots.forEach((row, i) => {
            if (i % 7 !== 0) {
                cells.push(row);
            } else {
                rows.push(cells);
                cells = [];
                cells.push(row);
            }
            if (i === totalSlots.length - 1) {
                // let insertRow = cells.slice();
                rows.push(cells);
            }
        });

        let daysinmonth = rows.map((d, i) => {
            return <tr>{d} </tr>;
        });

        return (
            <span>
                <div className="tanggal-list ">
                    <div className={!this.state.isStartDate ? "start" : "start blue-print"}
                    onClick={
                        ()=>{
                            this.setState({
                                isOpenCalendar:true,
                                isEndDate:false,
                                isStartDate:true
                            })
                        }
                    }
                    >
                        <div className="word">Dari Tanggal</div>
                        <div className={this.state.startDate==null ? "date" : "date blue-print"}>
                         {this.state.startDate==null? 'Pilih tanggal' : this.state.startDate}
                         </div>
                    </div>
                    <i className="fa fa-chevron-right custom-wagon"></i>
                    <div className={!this.state.isEndDate? "end" : "end blue-print"}
                     onClick={
                        ()=>{
                            this.setState({
                                isOpenCalendar:true,
                                isEndDate:true,
                                isStartDate:false
                            })
                        }
                    }
                    >
                        <div className="word">Hingga Tanggal</div>
                        <div className={this.state.stopDate==null ? "date" : "date blue-print"}>
                        {this.state.stopDate==null ? 'Pilih tanggal': this.state.stopDate}</div>
                    </div>
                </div>
                { this.state.isOpenCalendar?
                <div className="custom-j-calendar">
                    <div className="tail-datetime-calendar">
                        <i className="fa fa-chevron-left custom-wagon" onClick={this.onPrev}></i>
                        {!this.state.showMonthTable && !this.state.showYearEditor && (
                            <span
                                className="calendar-label-month"
                                onClick={e => {
                                    this.showMonth();
                                }}
                                class="calendar-label"
                            >
                                {this.month()}
                            </span>
                        )}
                        <span
                            className="calendar-label-year"
                            onClick={e => {
                                this.showYearEditor();
                            }}
                        >
                            {this.year()}
                        </span>
                        <i className="fa fa-chevron-right custom-wagon" onClick={this.onNext}></i>
                        {this.state.showCalendarTable && (
                            <div className="calendar-date">
                                <table className="calendar-day">
                                    <thead>
                                        <tr>{weekdayshortname}</tr>
                                    </thead>
                                    <tbody>{daysinmonth}</tbody>
                                </table>
                            </div>
                        )}
                        <span className="batal"
                        onClick={
                            () =>{
                                this.setState({
                                    isOpenCalendar:false,
                                    isEndDate:false,
                                    isStartDate:false
                                })
                            }
                        }
                        >Batal</span>
                        <span className="ok" onClick={
                            () => {
                                if(this.state.isEndDate){
                                    this.setState({
                                        stopDate:this.state.selectedDay
                                    });
                                }

                                if(this.state.isStartDate){
                                    this.setState({
                                        startDate:this.state.selectedDay
                                    });
                                }

                                this.setState({
                                    isOpenCalendar:false,
                                    isEndDate:false,
                                    isStartDate:false
                                })
                                //this.props.valueCalendar = this.state.selectedDay;
                                //localStorage.setItem('selectedDayValue',this.state.selectedDay);
                            }
                        }>OK</span>
                    </div>
                </div>:<div></div>
                }
            </span>
        );
    }
}