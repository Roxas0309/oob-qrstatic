import React from 'react';
import { BACK_END_POINT, GET, HEADER_AUTH, POST, TOKEN_AUTH, wsWithBody, wsWithoutBody } from '../../../master/masterComponent'
import '../../../assets/css/homeScreen.css';
import HeaderInfo from '../../../master/HeaderInfo';
import ChevronRightIcon from '@material-ui/icons/ChevronRight';
import docImg from '../../../../public/img/file-text.png'
// import bayar from '../../../../public/img/bayar.png'
export default class profileSaya extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            namaLengkap: '',
            nomorHandphone: true,
            email: '',
            nomorRekening: '',
            tipeAkun: '',
            tipeAkunCode:'',
            maksimalPembayaranPerHari: ''
        }
    }

    componentDidMount() {


        // history.pushState(null, null, location.href);
        window.onpopstate = function (event) {
            window.location.reload();
        };

        wsWithoutBody(BACK_END_POINT + "/loginCtl/detailToken", GET, {
            ...HEADER_AUTH, 'secret-token': localStorage.getItem(TOKEN_AUTH)
        }).then(response => {
            var result = response.data.result;
            this.setState({
                namaLengkap: result.userOobTableSuccessDto.namaPemilikUsaha,
                nomorHandphone: result.userOobTableSuccessDto.noHandphone,
                email: result.userOobTableSuccessDto.emailPemilikUsaha,
                nomorRekening: result.userOobTableSuccessDto.nomorRekening,
                tipeAkun: result.userOobMidDtlTableDto.akunOobMidDesc,
                tipeAkunCode : result.userOobMidDtlTableDto.akunOobMid
            })

        })
    }

    letMeLogOut = () => {
        var tokenVariable = localStorage.getItem(TOKEN_AUTH);
        wsWithBody(BACK_END_POINT + "/loginCtl/out-auth", POST, {
            tokenLocal: tokenVariable
        }, HEADER_AUTH).then(response => {
            localStorage.removeItem(TOKEN_AUTH);
            this.props.history.push('/login');
            location.reload();
        })
    }

    render() {
        return (
            <div>
                <HeaderInfo headerName={"Profile Saya"} urlBack={"/homeScreen"}></HeaderInfo>
                <div className="current-detail">
                    <div className="center-detail">
                        <div className="img-incubator">
                        <svg style={{margin:'auto',display:'block'}} width="6.522vw" height="6.763vw" viewBox="0 0 27 28" fill="none" xmlns="http://www.w3.org/2000/svg">
<circle cx="14.125" cy="6.125" r="6" fill="#F7F8F9"/>
<path fill-rule="evenodd" clip-rule="evenodd" d="M8.46638 13.125H6.82927C5.07325 13.125 3.52258 14.2703 3.00616 15.9487L0.967695 22.5737C0.176245 25.1459 2.09959 27.75 4.79081 27.75H22.0841C24.7754 27.75 26.6987 25.1459 25.9073 22.5737L23.8688 15.9487C23.3524 14.2703 21.8017 13.125 20.0457 13.125H19.7836C18.9599 15.4554 16.7374 17.125 14.125 17.125C11.5125 17.125 9.29006 15.4554 8.46638 13.125Z" fill="#F7F8F9"/>
</svg>
              
                        </div>
                    </div>
                    <div className="recent-detail-info">
                        <div className="container-recent">
                            <div className="comp-key">Nama Lengkap</div>
                            <div className="comp-value">{this.state.namaLengkap}</div>
                        </div>
                        <div className="container-recent">
                            <div className="comp-key">Nomor Handphone</div>
                            <div className="comp-value">{this.state.nomorHandphone}</div>
                        </div>
                        <div className="container-recent">
                            <div className="comp-key">Email</div>
                            <div className="comp-value">{this.state.email}</div>
                        </div>
                        <div className="container-recent">
                            <div className="comp-key">Nomor Rekening Bank Mandiri</div>
                            <div className="comp-value">{this.state.nomorRekening}</div>
                        </div>
                        <div className="container-recent">
                            <div className="comp-key">Tipe Akun</div>
                            <div className="comp-value">{this.state.tipeAkun}</div>
                        </div>
                        { this.state.tipeAkunCode === 'H1H0U-8WGRC-GHGYO-JYRAH' ?
                        <div className="container-recent">
                            <div className="comp-key">Maksimal Pencairan per Hari</div>
                            <div className="comp-value">Rp500.000</div>
                        </div>
                        :
                         <div className="container-recent">
                         <div className="comp-key">Maksimal Pencairan per Hari</div>
                         <div className="comp-value">Rp10.000.000</div>
                        </div>
                        }
                    </div>
                    <div className="tahukah-anda-info" onClick={
                        () => {
                            if(this.state.tipeAkunCode === 'H1H0U-8WGRC-GHGYO-JYRAH'){
                            this.props.history.push('/informasiSaya');
                            location.reload();
                            }
                            else{
                                this.props.history.push('/informasiSayaUpgrade');
                                location.reload();
                            }
                        }}>
                        <p>
                            Tahukah Anda? Tipe akun Bisnis memiliki keuntungan lebih.
                            Yuk, baca info selengkapnya di sini!
                        </p>
                    </div>

                    <div className="recent-footer">
                        <div className="button-footer-item" onClick={()=>{
                             this.props.history.push('/kebijakanPrivasiNHukum');
                             location.reload();
                        }}>

                            <div className="footer-item">
                                <svg width="5.430vw" height="5.430vw" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                                    <path d="M14 2H6C5.46957 2 4.96086 2.21071 4.58579 2.58579C4.21071 2.96086 4 3.46957 4 4V20C4 20.5304 4.21071 21.0391 4.58579 21.4142C4.96086 21.7893 5.46957 22 6 22H18C18.5304 22 19.0391 21.7893 19.4142 21.4142C19.7893 21.0391 20 20.5304 20 20V8M14 2L20 8M14 2V8H20M16 13H8M16 17H8M10 9H8" stroke="#0057E7" strokeWidth="2" strokeLinecap="round" strokeLinejoinn="round" />
                                </svg>
                                <span className='text-flatten-footer'>
                                    Kebijakan Privasi &amp; Hukum
                                </span>
                            </div>
                            <i className="fa fa-chevron-right custom-wagon"></i>
                          
                        </div>

                        <div className="button-footer-item"
                        onClick={()=>{
                            this.props.history.push('/snkMitraQris');
                            location.reload();
                        }}>

                        <div className="footer-item" >
                                <svg width="5.430vw" height="5.430vw" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                                    <path d="M14 2H6C5.46957 2 4.96086 2.21071 4.58579 2.58579C4.21071 2.96086 4 3.46957 4 4V20C4 20.5304 4.21071 21.0391 4.58579 21.4142C4.96086 21.7893 5.46957 22 6 22H18C18.5304 22 19.0391 21.7893 19.4142 21.4142C19.7893 21.0391 20 20.5304 20 20V8M14 2L20 8M14 2V8H20M16 13H8M16 17H8M10 9H8" stroke="#0057E7" strokeWidth="2" strokeLinecap="round" strokeLinejoin="round" />
                                </svg>
                                <span className='text-flatten-footer'>
                                    Syarat &amp; Ketentuan Usaha
                                </span>
                         </div>
                            <i className="fa fa-chevron-right custom-wagon"></i>
                        </div>
                        <div className="button-footer-item"  onClick={()=>{
                             this.props.history.push('/tentangAplikasiUsahaMandiri');
                             location.reload();
                        }}>

                        <div className="footer-item">
                               
                                <svg width="5.430vw" height="5.430vw" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                                    <path fillRule="evenodd" clipRule="evenodd" d="M19 16H20.0521C21.4983 16 22.4389 17.5646 21.7921 18.8944L20.8194 20.8944C20.4898 21.572 19.8162 22 19.0793 22H17H4.9479H4C2.8954 22 2 21.1046 2 20V4C2 2.8954 2.8954 2 4 2H17C18.1046 2 19 2.8954 19 4V16ZM4 17.4769L4.1806 17.1055C4.5101 16.428 5.1838 16 5.9207 16H17V4H4V17.4769ZM19.0303 20L19.9936 18.0197C19.9958 18.0089 20.0009 18.003 20.0073 18H19H17H5.9697L5.0065 19.9803C5.0042 19.9911 4.9992 19.997 4.9928 20H17H19H19.0303ZM6 6H15V8H6V6ZM15 9H6V11H15V9ZM6 12H12V14H6V12Z" fill="#007ACF" />
                                </svg>
                                <span className='text-flatten-footer'>
                                    Tentang Aplikasi Usaha Mandiri
                             </span>
                            </div>
                            <i className="fa fa-chevron-right custom-wagon"></i>
                        </div>

                        <div className="button-footer-logout" onClick={this.letMeLogOut}>Log Out</div>
                    </div>

                </div>
            </div>
        );
    }
}