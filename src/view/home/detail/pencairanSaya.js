import React from 'react';
import { BACK_END_POINT, GET, HEADER_AUTH, POST, TOKEN_AUTH, wsWithBody, wsWithoutBody } from '../../../master/masterComponent'
import '../detail/pencairanDetail.scss';
import HeaderInfo from '../../../master/HeaderInfo';
import ChevronRightIcon from '@material-ui/icons/ChevronRight';
export default class pencairanSaya extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            isYouKnowExpands: true,
            namaLengkap: '',
            nomorHandphone: true,
            email: '',
            nomorRekening: '',
            tipeAkun: '',
            maksimalPembayaranPerHari: '',
            isOnNotifikasiBar: true,
            isOnRincian : false
        }
    }

    componentDidMount() {

        window.onpopstate = function(event) {
            window.location.reload();
        };

        wsWithoutBody(BACK_END_POINT + "/loginCtl/detailToken", GET, {
            ...HEADER_AUTH, 'secret-token': localStorage.getItem(TOKEN_AUTH)
        }).then(response => {
            var result = response.data.result;
            this.setState({
                namaLengkap: result.userOobTableSuccessDto.namaPemilikUsaha,
                nomorHandphone: result.userOobTableSuccessDto.noHandphone,
                email: result.userOobTableSuccessDto.emailPemilikUsaha,
                nomorRekening: result.userOobTableSuccessDto.nomorRekening,
                tipeAkun: result.userOobMidDtlTableDto.akunOobMidDesc
            })

        })
    }

    render() {
        return (
            <div>
                <HeaderInfo headerName={"Pencairan"} urlBack={"/riwayatTransaksi"}></HeaderInfo>
                <div className="pencairan-me-container">
                    <div className="header-you-know">
                        <p className="informasi">Informasi yang perlu Anda ketahui</p>
                        <p className="jadwal">Jadwal pencairan 12:00, 16:00, dan 18:00
                    {this.state.isYouKnowExpands ? '' :
                                ' Maksimal pencairan Rp 500.000 per hari. Lebih dari itu maka sisa dana dicairkan besok'
                            }
                        </p>
                        <i className={this.state.isYouKnowExpands ? "fa fa-chevron-down custom-wagon"
                            : "fa fa-chevron-up custom-wagon"} onClick={
                                () => {
                                    var expands = this.state.isYouKnowExpands;
                                    if (expands) {
                                        this.setState({ isYouKnowExpands: false })
                                    }
                                    else {
                                        this.setState({ isYouKnowExpands: true })
                                    }
                                }
                            }></i>
                    </div>
                    <div className="rincian-detail-rekening">
                        <div className="header">SUDAH MASUK REKENING</div>
                        <div className="money">Rp 595.800</div>
                        <div className="rincian-button" onClick={
                            () => {
                                this.setState({
                                    isOnRincian:true
                                })
                            }
                        }>Rincian</div>
                        <div className="wording-footer">Setelah dikurangi biaya admin</div>
                    </div>

                    <div className="riwayat-container">
                        <p className="riwayat-wording">Riwayat Pencairan 13 Sep - 18 Sep 2020</p>
                    </div>

                    <div className="list-riwayat-container">
                        <img className="check" src="data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHZlcnNpb249IjEuMSIgeG1sbnM6eGxpbms9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkveGxpbmsiIHhtbG5zOnN2Z2pzPSJodHRwOi8vc3ZnanMuY29tL3N2Z2pzIiB3aWR0aD0iNTEyIiBoZWlnaHQ9IjUxMiIgeD0iMCIgeT0iMCIgdmlld0JveD0iMCAwIDUxMiA1MTIiIHN0eWxlPSJlbmFibGUtYmFja2dyb3VuZDpuZXcgMCAwIDUxMiA1MTIiIHhtbDpzcGFjZT0icHJlc2VydmUiPjxnPjxwYXRoIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgZD0ibTI1NiAwYy0xNDEuMTY0MDYyIDAtMjU2IDExNC44MzU5MzgtMjU2IDI1NnMxMTQuODM1OTM4IDI1NiAyNTYgMjU2IDI1Ni0xMTQuODM1OTM4IDI1Ni0yNTYtMTE0LjgzNTkzOC0yNTYtMjU2LTI1NnptMTI5Ljc1IDIwMS43NS0xMzguNjY3OTY5IDEzOC42NjQwNjJjLTQuMTYwMTU2IDQuMTYwMTU3LTkuNjIxMDkzIDYuMjUzOTA3LTE1LjA4MjAzMSA2LjI1MzkwN3MtMTAuOTIxODc1LTIuMDkzNzUtMTUuMDgyMDMxLTYuMjUzOTA3bC02OS4zMzIwMzEtNjkuMzMyMDMxYy04LjM0Mzc1LTguMzM5ODQzLTguMzQzNzUtMjEuODI0MjE5IDAtMzAuMTY0MDYyIDguMzM5ODQzLTguMzQzNzUgMjEuODIwMzEyLTguMzQzNzUgMzAuMTY0MDYyIDBsNTQuMjUgNTQuMjUgMTIzLjU4NTkzOC0xMjMuNTgyMDMxYzguMzM5ODQzLTguMzQzNzUgMjEuODIwMzEyLTguMzQzNzUgMzAuMTY0MDYyIDAgOC4zMzk4NDQgOC4zMzk4NDMgOC4zMzk4NDQgMjEuODIwMzEyIDAgMzAuMTY0MDYyem0wIDAiIGZpbGw9IiMwMDdhY2YiIGRhdGEtb3JpZ2luYWw9IiMwMDAwMDAiIHN0eWxlPSIiPjwvcGF0aD48L2c+PC9zdmc+" />
                        <div className="money">Rp 100.000</div>
                        <div className="date">Pencairan pertama , 18 Sep 2020, 12.00</div>
                        <div className="bottom-divider"></div>
                    </div>

                    <div className="list-riwayat-container">
                        <img className="check" src="data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHZlcnNpb249IjEuMSIgeG1sbnM6eGxpbms9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkveGxpbmsiIHhtbG5zOnN2Z2pzPSJodHRwOi8vc3ZnanMuY29tL3N2Z2pzIiB3aWR0aD0iNTEyIiBoZWlnaHQ9IjUxMiIgeD0iMCIgeT0iMCIgdmlld0JveD0iMCAwIDUxMiA1MTIiIHN0eWxlPSJlbmFibGUtYmFja2dyb3VuZDpuZXcgMCAwIDUxMiA1MTIiIHhtbDpzcGFjZT0icHJlc2VydmUiPjxnPjxwYXRoIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgZD0ibTI1NiAwYy0xNDEuMTY0MDYyIDAtMjU2IDExNC44MzU5MzgtMjU2IDI1NnMxMTQuODM1OTM4IDI1NiAyNTYgMjU2IDI1Ni0xMTQuODM1OTM4IDI1Ni0yNTYtMTE0LjgzNTkzOC0yNTYtMjU2LTI1NnptMTI5Ljc1IDIwMS43NS0xMzguNjY3OTY5IDEzOC42NjQwNjJjLTQuMTYwMTU2IDQuMTYwMTU3LTkuNjIxMDkzIDYuMjUzOTA3LTE1LjA4MjAzMSA2LjI1MzkwN3MtMTAuOTIxODc1LTIuMDkzNzUtMTUuMDgyMDMxLTYuMjUzOTA3bC02OS4zMzIwMzEtNjkuMzMyMDMxYy04LjM0Mzc1LTguMzM5ODQzLTguMzQzNzUtMjEuODI0MjE5IDAtMzAuMTY0MDYyIDguMzM5ODQzLTguMzQzNzUgMjEuODIwMzEyLTguMzQzNzUgMzAuMTY0MDYyIDBsNTQuMjUgNTQuMjUgMTIzLjU4NTkzOC0xMjMuNTgyMDMxYzguMzM5ODQzLTguMzQzNzUgMjEuODIwMzEyLTguMzQzNzUgMzAuMTY0MDYyIDAgOC4zMzk4NDQgOC4zMzk4NDMgOC4zMzk4NDQgMjEuODIwMzEyIDAgMzAuMTY0MDYyem0wIDAiIGZpbGw9IiMwMDdhY2YiIGRhdGEtb3JpZ2luYWw9IiMwMDAwMDAiIHN0eWxlPSIiPjwvcGF0aD48L2c+PC9zdmc+" />
                        <div className="money">Rp 100.000</div>
                        <div className="date">Pencairan pertama , 18 Sep 2020, 12.00</div>
                        <div className="bottom-divider"></div>
                    </div>

                    <div className="list-riwayat-container">
                        <img className="check" src="data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHZlcnNpb249IjEuMSIgeG1sbnM6eGxpbms9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkveGxpbmsiIHhtbG5zOnN2Z2pzPSJodHRwOi8vc3ZnanMuY29tL3N2Z2pzIiB3aWR0aD0iNTEyIiBoZWlnaHQ9IjUxMiIgeD0iMCIgeT0iMCIgdmlld0JveD0iMCAwIDUxMiA1MTIiIHN0eWxlPSJlbmFibGUtYmFja2dyb3VuZDpuZXcgMCAwIDUxMiA1MTIiIHhtbDpzcGFjZT0icHJlc2VydmUiPjxnPjxwYXRoIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgZD0ibTI1NiAwYy0xNDEuMTY0MDYyIDAtMjU2IDExNC44MzU5MzgtMjU2IDI1NnMxMTQuODM1OTM4IDI1NiAyNTYgMjU2IDI1Ni0xMTQuODM1OTM4IDI1Ni0yNTYtMTE0LjgzNTkzOC0yNTYtMjU2LTI1NnptMTI5Ljc1IDIwMS43NS0xMzguNjY3OTY5IDEzOC42NjQwNjJjLTQuMTYwMTU2IDQuMTYwMTU3LTkuNjIxMDkzIDYuMjUzOTA3LTE1LjA4MjAzMSA2LjI1MzkwN3MtMTAuOTIxODc1LTIuMDkzNzUtMTUuMDgyMDMxLTYuMjUzOTA3bC02OS4zMzIwMzEtNjkuMzMyMDMxYy04LjM0Mzc1LTguMzM5ODQzLTguMzQzNzUtMjEuODI0MjE5IDAtMzAuMTY0MDYyIDguMzM5ODQzLTguMzQzNzUgMjEuODIwMzEyLTguMzQzNzUgMzAuMTY0MDYyIDBsNTQuMjUgNTQuMjUgMTIzLjU4NTkzOC0xMjMuNTgyMDMxYzguMzM5ODQzLTguMzQzNzUgMjEuODIwMzEyLTguMzQzNzUgMzAuMTY0MDYyIDAgOC4zMzk4NDQgOC4zMzk4NDMgOC4zMzk4NDQgMjEuODIwMzEyIDAgMzAuMTY0MDYyem0wIDAiIGZpbGw9IiMwMDdhY2YiIGRhdGEtb3JpZ2luYWw9IiMwMDAwMDAiIHN0eWxlPSIiPjwvcGF0aD48L2c+PC9zdmc+" />
                        <div className="money">Rp 100.000</div>
                        <div className="date">Pencairan pertama , 18 Sep 2020, 12.00</div>
                        <div className="bottom-divider"></div>
                    </div>



                </div>
                { this.state.isOnRincian ?
              
                <div className="overlay-bottom-rincian">
                     <div className="overLay-background-deflect">
                     </div> 
                    <div className="rincian-detail-rekening">
                        <div className="header">Rincian Sudah Masuk Rekening</div>
                        <div className="topHeader">
                            <div className="money">Rp 595.800</div>
                            <div className="wording-footer">Pencairan terakhir 18 Sep 2020, 12:00</div>
                        </div>
                        {/* <div className="bottom-divider"></div> */}
                        <div className="alowanceHeader">
                            <div className="potongan">Pendapatan sebelum potongan</div>
                            <div className="potongan-money">Rp 600.000</div>
                        </div>
                        <div className="biayaHeader">
                            <div className="containerStruts">
                                <div className="biaya">Biaya 0,7%</div>
                                <div className="biaya-footer">Biaya yang diterapkan oleh pemerintah</div>
                            </div>
                            <div className="biaya-money">Rp1.740</div>
                        </div>
                        <div className="button-close-rincian" onClick={
                            () =>{
                                this.setState({
                                    isOnRincian : false
                                })
                            }
                        }>
                            <span>
                            Oke
                            </span>
                        </div>
                    </div>
                     
                </div> : <div></div>
                }
            </div>
        );
    }
}