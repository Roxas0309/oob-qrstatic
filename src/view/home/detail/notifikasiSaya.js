import React from 'react';
import { BACK_END_POINT, GET, HEADER_AUTH, POST, TOKEN_AUTH, wsWithBody, wsWithoutBody } from '../../../master/masterComponent'
import '../../../assets/css/homeScreen.css';
import HeaderInfo from '../../../master/HeaderInfo';
import ChevronRightIcon from '@material-ui/icons/ChevronRight';
export default class notifikasiSaya extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            namaLengkap: '',
            nomorHandphone: true,
            email: '',
            nomorRekening: '',
            tipeAkun: '',
            maksimalPembayaranPerHari: '',
            isOnNotifikasiBar: true,
            listNotifTransaksi : [],
            listNotifInformasi  : []
        }
    }

    componentDidMount() {
        window.onpopstate = function (event) {
            event.preventDefault();
            window.location.reload();
          };

       this.getAllNotifInformasi();
       this.getAllNotifTransaksi();
       setInterval(this.getAllNotifInformasi,10000);
       setInterval(this.getAllNotifTransaksi,10000);
    }

    getAllNotifTransaksi = () =>{
        wsWithoutBody(BACK_END_POINT + "/send-notification/get-all-my-notification/auth?type=Transaksi", GET, {
            ...HEADER_AUTH, 'secret-token': localStorage.getItem(TOKEN_AUTH)
        }).then(response => {
            var result = response.data.result;
            this.setState({
                listNotifTransaksi : result
            })
        });
    }

    getAllNotifInformasi = () =>{
        wsWithoutBody(BACK_END_POINT + "/send-notification/get-all-my-notification/auth?type=Informasi", GET, {
            ...HEADER_AUTH, 'secret-token': localStorage.getItem(TOKEN_AUTH)
        }).then(response => {
            var result = response.data.result;
            this.setState({
                listNotifInformasi : result
            })
        });
    }

    render() {
        return (
            <div>
                <HeaderInfo headerName={"Notifikasi"} urlBack={"/homeScreen"}></HeaderInfo>
                <div className="notifikasi-me-container">
                    <div className="button-anomali put-top-inmin2478vw">
                        <div className="container-anomali">
                            <div
                                onClick={() => {
                                    this.setState({ isOnNotifikasiBar: true })
                                }}
                                className={this.state.isOnNotifikasiBar ?
                                    'anomali new-anomali anomali-transaksi active' : 'anomali new-anomali anomali-transaksi'
                                }>Transaksi</div>
                            <div
                                onClick={() => {
                                    this.setState({ isOnNotifikasiBar: false })
                                }}
                                className=
                                {this.state.isOnNotifikasiBar ?
                                    'anomali new-anomali anomali-informasi' : 'anomali new-anomali anomali-informasi active'
                                }
                            >Informasi</div>
                        </div>
                    </div>

                    <div className="news-anomali">
                        {
                            this.state.isOnNotifikasiBar ?
                                <span>
                                    {
  this.state.listNotifTransaksi.map((value, i) => (
    <div className="container-news-anomali">
   {
       value.isRead?  <div className="news-black"></div> : <div className="news-gold"></div>
   }
  <div className="news-header">{value.header}</div>
  <div className="news-time">{value.dateInStr}</div>
    <div className="news-body">{value.body}
     </div>
     </div> 
  ))
}
                                  
                                </span>
                                :
                                <span>
                                         {
  this.state.listNotifInformasi.map((value, i) => (
    <div className="container-news-anomali">
    {
       value.isRead?  <div className="news-black"></div> : <div className="news-gold"></div>
   }
  <div className="news-header">{value.header}</div>
  <div className="news-time">{value.dateInStr}</div>
    <div className="news-body">{value.body}
     </div>
     </div> 
  ))
}
                                </span>
                        }

                    </div>

                </div>
            </div>
        );
    }
}