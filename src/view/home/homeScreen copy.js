import { blue } from '@material-ui/core/colors';
import React, { Component } from 'react';
import { BACK_END_POINT, GET, HEADER_AUTH, POST, TOKEN_AUTH, wsWithBody, wsWithoutBody } from '../../master/masterComponent';
import '../../assets/css/homeScreen.css';
import online from '../../../public/img/iklan/online-bussiness.png';
import { Button, Link } from '@material-ui/core';
import { t } from '../../helpers/translate';
import { Business, Height } from '@material-ui/icons';
import BlueHouse from '../../assets/svg/BlueHouse';
import BlueTriumph from '../../../public/img/iklan/blue-triumph.png';
import { EmailShareButton, WhatsappShareButton } from 'react-share';
import SockJsClient from 'react-stomp';
import moment from "moment";


export default class homeScreen extends Component {

    constructor(props) {
        super(props);
        this.state = {
            oobQr: '',
            oobQrCrop:'',
            isOnNotif:false,
            onQrFocus: false,
            tipeAkun: '',
            namaUsaha: '',
            idMid: '',
            headerTrx: null,
            startTime: '',
            endTime:''
        }
    }

    sendSubscribe = () => {
        var result = {
            ...HEADER_AUTH, 'secret-token': localStorage.getItem(TOKEN_AUTH)
        };
        var urlsubscribe = "/app/notification-push";
        var setter = JSON.stringify(result);
        //console.log("ready to subscribe : " + urlsubscribe);
        this.clientRef.sendMessage(urlsubscribe, setter); 
      }

    componentDidMount() {



        //  debugger;
        history.pushState(null, null, location.href);
        window.onpopstate = function (event) {
            event.preventDefault();
            history.go(0);
            window.location.reload();
        };

        wsWithoutBody(BACK_END_POINT + "/loginCtl/detailToken", GET, {
            ...HEADER_AUTH, 'secret-token': localStorage.getItem(TOKEN_AUTH)
        }).then(response => {
            this.setState({
                idMid: response.data.result.userOobMidDtlTableDto.idOob,
                tipeAkun: response.data.result.userOobMidDtlTableDto.akunOobMidDesc,
                namaUsaha: response.data.result.userOobTableSuccessDto.namaUsaha
            })
        })
        document.body.style = 'background: #E5E5E5;'
        //   document.body.style = 'background: rgba(0, 87, 231, 1);';
        wsWithoutBody(BACK_END_POINT + "/base64/getYourQrPicture", GET, {
            ...HEADER_AUTH, 'secret-token': localStorage.getItem(TOKEN_AUTH)
        }).then(response => {
            this.setState({
                oobQr: response.data.yourUrl,
                oobQrCrop :  response.data.yourUrlCrop
            })
        })
        var startTime = moment(new Date()).format("YYYYMMDD")
        var endTime = moment(new Date()).format("YYYYMMDD");
        this.getMyData(startTime, endTime);
        setInterval(this.getMyDataNew,10000);
    }


    getMyData = (startdate, enddate) => {

        wsWithoutBody(BACK_END_POINT + "/homeScreen/getDataTransaksi/auth/homeScreen?startDate=" + startdate + "&endDate=" + enddate+ "&isLimitValidated=false",
            GET, {
            ...HEADER_AUTH, 'secret-token': localStorage.getItem(TOKEN_AUTH)
        }).then(response => {
            var value = response.data.result;
            this.setState({headerTrx:value});
        }).catch(error => {
            this.getMyData(startdate, enddate);
        })

    }

    getMyDataNew = () => {
        var url = BACK_END_POINT + "/homeScreen/getDataTransaksi/auth/homeScreen?startDate=" + moment(new Date()).format("YYYYMMDD") + "&endDate=" + moment(new Date()).format("YYYYMMDD") + "&isLimitValidated=false";
      
        wsWithoutBody(url,
            GET, {
            ...HEADER_AUTH, 'secret-token': localStorage.getItem(TOKEN_AUTH)
        }).then(response => {
            var value = response.data.result;
            this.setState({headerTrx:value});
        }).catch(error => {
            this.getMyData(moment(new Date()).format("YYYYMMDD"), moment(new Date()).format("YYYYMMDD"));
        })

    }

    testing = () => {
        var isQrFocus = this.state.onQrFocus;

        if (isQrFocus) {
            this.setState({ onQrFocus: false })
            document.body.style = 'background: #E5E5E5;';
        }
        else {
            this.setState({ onQrFocus: true })
            document.body.style = 'background: rgba(0, 87, 231, 1);';
        }
    }

    downloadPicture = () => {
        wsWithoutBody(BACK_END_POINT + '/base64/downloadYourQrPicture', GET);
    }

    render() {

        let totalTran = 0
        let nf = new Intl.NumberFormat();


        return (

            <div >

                <SockJsClient url={BACK_END_POINT+"/oobws"}
                    topics={["/topic/notice-notif"]}
                    onConnect={() => 
                        { 
                          //  alert("ready to send subscribe to "+ SOCKET_TOPIC_TRX)
                          setInterval(this.sendSubscribe,1000);
                        }
                    }
                    onMessage={(msg) => {
                        if(msg.yourSecret===localStorage.getItem(TOKEN_AUTH)){
                      this.setState({
                          isOnNotif : msg.result
                      })
                    }
                    }}
                    ref={(client) => {
                        this.clientRef  = client;
                    }} />


                { this.state.onQrFocus ?
                    <div
                    // style={{ backgroundColor: "rgba(0, 87, 231, 1)", width: '100%', paddingBottom:'3vh'
                    //     ,height:'100vh'
                    // }}
                    >
                        <span>
                            <div className="close-cross">
                                <p onClick={this.testing}>X</p>
                            </div>
                            <img className="qr-class"
                                src={this.state.oobQr}>
                            </img>
                            <div className="wording-qr">
                                <p className="wording-me">
                                    Yuk, Download dan cetak QRIS usaha Anda untuk mulai
                                    menerima pembayaran dari berbagai macam uang elektronik
                               </p>
                            </div>

                            <div className="button-composer-qr">
                                <Link
                                    href={BACK_END_POINT + '/base64/downloadYourQrPicture/' + this.state.idMid}
                                >
                                    <div className="button-qr first">
                                        <img className="icon-flatten" src="data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHZlcnNpb249IjEuMSIgeG1sbnM6eGxpbms9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkveGxpbmsiIHhtbG5zOnN2Z2pzPSJodHRwOi8vc3ZnanMuY29tL3N2Z2pzIiB3aWR0aD0iNTEyIiBoZWlnaHQ9IjUxMiIgeD0iMCIgeT0iMCIgdmlld0JveD0iMCAwIDQ3Ny44NjcgNDc3Ljg2NyIgc3R5bGU9ImVuYWJsZS1iYWNrZ3JvdW5kOm5ldyAwIDAgNTEyIDUxMiIgeG1sOnNwYWNlPSJwcmVzZXJ2ZSIgY2xhc3M9IiI+PGcgdHJhbnNmb3JtPSJtYXRyaXgoMS4wMiwwLDAsMS4wMiwtNC43Nzg2NjAwMTEyOTEyNjUsLTQuNzc4NjcwMDQzOTQ1MTEyKSI+CjxnIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyI+Cgk8Zz4KCQk8cGF0aCBkPSJNNDQzLjczMywzMDcuMmMtOS40MjYsMC0xNy4wNjcsNy42NDEtMTcuMDY3LDE3LjA2N3YxMDIuNGMwLDkuNDI2LTcuNjQxLDE3LjA2Ny0xNy4wNjcsMTcuMDY3SDY4LjI2NyAgICBjLTkuNDI2LDAtMTcuMDY3LTcuNjQxLTE3LjA2Ny0xNy4wNjd2LTEwMi40YzAtOS40MjYtNy42NDEtMTcuMDY3LTE3LjA2Ny0xNy4wNjdzLTE3LjA2Nyw3LjY0MS0xNy4wNjcsMTcuMDY3djEwMi40ICAgIGMwLDI4LjI3NywyMi45MjMsNTEuMiw1MS4yLDUxLjJINDA5LjZjMjguMjc3LDAsNTEuMi0yMi45MjMsNTEuMi01MS4ydi0xMDIuNEM0NjAuOCwzMTQuODQxLDQ1My4xNTksMzA3LjIsNDQzLjczMywzMDcuMnoiIGZpbGw9IiNmZmZmZmYiIGRhdGEtb3JpZ2luYWw9IiMwMDAwMDAiIHN0eWxlPSIiIGNsYXNzPSIiPjwvcGF0aD4KCTwvZz4KPC9nPgo8ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciPgoJPGc+CgkJPHBhdGggZD0iTTMzNS45NDcsMjk1LjEzNGMtNi42MTQtNi4zODctMTcuMDk5LTYuMzg3LTIzLjcxMiwwTDI1NiwzNTEuMzM0VjE3LjA2N0MyNTYsNy42NDEsMjQ4LjM1OSwwLDIzOC45MzMsMCAgICBzLTE3LjA2Nyw3LjY0MS0xNy4wNjcsMTcuMDY3djMzNC4yNjhsLTU2LjIwMS01Ni4yMDFjLTYuNzgtNi41NDgtMTcuNTg0LTYuMzYtMjQuMTMyLDAuNDE5Yy02LjM4OCw2LjYxNC02LjM4OCwxNy4wOTksMCwyMy43MTMgICAgbDg1LjMzMyw4NS4zMzNjNi42NTcsNi42NzMsMTcuNDYzLDYuNjg3LDI0LjEzNiwwLjAzMWMwLjAxLTAuMDEsMC4wMi0wLjAyLDAuMDMxLTAuMDMxbDg1LjMzMy04NS4zMzMgICAgQzM0Mi45MTUsMzEyLjQ4NiwzNDIuNzI3LDMwMS42ODIsMzM1Ljk0NywyOTUuMTM0eiIgZmlsbD0iI2ZmZmZmZiIgZGF0YS1vcmlnaW5hbD0iIzAwMDAwMCIgc3R5bGU9IiIgY2xhc3M9IiI+PC9wYXRoPgoJPC9nPgo8L2c+CjxnIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyI+CjwvZz4KPGcgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIj4KPC9nPgo8ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciPgo8L2c+CjxnIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyI+CjwvZz4KPGcgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIj4KPC9nPgo8ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciPgo8L2c+CjxnIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyI+CjwvZz4KPGcgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIj4KPC9nPgo8ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciPgo8L2c+CjxnIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyI+CjwvZz4KPGcgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIj4KPC9nPgo8ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciPgo8L2c+CjxnIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyI+CjwvZz4KPGcgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIj4KPC9nPgo8ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciPgo8L2c+CjwvZz48L3N2Zz4=" />
                                        <div className="wording">Download QRIS</div>
                                    </div>
                                </Link>

                                <div className="button-qr last">
                                        <img className="icon-flatten" src="data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHZlcnNpb249IjEuMSIgeG1sbnM6eGxpbms9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkveGxpbmsiIHhtbG5zOnN2Z2pzPSJodHRwOi8vc3ZnanMuY29tL3N2Z2pzIiB3aWR0aD0iNTEyIiBoZWlnaHQ9IjUxMiIgeD0iMCIgeT0iMCIgdmlld0JveD0iMCAwIDUxMiA1MTIiIHN0eWxlPSJlbmFibGUtYmFja2dyb3VuZDpuZXcgMCAwIDUxMiA1MTIiIHhtbDpzcGFjZT0icHJlc2VydmUiPjxnPgo8ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciPgoJPGc+CgkJPHBhdGggZD0iTTQwNiwzMzJjLTI5LjYzNiwwLTU1Ljk2OSwxNC40MDItNzIuMzc4LDM2LjU3MWwtMTQxLjI3LTcyLjE5NUMxOTQuNzIyLDI4OC4zMjQsMTk2LDI3OS44MDksMTk2LDI3MSAgICBjMC0xMS45MzEtMi4zMzktMjMuMzI0LTYuNTc0LTMzLjc1M2wxNDguMDYtODguOTU4QzM1NC4wMDYsMTY3LjY3OSwzNzguNTksMTgwLDQwNiwxODBjNDkuNjI2LDAsOTAtNDAuMzc0LDkwLTkwICAgIGMwLTQ5LjYyNi00MC4zNzQtOTAtOTAtOTBjLTQ5LjYyNiwwLTkwLDQwLjM3NC05MCw5MGMwLDExLjQ3LDIuMTYxLDIyLjQ0Myw2LjA5LDMyLjU0bC0xNDguNDMsODkuMTggICAgQzE1Ny4xNTIsMTkyLjkwMiwxMzIuOTQxLDE4MSwxMDYsMTgxYy00OS42MjYsMC05MCw0MC4zNzQtOTAsOTBjMCw0OS42MjYsNDAuMzc0LDkwLDkwLDkwYzMwLjEyMiwwLDU2LjgzMi0xNC44NzYsNzMuMTc3LTM3LjY2NiAgICBsMTQwLjg2LDcxLjk4NUMzMTcuNDE0LDQwMy43NTMsMzE2LDQxMi43MTQsMzE2LDQyMmMwLDQ5LjYyNiw0MC4zNzQsOTAsOTAsOTBjNDkuNjI2LDAsOTAtNDAuMzc0LDkwLTkwICAgIEM0OTYsMzcyLjM3NCw0NTUuNjI2LDMzMiw0MDYsMzMyeiBNNDA2LDMwYzMzLjA4NCwwLDYwLDI2LjkxNiw2MCw2MHMtMjYuOTE2LDYwLTYwLDYwcy02MC0yNi45MTYtNjAtNjBTMzcyLjkxNiwzMCw0MDYsMzB6ICAgICBNMTA2LDMzMWMtMzMuMDg0LDAtNjAtMjYuOTE2LTYwLTYwczI2LjkxNi02MCw2MC02MHM2MCwyNi45MTYsNjAsNjBTMTM5LjA4NCwzMzEsMTA2LDMzMXogTTQwNiw0ODJjLTMzLjA4NCwwLTYwLTI2LjkxNi02MC02MCAgICBzMjYuOTE2LTYwLDYwLTYwczYwLDI2LjkxNiw2MCw2MFM0MzkuMDg0LDQ4Miw0MDYsNDgyeiIgZmlsbD0iI2ZmZmZmZiIgZGF0YS1vcmlnaW5hbD0iIzAwMDAwMCIgc3R5bGU9IiI+PC9wYXRoPgoJPC9nPgo8L2c+CjxnIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyI+CjwvZz4KPGcgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIj4KPC9nPgo8ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciPgo8L2c+CjxnIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyI+CjwvZz4KPGcgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIj4KPC9nPgo8ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciPgo8L2c+CjxnIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyI+CjwvZz4KPGcgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIj4KPC9nPgo8ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciPgo8L2c+CjxnIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyI+CjwvZz4KPGcgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIj4KPC9nPgo8ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciPgo8L2c+CjxnIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyI+CjwvZz4KPGcgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIj4KPC9nPgo8ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciPgo8L2c+CjwvZz48L3N2Zz4=" />
                                        <div className="wording">Bagikan QRIS</div>
                                    </div>
                                {/* <WhatsappShareButton url={this.state.oobQr} subject="Qr_picture">
                                    <div className="button-qr last">
                                        <img className="icon-flatten" src="data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHZlcnNpb249IjEuMSIgeG1sbnM6eGxpbms9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkveGxpbmsiIHhtbG5zOnN2Z2pzPSJodHRwOi8vc3ZnanMuY29tL3N2Z2pzIiB3aWR0aD0iNTEyIiBoZWlnaHQ9IjUxMiIgeD0iMCIgeT0iMCIgdmlld0JveD0iMCAwIDUxMiA1MTIiIHN0eWxlPSJlbmFibGUtYmFja2dyb3VuZDpuZXcgMCAwIDUxMiA1MTIiIHhtbDpzcGFjZT0icHJlc2VydmUiPjxnPgo8ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciPgoJPGc+CgkJPHBhdGggZD0iTTQwNiwzMzJjLTI5LjYzNiwwLTU1Ljk2OSwxNC40MDItNzIuMzc4LDM2LjU3MWwtMTQxLjI3LTcyLjE5NUMxOTQuNzIyLDI4OC4zMjQsMTk2LDI3OS44MDksMTk2LDI3MSAgICBjMC0xMS45MzEtMi4zMzktMjMuMzI0LTYuNTc0LTMzLjc1M2wxNDguMDYtODguOTU4QzM1NC4wMDYsMTY3LjY3OSwzNzguNTksMTgwLDQwNiwxODBjNDkuNjI2LDAsOTAtNDAuMzc0LDkwLTkwICAgIGMwLTQ5LjYyNi00MC4zNzQtOTAtOTAtOTBjLTQ5LjYyNiwwLTkwLDQwLjM3NC05MCw5MGMwLDExLjQ3LDIuMTYxLDIyLjQ0Myw2LjA5LDMyLjU0bC0xNDguNDMsODkuMTggICAgQzE1Ny4xNTIsMTkyLjkwMiwxMzIuOTQxLDE4MSwxMDYsMTgxYy00OS42MjYsMC05MCw0MC4zNzQtOTAsOTBjMCw0OS42MjYsNDAuMzc0LDkwLDkwLDkwYzMwLjEyMiwwLDU2LjgzMi0xNC44NzYsNzMuMTc3LTM3LjY2NiAgICBsMTQwLjg2LDcxLjk4NUMzMTcuNDE0LDQwMy43NTMsMzE2LDQxMi43MTQsMzE2LDQyMmMwLDQ5LjYyNiw0MC4zNzQsOTAsOTAsOTBjNDkuNjI2LDAsOTAtNDAuMzc0LDkwLTkwICAgIEM0OTYsMzcyLjM3NCw0NTUuNjI2LDMzMiw0MDYsMzMyeiBNNDA2LDMwYzMzLjA4NCwwLDYwLDI2LjkxNiw2MCw2MHMtMjYuOTE2LDYwLTYwLDYwcy02MC0yNi45MTYtNjAtNjBTMzcyLjkxNiwzMCw0MDYsMzB6ICAgICBNMTA2LDMzMWMtMzMuMDg0LDAtNjAtMjYuOTE2LTYwLTYwczI2LjkxNi02MCw2MC02MHM2MCwyNi45MTYsNjAsNjBTMTM5LjA4NCwzMzEsMTA2LDMzMXogTTQwNiw0ODJjLTMzLjA4NCwwLTYwLTI2LjkxNi02MC02MCAgICBzMjYuOTE2LTYwLDYwLTYwczYwLDI2LjkxNiw2MCw2MFM0MzkuMDg0LDQ4Miw0MDYsNDgyeiIgZmlsbD0iI2ZmZmZmZiIgZGF0YS1vcmlnaW5hbD0iIzAwMDAwMCIgc3R5bGU9IiI+PC9wYXRoPgoJPC9nPgo8L2c+CjxnIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyI+CjwvZz4KPGcgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIj4KPC9nPgo8ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciPgo8L2c+CjxnIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyI+CjwvZz4KPGcgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIj4KPC9nPgo8ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciPgo8L2c+CjxnIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyI+CjwvZz4KPGcgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIj4KPC9nPgo8ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciPgo8L2c+CjxnIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyI+CjwvZz4KPGcgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIj4KPC9nPgo8ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciPgo8L2c+CjxnIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyI+CjwvZz4KPGcgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIj4KPC9nPgo8ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciPgo8L2c+CjwvZz48L3N2Zz4=" />
                                        <div className="wording">Bagikan QRIS</div>
                                    </div>
                                </WhatsappShareButton> */}
                            </div>
                        </span>
                    </div>
                    :
                    <div>
                        <div id="header-dashboard">
                            <span >


<span style={{position:'relative',paddingLeft:'5.797vw',top:'1.805vw'}}>

<svg width="25.629vw" height="7.323vw" viewBox="0 0 112 32" fill="none" xmlns="http://www.w3.org/2000/svg">
<path d="M0.115118 31.6899H3.56865V23.5644C3.56865 23.1694 3.62621 22.7744 3.74132 22.4076C4.08668 21.4766 5.14841 20.4609 6.47226 20.4609C8.11269 20.4609 8.88973 21.8151 8.88973 23.7337V31.6899H12.3433V23.4515C12.3433 23.0565 12.4008 22.6051 12.5159 22.2948C12.8901 21.3073 13.7822 20.4609 14.991 20.4609C16.689 20.4609 17.4948 21.8151 17.4948 24.1004V31.6899H20.9483V23.5926C20.9483 19.1913 18.5884 17.6395 16.286 17.6395C15.1061 17.6395 14.2139 17.9217 13.4081 18.4577C12.775 18.8527 12.1994 19.417 11.7101 20.1788H11.6526C11.0482 18.6552 9.60922 17.6395 7.76734 17.6395C5.37865 17.6395 3.94278 18.9091 3.28085 19.9813H3.19452L3.02184 17.9499H0C0.0575588 19.1349 0.115118 20.4609 0.115118 22.0409V31.6899Z" fill="white"/>
<path d="M34.3652 23.4515C34.3652 20.4327 33.0701 17.6395 28.5805 17.6395C26.3645 17.6395 24.5514 18.232 23.5153 18.8527L24.206 21.1098C25.1557 20.5173 26.5947 20.0941 27.9761 20.0941C30.4512 20.0941 30.8253 21.5894 30.8253 22.4923V22.718C25.645 22.6898 22.508 24.4672 22.508 27.9093C22.508 29.9971 24.0909 32.0003 26.9113 32.0003C28.7532 32.0003 30.2209 31.2385 31.0555 30.1664H31.1419L31.4009 31.6899H34.5954C34.4227 30.8435 34.3652 29.6303 34.3652 28.3889V23.4515ZM30.9116 26.8654C30.9116 27.1193 30.9116 27.3732 30.8253 27.6271C30.4799 28.6146 29.4727 29.5175 28.0337 29.5175C26.9113 29.5175 26.0191 28.8968 26.0191 27.5425C26.0191 25.4547 28.4078 24.9469 30.9116 24.9751V26.8654Z" fill="white"/>
<path d="M36.9922 31.6899H40.5321V23.5926C40.5321 23.1976 40.5897 22.7744 40.7048 22.4923C41.0789 21.4484 42.0574 20.4609 43.4676 20.4609C45.3958 20.4609 46.1441 21.9562 46.1441 23.9312V31.6899H49.684V23.5362C49.684 19.1913 47.1514 17.6395 44.7051 17.6395C42.374 17.6395 40.8487 18.9374 40.2155 20.0095H40.1292L39.9565 17.9499H36.8771C36.9347 19.1349 36.9922 20.4609 36.9922 22.0409V31.6899Z" fill="white"/>
<path d="M61.5228 12.4365V19.417H61.4652C60.8321 18.4013 59.4506 17.6395 57.5224 17.6395C54.1552 17.6395 51.2197 20.3762 51.2485 24.9751C51.2485 29.2071 53.8962 32.0003 57.2634 32.0003C59.278 32.0003 60.9759 31.041 61.8105 29.5175H61.8681L62.0408 31.6899H65.1777C65.1202 30.7589 65.0626 29.2353 65.0626 27.8246V12.4365H61.5228ZM61.5228 25.7368C61.5228 26.1036 61.494 26.4422 61.4076 26.7525C61.0623 28.2478 59.796 29.2071 58.357 29.2071C56.1122 29.2071 54.8459 27.3732 54.8459 24.834C54.8459 22.2948 56.1122 20.3198 58.3858 20.3198C59.9974 20.3198 61.1198 21.4202 61.4364 22.7462C61.494 23.0283 61.5228 23.3951 61.5228 23.6772V25.7368Z" fill="white"/>
<path d="M71.0816 31.6899V17.9499H67.5417V31.6899H71.0816Z" fill="white"/>
<path d="M73.6846 31.6899H77.2244V24.5519C77.2244 24.1851 77.2532 23.8183 77.3108 23.508C77.6273 21.928 78.8073 20.8841 80.4765 20.8841C80.9082 20.8841 81.2248 20.9123 81.5414 20.9687V17.8683C81.2474 17.8683 81.1618 17.8683 80.8164 17.8683C79.3121 17.8683 77.5343 18.5646 76.8503 20.5455H76.764L76.6201 17.9499H73.5695C73.6558 19.1631 73.6846 20.5173 73.6846 22.3794V31.6899Z" fill="white"/>
<path d="M86.7592 31.6899V17.9499H83.2194V31.6899H86.7592Z" fill="white"/>
<path fill-rule="evenodd" clip-rule="evenodd" d="M109.061 3.28316C107.289 1.29222 105.405 2.19341 103.908 2.92621C103.278 3.23252 98.7324 5.7702 98.7324 5.7702C96.2418 6.78151 94.3025 5.67784 93.5417 4.7488C93.4689 4.66071 93.4031 4.5672 93.3451 4.46911L93.1677 4.16658C92.868 3.65601 92.5348 3.16514 92.1704 2.69714C91.9493 2.41344 91.7097 2.14414 91.4533 1.89088C89.7492 0.196126 87.026 -0.98978 82.474 1.16438C80.3188 2.39462 73.2981 6.34485 73.2981 6.34485C70.6596 7.41433 68.642 6.11195 67.9865 5.15634C67.9571 5.1206 67.9312 5.082 67.9094 5.04119L67.8234 4.89691L67.3787 4.18051C67.178 3.8667 66.9627 3.56219 66.7335 3.26795C66.5102 2.9861 66.2703 2.71728 66.0152 2.46297C64.3226 0.778348 61.6135 -0.405046 57.1014 1.70866C54.373 3.24266 48.027 6.81567 48.027 6.81567L48.0244 6.81822L43.3799 9.43183L45.7831 12.3378C46.7945 13.2871 48.6952 14.8439 51.6357 13.5314C51.6357 13.5314 59.7513 8.90276 59.7835 8.88888C66.7219 5.13863 70.1456 8.72181 71.6595 11.1102C71.7122 11.1848 71.7546 11.2658 71.8022 11.338L71.8047 11.3418C71.824 11.3709 71.8356 11.3974 71.8523 11.4253L72.0232 11.7012C72.7609 12.8023 73.9239 13.5137 75.3106 13.4808C76.0753 13.4656 76.5945 13.22 77.3501 12.8061L85.2024 8.32058L85.2191 8.313C92.245 4.51718 95.7264 8.3345 97.2134 10.7228C97.329 10.9291 97.4601 11.1266 97.5951 11.3101C98.3314 12.2935 99.3467 12.9378 100.637 12.9061C101.489 12.8897 102.444 12.386 102.587 12.2935L112 6.94223C112.003 6.94478 111.042 5.51329 109.061 3.28316Z" fill="#FFB700"/>
</svg>


</span>

                                <div className="close-cross small-text" >

{ this.state.isOnNotif?
                                <svg style={{position:'absolute',top:'1vw',left:'5vw'}}
                                width="2.415vw" height="2.415vw" viewBox="0 0 10 10" 
                                fill="none" xmlns="http://www.w3.org/2000/svg">
                                <circle cx="5" cy="5" r="5" fill="#FFB700"/>
                                </svg>
                                :''
}


                                <svg className="svgs-close-cross"
                                width="3.865vw" height="3.865vw" viewBox="0 0 16 16" fill="none" xmlns="http://www.w3.org/2000/svg">
<path d="M13.8886 11.6131L12.4746 9.44267V5.85747C12.4746 5.84288 12.474 5.82827 12.4733 5.81407C12.4615 3.34101 10.4596 1.33301 8.00004 1.33301C5.55997 1.33301 3.56984 3.31021 3.52877 5.77801C3.52557 5.80421 3.52424 5.83067 3.52424 5.85754V9.44207L2.11084 11.6134C1.97537 11.8217 1.96304 12.089 2.07957 12.3089C2.19551 12.5293 2.42137 12.6663 2.66684 12.6663H5.18904C5.51404 13.8145 6.71364 14.6663 8.00017 14.6663C9.28671 14.6663 10.4863 13.8145 10.8113 12.6663H13.3332C13.5786 12.6663 13.8045 12.5289 13.9205 12.3089C14.0371 12.0887 14.0247 11.8213 13.8886 11.6131ZM8.00017 13.333C7.49097 13.333 7.05004 13.0629 6.81024 12.6663H9.19004C8.95037 13.0629 8.50951 13.333 8.00017 13.333ZM3.88424 11.333L4.64164 10.1694L4.85751 9.83781V9.44207V5.89627L4.86071 5.87007L4.86191 5.80021C4.89064 4.07221 6.29844 2.66634 8.00004 2.66634C9.72317 2.66634 11.1318 4.08127 11.14 5.82041V5.83014L11.1413 5.85747V9.44267V9.83874L11.3575 10.1705L12.1148 11.333H3.88424Z" fill="white"/>
</svg>

                                    <p style={{ fontWeight: '700', fontSize: '3.167vw', position:'relative',top:'-1.4vw' }} onClick={
                                        () => {
                                            this.props.history.push('/notifikasiSaya');
                                            location.reload();
                                        }
                                    }>
                                    Notifikasi
                                    </p>
                                </div>
                                <img id="oob-crop-circle" className="qr-class small-text" onClick={this.testing}
                                    src={BACK_END_POINT+this.state.oobQrCrop}>
                                </img>
                                <div>
                                    <div className="tipe-akun">
                                        <p>
                                            Akun {this.state.tipeAkun}
                                        </p>
                                    </div>
                                    <div className="nama-usaha" onClick={
                                        () => {
                                            this.props.history.push('/profileSaya');
                                            location.reload();
                                        }
                                    }>
                                        <p>
                                            {this.state.namaUsaha} <img className="icon-flatten-disposal" src="data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHZlcnNpb249IjEuMSIgeG1sbnM6eGxpbms9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkveGxpbmsiIHhtbG5zOnN2Z2pzPSJodHRwOi8vc3ZnanMuY29tL3N2Z2pzIiB3aWR0aD0iNTEyIiBoZWlnaHQ9IjUxMiIgeD0iMCIgeT0iMCIgdmlld0JveD0iMCAwIDUxMiA1MTIiIHN0eWxlPSJlbmFibGUtYmFja2dyb3VuZDpuZXcgMCAwIDUxMiA1MTIiIHhtbDpzcGFjZT0icHJlc2VydmUiIGNsYXNzPSIiPjxnPgo8ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciPgoJPGc+CgkJPHBhdGggZD0iTTI1NS44OTMsMTUwLjU2OWMtNTguODQxLDAtMTA2LjcxMSw0Ny44NzEtMTA2LjcxMSwxMDYuNzExczQ3Ljg3MSwxMDYuNzExLDEwNi43MTEsMTA2LjcxMSAgICBzMTA2LjcxMS00Ny44NzEsMTA2LjcxMS0xMDYuNzExUzMxNC43MzQsMTUwLjU2OSwyNTUuODkzLDE1MC41Njl6IE0yNTUuODkzLDMyMS4zMDdjLTM1LjMsMC02NC4wMjctMjguNzI3LTY0LjAyNy02NC4wMjcgICAgczI4LjcyNy02NC4wMjcsNjQuMDI3LTY0LjAyN2MzNS4zMjEsMCw2NC4wMjcsMjguNzI3LDY0LjAyNyw2NC4wMjdTMjkxLjE5MywzMjEuMzA3LDI1NS44OTMsMzIxLjMwN3oiIGZpbGw9IiNmZmZmZmYiIGRhdGEtb3JpZ2luYWw9IiMwMDAwMDAiIHN0eWxlPSIiIGNsYXNzPSIiPjwvcGF0aD4KCTwvZz4KPC9nPgo8ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciPgoJPGc+CgkJPHBhdGggZD0iTTQ5My42ODgsMTkzLjUxbC02MC42NzYtOS43NzVsMzYuMjgyLTQ4LjIzM2M2LjM2LTguNDczLDUuNTctMjAuMjk2LTEuODU3LTI3LjgzbC02MC42MzMtNjEuNDg3ICAgIGMtNy41MTMtNy42NDEtMTkuNDY0LTguNTE2LTI3Ljk4LTIuMTEzbC00OS41NzgsMzQuODMxbC05LjYwNC01OS43NThjLTEuNjY1LTEwLjM1MS0xMC41ODYtMTcuOTQ5LTIxLjA2NS0xNy45NDloLTg1LjM2OSAgICBjLTEwLjYyOCwwLTE5LjYzNSw3LjgxMS0yMS4xMjksMTguMzEybC05Ljk4OCw2MC40ODRMMTMyLjc3LDQzLjkyMmMtOC41MTYtNi4yMzItMjAuMjMyLTUuMzM2LTI3LjY4MSwyLjExM0w0NC42OTEsMTA2LjIyICAgIGMtNy41NTUsNy41MTItOC4zODgsMTkuNDQzLTEuOTg1LDI3Ljk1OGwzNS4zNjQsNDkuNDkzbC01OS43NTgsOC41MzdDNy44MTEsMTkzLjcwMiwwLDIwMi43MDksMCwyMTMuMzM3djg1LjM2OSAgICBjMCwxMC42MjgsNy44MTEsMTkuNjM1LDE4LjMxMiwyMS4wODZsNjAuNjc2LDkuNzc1TDQyLjcwNiwzNzcuOGMtNi40MDMsOC41MTYtNS41NywyMC40MDMsMS45NjQsMjcuOTM3bDYwLjM5OCw2MC4zOTggICAgYzcuNTM0LDcuNTc2LDE5LjQ0Myw4LjM0NSwyNy45MzcsMS45NjNsNDkuNDcxLTM1LjM2NGw4LjUzNyw1OS43NThjMS40OTQsMTAuNSwxMC41LDE4LjMxMiwyMS4xMjksMTguMzEyaDg1LjM2OSAgICBjMTAuNjI4LDAsMTkuNjM1LTcuNzksMjEuMTI5LTE4LjI0OGw5Ljc3NS02MC42NzZsNDguMjMzLDM2LjI4MmM4LjUzNyw2LjQwMywyMC40MjQsNS41OTIsMjcuOTM3LTEuOTYzbDYwLjM5OC02MC4zOTggICAgYzcuNTM0LTcuNTM0LDguMzY2LTE5LjQ0MywxLjk2My0yNy45MzdsLTM1LjkxOS00OS40MDdsNjIuMTA2LTcuMjU2QzUwMy44OSwzMTkuOTYzLDUxMiwzMTAuODUsNTEyLDMwMC4wMDh2LTg1LjM2OSAgICBDNTEyLDIwNC4wMSw1MDQuMTg5LDE5NS4wMDQsNDkzLjY4OCwxOTMuNTF6IE00NjkuMzM3LDI4MC45OTJsLTQzLjU1OSw1LjA3OWMtMTUuODU3LDIuMDctMjkuMTExLDEyLjY5OS0zNC4wNDEsMjYuNCAgICBjLTYuMTg5LDE0LTQuMzc1LDMwLjM3LDQuODQ1LDQyLjgxM2wyNS4xNDEsMzMuNDAxbC0zNC4xOSwzNC4xOWwtMzMuNTUtMjUuMjQ4Yy0xMi4zNTctOS4xMzQtMjguNzI3LTEwLjk0OS00MS4zNC01LjI1ICAgIGMtMTUuMDI1LDUuNDQyLTI1LjY1MywxOC42OTYtMjcuNzI0LDM0LjMxOGwtNS44OSw0MS40MjVoLTQ4LjM2MmwtNS45MTItNDEuMTY5Yy0yLjA3LTE1Ljg1Ny0xMi42OTktMjkuMTExLTI2LjM3OS0zNC4wMTkgICAgYy0xMy45NzktNi4xODktMzAuMzA2LTQuNDE4LTQyLjgzNCw0LjgyM2wtMzMuNDAxLDI1LjE0MWwtMzQuMTktMzQuMTlsMjUuMjQ4LTMzLjU1YzkuMTEzLTEyLjMxNCwxMC45MjctMjguNjYzLDUuMjUtNDEuMzQgICAgYy01LjQ2NC0xNS4wMjUtMTguNjk2LTI1LjY3NS0zNC4zNC0yNy43MjRsLTQxLjQyNS01Ljg5di00OC4zNjJsNDEuMTktNS45MTJjMTUuODU3LTIuMDcsMjkuMTExLTEyLjY5OSwzNC4wMi0yNi4zNzkgICAgYzYuMjExLTE0LDQuMzk2LTMwLjMyNy00LjgwMi00Mi44MzRsLTI1LjEyLTMzLjM3OWwzNC4yOTctMzQuMTlsMzQuNTEsMjUuMjI3YzEyLjMzNiw5LjEzNCwyOC42NjMsMTAuODg1LDQxLjM0LDUuMjUgICAgYzE1LjA0Ni01LjQ2NCwyNS42NTMtMTguNzYsMjcuNjgxLTM0LjMxOGw1LjkxMi00MS40NDdoNDguNjgybDYuNjE2LDQxLjE0OGMyLjA0OSwxNS44NzksMTIuNjU2LDI5LjE1MywyNi40LDM0LjEwNSAgICBjMTMuOTc5LDYuMTQ3LDMwLjMyNyw0LjM5Niw0Mi43NDgtNC44MDJsMzMuMzM3LTI0LjkyOGwzNC42ODEsMzUuMTI5bC0yNS4zMzMsMzMuNjU3Yy05LjExMywxMi4zMTUtMTAuOTI3LDI4LjY2My01LjI1LDQxLjM2MSAgICBjNS40NjQsMTUuMDQ2LDE4Ljc2LDI1LjY1MywzNC4zMTgsMjcuNjgxbDQxLjQyNSw1LjkxMlYyODAuOTkyeiIgZmlsbD0iI2ZmZmZmZiIgZGF0YS1vcmlnaW5hbD0iIzAwMDAwMCIgc3R5bGU9IiIgY2xhc3M9IiI+PC9wYXRoPgoJPC9nPgo8L2c+CjxnIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyI+CjwvZz4KPGcgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIj4KPC9nPgo8ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciPgo8L2c+CjxnIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyI+CjwvZz4KPGcgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIj4KPC9nPgo8ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciPgo8L2c+CjxnIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyI+CjwvZz4KPGcgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIj4KPC9nPgo8ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciPgo8L2c+CjxnIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyI+CjwvZz4KPGcgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIj4KPC9nPgo8ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciPgo8L2c+CjxnIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyI+CjwvZz4KPGcgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIj4KPC9nPgo8ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciPgo8L2c+CjwvZz48L3N2Zz4=" />
                                        </p>
                                    </div>
                                </div>
                                <div className="button-composer-qr">
                                    <Link
                                        href={BACK_END_POINT + '/base64/downloadYourQrPicture/' + this.state.idMid}
                                    >
                                        <div className="button-qr first">
                                            <img className="icon-flatten" src="data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHZlcnNpb249IjEuMSIgeG1sbnM6eGxpbms9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkveGxpbmsiIHhtbG5zOnN2Z2pzPSJodHRwOi8vc3ZnanMuY29tL3N2Z2pzIiB3aWR0aD0iNTEyIiBoZWlnaHQ9IjUxMiIgeD0iMCIgeT0iMCIgdmlld0JveD0iMCAwIDQ3Ny44NjcgNDc3Ljg2NyIgc3R5bGU9ImVuYWJsZS1iYWNrZ3JvdW5kOm5ldyAwIDAgNTEyIDUxMiIgeG1sOnNwYWNlPSJwcmVzZXJ2ZSIgY2xhc3M9IiI+PGcgdHJhbnNmb3JtPSJtYXRyaXgoMS4wMiwwLDAsMS4wMiwtNC43Nzg2NjAwMTEyOTEyNjUsLTQuNzc4NjcwMDQzOTQ1MTEyKSI+CjxnIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyI+Cgk8Zz4KCQk8cGF0aCBkPSJNNDQzLjczMywzMDcuMmMtOS40MjYsMC0xNy4wNjcsNy42NDEtMTcuMDY3LDE3LjA2N3YxMDIuNGMwLDkuNDI2LTcuNjQxLDE3LjA2Ny0xNy4wNjcsMTcuMDY3SDY4LjI2NyAgICBjLTkuNDI2LDAtMTcuMDY3LTcuNjQxLTE3LjA2Ny0xNy4wNjd2LTEwMi40YzAtOS40MjYtNy42NDEtMTcuMDY3LTE3LjA2Ny0xNy4wNjdzLTE3LjA2Nyw3LjY0MS0xNy4wNjcsMTcuMDY3djEwMi40ICAgIGMwLDI4LjI3NywyMi45MjMsNTEuMiw1MS4yLDUxLjJINDA5LjZjMjguMjc3LDAsNTEuMi0yMi45MjMsNTEuMi01MS4ydi0xMDIuNEM0NjAuOCwzMTQuODQxLDQ1My4xNTksMzA3LjIsNDQzLjczMywzMDcuMnoiIGZpbGw9IiNmZmZmZmYiIGRhdGEtb3JpZ2luYWw9IiMwMDAwMDAiIHN0eWxlPSIiIGNsYXNzPSIiPjwvcGF0aD4KCTwvZz4KPC9nPgo8ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciPgoJPGc+CgkJPHBhdGggZD0iTTMzNS45NDcsMjk1LjEzNGMtNi42MTQtNi4zODctMTcuMDk5LTYuMzg3LTIzLjcxMiwwTDI1NiwzNTEuMzM0VjE3LjA2N0MyNTYsNy42NDEsMjQ4LjM1OSwwLDIzOC45MzMsMCAgICBzLTE3LjA2Nyw3LjY0MS0xNy4wNjcsMTcuMDY3djMzNC4yNjhsLTU2LjIwMS01Ni4yMDFjLTYuNzgtNi41NDgtMTcuNTg0LTYuMzYtMjQuMTMyLDAuNDE5Yy02LjM4OCw2LjYxNC02LjM4OCwxNy4wOTksMCwyMy43MTMgICAgbDg1LjMzMyw4NS4zMzNjNi42NTcsNi42NzMsMTcuNDYzLDYuNjg3LDI0LjEzNiwwLjAzMWMwLjAxLTAuMDEsMC4wMi0wLjAyLDAuMDMxLTAuMDMxbDg1LjMzMy04NS4zMzMgICAgQzM0Mi45MTUsMzEyLjQ4NiwzNDIuNzI3LDMwMS42ODIsMzM1Ljk0NywyOTUuMTM0eiIgZmlsbD0iI2ZmZmZmZiIgZGF0YS1vcmlnaW5hbD0iIzAwMDAwMCIgc3R5bGU9IiIgY2xhc3M9IiI+PC9wYXRoPgoJPC9nPgo8L2c+CjxnIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyI+CjwvZz4KPGcgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIj4KPC9nPgo8ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciPgo8L2c+CjxnIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyI+CjwvZz4KPGcgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIj4KPC9nPgo8ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciPgo8L2c+CjxnIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyI+CjwvZz4KPGcgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIj4KPC9nPgo8ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciPgo8L2c+CjxnIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyI+CjwvZz4KPGcgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIj4KPC9nPgo8ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciPgo8L2c+CjxnIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyI+CjwvZz4KPGcgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIj4KPC9nPgo8ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciPgo8L2c+CjwvZz48L3N2Zz4=" />
                                            <div className="wording">Download QRIS</div>
                                        </div>
                                    </Link>

                                    <div className="button-qr last"
                                    

                                    onClick={() => {
                                    if (navigator.share) {
                                           
                                        navigator
                                          .share({
                                            title: "QR Mandiri",
                                            url: this.state.oobQr
                                          })
                                          .then(() => {
                                            console.log('Successfully shared');
                                          })
                                          .catch(error => {
                                            console.error('Something went wrong sharing the blog', error);
                                          });
                                      }
                                      
                                    
                                    }}

                                    >
                                            <img className="icon-flatten" src="data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHZlcnNpb249IjEuMSIgeG1sbnM6eGxpbms9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkveGxpbmsiIHhtbG5zOnN2Z2pzPSJodHRwOi8vc3ZnanMuY29tL3N2Z2pzIiB3aWR0aD0iNTEyIiBoZWlnaHQ9IjUxMiIgeD0iMCIgeT0iMCIgdmlld0JveD0iMCAwIDUxMiA1MTIiIHN0eWxlPSJlbmFibGUtYmFja2dyb3VuZDpuZXcgMCAwIDUxMiA1MTIiIHhtbDpzcGFjZT0icHJlc2VydmUiPjxnPgo8ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciPgoJPGc+CgkJPHBhdGggZD0iTTQwNiwzMzJjLTI5LjYzNiwwLTU1Ljk2OSwxNC40MDItNzIuMzc4LDM2LjU3MWwtMTQxLjI3LTcyLjE5NUMxOTQuNzIyLDI4OC4zMjQsMTk2LDI3OS44MDksMTk2LDI3MSAgICBjMC0xMS45MzEtMi4zMzktMjMuMzI0LTYuNTc0LTMzLjc1M2wxNDguMDYtODguOTU4QzM1NC4wMDYsMTY3LjY3OSwzNzguNTksMTgwLDQwNiwxODBjNDkuNjI2LDAsOTAtNDAuMzc0LDkwLTkwICAgIGMwLTQ5LjYyNi00MC4zNzQtOTAtOTAtOTBjLTQ5LjYyNiwwLTkwLDQwLjM3NC05MCw5MGMwLDExLjQ3LDIuMTYxLDIyLjQ0Myw2LjA5LDMyLjU0bC0xNDguNDMsODkuMTggICAgQzE1Ny4xNTIsMTkyLjkwMiwxMzIuOTQxLDE4MSwxMDYsMTgxYy00OS42MjYsMC05MCw0MC4zNzQtOTAsOTBjMCw0OS42MjYsNDAuMzc0LDkwLDkwLDkwYzMwLjEyMiwwLDU2LjgzMi0xNC44NzYsNzMuMTc3LTM3LjY2NiAgICBsMTQwLjg2LDcxLjk4NUMzMTcuNDE0LDQwMy43NTMsMzE2LDQxMi43MTQsMzE2LDQyMmMwLDQ5LjYyNiw0MC4zNzQsOTAsOTAsOTBjNDkuNjI2LDAsOTAtNDAuMzc0LDkwLTkwICAgIEM0OTYsMzcyLjM3NCw0NTUuNjI2LDMzMiw0MDYsMzMyeiBNNDA2LDMwYzMzLjA4NCwwLDYwLDI2LjkxNiw2MCw2MHMtMjYuOTE2LDYwLTYwLDYwcy02MC0yNi45MTYtNjAtNjBTMzcyLjkxNiwzMCw0MDYsMzB6ICAgICBNMTA2LDMzMWMtMzMuMDg0LDAtNjAtMjYuOTE2LTYwLTYwczI2LjkxNi02MCw2MC02MHM2MCwyNi45MTYsNjAsNjBTMTM5LjA4NCwzMzEsMTA2LDMzMXogTTQwNiw0ODJjLTMzLjA4NCwwLTYwLTI2LjkxNi02MC02MCAgICBzMjYuOTE2LTYwLDYwLTYwczYwLDI2LjkxNiw2MCw2MFM0MzkuMDg0LDQ4Miw0MDYsNDgyeiIgZmlsbD0iI2ZmZmZmZiIgZGF0YS1vcmlnaW5hbD0iIzAwMDAwMCIgc3R5bGU9IiI+PC9wYXRoPgoJPC9nPgo8L2c+CjxnIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyI+CjwvZz4KPGcgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIj4KPC9nPgo8ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciPgo8L2c+CjxnIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyI+CjwvZz4KPGcgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIj4KPC9nPgo8ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciPgo8L2c+CjxnIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyI+CjwvZz4KPGcgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIj4KPC9nPgo8ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciPgo8L2c+CjxnIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyI+CjwvZz4KPGcgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIj4KPC9nPgo8ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciPgo8L2c+CjxnIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyI+CjwvZz4KPGcgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIj4KPC9nPgo8ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciPgo8L2c+CjwvZz48L3N2Zz4=" />
                                            <div className="wording">Bagikan QRIS</div>
                                    </div>

                                    {/* <WhatsappShareButton url={this.state.oobQr} subject="Qr_picture">
                                        <div className="button-qr last">
                                            <img className="icon-flatten" src="data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHZlcnNpb249IjEuMSIgeG1sbnM6eGxpbms9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkveGxpbmsiIHhtbG5zOnN2Z2pzPSJodHRwOi8vc3ZnanMuY29tL3N2Z2pzIiB3aWR0aD0iNTEyIiBoZWlnaHQ9IjUxMiIgeD0iMCIgeT0iMCIgdmlld0JveD0iMCAwIDUxMiA1MTIiIHN0eWxlPSJlbmFibGUtYmFja2dyb3VuZDpuZXcgMCAwIDUxMiA1MTIiIHhtbDpzcGFjZT0icHJlc2VydmUiPjxnPgo8ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciPgoJPGc+CgkJPHBhdGggZD0iTTQwNiwzMzJjLTI5LjYzNiwwLTU1Ljk2OSwxNC40MDItNzIuMzc4LDM2LjU3MWwtMTQxLjI3LTcyLjE5NUMxOTQuNzIyLDI4OC4zMjQsMTk2LDI3OS44MDksMTk2LDI3MSAgICBjMC0xMS45MzEtMi4zMzktMjMuMzI0LTYuNTc0LTMzLjc1M2wxNDguMDYtODguOTU4QzM1NC4wMDYsMTY3LjY3OSwzNzguNTksMTgwLDQwNiwxODBjNDkuNjI2LDAsOTAtNDAuMzc0LDkwLTkwICAgIGMwLTQ5LjYyNi00MC4zNzQtOTAtOTAtOTBjLTQ5LjYyNiwwLTkwLDQwLjM3NC05MCw5MGMwLDExLjQ3LDIuMTYxLDIyLjQ0Myw2LjA5LDMyLjU0bC0xNDguNDMsODkuMTggICAgQzE1Ny4xNTIsMTkyLjkwMiwxMzIuOTQxLDE4MSwxMDYsMTgxYy00OS42MjYsMC05MCw0MC4zNzQtOTAsOTBjMCw0OS42MjYsNDAuMzc0LDkwLDkwLDkwYzMwLjEyMiwwLDU2LjgzMi0xNC44NzYsNzMuMTc3LTM3LjY2NiAgICBsMTQwLjg2LDcxLjk4NUMzMTcuNDE0LDQwMy43NTMsMzE2LDQxMi43MTQsMzE2LDQyMmMwLDQ5LjYyNiw0MC4zNzQsOTAsOTAsOTBjNDkuNjI2LDAsOTAtNDAuMzc0LDkwLTkwICAgIEM0OTYsMzcyLjM3NCw0NTUuNjI2LDMzMiw0MDYsMzMyeiBNNDA2LDMwYzMzLjA4NCwwLDYwLDI2LjkxNiw2MCw2MHMtMjYuOTE2LDYwLTYwLDYwcy02MC0yNi45MTYtNjAtNjBTMzcyLjkxNiwzMCw0MDYsMzB6ICAgICBNMTA2LDMzMWMtMzMuMDg0LDAtNjAtMjYuOTE2LTYwLTYwczI2LjkxNi02MCw2MC02MHM2MCwyNi45MTYsNjAsNjBTMTM5LjA4NCwzMzEsMTA2LDMzMXogTTQwNiw0ODJjLTMzLjA4NCwwLTYwLTI2LjkxNi02MC02MCAgICBzMjYuOTE2LTYwLDYwLTYwczYwLDI2LjkxNiw2MCw2MFM0MzkuMDg0LDQ4Miw0MDYsNDgyeiIgZmlsbD0iI2ZmZmZmZiIgZGF0YS1vcmlnaW5hbD0iIzAwMDAwMCIgc3R5bGU9IiI+PC9wYXRoPgoJPC9nPgo8L2c+CjxnIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyI+CjwvZz4KPGcgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIj4KPC9nPgo8ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciPgo8L2c+CjxnIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyI+CjwvZz4KPGcgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIj4KPC9nPgo8ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciPgo8L2c+CjxnIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyI+CjwvZz4KPGcgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIj4KPC9nPgo8ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciPgo8L2c+CjxnIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyI+CjwvZz4KPGcgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIj4KPC9nPgo8ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciPgo8L2c+CjxnIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyI+CjwvZz4KPGcgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIj4KPC9nPgo8ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciPgo8L2c+CjwvZz48L3N2Zz4=" />
                                            <div className="wording">Bagikan QRIS</div>
                                        </div>
                                    </WhatsappShareButton> */}
                                </div>
                            </span>
                        </div>
                        <div id="body-dashboard">
                            {this.state.headerTrx != null ? <span>
                                <div className="container-body-iklan" onClick={()=>{
                                        this.props.history.push('/kontenMeningkatPenjualan');
                                        location.reload();
                                }}>
                                    <div className="strip-body-iklan">
                                        <img src={BlueTriumph}></img>
                                        <p>
                                            Cara Meningkatkan Penjualan dari Online
                                            </p>
                                    </div>
                                </div>

{this.state.headerTrx.detail.length != 0 ?
                                <span>
                                <div className="container-body-transaksi">
                                    <div className="strip-body-transaksi">
                                         <div className = "strip-jumlah-transaksi">
                                            <p className="strip-jumlah-transaksi-header">
                                               TRANSAKSI ANDA HARI INI
                                            </p>
                                            <p className="strip-jumlah-transaksi-money">
                                               {this.state.headerTrx.sumAllTransaksiAfterFee}
                                            </p>
                                            <p className="strip-jumlah-transaksi-transaction">
                                            {this.state.headerTrx.countAllTransaksi} Transaksi
                                            </p>
                                          </div>
                                            <BlueHouse className='strip-body-header'></BlueHouse>
                                        
                                    </div>
                                </div>

                                {this.state.headerTrx.detail.slice(0,7).map((value, id) => (
                                  
                                  <div key={id} className="container-body-transaksi more-narrow">
                                    <div className="strip-body-transaksi smaller">
                                         <div className = "strip-jumlah-transaksi">
                                         
                                            <p className="strip-jumlah-transaksi-transaction payment">
                                              Pembayaran via {value.issuerPayment} 
                                            </p>
                                            <p className="strip-jumlah-transaksi-transaction time">
                                            {value.time}
                                            </p>
                                            <p className="strip-jumlah-transaksi-transaction number">
                                             {value.number}
                                         </p>
                                          </div>
                                            <p className="new-strip-boulder">
                                             {value.detail.transferAmount}
                                            </p>
                                    </div>

                                    
                                </div>))
                                }
                                </span> : 
                                <span>
  <div className="container-body-transaksi">
                                    <div className="strip-body-transaksi">
                                         <div className = "strip-jumlah-transaksi">
                                            <p className="strip-jumlah-transaksi-header"
                                            style={{width:'45vw',fontSize:'4.715vw'}}
                                            >
                                            Ayo, Mulai Gunakan QRIS Mandiri
                                            </p>
                                          </div>
                                            <BlueHouse className='strip-body-header'></BlueHouse>
                                        
                                    </div>
                                </div>
                                </span>
}

                                    <div className="container-body-transaksi more-narrow">
                                        <div className="strip-body-footer">
                                            <Link href="/riwayatTransaksi">
                                                <p>Lihat Riwayat Transaksi</p>
                                            </Link>

                                        </div>
                                    </div>

                                    <div className="strip-footer-help">
                                        <p style={{ fontWeight: '400', fontSize: '3.167vw', color: '#007ACF' }}
                                            onClick={
                                                () => {
                                                    this.props.history.push('/helpSaya');
                                                    location.reload();
                                                }
                                            }
                                        >
                                            <img className="icon-flatten-helpMe" src="data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHZlcnNpb249IjEuMSIgeG1sbnM6eGxpbms9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkveGxpbmsiIHhtbG5zOnN2Z2pzPSJodHRwOi8vc3ZnanMuY29tL3N2Z2pzIiB3aWR0aD0iNTEyIiBoZWlnaHQ9IjUxMiIgeD0iMCIgeT0iMCIgdmlld0JveD0iMCAwIDUxMiA1MTIiIHN0eWxlPSJlbmFibGUtYmFja2dyb3VuZDpuZXcgMCAwIDUxMiA1MTIiIHhtbDpzcGFjZT0icHJlc2VydmUiIGNsYXNzPSIiPjxnPgo8ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciPgoJPGc+CgkJPHBhdGggZD0iTTEzNiwyMTBjLTI0LjgxNCwwLTQ1LDIwLjE4Ni00NSw0NXYxMjJjMCwyNC44MTQsMjAuMTg2LDQ1LDQ1LDQ1YzI0LjgxNCwwLDQ1LTIwLjE4Niw0NS00NVYyNTUgICAgQzE4MSwyMzAuMTg2LDE2MC44MTQsMjEwLDEzNiwyMTB6IiBmaWxsPSIjMDA3YWNmIiBkYXRhLW9yaWdpbmFsPSIjMDAwMDAwIiBzdHlsZT0iIj48L3BhdGg+Cgk8L2c+CjwvZz4KPGcgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIj4KCTxnPgoJCTxwYXRoIGQ9Ik02MSwyNTVjMC00LjMyNywwLjU3MS04LjUwNywxLjI3OC0xMi42MzRDNDQuMiwyNDguMjA5LDMxLDI2NS4wMDEsMzEsMjg1djYyYzAsMTkuOTk5LDEzLjIsMzYuNzkxLDMxLjI3OCw0Mi42MzQgICAgQzYxLjU3MSwzODUuNTA3LDYxLDM4MS4zMjcsNjEsMzc3VjI1NXoiIGZpbGw9IiMwMDdhY2YiIGRhdGEtb3JpZ2luYWw9IiMwMDAwMDAiIHN0eWxlPSIiPjwvcGF0aD4KCTwvZz4KPC9nPgo8ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciPgoJPGc+CgkJPHBhdGggZD0iTTM3NiwyMTBjLTI0LjgxNCwwLTQ1LDIwLjE4Ni00NSw0NXYxMjJjMCwyNC44MTQsMjAuMTg2LDQ1LDQ1LDQ1YzQuNTEsMCw4Ljc4Mi0wLjg2OCwxMi44OTItMi4xMDggICAgQzM4My4zMDgsNDM4LjQwMSwzNjYuMzA1LDQ1MiwzNDYsNDUyaC00Ny43NjNjLTYuMjEzLTE3LjQyMi0yMi43MDctMzAtNDIuMjM3LTMwYy0yNC44MTQsMC00NSwyMC4xODYtNDUsNDUgICAgYzAsMjQuODE0LDIwLjE4Niw0NSw0NSw0NWMxOS41MywwLDM2LjAyNC0xMi41NzgsNDIuMjM3LTMwSDM0NmM0MS4zNTMsMCw3NS0zMy42NDcsNzUtNzV2LTMwVjI1NSAgICBDNDIxLDIzMC4xODYsNDAwLjgxNCwyMTAsMzc2LDIxMHoiIGZpbGw9IiMwMDdhY2YiIGRhdGEtb3JpZ2luYWw9IiMwMDAwMDAiIHN0eWxlPSIiPjwvcGF0aD4KCTwvZz4KPC9nPgo8ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciPgoJPGc+CgkJPHBhdGggZD0iTTQ0OS43MjIsMjQyLjM2NkM0NTAuNDI5LDI0Ni40OTMsNDUxLDI1MC42NzMsNDUxLDI1NXYxMjJjMCw0LjMyNy0wLjU3MSw4LjUwNy0xLjI3OCwxMi42MzQgICAgQzQ2Ny44LDM4My43OTEsNDgxLDM2Ni45OTksNDgxLDM0N3YtNjJDNDgxLDI2NS4wMDEsNDY3LjgsMjQ4LjIwOSw0NDkuNzIyLDI0Mi4zNjZ6IiBmaWxsPSIjMDA3YWNmIiBkYXRhLW9yaWdpbmFsPSIjMDAwMDAwIiBzdHlsZT0iIj48L3BhdGg+Cgk8L2c+CjwvZz4KPGcgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIj4KCTxnPgoJCTxwYXRoIGQ9Ik0yNTYsMEMxMzEuOTI4LDAsMzEsMTAwLjkyOCwzMSwyMjV2MC4zODNjOC45MzctNi43NjYsMTkuMjc3LTExLjcxNywzMC42ODctMTMuOTM0QzY4LjY5OCwxMTAuMjUxLDE1My4wNTQsMzAsMjU2LDMwICAgIHMxODcuMzAyLDgwLjI1MSwxOTQuMzEzLDE4MS40NDhjMTEuNDA5LDIuMjE3LDIxLjc0OSw3LjE2OSwzMC42ODcsMTMuOTM0VjIyNUM0ODEsMTAwLjkyOCwzODAuMDcyLDAsMjU2LDB6IiBmaWxsPSIjMDA3YWNmIiBkYXRhLW9yaWdpbmFsPSIjMDAwMDAwIiBzdHlsZT0iIj48L3BhdGg+Cgk8L2c+CjwvZz4KPGcgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIj4KPC9nPgo8ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciPgo8L2c+CjxnIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyI+CjwvZz4KPGcgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIj4KPC9nPgo8ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciPgo8L2c+CjxnIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyI+CjwvZz4KPGcgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIj4KPC9nPgo8ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciPgo8L2c+CjxnIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyI+CjwvZz4KPGcgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIj4KPC9nPgo8ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciPgo8L2c+CjxnIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyI+CjwvZz4KPGcgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIj4KPC9nPgo8ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciPgo8L2c+CjxnIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyI+CjwvZz4KPC9nPjwvc3ZnPg==" />

                                        Pusat Bantuan </p>
                                    </div>
                            </span> 
                            : <span></span>}
                        </div>
                    </div>
                }
            </div>
        );
    }

}