import { Box, Button, Grid, Link, makeStyles, Paper ,withStyles} from "@material-ui/core";
import {
  Skeleton,
  Timeline,
  TimelineConnector,
  TimelineContent,
  TimelineDot,
  TimelineItem,
  TimelineSeparator,
} from "@material-ui/lab";
import { compose } from "recompose";
import React, { Component } from "react";
import LoadingMaker from "../../helpers/loadingMaker/LoadingMaker";
import '../Login/login.css';
import { BACK_END_POINT, GET, HEADER_AUTH, POST, TOKEN_AUTH, wsCallingWithBody, wsWithBody, wsWithoutBody } from '../../master/masterComponent';


const useStyles = theme => ({

  paper: {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center'
  },
  containerPaper: {
    width: '92%'
  },
  avatar: {
    margin: theme.spacing(1),
    backgroundColor: theme.palette.secondary.main,
  },
  form: {
    width: '100%', // Fix IE 11 issue.
    marginTop: theme.spacing(1),
  },
  submit: {
    margin: theme.spacing(3, 0, 2),
  },
  textField: {
    boxSizing: 'border-box',
    paddingLeft: '16px',
    border: 'none',

    width: '100%',
    background: 'none',
    fontSize: "4.072vw",
    height: '100%',
    fontWeight: 'bold',
    fontStyle: 'normal',
    fontFamily: 'Nunito, sans-serif',
    textTransform: 'none',
    fontWeight: 500,
    position: 'relative',
    zIndex: 1,
    paddingTop: '16px'
  },
  containerTextField: {
    position: 'relative',
    width: '100%',
    height: '8.370702541106128vh',
    marginBottom: '16px',
    borderRadius: '6px',
    backgroundColor: '#F7F8F9'
  },
  backdrop: {
    zIndex: theme.zIndex.drawer + 1,
    color: '#fff',
  }
});

class StatusPendaftaran extends Component {
  constructor(props) {
    super(props);

    this.state = {
        isLoading : true,
        nomorPendaftaran : '',
        namaUsaha : '',
        showBagan :false,
        mapBagan : {},
        isAlreadyLogin : false
    }
  }

  componentDidMount(){

    window.onpopstate = function (event) {
      event.preventDefault();
      window.location.reload();
    };

    wsWithBody(BACK_END_POINT + "/UserOob/checkStatusPendaftaran", POST,
      {
        qNoHp: localStorage.getItem('noHpTerdaftar'),
        qNoPendaftaran: localStorage.getItem('noPendaftaran')
      }, HEADER_AUTH).then(response => {
        this.setState({ isLoading: false, namaUsaha : response.data.result.infoNasabah.namaUsaha,
          nomorPendaftaran : localStorage.getItem('noPendaftaran'),
          showBagan : true, mapBagan : response.data.result.mapStatus});
          Object.entries(response.data.result.mapStatus).map(([idKey, value]) =>{
            if(idKey==3 && value.isOnOpen && !value.isOnTolak){
              this.setState({isAlreadyLogin:true});
            }
          });
      }).catch(error => {
        this.setState({ isLoading: false });
        alert(error.response.data.message);
        this.props.history.push('/cekStatusPendaftaran');
        location.reload();
      })
  }

  render() {
    const { loading, classes } = this.props;
    return (
      <div className={classes.paper}>
        <div className={classes.containerPaper}>
          <LoadingMaker onLoading={this.state.isLoading}></LoadingMaker>
        <Grid className="cardSlid" style={{ marginTop: "80px" }}>
          {loading ? (
            <Skeleton animation="wave" variant="text"></Skeleton>
          ) : (
            <Box>
              <h3 style={{fontFamily: 'Montserrat, sans-serif', fontSize: '4.6vw'}}>Status Pendaftaran</h3>
              <p style={{ color: "rgba(107, 121, 132, 1)",
                 fontSize: '3.62vw', fontFamily: 'Nunito, sans-serif',
                 lineHeight:"5.430vw",margin:"0 -1.38vw" }}>
                Pendaftaran Anda sedang diproses. Kami akan mengabari status
                selanjutnya melalui SMS dan email
              </p>
            </Box>
          )}
        </Grid>
        <hr style={{ width: "90.602vw", border:"1px solid #E5E8EA" }}></hr>
        <Grid container spacing={3} style={{ paddingLeft: "2px" }}>
          <Grid className="cardSlid" item xs={6}>
            {loading ? (
              <Skeleton animation="wave" variant="text"></Skeleton>
            ) : (
              <div style={{textAlign:"left",marginLeft:'4.335vw'}}>
                 <p className="text-muted" style={{fontSize: '3vw', fontFamily: 'Nunito, sans-serif', 
                 color: "rgba(107, 121, 132, 1)", marginLeft:'-0.16vw', marginTop:'4vw'}}>Nomor Pendaftaran</p>
                 <h4 style={{fontSize: '3.620vw', fontFamily: 'Nunito, sans-serif', fontWeight:'400'}}>{this.state.nomorPendaftaran}</h4>
              </div>
            )}
          </Grid>
          <Grid className="cardSlid" item xs={6}>
            {loading ? (
              <Skeleton animation="wave" variant="text"></Skeleton>
            ) : (
              <div style={{textAlign:"left", marginTop:'4vw'}}>
                <p className="text-muted" style={{fontSize: '3vw', fontFamily: 'Nunito, sans-serif',color: "rgba(107, 121, 132, 1)",marginLeft:'-0.16vw'}}>Nama Usaha</p>
                 <h4 style={{fontSize: '3.620vw', fontFamily: 'Nunito, sans-serif',fontWeight:'400'}}>{this.state.namaUsaha}</h4>
              </div>
            )}
          </Grid>
          {/* <hr></hr> */}
        </Grid>
        <hr style={{ width: "90.602vw", border:"1px solid #E5E8EA" }}></hr>
        <Grid
          className="status"
          style={{ marginTop: "0.29895366218236175vh", marginLeft: "3.154734vw" }}
        >
          {loading ? (
            <Skeleton animation="wave" variant="text"></Skeleton>
          ) : (
            <Box>
              <p style={{ color: "rgba(107, 121, 132, 1)", fontSize:"2.862vw", fontFamily: 'TTInterfaces', left:'-3vw',top:'2vw',position:'relative',
                           }}>STATUS</p>
            </Box>
          )}
        </Grid>
        {
          this.state.showBagan ? 
          <span>
        <Timeline style={{ marginLeft: "-86.408vw" }}>
           {
              Object.entries(this.state.mapBagan).map(([idKey, value]) =>
              {
                  return    <TimelineItem>
                  <TimelineSeparator>
                    <TimelineDot style={{backgroundColor:(value.isOnOpen ? 'blue':'#CFE0F9'), border:(value.isOnOpen ? '0.733vw solid #97BBF5':''), width:'3.882vw', height:'3.882vw', boxShadow:'none'}}>
                  


{ idKey==3 && value.isOnOpen ?

<svg width="4.267vw" height="4.267vw" viewBox="0 0 4.267vw 4.267vw" fill="none" xmlns="http://www.w3.org/2000/svg">
<path d="M9.80778 2.75L11.9169 4.84137V13.25H3.75022V2.75H9.80778ZM10.2333 1H2.74105C2.53969 1.00504 2.34857 1.08981 2.20966 1.23567C2.07075 1.38153 1.99542 1.57657 2.00022 1.77793V14.2221C1.99542 14.4234 2.07075 14.6185 2.20966 14.7643C2.34857 14.9102 2.53969 14.995 2.74105 15H12.926C13.1274 14.995 13.3185 14.9102 13.4574 14.7643C13.5963 14.6185 13.6717 14.4234 13.6669 14.2221V4.4104C13.6669 4.3158 13.6482 4.22213 13.6118 4.13481C13.5754 4.04748 13.5221 3.96823 13.4549 3.90162L10.7378 1.20778C10.6036 1.07467 10.4223 0.999988 10.2333 1Z" fill="white"/>
<path d="M7.37023 11.147L4.99023 9.18588L5.91773 8.06004L7.1894 9.10888L9.63007 6.24121L10.7396 7.18621L7.37023 11.147Z" fill="white"/>
</svg> :
value.isOnOpen ? 
<svg width="4.267vw" height="4.267vw" viewBox="0 0 4.267vw 4.267vw" fill="none" xmlns="http://www.w3.org/2000/svg">
<path d="M8 2.75386C9.03759 2.75386 10.0519 3.06154 10.9146 3.638C11.7773 4.21446 12.4497 5.0338 12.8468 5.99241C13.2439 6.95103 13.3478 8.00586 13.1453 9.02351C12.9429 10.0412 12.4432 10.9759 11.7095 11.7096C10.9758 12.4433 10.0411 12.9429 9.0234 13.1454C8.00574 13.3478 6.95091 13.2439 5.99231 12.8468C5.0337 12.4497 4.21438 11.7773 3.63794 10.9145C3.0615 10.0518 2.75384 9.03748 2.75386 7.99988C2.75544 6.60901 3.30867 5.27557 4.29217 4.29208C5.27567 3.3086 6.60913 2.75541 8 2.75386ZM8 1C6.61553 1 5.26214 1.41055 4.111 2.17972C2.95985 2.9489 2.06265 4.04215 1.53284 5.32124C1.00303 6.60033 0.864409 8.0078 1.13451 9.36567C1.40462 10.7235 2.07132 11.9708 3.0503 12.9498C4.02928 13.9288 5.27657 14.5954 6.63444 14.8655C7.99232 15.1356 9.39979 14.997 10.6789 14.4671C11.9579 13.9373 13.0512 13.0401 13.8203 11.8889C14.5895 10.7377 15 9.38436 15 7.99988C15 6.14339 14.2625 4.36294 12.9497 3.05021C11.637 1.73748 9.8565 1 8 1Z" fill="white"/>
<path d="M8.02957 4.47559C7.09926 4.47559 6.20706 4.84515 5.54924 5.50297C4.89141 6.1608 4.52185 7.053 4.52185 7.9833H8.02957V4.47559Z" fill="white"/>
</svg>
:
<div></div>


}


                    </TimelineDot>
                    {
                    idKey <=2 ?  
                    <TimelineConnector style={{height:'12vw',
                       border:'0.294vw dashed #A1CEED', background:'none',width:'0', borderLeft:'none'}}/> 
                    : <span></span>
                    }
                  </TimelineSeparator>
                  <TimelineContent>
                    <span style={{fontFamily: 'Nunito, sans-serif', fontWeight : (value.isOnOpen ? '700':''),
                    color:'rgba(1, 26, 44, 1)', fontSize:'4.62vw'}}>
                    {value.headerStatus}
                    </span>
                    <p style={{ color: (value.isOnOpen ?"rgba(107, 121, 132, 1)":"rgba(48, 59, 74, 0.4)"), 
                         marginBottom: "0.452vw",
                         fontFamily: 'Nunito, sans-serif', fontSize:'3.167vw' }}>
                         
                      {value.statusInDate}  {
                          value.statusInDate!=null ?  '•' : ''
                            }  {value.statusInTime}
                    </p>
                    { value.isOnOpen ?
                    <span key = {idKey}>
                    <p style={{ color: (value.isOnOpen ?"rgba(107, 121, 132, 1)":"rgba(48, 59, 74, 0.4)"),fontFamily: 'Nunito, sans-serif', fontSize:'3.167vw'  }}>
                      {value.bodyStatus}
                    </p>
                    </span> : <span></span>
                    }
                  </TimelineContent>
                  </TimelineItem>
              }
              )
           }
        </Timeline>
        </span>:<span></span>
        }
        
        { this.state.isAlreadyLogin?
        <Link href="/login">
        <Button
          type="submit"
          variant="contained"
          size="large"
          style={{
            width:'100%',
            position:'relative',
            marginTop:'6.100vw',
            marginBottom:'6.240vw',
            textTransform:'none',
            height:'12.670vw',
            borderRadius:''
          }}
          color="primary"
        >
          <p style={{fontSize:'3.620vw'}}>
          Login
          </p>
        </Button>
        </Link>
        :
        <Link href="/">
        <Button
          type="submit"
          variant="contained"
          size="large"
          style={{
            width:'100%',
            position:'relative',
            marginTop:'6.100vw',
            marginBottom:'6.240vw',
            textTransform:'none',
            height:'12.670vw',
            borderRadius:''
          }}
          color="primary"
        >
          <p style={{fontSize:'3.620vw'}}>
          Kembali ke Beranda
          </p>
        </Button>
        </Link>}
        {/* <div>
          <Grid container spacing={3}>
            <Grid item xs={6}>
              <Paper>xs=6</Paper>
            </Grid>
            <Grid item xs={6}>
              <Paper>xs=6</Paper>
            </Grid>
          </Grid>
        </div> */}
      </div>
      </div>
    );
  }
}

export default compose(
  withStyles(useStyles),
)(StatusPendaftaran);
