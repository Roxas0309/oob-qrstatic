import { Backdrop, Box, Button, CircularProgress, Fade, Grid, TextField, Typography, withStyles } from "@material-ui/core";
import { Skeleton } from "@material-ui/lab";
import React, { Component } from "react";
import { compose } from "recompose";
import StatusPendaftaran from "../../../public/img/Status Pendaftaran.png";
import { createBrowserHistory } from "history";
import LoadingMaker from "../../helpers/loadingMaker/LoadingMaker";
import '../Login/login.css';
import { BACK_END_POINT, GET, HEADER_AUTH, POST, TOKEN_AUTH, wsCallingWithBody, wsWithBody, wsWithoutBody } from '../../master/masterComponent';
import '../../assets/css/uploadCustom.scss';
// import {Vibration } from "react-native";
const useStyles = theme => ({

  paper: {
    marginTop: '14vw',
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center'
  },
  containerPaper: {
    width: '90%'
  },
  avatar: {
    margin: theme.spacing(1),
    backgroundColor: theme.palette.secondary.main,
  },
  form: {
    width: '100%', // Fix IE 11 issue.
    marginTop: theme.spacing(1),
  },
  submit: {
    margin: theme.spacing(3, 0, 2),
  },
  textField: {
    boxSizing: 'border-box',
    paddingLeft: '3.620vw',
    border: 'none',
    height: '100%',
    width: '100%',
    background: 'none',
    height: '100%',
    fontWeight: 'bold',
    fontStyle: 'normal',
    fontFamily: 'Nunito, sans-serif',
    textTransform: 'none',
    fontWeight: 500,
    position: 'relative',
    zIndex: 1,
    paddingTop: '3.620vw',
    fontSize: "4.072vw"
  },
  containerTextField: {
    fontSize: "4.072vw",
    position: 'relative',
    width: '100%',
    height: '12.670vw',
    marginBottom: '4.357vw',
    borderRadius: '1.357vw',
    backgroundColor: '#F7F8F9'
  },
  containerTextFieldError: {
    fontSize: "4.072vw",
    position: 'relative',
    width: '100%',
    height: '12.670vw',
    marginBottom: '4.357vw',
    borderRadius: '1.357vw',
    backgroundColor: '#F7F8F9',
    animation: 'shake 0.5s',
    animationIterationCount: '1'
  },
  backdrop: {
    zIndex: theme.zIndex.drawer + 1,
    color: '#fff',
  }
});

const history = createBrowserHistory();

class CekStatusPendaftaran extends Component {
  constructor(props) {
    super(props);

    this.state = {
      data: "",
      openAlert: false,
      query: "idle",
      proses: "",
      userPhoneTerdaftar: '',
      nomorPendaftaran: '',
      isLoading: false,
      showErrorLogin: false,
      errorMessage: '',
      errorNoHp: false,
      errorNoPendaftaran:false,
      allowClick:false,
      allowClick2:false,
      loginErrorStatus : null,
      onKeyDownNomorPendaftaran : null,
    };
  }

  cekStatus = () => {
    this.setState({ isLoading: true });
    wsWithBody(BACK_END_POINT + "/UserOob/checkStatusPendaftaran", POST,
      {
        qNoHp: this.state.userPhoneTerdaftar,
        qNoPendaftaran: this.state.nomorPendaftaran
      }, HEADER_AUTH).then(response => {
        this.setState({ isLoading: false });
        localStorage.setItem('noHpTerdaftar', this.state.userPhoneTerdaftar);
        localStorage.setItem('noPendaftaran', this.state.nomorPendaftaran);
        history.push('/statusPendaftaran');
        location.reload();
      }).catch(error => {
        this.setState({ isLoading: false });
        this.setState({ showErrorLogin: true, errorMessage: error.response.data.message, 
          loginErrorStatus : error.response.data.status });
     
      })
  }

  validateCheckNoHp = (words) => {
    var x = words.split("");
    var result = false;

    var i = 0;


    x.map((xs) => {
      var result0 = (i == 0 && xs != 0);
      var result1 = (i == 0 && xs != '+');
      if (result0 && result1) {
        result = true;
      }
      else {
        if (isNaN(xs) && i !== 0) {
          result = true;
        }
      }
      i++;
    });


    if (x[0] == 0) {
      if (words.length >= 2) {
        if (x[1] != 8) {
          result = true;
        }
      }
    }
    else if (x[0] == '+') {
      if (words.length >= 2) {
        if (x[1] != 6) {
          result = true;
        }
      }
      if (words.length >= 3) {
        if (x[1] != 6) {
          result = true;
        }
        if (x[2] != 2) {
          result = true;
        }
      }
      if (words.length >= 4) {
        if (x[1] != 6) {
          result = true;
        }
        if (x[2] != 2) {
          result = true;
        }
        if (x[3] != 8) {
          result = true;
        }
      }
    }

    return result;
  }

componentDidMount(){
  


        //  debugger;
      //  history.pushState(null, null, location.href);
        window.onpopstate = function (event) {
          event.preventDefault();
          window.location.reload();
        };
}

  render() {
    const { loading, classes } = this.props;
    const alpha = /^[a-zA-Z_]+( [a-zA-Z_]+)*$/;
    const numb = /^[0-9\b]+$/;


    return (
      <div className={classes.paper}>
        <div className={classes.containerPaper}>
          <LoadingMaker onLoading={this.state.isLoading}></LoadingMaker>

          <Grid className="cardSlid" style={{ marginTop: "18.100vw" }}>
            {loading ? (
              <Skeleton animation="wave" variant="text"></Skeleton>
            ) : (
                <Box>
                  <h5 style={{ fontFamily: 'Montserrat, sans-serif', fontSize: '4.072vw', marginTop: '-30vw' }}>Cek Status Pendaftaran MORIS</h5>
                </Box>
              )}
          </Grid>
          <Grid
            container
            wrap="nowrap"
            style={{
              justifyContent: "center",
              alignItems: "center",
              display: "flex",
              marginTop: "-5vw"
            }}
          >
            {loading ? (
              <Skeleton animation="wave" variant="rect" height={300}></Skeleton>
            ) : (
                <Box
                  style={{
                    textAlign: "center",
                    position: "fixed",
                    display: "contents",
                  }}
                >
                  <img src={StatusPendaftaran} style={{ height: "100%", width: "100vw" }} />
                </Box>
              )}
          </Grid>

          

          {
            this.state.showErrorLogin ?
              <div className="error-label">
                {
                  this.state.loginErrorStatus === 404 ?
                    <h5>Data Tidak Ditemukan</h5>
                    :
                    this.state.loginErrorStatus === 403 ?
                      <h5>Sesi telah berakhir</h5>
                      : this.state.loginErrorStatus === 500 ?
                      <h5>Internal Server Error</h5> : 
                      <h5>Ups, Data tidak sesuai</h5>
                }
                { this.state.loginErrorStatus === 500 ?
                 <p>Service provider internal system error</p> 
                 :
                 <p>{this.state.errorMessage}</p>
                }
                <img src="data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHZlcnNpb249IjEuMSIgeG1sbnM6eGxpbms9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkveGxpbmsiIHhtbG5zOnN2Z2pzPSJodHRwOi8vc3ZnanMuY29tL3N2Z2pzIiB3aWR0aD0iNTEyIiBoZWlnaHQ9IjUxMiIgeD0iMCIgeT0iMCIgdmlld0JveD0iMCAwIDgxLjk5OCA4MS45OTgiIHN0eWxlPSJlbmFibGUtYmFja2dyb3VuZDpuZXcgMCAwIDUxMiA1MTIiIHhtbDpzcGFjZT0icHJlc2VydmUiIGNsYXNzPSIiPjxnPgo8ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciPgoJPHBhdGggZD0iTTQwLjk5OSwwQzE4LjM5MiwwLDAsMTguMzkyLDAsNDAuOTk5YzAsMjIuNjA2LDE4LjM5Miw0MC45OTksNDAuOTk5LDQwLjk5OWMyMi42MDYsMCw0MC45OTktMTguMzkzLDQwLjk5OS00MC45OTkgICBDODEuOTk4LDE4LjM5Miw2My42MDcsMCw0MC45OTksMHogTTQwLjk5OSw3Ny45OThDMjAuNTk4LDc3Ljk5OCw0LDYxLjQsNCw0MC45OTlDNCwyMC41OTgsMjAuNTk4LDQsNDAuOTk5LDQgICBzMzYuOTk5LDE2LjU5OCwzNi45OTksMzYuOTk5Qzc3Ljk5OCw2MS40LDYxLjQsNzcuOTk4LDQwLjk5OSw3Ny45OTh6IE00NS43MDgsMTIuNzQ0YzAuMzc2LDAuMzg3LDAuNTgxLDAuOTA4LDAuNTY3LDEuNDQ3ICAgbC0xLjMxOSw0MS4zNzJjLTAuMDI4LDEuMDg0LTAuOTE1LDEuOTQ5LTIsMS45NDloLTMuOTE1Yy0xLjA4NCwwLTEuOTcyLTAuODY1LTItMS45NDlsLTEuMzE5LTQxLjM3MiAgIGMtMC4wMTQtMC41MzksMC4xOTEtMS4wNjEsMC41NjctMS40NDdjMC4zNzYtMC4zODYsMC44OTMtMC42MDQsMS40MzItMC42MDRoNi41NTVDNDQuODE0LDEyLjE0MSw0NS4zMzIsMTIuMzU4LDQ1LjcwOCwxMi43NDR6ICAgIE00Ni4xOTEsNjMuMDAydjcuNDljMCwxLjEwNC0wLjg5NiwyLTIsMmgtNi4zODVjLTEuMTA0LDAtMi0wLjg5Ni0yLTJ2LTcuNDljMC0xLjEwNCwwLjg5Ni0yLDItMmg2LjM4NSAgIEM0NS4yOTYsNjEuMDAyLDQ2LjE5MSw2MS44OTYsNDYuMTkxLDYzLjAwMnoiIGZpbGw9IiNmMTA1MDUiIGRhdGEtb3JpZ2luYWw9IiMwMDAwMDAiIHN0eWxlPSIiIGNsYXNzPSIiPjwvcGF0aD4KPC9nPgo8ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciPgo8L2c+CjxnIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyI+CjwvZz4KPGcgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIj4KPC9nPgo8ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciPgo8L2c+CjxnIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyI+CjwvZz4KPGcgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIj4KPC9nPgo8ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciPgo8L2c+CjxnIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyI+CjwvZz4KPGcgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIj4KPC9nPgo8ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciPgo8L2c+CjxnIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyI+CjwvZz4KPGcgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIj4KPC9nPgo8ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciPgo8L2c+CjxnIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyI+CjwvZz4KPGcgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIj4KPC9nPgo8L2c+PC9zdmc+" />
                {/* <i className="fa fa-exclamation icon-error" aria-hidden="true"></i> */}
              </div> : <div></div>
          }



          <div className={this.state.errorNoHp ? classes.containerTextFieldError : classes.containerTextField} >
            <input
              onPaste={
                (e)=>{
                  e.preventDefault()
                  return false;
                }
              } 
              className={classes.textField}
              value={this.state.userPhoneTerdaftar}
              onChange={(event) => {
                  // navigator.mozVibrate(1000);
                if (this.validateCheckNoHp(event.target.value)) {
                  if (!this.state.errorNoHp) {
                    if ("vibrate" in navigator) {
                      window.navigator.vibrate(1000);
                    }
                  }
                  this.setState({ errorNoHp: true, allowClick:false })
                }
                else if (event.target.value.length > 13) {
                  if ("vibrate" in navigator) {
                    navigator.vibrate(1000);
                  }
                  this.setState({ errorNoHp: true, allowClick:false  })
                }
                else {
                  this.setState({ errorNoHp: false, allowClick:true })
                  this.setState({ userPhoneTerdaftar: event.target.value })
                }

                if (!this.state.errorNoHp) {
                  this.setState({ userPhoneTerdaftar: event.target.value })
                }
              }
              }
            >
            </input>
            <label id="label-textBoardLogin" className={!this.state.userPhoneTerdaftar ? '' : 
                                                         this.state.errorNoHp ? 'validInput error' : 'validInput'}>
                                                           Nomor Hp Terdaftar</label>
            {this.state.errorNoHp ?
              // <i className="fa fa-exclamation-circle icon-error" aria-hidden="true"></i> 
              <img className="icon-error" src="data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHZlcnNpb249IjEuMSIgeG1sbnM6eGxpbms9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkveGxpbmsiIHhtbG5zOnN2Z2pzPSJodHRwOi8vc3ZnanMuY29tL3N2Z2pzIiB3aWR0aD0iNTEyIiBoZWlnaHQ9IjUxMiIgeD0iMCIgeT0iMCIgdmlld0JveD0iMCAwIDgxLjk5OCA4MS45OTgiIHN0eWxlPSJlbmFibGUtYmFja2dyb3VuZDpuZXcgMCAwIDUxMiA1MTIiIHhtbDpzcGFjZT0icHJlc2VydmUiIGNsYXNzPSIiPjxnPgo8ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciPgoJPHBhdGggZD0iTTQwLjk5OSwwQzE4LjM5MiwwLDAsMTguMzkyLDAsNDAuOTk5YzAsMjIuNjA2LDE4LjM5Miw0MC45OTksNDAuOTk5LDQwLjk5OWMyMi42MDYsMCw0MC45OTktMTguMzkzLDQwLjk5OS00MC45OTkgICBDODEuOTk4LDE4LjM5Miw2My42MDcsMCw0MC45OTksMHogTTQwLjk5OSw3Ny45OThDMjAuNTk4LDc3Ljk5OCw0LDYxLjQsNCw0MC45OTlDNCwyMC41OTgsMjAuNTk4LDQsNDAuOTk5LDQgICBzMzYuOTk5LDE2LjU5OCwzNi45OTksMzYuOTk5Qzc3Ljk5OCw2MS40LDYxLjQsNzcuOTk4LDQwLjk5OSw3Ny45OTh6IE00NS43MDgsMTIuNzQ0YzAuMzc2LDAuMzg3LDAuNTgxLDAuOTA4LDAuNTY3LDEuNDQ3ICAgbC0xLjMxOSw0MS4zNzJjLTAuMDI4LDEuMDg0LTAuOTE1LDEuOTQ5LTIsMS45NDloLTMuOTE1Yy0xLjA4NCwwLTEuOTcyLTAuODY1LTItMS45NDlsLTEuMzE5LTQxLjM3MiAgIGMtMC4wMTQtMC41MzksMC4xOTEtMS4wNjEsMC41NjctMS40NDdjMC4zNzYtMC4zODYsMC44OTMtMC42MDQsMS40MzItMC42MDRoNi41NTVDNDQuODE0LDEyLjE0MSw0NS4zMzIsMTIuMzU4LDQ1LjcwOCwxMi43NDR6ICAgIE00Ni4xOTEsNjMuMDAydjcuNDljMCwxLjEwNC0wLjg5NiwyLTIsMmgtNi4zODVjLTEuMTA0LDAtMi0wLjg5Ni0yLTJ2LTcuNDljMC0xLjEwNCwwLjg5Ni0yLDItMmg2LjM4NSAgIEM0NS4yOTYsNjEuMDAyLDQ2LjE5MSw2MS44OTYsNDYuMTkxLDYzLjAwMnoiIGZpbGw9IiNmMTA1MDUiIGRhdGEtb3JpZ2luYWw9IiMwMDAwMDAiIHN0eWxlPSIiIGNsYXNzPSIiPjwvcGF0aD4KPC9nPgo8ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciPgo8L2c+CjxnIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyI+CjwvZz4KPGcgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIj4KPC9nPgo8ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciPgo8L2c+CjxnIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyI+CjwvZz4KPGcgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIj4KPC9nPgo8ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciPgo8L2c+CjxnIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyI+CjwvZz4KPGcgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIj4KPC9nPgo8ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciPgo8L2c+CjxnIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyI+CjwvZz4KPGcgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIj4KPC9nPgo8ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciPgo8L2c+CjxnIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyI+CjwvZz4KPGcgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIj4KPC9nPgo8L2c+PC9zdmc+" />

              : <span></span>}
            {this.state.errorNoHp ? <div className="lineError"></div> : ''}
          </div>
          {this.state.errorNoHp ?
            <span style={{ color: "red", position: 'relative', top: '-4vw', left: '4vw', fontSize: '3vw' }}>
              Mohon gunakan format no Handphone</span>
            : <span></span>
          }

          <div className={this.state.errorNoPendaftaran ? classes.containerTextFieldError :classes.containerTextField}>
            <input
              className={classes.textField}
              value={this.state.nomorPendaftaran}
      
              onChange={(event) => {
             

if( ((event.target.value.length-event.target.defaultValue.length)<0) && event.target.value.length > 11){
  this.setState({ nomorPendaftaran: '' })
}

                if(event.target.value.length > 10){
                  if ("vibrate" in navigator) {
                    navigator.vibrate(1000);
                  }
                  this.setState({ errorNoPendaftaran: true, allowClick2:false  })
                }
                else {
                  this.setState({ errorNoPendaftaran: false, allowClick2:true })
                  this.setState({ nomorPendaftaran: event.target.value })
                }

                if (!this.state.errorNoPendaftaran) {
                  this.setState({ nomorPendaftaran: event.target.value })
                } 
                }
              }
            >
            </input> 
            <label id="label-textBoardLogin" className={!this.state.nomorPendaftaran ? '' : 
            this.state.errorNoPendaftaran ? 'validInput error' : 'validInput'}>
             Nomor Pendaftaran</label>
            {this.state.errorNoPendaftaran ?
              // <i className="fa fa-exclamation-circle icon-error" aria-hidden="true"></i> 
              <img className="icon-error" src="data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHZlcnNpb249IjEuMSIgeG1sbnM6eGxpbms9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkveGxpbmsiIHhtbG5zOnN2Z2pzPSJodHRwOi8vc3ZnanMuY29tL3N2Z2pzIiB3aWR0aD0iNTEyIiBoZWlnaHQ9IjUxMiIgeD0iMCIgeT0iMCIgdmlld0JveD0iMCAwIDgxLjk5OCA4MS45OTgiIHN0eWxlPSJlbmFibGUtYmFja2dyb3VuZDpuZXcgMCAwIDUxMiA1MTIiIHhtbDpzcGFjZT0icHJlc2VydmUiIGNsYXNzPSIiPjxnPgo8ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciPgoJPHBhdGggZD0iTTQwLjk5OSwwQzE4LjM5MiwwLDAsMTguMzkyLDAsNDAuOTk5YzAsMjIuNjA2LDE4LjM5Miw0MC45OTksNDAuOTk5LDQwLjk5OWMyMi42MDYsMCw0MC45OTktMTguMzkzLDQwLjk5OS00MC45OTkgICBDODEuOTk4LDE4LjM5Miw2My42MDcsMCw0MC45OTksMHogTTQwLjk5OSw3Ny45OThDMjAuNTk4LDc3Ljk5OCw0LDYxLjQsNCw0MC45OTlDNCwyMC41OTgsMjAuNTk4LDQsNDAuOTk5LDQgICBzMzYuOTk5LDE2LjU5OCwzNi45OTksMzYuOTk5Qzc3Ljk5OCw2MS40LDYxLjQsNzcuOTk4LDQwLjk5OSw3Ny45OTh6IE00NS43MDgsMTIuNzQ0YzAuMzc2LDAuMzg3LDAuNTgxLDAuOTA4LDAuNTY3LDEuNDQ3ICAgbC0xLjMxOSw0MS4zNzJjLTAuMDI4LDEuMDg0LTAuOTE1LDEuOTQ5LTIsMS45NDloLTMuOTE1Yy0xLjA4NCwwLTEuOTcyLTAuODY1LTItMS45NDlsLTEuMzE5LTQxLjM3MiAgIGMtMC4wMTQtMC41MzksMC4xOTEtMS4wNjEsMC41NjctMS40NDdjMC4zNzYtMC4zODYsMC44OTMtMC42MDQsMS40MzItMC42MDRoNi41NTVDNDQuODE0LDEyLjE0MSw0NS4zMzIsMTIuMzU4LDQ1LjcwOCwxMi43NDR6ICAgIE00Ni4xOTEsNjMuMDAydjcuNDljMCwxLjEwNC0wLjg5NiwyLTIsMmgtNi4zODVjLTEuMTA0LDAtMi0wLjg5Ni0yLTJ2LTcuNDljMC0xLjEwNCwwLjg5Ni0yLDItMmg2LjM4NSAgIEM0NS4yOTYsNjEuMDAyLDQ2LjE5MSw2MS44OTYsNDYuMTkxLDYzLjAwMnoiIGZpbGw9IiNmMTA1MDUiIGRhdGEtb3JpZ2luYWw9IiMwMDAwMDAiIHN0eWxlPSIiIGNsYXNzPSIiPjwvcGF0aD4KPC9nPgo8ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciPgo8L2c+CjxnIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyI+CjwvZz4KPGcgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIj4KPC9nPgo8ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciPgo8L2c+CjxnIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyI+CjwvZz4KPGcgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIj4KPC9nPgo8ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciPgo8L2c+CjxnIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyI+CjwvZz4KPGcgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIj4KPC9nPgo8ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciPgo8L2c+CjxnIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyI+CjwvZz4KPGcgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIj4KPC9nPgo8ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciPgo8L2c+CjxnIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyI+CjwvZz4KPGcgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIj4KPC9nPgo8L2c+PC9zdmc+" />

              : <span></span>}
            {this.state.errorNoPendaftaran ? <div className="lineError"></div> : ''}
            </div>
            {this.state.errorNoPendaftaran ?
              <span style={{ top: '-5vw',color: "red",position:'relative', left: '4vw', fontSize: '3vw' }}>
                panjang karakter 10 digit</span>
              : <span></span>
            }
            <br/>
            <span style={{color: "black", position: 'relative', top: '-5vw', paddingLeft:'2.5vw', fontSize: '3vw', fontFamily: 'Nunito, sans-serif', color: 'rgba(48, 59, 74, 0.72)' }}>Nomor pendaftaran telah kami kirimkan melalui email dan SMS</span>
         

        { this.state.allowClick && this.state.allowClick2 ?
            <div
              onClick={this.cekStatus}
              style={{
                backgroundColor: '#0057E7',
                width: "100%",
                color: 'white',
                fontSize: "3.572vw",
                height: '12.670vw',
                fontWeight: 'bold',
                fontStyle: 'normal',
                fontFamily: 'Nunito, sans-serif',
                marginTop: '10.29vw', textTransform: 'none',
                borderRadius: '2vw',
                display: 'flex',
                justifyContent: 'center'
              }}>
              <div style={{ position: 'relative', top: '32%' }}>
                Cek Status Pendaftaran
              </div>
            </div>
            :
            <div
            style={{
              backgroundColor: ' rgba(236, 238, 242, 1)',
              width: "100%",
              color: 'rgba(48, 59, 74, 0.24)',
              fontSize: "3.572vw",
              height: '12.670vw',
              fontWeight: 'bold',
              fontStyle: 'normal',
              fontFamily: 'Nunito, sans-serif',
              marginTop: '10.29vw', textTransform: 'none',
              borderRadius: '2vw',
              display: 'flex',
              justifyContent: 'center'
            }}>
            <div style={{ position: 'relative', top: '32%' }}>
              Cek Status Pendaftaran
            </div>
           </div>
          }

        </div>
      </div>

    );
  }
}

export default compose(
  withStyles(useStyles))
  (CekStatusPendaftaran)
