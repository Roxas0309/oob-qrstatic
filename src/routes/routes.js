import React from 'react';

const Register = React.lazy(() => import('../view/Registrasi/registrasi'));
const StartReg = React.lazy(() => import('../view/Registrasi/startReg'));

const routes = [
    { path: '/registrasi', name: 'Registrasi', component: Register },
    { path: '/startReg', name: 'start', component: StartReg }
];
export default routes;